SQLクエリを実行するウェブアプリケーションです。
クエリ設定は保存でき、ウェブページを表示するとクエリが実行されます。

開発環境の準備
・必須
	JDK7
	Gradle
	Postgresql
	Git
	Yamabuki-Java-Common
	
・オプション
	Eclipse
	Ruby
	Vagrant
	Chef

Eclipse環境の構築
$ gradle eclipse

$ cd izpack/listener
$ gradle eclipse


ライブラリビルド
$ cd yamabuki-java-common
$ gradle

ビルド
$ gradle clean build

$ cd wakaba-server 
$ gradle pkg

$ cd wakaba-package
$ gradle fatJar

$ cd izpack/listener
$ gradle clean build

インストーラ作成
$ cd izpack/listener
$ CALL buildInstaller

配布物
izpack/installer/install.jar
