package net.jp.yamabuki.util;

import static org.hamcrest.CoreMatchers.is;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ManagerAppId;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class AppIdUtilsTest {
	@Test(expected = IllegalArgumentException.class)
	public void nullValue() {
		AppIdUtils.create(null);
	}

	@Test
	public void managerAppId() {
		AppId appId = AppIdUtils.create("*");
		assertThat(appId, is(instanceOf(ManagerAppId.class)));
		assertThat(appId.getValue(), is("*"));
	}

	@Test
	public void normalAppId() {
		AppId appId = AppIdUtils.create("abc");
		assertThat(appId, is(instanceOf(AppIdImpl.class)));
		assertThat(appId.getValue(), is("abc"));
	}
}
