package net.jp.yamabuki.util;

import static org.hamcrest.CoreMatchers.is;
import net.jp.yamabuki.model.ManagerUserId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UserIdUtilsTest {
	@Test(expected = IllegalArgumentException.class)
	public void nullValue() {
		AppIdUtils.create(null);
	}

	@Test
	public void managerUserId() {
		UserId userId = UserIdUtils.create("*");
		assertThat(userId, is(instanceOf(ManagerUserId.class)));
		assertThat(userId.getValue(), is("*"));
	}

	@Test
	public void normalUserId() {
		UserId userId = UserIdUtils.create("abc");
		assertThat(userId, is(instanceOf(UserIdImpl.class)));
		assertThat(userId.getValue(), is("abc"));
	}
}
