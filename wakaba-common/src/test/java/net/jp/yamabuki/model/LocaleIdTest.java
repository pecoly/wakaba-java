package net.jp.yamabuki.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class LocaleIdTest {
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void nullValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new LocaleIdImpl(null);
		}

		@Test
		public void emptyValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new LocaleIdImpl("");
		}

		@Test
		public void invalidValue_001() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' is not valid name.");
			new LocaleIdImpl("$");
		}
		
		@Test
		public void invalidValue_002() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must be less than or equal to 2.");
			new LocaleIdImpl("abc");
		}

	}

	public static class Constructor_Ok {
		@Test
		public void ok() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			assertThat(appId.getValue(), is("ja"));
		}
	}

	public static class Equals {
		@Test
		public void equals_001() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			assertThat(appId.equals(null), is(false));
		}

		@Test
		public void equals_002() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			assertThat(appId.equals("ja"), is(false));
		}

		@Test
		public void equals_003() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			LocaleIdImpl tmp = new LocaleIdImpl("en");
			assertThat(appId.equals(tmp), is(false));
		}

		@Test
		public void equals_004() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			LocaleIdImpl tmp = new LocaleIdImpl("ja");
			assertThat(appId.equals(tmp), is(true));
		}

		@Test
		public void equals_005() {
			LocaleIdImpl appId = new LocaleIdImpl("ja");
			assertThat(appId.equals(appId), is(true));
		}
	}
}
