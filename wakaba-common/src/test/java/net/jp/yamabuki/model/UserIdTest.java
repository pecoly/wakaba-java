package net.jp.yamabuki.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class UserIdTest {
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void nullValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new UserIdImpl(null);
		}

		@Test
		public void emptyValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new UserIdImpl("");
		}

		@Test
		public void invalidValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' is not valid name.");
			new UserIdImpl("$");
		}

	}

	public static class Constructor_Ok {
		@Test
		public void ok() {
			UserIdImpl userId = new UserIdImpl("abc");
			assertThat(userId.getValue(), is("abc"));
		}
	}

	public static class Equals {
		@Test
		public void equals_001() {
			UserIdImpl userId = new UserIdImpl("abc");
			assertThat(userId.equals(null), is(false));
		}

		@Test
		public void equals_002() {
			UserIdImpl userId = new UserIdImpl("abc");
			assertThat(userId.equals("abc"), is(false));
		}

		@Test
		public void equals_003() {
			UserIdImpl userId = new UserIdImpl("abc");
			UserIdImpl tmp = new UserIdImpl("def");
			assertThat(userId.equals(tmp), is(false));
		}

		@Test
		public void equals_004() {
			UserIdImpl userId = new UserIdImpl("abc");
			UserIdImpl tmp = new UserIdImpl("abc");
			assertThat(userId.equals(tmp), is(true));
		}

		@Test
		public void equals_005() {
			UserIdImpl userId = new UserIdImpl("abc");
			assertThat(userId.equals(userId), is(true));
		}
	}
}
