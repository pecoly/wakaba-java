package net.jp.yamabuki.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class UserModelTest {
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void userIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'userId' must not be null.");
			new UserModel(null);
		}

		@Test
		public void modelIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'id' must not be null.");
			new UserModel(null, new UserIdImpl("a"));
		}

		@Test
		public void modelIdIsNotNull_userIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'userId' must not be null.");
			new UserModel(new ModelIdImpl("a"), null);
		}

	}

	public static class Constructor_Ok {
		@Test
		public void hasModelId() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			assertThat(userModel.getId().getValue(), is(new ModelIdImpl("a").getValue()));
			assertThat(userModel.getUserId().getValue(), is(new UserIdImpl("b").getValue()));
		}

		@Test
		public void hasNotModelId() {
			UserModel userModel = new UserModel(new UserIdImpl("b"));
			assertThat(userModel.getUserId().getValue(), is(new UserIdImpl("b").getValue()));
		}
	}

	public static class Equals {
		@Test
		public void equals_001() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			assertThat(userModel.equals(null), is(false));
		}

		@Test
		public void equals_002() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			UserModel tmp = new UserModel(new ModelIdImpl("a"), new UserIdImpl("c"));
			assertThat(userModel.equals(tmp), is(false));
		}

		@Test
		public void equals_003() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			UserModel tmp = new UserModel(new ModelIdImpl("c"), new UserIdImpl("b"));
			assertThat(userModel.equals(tmp), is(false));
		}

		@Test
		public void equals_004() {
			UserModel userModel = new UserModel(new UserIdImpl("b"));
			UserModel tmp = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			assertThat(userModel.equals(tmp), is(false));
		}

		@Test
		public void equals_005() {
			UserModel userModel = new UserModel(new UserIdImpl("b"));
			UserModel tmp = new UserModel(new UserIdImpl("c"));
			assertThat(userModel.equals(tmp), is(false));
		}

		@Test
		public void equals_006() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			UserModel tmp = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			assertThat(userModel.equals(tmp), is(true));
		}

		@Test
		public void equals_007() {
			UserModel userModel = new UserModel(new UserIdImpl("b"));
			UserModel tmp = new UserModel(new UserIdImpl("b"));
			assertThat(userModel.equals(tmp), is(true));
		}

		@Test
		public void equals_008() {
			UserModel userModel = new UserModel(new ModelIdImpl("a"), new UserIdImpl("b"));
			assertThat(userModel.equals(userModel), is(true));
		}

		@Test
		public void equals_009() {
			UserModel userModel = new UserModel(new UserIdImpl("b"));
			assertThat(userModel.equals(userModel), is(true));
		}
	}
}
