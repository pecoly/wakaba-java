package net.jp.yamabuki.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ModelIdTest {
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void nullValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new ModelIdImpl(null);
		}

		@Test
		public void emptyValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new ModelIdImpl("");
		}

		@Test
		public void invalidValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' is not valid name.");
			new ModelIdImpl("$");
		}

	}

	public static class Constructor_Ok {
		@Test
		public void ok() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			assertThat(modelId.getValue(), is("abc"));
		}
	}

	public static class Equals {
		@Test
		public void equals_001() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			assertThat(modelId.equals(null), is(false));
		}

		@Test
		public void equals_002() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			assertThat(modelId.equals("abc"), is(false));
		}

		@Test
		public void equals_003() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			ModelIdImpl tmp = new ModelIdImpl("def");
			assertThat(modelId.equals(tmp), is(false));
		}

		@Test
		public void equals_004() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			ModelIdImpl tmp = new ModelIdImpl("abc");
			assertThat(modelId.equals(tmp), is(true));
		}

		@Test
		public void equals_005() {
			ModelIdImpl modelId = new ModelIdImpl("abc");
			assertThat(modelId.equals(modelId), is(true));
		}
	}
}
