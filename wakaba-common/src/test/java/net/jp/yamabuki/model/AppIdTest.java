package net.jp.yamabuki.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class AppIdTest {
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void nullValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new AppIdImpl(null);
		}

		@Test
		public void emptyValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' must not be blank.");
			new AppIdImpl("");
		}

		@Test
		public void invalidValue() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'value' is not valid name.");
			new AppIdImpl("$");
		}

	}

	public static class Constructor_Ok {
		@Test
		public void ok() {
			AppId appId = new AppIdImpl("abc");
			assertThat(appId.getValue(), is("abc"));
		}
	}

	public static class Equals {
		@Test
		public void equals_001() {
			AppId appId = new AppIdImpl("abc");
			assertThat(appId.equals(null), is(false));
		}

		@Test
		public void equals_002() {
			AppId appId = new AppIdImpl("abc");
			assertThat(appId.equals("abc"), is(false));
		}

		@Test
		public void equals_003() {
			AppId appId = new AppIdImpl("abc");
			AppId tmp = new AppIdImpl("def");
			assertThat(appId.equals(tmp), is(false));
		}

		@Test
		public void equals_004() {
			AppId appId = new AppIdImpl("abc");
			AppId tmp = new AppIdImpl("abc");
			assertThat(appId.equals(tmp), is(true));
		}

		@Test
		public void equals_005() {
			AppId appId = new AppIdImpl("abc");
			assertThat(appId.equals(appId), is(true));
		}
	}
}
