package net.jp.yamabuki.io;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.eventFrom;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.hasXPath;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.typeCompatibleWith;
import static org.junit.Assert.*;

import net.jp.yamabuki.exception.WakabaException;

import org.junit.*;
import org.junit.rules.ExpectedException;

public class MessageFileTest {
	public static class GetLocaleId {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void localeIdIsDefault() {
			MessagePropertiesFile m = new MessagePropertiesFile();
			String actual = m.getLocaleId("messages.properties");
			assertThat(actual, is("*"));
		}

		@Test
		public void localeIdIsJa() {
			MessagePropertiesFile m = new MessagePropertiesFile();
			String actual = m.getLocaleId("messages_ja.properties");
			assertThat(actual, is("ja"));
		}

		@Test
		public void localeIdIsEn() {
			MessagePropertiesFile m = new MessagePropertiesFile();
			String actual = m.getLocaleId("messages_en.properties");
			assertThat(actual, is("en"));
		}

		@Test
		public void localeIdIsInvalid() {
			MessagePropertiesFile m = new MessagePropertiesFile();
			thrown.expect(WakabaException.class);
			thrown.expectMessage("Invalid message file name. name : messages_abc.properties");
			m.getLocaleId("messages_abc.properties");
		}
	}
}

