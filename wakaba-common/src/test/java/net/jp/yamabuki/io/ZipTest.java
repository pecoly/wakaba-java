package net.jp.yamabuki.io;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.junit.Test;

public class ZipTest {
	@Test
	public void test() throws IOException {
		ByteArrayOutputStream x = new ByteArrayOutputStream();
		//BufferedOutputStream o = new BufferedOutputStream()
		// １．sampleComp.zipに書き込むZipArchiveOutputStreamを作成します
		ZipArchiveOutputStream os = new ZipArchiveOutputStream(new File("sampleComp.zip"));
		//ZipArchiveOutputStream os = new ZipArchiveOutputStream(x);
		// ２．中の圧縮ファイルに適用するエンコードを指定します。システム標準にしてみたのでWindowsならMS932になります。
		//os.setEncoding(System.getProperty("file.encoding"));
		os.setEncoding("MS932");

		// ３．ZipArchiveEntry（Zipに入れるファイルのような物）を作成
		ZipArchiveEntry zae = new ZipArchiveEntry("a/ほげほげ.txt");
		// ４．ZipArchiveOutputStreamにエントリを入れます。
		os.putArchiveEntry(zae);
		// ５．エントリの中身を書き込みます。
		// 中身が多い場合は、500バイトずつとか何回かに分けて書き込むと良いです。
		os.write("ほげほげ".getBytes());
		// ６．エントリをクローズします
		os.closeArchiveEntry();
		// 複数のファイルをzipファイルに入れる場合は、３～６の工程をファイルの数だけ繰り返します。

		// ７．最後にアウトプットストリームをクローズします。
		os.close();

		// これで、「ほげほげ.txt」の入った「sampleComp.zip」が出来上がったはずです。

		System.out.println(x.size());
	}
}
