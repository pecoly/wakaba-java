package net.jp.yamabuki.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.jp.yamabuki.exception.PropertyNotFoundException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.util.StringUtils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.*;
import org.junit.rules.ExpectedException;

public class AppFileTest {
	public static class Load {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void ok() throws IOException {
			thrown = ExpectedException.none();

			String text = "";
			text += "userId=abc";
			text += "\n";
			text += "appId=def";
			text += "\n";
			text += "appName=ghi";
			text += "\n";

			try (ByteArrayInputStream is = StringUtils.toByteArrayInputStream(text)) {
				AppFile appFile = new AppFile();
				appFile.load(is);
				assertThat(appFile.getUserId().getValue(), is("abc"));
				assertThat(appFile.getAppId().getValue(), is("def"));
				assertThat(appFile.getAppName(), is("ghi"));
			}
		}

		@Test
		public void userIdIsNothing() throws IOException {
			thrown.expect(PropertyNotFoundException.class);
			thrown.expectMessage("Property 'userId' not found.");

			String text = "";
			text += "appId=def";
			text += "\n";
			text += "appName=ghi";
			text += "\n";

			try (ByteArrayInputStream is = StringUtils.toByteArrayInputStream(text)) {
				AppFile appFile = new AppFile();
				appFile.load(is);
			}
		}

		@Test
		public void appIdIsNothing() throws IOException {
			thrown.expect(PropertyNotFoundException.class);
			thrown.expectMessage("Property 'appId' not found.");

			String text = "";
			text += "userId=abc";
			text += "\n";
			text += "appName=ghi";
			text += "\n";

			try (ByteArrayInputStream is = StringUtils.toByteArrayInputStream(text)) {
				AppFile appFile = new AppFile();
				appFile.load(is);
			}
		}

		@Test
		public void appNameIsNothing() throws IOException {
			thrown.expect(PropertyNotFoundException.class);
			thrown.expectMessage("Property 'appName' not found.");

			String text = "";
			text += "userId=abc";
			text += "\n";
			text += "appId=def";
			text += "\n";

			try (ByteArrayInputStream is = StringUtils.toByteArrayInputStream(text)) {
				AppFile appFile = new AppFile();
				appFile.load(is);
			}
		}
	}

	public static class LoadFile {

	}

	public static class LoadFilePath {

	}

	public static class Save {
		AppFile appFile;

		@Before
		public void before() {
			appFile = new AppFile();
		}

		@Test
		public void ok() {
			appFile.setUserId(new UserIdImpl("a"));
			appFile.setAppId(new AppIdImpl("b"));
			appFile.setAppName("c");
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			appFile.save(out);
			String text = StringUtils.toString(out.toByteArray());
			assertThat(text, containsString("userId = a"));
			assertThat(text, containsString("appId = b"));
			assertThat(text, containsString("appName = c"));
		}
	}
}
