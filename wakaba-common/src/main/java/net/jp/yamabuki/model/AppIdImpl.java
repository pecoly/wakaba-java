package net.jp.yamabuki.model;

import net.jp.yamabuki.check.Argument;

/**
 * アプリケーションIdを表すクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class AppIdImpl extends AppId {

	private final String value;

	/**
	 * コンストラクタ。
	 * @param value アプリケーションId
	 */
	public AppIdImpl(String value) {
		Argument.isNotBlank(value, "value");
		Argument.isVariableName(value, "value");

		this.value = value;
	}

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return this.value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
