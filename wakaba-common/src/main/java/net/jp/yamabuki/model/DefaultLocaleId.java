package net.jp.yamabuki.model;

/**
 * 既定のロケールIdを表すクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class DefaultLocaleId extends LocaleId {

	private static DefaultLocaleId instance = new DefaultLocaleId();

	/**
	 * コンストラクタ。
	 */
	private DefaultLocaleId() {
	}

	/**
	 * 唯一のインスタンスを取得します。
	 * @return 唯一のインスタンス
	 */
	public static DefaultLocaleId getInstance() {
		return instance;
	}

	@Override
	public String getValue() {
		return "*";
	}
}
