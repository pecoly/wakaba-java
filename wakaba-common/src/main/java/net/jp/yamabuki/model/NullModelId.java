package net.jp.yamabuki.model;

/**
 * モデルIdのnullを表すクラス。
 *
 */
public final class NullModelId extends ModelId {

	private static NullModelId instance = new NullModelId();

	/**
	 * コンストラクタ。
	 */
	private NullModelId() {
	}

	/**
	 * 唯一のインスタンスを取得します。
	 * @return 唯一のインスタンス
	 */
	public static NullModelId getInstance() {
		return instance;
	}

	@Override
	public String getValue() {
		return null;
	}
}
