package net.jp.yamabuki.model;

public abstract class ModelId {

	/**
	 * モデルId文字列を取得します。
	 * @return モデルId文字列
	 */
	public abstract String getValue();
}
