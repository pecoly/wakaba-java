package net.jp.yamabuki.model;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.util.DateTimeUtils;

/**
 * ユーザーごとのモデルを表すクラス。
 *
 */
public class UserModel {

	private final ModelId id;

	private final UserId userId;

	private final DateTime lastModified;

	public static final String KEY_ID = "id";

	public static final String KEY_USER_ID = "userId";

	public static final String KEY_LAST_MODIFIED = "lastModified";

	/**
	 * コンストラクタ。
	 * @param id モデルId
	 * @param userId ユーザーId
	 * @param lastModified 最終更新日時
	 */
	public UserModel(ModelId id, UserId userId, DateTime lastModified) {
		Argument.isNotNull(id, "id");
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(lastModified, "lastModified");

		this.id = id;
		this.userId = userId;
		this.lastModified = lastModified;
	}

	/**
	 * コンストラクタ。
	 * @param id モデルId
	 * @param userId ユーザーId
	 */
	public UserModel(ModelId id, UserId userId) {
		Argument.isNotNull(id, "id");
		Argument.isNotNull(userId, "userId");

		this.id = id;
		this.userId = userId;
		this.lastModified = new DateTime();
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 */
	public UserModel(UserId userId) {
		Argument.isNotNull(userId, "userId");

		this.id = NullModelId.getInstance();
		this.userId = userId;
		this.lastModified = new DateTime();
	}

	/**
	 * モデルIdを取得します。
	 * @return モデルId
	 */
	public ModelId getId() {
		return this.id;
	}

	/**
	 * ユーザーIdを取得します。
	 * @return ユーザーId
	 */
	public UserId getUserId() {
		return this.userId;
	}

	/**
	 * 最終更新日時を取得します。
	 * @return 最終更新日時
	 */
	public DateTime getLastModified() {
		return new DateTime(this.lastModified);
	}

	/**
	 * インスタンスの文字列マップ表現を取得します。
	 * @return インスタンスの文字列マップ表現
	 */
	public Map<String, String> toStringMap() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.getId().getValue());
		map.put("userId", this.getUserId().getValue());
		map.put("lastModified", DateTimeUtils
				.toStringYYYYMMDDHHMISS(this.getLastModified()));

		return map;
	}


	@Override
	public String toString() {
		String s = String.format("{ id : %s, userId : %s }",
				this.id.getValue(), this.userId.getValue());
		return s;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
