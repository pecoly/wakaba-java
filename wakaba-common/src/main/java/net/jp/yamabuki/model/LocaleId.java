package net.jp.yamabuki.model;

public abstract class LocaleId {

	/**
	 * ロケールId文字列を取得します。
	 * @return ロケールId文字列
	 */
	public abstract String getValue();
}
