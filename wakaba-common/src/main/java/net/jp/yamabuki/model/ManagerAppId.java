package net.jp.yamabuki.model;

/**
 * マネージャーアプリケーションIdを表すクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class ManagerAppId extends AppId {

	private static ManagerAppId instance = new ManagerAppId();

	/**
	 * コンストラクタ。
	 */
	private ManagerAppId() {
	}

	/**
	 * 唯一のインスタンスを取得します。
	 * @return 唯一のインスタンス
	 */
	public static ManagerAppId getInstance() {
		return instance;
	}

	@Override
	public String getValue() {
		return "*";
	}
}
