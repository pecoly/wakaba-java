package net.jp.yamabuki.model;

public abstract class AppId {

	/**
	 * アプリケーションId文字列を取得します。
	 * @return アプリケーションId文字列
	 */
	public abstract String getValue();
}
