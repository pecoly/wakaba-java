package net.jp.yamabuki.model.sql.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.jp.yamabuki.model.accessor.Accessor;
import net.jp.yamabuki.model.accessor.DateAccessor;
import net.jp.yamabuki.model.accessor.IntegerAccessor;
import net.jp.yamabuki.model.accessor.LongAccessor;
import net.jp.yamabuki.model.accessor.NumericAccessor;
import net.jp.yamabuki.model.accessor.StringAccessor;
import net.jp.yamabuki.model.accessor.TimestampAccessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;


/**
 * 結果を、(カラム名、指定の型)のペアで取得するマッパークラス。
 * カラム名は小文字に変換されます。
 * インスタンスの再利用はできません。
 *
 */
public class ObjectMapper implements RowMapper<Map<String, Object>> {
	private Map<String, Accessor> getObjectList = new LinkedHashMap<>();

	private boolean initialized;

	@Autowired
	private IntegerAccessor integerAccessor;

	@Autowired
	private LongAccessor longAccessor;

	@Autowired
	private StringAccessor stringAccessor;

	@Autowired
	private DateAccessor dateAccessor;

	@Autowired
	private NumericAccessor numericAccessor;

	@Autowired
	private TimestampAccessor timestampAccessor;

	@Override
	public Map<String, Object> mapRow(ResultSet rs, int rowNumber) throws SQLException {
		if (!this.initialized) {
			this.initialized = true;
			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
				String columnName = rs.getMetaData().getColumnLabel(i + 1);
				int type = rs.getMetaData().getColumnType(i + 1);
				Accessor accessor = this.getAccessor(type);
				if (accessor != null) {
					this.getObjectList.put(columnName, accessor);
				}
			}
		}

		Map<String, Object> map = new LinkedHashMap<>();

		for (Entry<String, Accessor> x : this.getObjectList.entrySet()) {
			map.put(x.getKey().toLowerCase(), x.getValue().getObject(rs, x.getKey()));
		}

		return map;
	}

	/**
	 * java.sql.Typesの値に対応したオデータ取得インスタンスを取得します。
	 * @param type Sqlデータ型
	 * @return データ取得インスタンス
	 */
	protected Accessor getAccessor(int type) {
		switch (type) {
		case Types.INTEGER:
		case Types.SMALLINT:
		case Types.TINYINT:
			return this.integerAccessor;
		case Types.BIGINT:
			return this.longAccessor;
		case Types.VARCHAR:
		case Types.LONGVARCHAR:
		case Types.CHAR:
			return this.stringAccessor;
		case Types.DATE:
			return this.dateAccessor;
		case Types.NUMERIC:
			return this.numericAccessor;
		case Types.TIMESTAMP:
			return this.timestampAccessor;
			/*
		case Types.TIME:
			return null;
			*/
		default:
			throw new RuntimeException("Unsuppported sql type. type : " + type);
		}
	}
}