package net.jp.yamabuki.model;

import net.jp.yamabuki.check.Argument;

/**
 * ユーザーIdを表すクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class UserIdImpl extends UserId {

	private String value;

	/**
	 * コンストラクタ。
	 * @param value ユーザーId
	 */
	public UserIdImpl(String value) {
		Argument.isNotBlank(value, "value");
		Argument.isVariableName(value, "value");

		this.value = value;
	}

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return this.value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
