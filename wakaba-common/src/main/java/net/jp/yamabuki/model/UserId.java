package net.jp.yamabuki.model;

public abstract class UserId {

	/**
	 * ユーザーId文字列を取得します。
	 * @return ユーザーId文字列
	 */
	public abstract String getValue();
}
