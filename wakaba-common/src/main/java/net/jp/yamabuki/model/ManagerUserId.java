package net.jp.yamabuki.model;

/**
 * マネージャーユーザーIdを表すクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class ManagerUserId extends UserId {

	private static ManagerUserId instance = new ManagerUserId();

	/**
	 * コンストラクタ。
	 */
	private ManagerUserId() {
	}

	/**
	 * 唯一のインスタンスを取得します。
	 * @return 唯一のインスタンス
	 */
	public static ManagerUserId getInstance() {
		return instance;
	}

	@Override
	public String getValue() {
		return "*";
	}
}
