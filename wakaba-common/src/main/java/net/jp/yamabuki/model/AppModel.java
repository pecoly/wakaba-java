package net.jp.yamabuki.model;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.util.DateTimeUtils;

/**
 * ユーザーごと、アプリケーションごとのモデルを表すクラス。
 */
public abstract class AppModel {

	private final ModelId id;

	private final UserId userId;

	private final AppId appId;

	private final DateTime lastModified;

	public static final String KEY_ID = "id";

	public static final String KEY_USER_ID = "userId";

	public static final String KEY_APP_ID = "appId";

	public static final String KEY_LAST_MODIFIED = "lastModified";

	/**
	 * コンストラクタ。
	 * @param id モデルId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param lastModified 最終更新日時
	 */
	public AppModel(ModelId id, UserId userId, AppId appId,
			DateTime lastModified) {
		Argument.isNotNull(id, "id");
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(lastModified, "lastModified");

		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.lastModified = lastModified;
	}

	/**
	 * コンストラクタ。
	 * 最終更新日時には現在時刻が割り当てられます。
	 * @param id モデルId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 */
	public AppModel(ModelId id, UserId userId, AppId appId) {
		Argument.isNotNull(id, "id");
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");

		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.lastModified = new DateTime();
	}

	/**
	 * コンストラクタ。
	 * モデルIdにはnullが割り当てられます。
	 * 最終更新日時には現在時刻が割り当てられます。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 */
	public AppModel(UserId userId, AppId appId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");

		this.id = NullModelId.getInstance();
		this.userId = userId;
		this.appId = appId;
		this.lastModified = new DateTime();
	}

	/**
	 * モデルIdを取得します。
	 * @return モデルId
	 */
	public ModelId getId() {
		return this.id;
	}

	/**
	 * ユーザーIdを取得します。
	 * @return ユーザーId
	 */
	public UserId getUserId() {
		return this.userId;
	}

	/**
	 * アプリケーションIdを取得します。
	 * @return アプリケーションId
	 */
	public AppId getAppId() {
		return this.appId;
	}

	/**
	 * 最終更新日時を取得します。
	 * @return 最終更新日時
	 */
	public DateTime getLastModified() {
		return new DateTime(this.lastModified);
	}

	/**
	 * インスタンスの文字列マップ表現を取得します。
	 * @return インスタンスの文字列マップ表現
	 */
	public Map<String, String> toStringMap() {
		Map<String, String> map = new HashMap<>();

		map.put(KEY_ID, this.id.getValue());
		map.put(KEY_USER_ID, this.userId.getValue());
		map.put(KEY_APP_ID, this.appId.getValue());
		map.put(KEY_LAST_MODIFIED, DateTimeUtils
				.toStringYYYYMMDDHHMISS(this.lastModified));

		return map;
	}
}
