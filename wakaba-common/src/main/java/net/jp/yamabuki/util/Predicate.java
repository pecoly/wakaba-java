package net.jp.yamabuki.util;

public interface Predicate<T> {
	boolean evaluate(T t);
}
