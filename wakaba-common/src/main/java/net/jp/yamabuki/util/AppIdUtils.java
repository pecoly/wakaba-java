package net.jp.yamabuki.util;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ManagerAppId;

/**
 * アプリケーションIdのユーティリティクラス。
 *
 */
public final class AppIdUtils {
	/**
	 * コンストラクタ。
	 */
	private AppIdUtils() {
	}

	/**
	 * 文字列からアプリケーションIdを作成します。
	 * @param appId アプリケーションId文字列
	 * @return アプリケーションId
	 */
	public static AppId create(String appId) {
		Argument.isNotNull(appId, "appId");

		if (appId.equals("*")) {
			return ManagerAppId.getInstance();
		}
		else {
			return new AppIdImpl(appId);
		}
	}

}
