package net.jp.yamabuki.util;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.ManagerUserId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

/**
 * ユーザーIdユーティリティクラス。
 *
 */
public final class UserIdUtils {
	/**
	 * コンストラクタ。
	 */
	private UserIdUtils() {
	}

	/**
	 * ユーザーIdを作成します。
	 * @param userId ユーザーId
	 * @return ユーザーId
	 */
	public static UserId create(String userId) {
		Argument.isNotNull(userId, "userId");

		if (userId.equals("*")) {
			return ManagerUserId.getInstance();
		}
		else {
			return new UserIdImpl(userId);
		}
	}

}
