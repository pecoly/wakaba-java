package net.jp.yamabuki.util;

public interface Transformer<IN, OUT> {
	OUT transform(IN i);
}
