package net.jp.yamabuki.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class CollectionUtils {
	private CollectionUtils() {
	}

	/*
	public static <IN, OUT> List<OUT> collectList(List<IN> inList, Transformer<IN, OUT> transformer) {
		List<OUT> outList = new ArrayList<>(inList.size());

		Iterator<IN> itr = inList.iterator();
		while (itr.hasNext()) {
			IN in = itr.next();
			OUT out = transformer.transform(in);
			outList.add(out);
		}

		return outList;
	}
	*/

	public static <T> List<T> selectList(List<T> inList, Predicate<T> predicate) {
		List<T> outList = new ArrayList<>();

		Iterator<T> itr = inList.iterator();
		while (itr.hasNext()) {
			T t = itr.next();
			if (predicate.evaluate(t)) {
				outList.add(t);
			}
		}


		return outList;
	}
}
