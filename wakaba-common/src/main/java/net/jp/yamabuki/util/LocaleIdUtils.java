package net.jp.yamabuki.util;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.DefaultLocaleId;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.LocaleIdImpl;

/**
 * ロケールIdのユーティリティクラス。
 *
 */
public final class LocaleIdUtils {
	/**
	 * コンストラクタ。
	 */
	private LocaleIdUtils() {
	}

	/**
	 * ロケールIdを作成します。
	 * @param localeId ロケールId
	 * @return ロケールId
	 */
	public static LocaleId create(String localeId) {
		Argument.isNotNull(localeId, "localeId");

		if (localeId.equals("*")) {
			return DefaultLocaleId.getInstance();
		}
		else {
			return new LocaleIdImpl(localeId);
		}
	}

}
