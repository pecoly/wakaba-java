package net.jp.yamabuki.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;

/**
 * テンプレートのコンフィグファイルクラス。
 *
 */
public class TemplatePropertiesFile {
	private String name;

	private String description;

	/**
	 * コンフィグファイルを読み込みます。
	 * @param inputStream 入力元
	 */
	public void load(InputStream inputStream) {
		PropertiesConfig p = new PropertiesConfig();
		p.load(inputStream);
		this.name = p.getString("name");
		this.description = p.getString("description");
	}

	/**
	 * コンフィグファイルを読み込みます。
	 * @param configFile コンフィグファイル
	 */
	public void load(File configFile) {
		try (InputStream is = new FileInputStream(configFile)) {
			this.load(is);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND,
					"file : " + configFile.getAbsolutePath());
		}
		catch (IOException ex) {
			throw new WakabaException(
					WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	/**
	 * コンフィグファイルから設定を取得します。
	 * @param configFilePath コンフィグファイルのパス
	 */
	public void load(String configFilePath) {
		this.load(new File(configFilePath));
	}

	/**
	 * テンプレートの名前を取得します。
	 * @return テンプレートの名前
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * テンプレートの概要を取得します。
	 * @return テンプレートの概要
	 */
	public String getDescription() {
		return this.description;
	}
}
