package net.jp.yamabuki.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.util.LocaleIdUtils;

/**
 * メッセージファイル(messages.properties,messages_**.properties)のクラス。
 *
 */
public class MessagePropertiesFile {

	private Map<String, String> messageList = new HashMap<>();

	private Pattern pattern;

	private LocaleId localeId;

	/**
	 * コンストラクタ。
	 */
	public MessagePropertiesFile() {
		this.pattern = Pattern.compile("messages(_..){0,1}\\.properties");
	}

	/**
	 * メッセージファイルを読み込みます。
	 * @param inputStream 入力元
	 */
	public void load(InputStream inputStream) {
		this.messageList.clear();

		PropertiesConfig p = new PropertiesConfig();
		p.load(inputStream);

		for (String key : p.getKeys()) {
			String value = p.getString(key);

			this.messageList.put(key, value);
		}
	}

	/**
	 * メッセージファイルを読み込みます。
	 * @param messageFile 読み込むメッセージファイル
	 */
	public void load(File messageFile) {
		try (InputStream is = new FileInputStream(messageFile)) {
			this.load(is);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND,
					"file : " + messageFile.getAbsolutePath());
		}
		catch (IOException ex) {
			throw new WakabaException(
					WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	/**
	 * メッセージファイルを読み込みます。
	 * @param messageFilePath メッセージファイルパス
	 */
	public void load(String messageFilePath) {
		this.messageList.clear();

		String messageFileName = Paths.get(messageFilePath).getFileName().toString();
		this.localeId = LocaleIdUtils.create(this.getLocaleId(messageFileName));

		this.load(new File(messageFilePath));
	}

	/**
	 * ファイル名からロケールIdを取得します。
	 * ファイル名がmessages.propertiesのときは'*'を返却します。
	 * ファイル名がmessages_ja.propertiesのときは'ja'を返却します。
	 * @param fileName ファイル名
	 * @return ロケールId
	 */
	String getLocaleId(String fileName) {
		Matcher matcher = this.pattern.matcher(fileName);
		if (!matcher.matches()) {
			String m = String.format("Invalid message file name. name : %s",
					fileName);
			throw new WakabaException(WakabaErrorCode.INTERNAL_ERROR, m);
		}

		if (matcher.groupCount() == 1) {
			if (matcher.group(1) != null) {
				return matcher.group(1).substring(1);
			}
		}

		return "*";
	}

	/**
	 * メッセージ一覧を取得します。
	 * @return メッセージ一覧
	 */
	public Map<String, String> getMessageList() {
		Map<String, String> result = new HashMap<>(this.messageList.size());

		result.putAll(this.messageList);

		return result;
	}

	/**
	 * ロケールIdを取得します。
	 * @return ロケールId
	 */
	public LocaleId getLocaleId() {
		return this.localeId;
	}
}
