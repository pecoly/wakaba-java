package net.jp.yamabuki.io;

/**
 * コンフィグのインターフェース。
 *
 */
public interface Config {

	/**
	 * データベース種別を取得します。
	 * @return データベース種別
	 */
	String getDbType();

	/**
	 * Jdbcのドライバを取得します。
	 * @return Jdbcのドライバ
	 */
	String getDbDriver();

	/**
	 * JdbcのUrlを取得します。
	 * @return JdbcのUrl
	 */
	String getDbUrl();

	/**
	 * Jdbcのデータベースを除いたUrlを取得します。
	 * @return Jdbcのデータベースを除いたUrl
	 */
	String getDbBaseUrl();

	/**
	 * Jdbcのユーザー名を取得します。
	 * @return Jdbcのユーザー名
	 */
	String getDbUsername();

	/**
	 * Jdbcのパスワードを取得します。
	 * @return Jdbcのパスワード
	 */
	String getDbPassword();
}
