package net.jp.yamabuki.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;

/**
 * アプリケーションのコンフィグファイルクラス。
 *
 */
public class ConfigPropertiesFile implements Config {
	private String dbType;
	private String dbDriver;
	private String dbBaseUrl;
	private String dbUrl;
	private String dbUsername;
	private String dbPassword;

	/**
	 * コンフィグファイルから設定を取得します。
	 * @param configFilePath コンフィグファイルのパス
	 */
	public void load(String configFilePath) {
		File configFile = new File(configFilePath);
		if (!configFile.exists()) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND,
					"file : " + configFilePath);
		}

		PropertiesConfig p = new PropertiesConfig();
		try {
			p.load(new FileInputStream(configFile));
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND, ex);
		}

		this.dbType = p.getString("dbType");
		this.dbDriver = p.getString("dbDriver");
		this.dbBaseUrl = p.getString("dbBaseUrl");
		this.dbBaseUrl = this.dbBaseUrl.replace("${user.home}", System.getProperty("user.home"));
		this.dbBaseUrl = this.dbBaseUrl.replace("\\", "/");
		this.dbUrl = p.getString("dbUrl");
		this.dbUrl = this.dbUrl.replace("${user.home}", System.getProperty("user.home"));
		this.dbUrl = this.dbUrl.replace("\\", "/");
		this.dbUsername = p.getString("dbUsername");
		this.dbPassword = p.getString("dbPassword");
	}

	@Override
	public String getDbType() {
		return this.dbType;
	}

	@Override
	public String getDbDriver() {
		return this.dbDriver;
	}

	@Override
	public String getDbBaseUrl() {
		return this.dbBaseUrl;
	}

	@Override
	public String getDbUrl() {
		return this.dbUrl;
	}

	@Override
	public String getDbUsername() {
		return this.dbUsername;
	}

	@Override
	public String getDbPassword() {
		return this.dbPassword;
	}
}
