package net.jp.yamabuki.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.PropertyNotFoundException;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.StringUtils;
import net.jp.yamabuki.util.UserIdUtils;

/**
 * アプリケーションファイルのクラス。
 *
 */
public class AppFile {

	private UserId userId;

	private AppId appId;

	private String appName;

	/**
	 * アプリケーションファイルを読み込みます。
	 * @param inputStream 入力元
	 */
	public void load(InputStream inputStream) {
		PropertiesConfig p = new PropertiesConfig();
		p.load(inputStream);

		String userIdS = p.getString("userId");
		String appIdS = p.getString("appId");
		String appNameS = p.getString("appName");

		if (StringUtils.isBlank(userIdS)) {
			throw new PropertyNotFoundException(
					"Property 'userId' not found.");
		}

		if (StringUtils.isBlank(appIdS)) {
			throw new PropertyNotFoundException(
					"Property 'appId' not found.");
		}

		if (StringUtils.isBlank(appNameS)) {
			throw new PropertyNotFoundException(
					"Property 'appName' not found.");
		}

		this.userId = UserIdUtils.create(userIdS);
		this.appId = AppIdUtils.create(appIdS);
		this.appName = appNameS;
	}

	/**
	 * アプリケーションファイルを読み込みます。
	 * @param file 読み込むアプリケーションファイル
	 */
	public void load(File file) {
		try (InputStream is = new FileInputStream(file)) {
			this.load(is);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND,
					"file : " + file.getAbsolutePath());
		}
		catch (IOException ex) {
			throw new WakabaException(
					WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	/**
	 * アプリケーショのを読み込みます。
	 * @param filePath アプリケーションファイルパス
	 */
	public void load(String filePath) {
		this.load(new File(filePath));
	}

	/**
	 * アプリケーションファイルを書き込みます。
	 * @param outputStream 出力先
	 */
	public void save(OutputStream outputStream) {
		PropertiesConfig p = new PropertiesConfig();

		p.setProperty("userId", this.userId.getValue());
		p.setProperty("appId", this.appId.getValue());
		p.setProperty("appName", this.appName);

		p.save(outputStream);
	}

	/**
	 * アプリケーションファイルを保存します。
	 * @param file 出力先
	 */
	public void save(File file) {
		try (FileOutputStream os = new FileOutputStream(file)) {
			this.save(os);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(
					WakabaErrorCode.FILE_NOT_FOUND,
					"file : " + file.getAbsolutePath());
		}
		catch (IOException ex) {
			throw new WakabaException(
					WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	/**
	 * アプリケーションファイルを保存します。
	 * @param filePath アプリケーションファイルパス
	 */
	public void save(String filePath) {
		this.save(new File(filePath));
	}

	/**
	 * ユーザーIdを取得します。
	 * @return ユーザーId
	 */
	public UserId getUserId() {
		return this.userId;
	}

	/**
	 * ユーザーIdを設定します。
	 * @param userId ユーザーId
	 */
	public void setUserId(UserId userId) {
		Argument.isNotNull(userId, "userId");
		this.userId = userId;
	}

	/**
	 * アプリケーションIdを取得します。
	 * @return アプリケーションId
	 */
	public AppId getAppId() {
		return this.appId;
	}

	/**
	 * アプリケーションIdを設定します。
	 * @param appId アプリケーションId
	 */
	public void setAppId(AppId appId) {
		Argument.isNotNull(appId, "appId");
		this.appId = appId;
	}

	/**
	 * アプリケーション名を取得します。
	 * @return アプリケーション名
	 */
	public String getAppName() {
		return this.appName;
	}

	/**
	 * アプリケーション名を設定します。
	 * @param appName アプリケーション名
	 */
	public void setAppName(String appName) {
		Argument.isNotBlank(appName, "appName");
		this.appName = appName;
	}
}
