package net.jp.yamabuki.io;

/**
 * デフォルトのコンフィグクラス。
 *
 */
public class DefaultConfig implements Config {
	private final String wakabaHomeDirPath;

	/**
	 * コンストラクタ。
	 * @param wakabaHomeDirPath ホームディレクトリ
	 */
	public DefaultConfig(String wakabaHomeDirPath) {
		this.wakabaHomeDirPath = wakabaHomeDirPath;
	}

	@Override
	public String getDbType() {
		return "h2";
	}

	@Override
	public String getDbDriver() {
		return "";
	}

	@Override
	public String getDbBaseUrl() {
		return String.format("jdbc:h2:file:%s/db/",
				this.wakabaHomeDirPath);
	}

	@Override
	public String getDbUrl() {
		return String.format("jdbc:h2:file:%s/db/wakaba",
				this.wakabaHomeDirPath);
	}

	@Override
	public String getDbUsername() {
		return "WAKABA";
	}

	@Override
	public String getDbPassword() {
		return "WAKABA";
	}
}
