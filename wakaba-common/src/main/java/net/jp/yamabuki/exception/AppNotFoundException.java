package net.jp.yamabuki.exception;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * アプリケーションが見つからないときの例外クラス。
 *
 */
@SuppressWarnings("serial")
public class AppNotFoundException extends WakabaException {
	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 */
	public AppNotFoundException(UserId userId, AppId appId) {
		super(WakabaErrorCode.APP_NOT_FOUND);

		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
	}
}
