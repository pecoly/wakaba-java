package net.jp.yamabuki.exception;

/**
 * エラーコード。
 *
 */
public enum WakabaErrorCode {
	// Wakaba-Commonは0000番代を使用

	// 実行時例外
	MODEL_NOT_FOUND(10001, "Model not found."),
	APP_NOT_FOUND(10002, "App not found."),

	PROPERTY_NOT_FOUND(10003, "Property not found."),
	NOT_IMPLEMENTED(10004, "Not implemented."),
	VALIDATION_ERROR(10005, "Validation error."),
	INVALID_ENCRYPTED_DATA(10006, "Invalid encrypted data."),
	INVALID_DECRYPTED_DATA(10007, "Invalid decrypted data."),
	FILE_NOT_FOUND(10008, "File not found."),
	INVALID_CAST(10009, "Invalid cast."),
	BAD_SQL(10010, "Bad SQL grammer."),
	NO_SUCH_QUERY(10011, "No such query."),
	NO_SUCH_UPDATE(10012, "No such update."),
	NO_SUCH_MESSAGE(10013, "No such message."),
	RESOURCE_NOT_FOUND(10014, "Resource not found."),
	CONNECTION_ERROR(10015, "Connection error."),
	XML_NODE_NOT_FOUND(10016, "XML node not found."),
	EMPTY_XML_VALUE(10017, "XML value is empty."),

	APP_ALREADY_EXISTS(10102, "App already exists."),

	APP_USER_NOT_FOUND(11001, "App user not found."),
	VIEW_NOT_FOUND(11002, "View not found."),
	VIEW_TEMPLATE_NOT_FOUND(11003, "View template not found."),
	CSS_NOT_FOUND(11004, "CSS not found."),
	JS_NOT_FOUND(11005, "JS not found."),
	MESSAGE_NOT_FOUND(11006, "Message not found."),
	JDBC_CONNECTION_NOT_FOUND(11007, "JDBC connection not found."),
	QUERY_NOT_FOUND(11108, "Query not found."),
	UPDATE_NOT_FOUND(11109, "Update not found."),

	APP_USER_ALREADY_EXISTS(11101, "App user already exists."),
	MESSAGE_ALREADY_EXISTS(11102, "Message already exists."),
	JDBC_CONNECTION_ALREADY_EXISTS(11103, "JDBC connection already exists."),
	CSS_ALREADY_EXISTS(11104, "CSS already exists."),
	JS_ALREADY_EXISTS(11105, "JS already exists."),
	VIEW_ALREADY_EXISTS(11106, "View already exists."),
	VIEW_TEMPLATE_ALREADY_EXISTS(11107, "View template already exists."),
	QUERY_ALREADY_EXISTS(11108, "Query already exists."),
	UPDATE_ALREADY_EXISTS(11109, "Update already exists."),

	PLUGIN_ALREADY_EXISTS(11110, "Plugin already exists."),
	TEST_QUERY_ALREADY_EXISTS(11111, "Test query already exists."),
	TEST_UPDATE_ALREADY_EXISTS(11112, "Test update already exists."),


	INVALID_PACKAGE_NAME(12001, "Invalid package name."),
	PARSE_ERROR(12002, "Parse error."),

	// ユーザー設定例外
	CONFIG_FILE_NOT_FOUND(20001, "Config file not found."),
	CONFIGURATION_ERROR(20002, "Configuration error."),

	// 内部例外
	INTERNAL_ERROR(30001, ""),
	AES_ERROR(30002, ""),


	UNDEFINIED(90001, ""),
	UNKNOWN(90003, "");

	private final int number;

	private final String message;

	/**
	 * コンストラクタ。
	 * @param number エラーコード
	 * @param message エラー内容
	 */
	WakabaErrorCode(int number, String message) {
		this.number = number;
		this.message = message;
	}

	/**
	 * エラーコードを取得します。
	 * @return エラーコード
	 */
	public int getNumber() {
		return this.number;
	}

	/**
	 * エラー内容を取得します。
	 * @return エラー内容
	 */
	public String getString() {
		return Integer.toString(this.number) + " " + this.message;
	}
}
