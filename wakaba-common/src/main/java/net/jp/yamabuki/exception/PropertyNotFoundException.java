package net.jp.yamabuki.exception;

/**
 * プロパティが見つからないときの例外クラス。
 *
 */
@SuppressWarnings("serial")
public class PropertyNotFoundException extends WakabaException {
	/**
	 * コンストラクタ。
	 * @param message メッセージ
	 */
	public PropertyNotFoundException(String message) {
		super(WakabaErrorCode.PROPERTY_NOT_FOUND, message);
	}
}
