package net.jp.yamabuki.exception;

/**
 * モデルが見つからないときの例外クラス。
 *
 */
@SuppressWarnings("serial")
public class ModelNotFoundException extends WakabaException {
	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 * @param message メッセージ
	 */
	public ModelNotFoundException(WakabaErrorCode errorCode, String message) {
		super(errorCode, message);
	}
}
