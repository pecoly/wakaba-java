package net.jp.yamabuki.exception;

/**
 * 基底例外クラス。
 *
 */
@SuppressWarnings("serial")
public class WakabaException extends YamabukiException {
	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 */
	public WakabaException(WakabaErrorCode errorCode) {
		super(errorCode.getString());
	}

	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 * @param message メッセージ
	 */
	public WakabaException(WakabaErrorCode errorCode, String message) {
		super(errorCode.getString() + " " + message);
	}

	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 * @param t 元の例外
	 */
	public WakabaException(WakabaErrorCode errorCode, Throwable t) {
		super(errorCode.getString(), t);
	}

	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 * @param message メッセージ
	 * @param t 元の例外
	 */
	public WakabaException(WakabaErrorCode errorCode, String message, Throwable t) {
		super(errorCode.getString() + " " + message, t);
	}
}
