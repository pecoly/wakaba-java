package net.jp.yamabuki.exception;

/**
 * モデルがすでに存在するときの例外クラス。
 *
 */
@SuppressWarnings("serial")
public class ModelAlreadyExistsException extends WakabaException {
	/**
	 * コンストラクタ。
	 * @param errorCode エラーコード
	 * @param message メッセージ
	 */
	public ModelAlreadyExistsException(WakabaErrorCode errorCode, String message) {
		super(errorCode, message);
	}
}
