package net.jp.yamabuki.security;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.util.StringUtils;

/**
 * 共通鍵暗号クラス。
 *
 */
public class Aes {
	private Key key;

	private Cipher encryptionCipher;

	private Cipher decryptionCipher;

	private static final int KEY_BYTE_SIZE = 16;

	/**
	 * 初期化します。
	 * @param keySource 16バイトのキー
	 */
	public void initialize(byte[] keySource) {
		Argument.isArrayWithSize(keySource, "keySource", KEY_BYTE_SIZE);

		this.key = new SecretKeySpec(keySource, "AES");
		try {
			this.encryptionCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.decryptionCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		}
		catch (NoSuchAlgorithmException ex) {
			throw new WakabaException(WakabaErrorCode.AES_ERROR, ex);
		}
		catch (NoSuchPaddingException ex) {
			throw new WakabaException(WakabaErrorCode.AES_ERROR, ex);
		}

		try {
			this.encryptionCipher.init(Cipher.ENCRYPT_MODE, this.key);
			this.decryptionCipher.init(Cipher.DECRYPT_MODE, this.key);
		}
		catch (InvalidKeyException ex) {
			throw new WakabaException(WakabaErrorCode.AES_ERROR, ex);
		}
	}

	/**
	 * 暗号化します。
	 * @param data 暗号化するデータ
	 * @return 暗号化したデータ
	 */
	public byte[] encrypt(byte[] data) {
		try {
			return this.encryptionCipher.doFinal(data);
		}
		catch (IllegalBlockSizeException ex) {
			throw new WakabaException(WakabaErrorCode.INVALID_DECRYPTED_DATA, ex);
		}
		catch (BadPaddingException ex) {
			throw new WakabaException(WakabaErrorCode.INVALID_DECRYPTED_DATA, ex);
		}
	}

	/**
	 * 文字列を暗号化してBase64でエンコードします。
	 * @param value 暗号化する文字列
	 * @return 暗号化した文字列
	 */
	public String encrypt(String value) {
		return StringUtils.encodeBase64(
				this.encrypt(StringUtils.toByteArray(value)));
	}

	/**
	 * 復号します。
	 * @param data 復号するデータ
	 * @return 復号したデータ
	 */
	public byte[] decrypt(byte[] data) {
		try {
			return this.decryptionCipher.doFinal(data);
		}
		catch (IllegalBlockSizeException ex) {
			throw new WakabaException(WakabaErrorCode.INVALID_ENCRYPTED_DATA, ex);
		}
		catch (BadPaddingException ex) {
			throw new WakabaException(WakabaErrorCode.INVALID_ENCRYPTED_DATA, ex);
		}
	}

	/**
	 * 文字列をBase64でデコードして複合します。
	 * @param value 復号する文字列
	 * @return 復号した文字列
	 */
	public String decrypt(String value) {
		return StringUtils.toString(
				this.decrypt(StringUtils.decodeBase64(value)));
	}
}
