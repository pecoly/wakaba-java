開発環境
	flywayのDB切り替えはやめて、初回起動時に作成にする
	そのかわり、flywayのCleanは全てのDBに対して実行する
	テストはPostgres,h2全て実行する
	
package
	パッケージの追加コマンド対応
	指定するのはサブコマンドとパッケージパス
	パッケージパスはフォルダ
	パッケージフォルダには
	メッセージファイル・ビューファイル・Query/Updateファイル・コネクションファイルなど
	java -jar wakaba-package.jar import sample/sample_2
	java -jar wakaba-package.jar remove sample/sample_2
	java -jar wakaba-package.jar export sample/sample_2

install
	初期インストール 対話形式とする
	jettyの解凍
	user.home/.wakabaの作成
	初期設定ファイルの作成
	java -jar wakaba-install.jar

export
	データのエクスポート
	
client
	ユーザーの追加、SQLの追加、コネクションの追加
	ユーザーの表示、コネクションの表示など
	