package net.jp.yamabuki.view;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FindView extends BaseView {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testFindView() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/view/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// ビューを追加
		this.addModel(new Object[] {"aab", "abc"});
		this.addModel(new Object[] {"abc", "def"});
		this.addModel(new Object[] {"cba", "ghi"});
		
		// ヘッダ行とビュー3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
		
		this.findModels(new Object[] {"ab"});
		
		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("aab"));
		assertThat(list.get(1), is("abc"));
	}
}
