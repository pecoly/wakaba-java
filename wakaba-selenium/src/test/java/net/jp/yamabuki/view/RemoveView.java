package net.jp.yamabuki.view;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RemoveView extends BaseView {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	public void testRemoveMessage() {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/view/list");
		waitForTitle(this.getListTitle());

		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// ビューを追加
		this.addModel(new Object[] {"aab", "abc"});
		this.addModel(new Object[] {"abc", "def"});
		this.addModel(new Object[] {"cba", "ghi"});

		this.removeModels(new Object[] {"abc"});
		
		this.findModels(new Object[] {""} );
		
		// ヘッダ行とビューの3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));

		List<WebElement> rows = driver.findElement(By.id("app"))
				.findElements(By.tagName("tr"));

		// ヘッダ行とビューの3行になっていること
		assertThat(rows.size(), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("aab"));
		assertThat(list.get(1), is("cba"));
	}
}
