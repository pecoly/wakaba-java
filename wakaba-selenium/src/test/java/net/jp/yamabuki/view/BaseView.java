package net.jp.yamabuki.view;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.jp.yamabuki.Base;

public class BaseView extends Base {
	@Override
	protected String getListTitle() {
		return "Views";
	}

	@Override
	protected String getAddTitle() {
		return "New view";
	}

	@Override
	protected String getUpdateTitle() {
		return "Update view";
	}

	@Override
	protected String getTypeName() {
		return "view";
	}
	
	@Override
	protected int getColSize() {
		return 3;
	}
	
	@Override
	protected void findAllModels() {
		this.findModels(new Object[] {
				"", });
	}
	
	@Override
	protected void setFindParameters(Object[] parameters) {
		String m = String.format(
				"Find %s. name : %s",
				this.getTypeName(),
				(String)parameters[0]);
		System.out.println(m);

		setInputText(By.id("name"), (String)parameters[0]);
	}
	
	@Override
	public void setModelParameters(Object[] parameters) {
		String m = String.format(
				"Add %s. name : { name : %s, content : %s }",
				this.getTypeName(),
				(String)parameters[0],
				(String)parameters[1]);
		System.out.println(m);

		setInputText(By.id("view_name"), (String)parameters[0]);
		setInputText(By.id("view_content"), (String)parameters[1]);
	}
	
	@Override
	protected String getKey(List<WebElement> columns) {
		return columns.get(0).getText();		
	}
}
