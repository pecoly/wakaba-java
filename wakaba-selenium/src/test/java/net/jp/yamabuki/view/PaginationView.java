package net.jp.yamabuki.view;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PaginationView extends BaseView {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testPaginationView() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/view/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（ビューが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));
		
		// ビューを追加
		this.addModel(new Object[] {"a01", "x01"});
		this.addModel(new Object[] {"a02", "x02"});
		this.addModel(new Object[] {"a03", "x03"});
		this.addModel(new Object[] {"a04", "x04"});
		this.addModel(new Object[] {"a05", "x05"});
		this.addModel(new Object[] {"a06", "x06"});
		this.addModel(new Object[] {"a07", "x07"});
		this.addModel(new Object[] {"a08", "x08"});
		this.addModel(new Object[] {"a09", "x09"});
		this.addModel(new Object[] {"a10", "x10"});
		this.addModel(new Object[] {"a11", "x11"});
		this.addModel(new Object[] {"a12", "x12"});
		
		// ヘッダ行とビュー10の11行になっていること
		assertThat(getTableRowSize(By.id("app")), is(11));

		driver.findElement(By.cssSelector("a.page")).click();
		wait(1000);

		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
		
		// ヘッダ行とメッセージ10の11行になっていること
		assertThat(getTableRowSize(By.id("app")), is(11));
	}
}
