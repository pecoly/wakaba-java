package net.jp.yamabuki;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.*;

import com.thoughtworks.selenium.Wait;

public abstract class Base {
	protected WebDriver driver;
	protected String baseUrl;

	public int getTableRowSize(By by) {
		return driver.findElement(by).findElements(By.tagName("tr")).size();
	}
  
	public void waitForTitle(final String title) {
		System.out.println("wait for title. title : " + title);
		Wait wait = new Wait() {
			@Override
			public boolean until() { return driver.getTitle().equals(title); }  // until()がtrueを返すようになるまで、
		};
		wait.wait("Failed to waitForTitle() at " + title + ".", 5 * 1000);
	}
/*
  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
  */
	public static void wait(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void clearInput(By x) {
		for (int j = 0; j < 10; j++) {
			driver.findElement(x).sendKeys(Keys.BACK_SPACE);
		}
	}
	
	public void setInputText(By by, String value) {
		if (StringUtils.isBlank(value)) {
			clearInput(by);
		}
		else {
			driver.findElement(by).sendKeys(value);
		}
	}

	public void addModel(Object[] parameters) {		
		driver.findElement(By.cssSelector("div.pull-right > a")).click();
		waitForTitle(this.getAddTitle());
		this.setModelParameters(parameters);
		driver.findElement(By.id("addButton1")).click();
		waitForTitle(this.getListTitle());	  
	}

	
	public List<String> getKeyList() {
		List<WebElement> rows = driver.findElement(By.id("app"))
				.findElements(By.tagName("tr"));
		
		List<String> result = new ArrayList<>();
		for (int row = 0; row < rows.size(); row++) {
			if (row == 0) {
				continue;
			}
			
			List<WebElement> cols = rows.get(row).findElements(By.tagName("td"));
			assertThat(cols.size(), is(this.getColSize()));
			
			result.add(getKey(cols));
		}
		
		return result;
	}
	
	public void removeAllModels() {
		System.out.println("begin removeAllModels()");
		System.out.println("find all " + this.getTypeName() + ".");
		
		this.findAllModels();

		int count = getTableRowSize(By.id("app")) - 1;
		System.out.println(this.getTypeName() + " count = " + count);
		
		while (getTableRowSize(By.id("app")) > 1) {
			WebElement we
					= driver.findElement(By.id("app"))
					.findElement(By.tagName("table"));
			
			// 編集ボタンをクリック
			we.findElement(By.tagName("a")).click();
			waitForTitle(this.getUpdateTitle());
			
			// 削除ボタンをクリック
			driver.findElement(By.id("removeButton")).click();
			waitForTitle(this.getListTitle());
		}

		count = getTableRowSize(By.id("app")) - 1;
		System.out.println(this.getTypeName() + " count = " + count);

		System.out.println("end removeAllModels()");
	}

	
	public void removeModels(Object[] parameters) {
		/*
		String m = String.format(
				"Remove %s. { cod : %s, localeId : %s, message : %s }",
				this.getTypeName(), code, localeId, message);
		System.out.println(m);
		*/
		
		this.findModels(parameters);

		int count = getTableRowSize(By.id("app")) - 1;
		System.out.println(this.getTypeName() + " count = " + count);
		
		while (true) {
			WebElement we
					= driver.findElement(By.id("app"))
					.findElement(By.tagName("table"));
			
			// 編集ボタンをクリック
			we.findElement(By.tagName("a")).click();
			waitForTitle(this.getUpdateTitle());
			
			// 削除ボタンをクリック
			driver.findElement(By.id("removeButton")).click();
			waitForTitle(this.getListTitle());
			
			if (getTableRowSize(By.id("app")) == 1) {
				break;
			}
		}		
	}

	
	public void findModels(Object[] parameters) {
		setFindParameters(parameters);
		driver.findElement(By.id("findButton")).click();
		wait(1000);

		int count = getTableRowSize(By.id("app")) - 1;
		System.out.println(this.getTypeName() + " count = " + count);
		
		List<WebElement> rows = driver.findElement(By.id("app"))
				.findElements(By.tagName("tr"));
		for (int row = 0; row < rows.size(); row++) {
			if (row == 0) {
				continue;
			}
			
			List<WebElement> cols = rows.get(row).findElements(By.tagName("td"));
			assertThat(cols.size(), is(this.getColSize()));
		}
	}
	protected abstract void setFindParameters(Object[] parameters);
	
	protected abstract void findAllModels();
	protected abstract String getListTitle();
	protected abstract String getAddTitle();
	protected abstract String getUpdateTitle();
	protected abstract String getTypeName();	
	protected abstract int getColSize();
	
	protected abstract String getKey(List<WebElement> columns);
	protected abstract void setModelParameters(Object[] parameters);
}
