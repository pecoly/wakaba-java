package net.jp.yamabuki.message;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FindMessage extends BaseMessage {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testFindMessageByCode() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// メッセージを追加
		this.addModel(new Object[] {"xxy", "ja", "abc"});
		this.addModel(new Object[] {"xyz", "ja", "def"});
		this.addModel(new Object[] {"yzx", "ja", "ghi"});
		
		// ヘッダ行とメッセージ3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
		
		this.findModels(new Object[] {"xy", "", ""});
		
		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("xxy"));
		assertThat(list.get(1), is("xyz"));
	}	

	@Test
	public void testFindMessageByLocaleId() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// メッセージを追加
		this.addModel(new Object[] {"xxy", "ja", "abc"});
		this.addModel(new Object[] {"xyz", "en", "def"});
		this.addModel(new Object[] {"yzx", "ja", "ghi"});
		
		// ヘッダ行とメッセージ3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
		
		findModels(new Object[] {"", "ja", ""});
		
		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("xxy"));
		assertThat(list.get(1), is("yzx"));
	}	

	@Test
	public void testFindMessageByMessage() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// メッセージを追加
		this.addModel(new Object[] {"xxy", "ja", "sst"});
		this.addModel(new Object[] {"xyz", "en", "stu"});
		this.addModel(new Object[] {"yzx", "ja", "tuv"});
		
		// ヘッダ行とメッセージ3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
		
		findModels(new Object[] {"", "", "st"});
		
		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("xxy"));
		assertThat(list.get(1), is("xyz"));
	}	
}
