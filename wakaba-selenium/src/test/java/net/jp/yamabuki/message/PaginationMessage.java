package net.jp.yamabuki.message;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PaginationMessage extends BaseMessage {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testPatinationMessage() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));
		
		// メッセージを追加
		this.addModel(new Object[] {"a01", "ja", "x01"});
		this.addModel(new Object[] {"a02", "ja", "x02"});
		this.addModel(new Object[] {"a03", "ja", "x03"});
		this.addModel(new Object[] {"a04", "ja", "x04"});
		this.addModel(new Object[] {"a05", "ja", "x05"});
		this.addModel(new Object[] {"a06", "ja", "x06"});
		this.addModel(new Object[] {"a07", "ja", "x07"});
		this.addModel(new Object[] {"a08", "ja", "x08"});
		this.addModel(new Object[] {"a09", "ja", "x09"});
		this.addModel(new Object[] {"a10", "ja", "x10"});
		this.addModel(new Object[] {"a11", "ja", "x11"});
		this.addModel(new Object[] {"a12", "ja", "x12"});
		
		// ヘッダ行とメッセージ10の11行になっていること
		assertThat(getTableRowSize(By.id("app")), is(11));
		
		driver.findElement(By.cssSelector("a.page")).click();
		wait(1000);

		// ヘッダ行とメッセージ2の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));

		driver.findElement(By.cssSelector("a.page")).click();
		wait(1000);
		
		// ヘッダ行とメッセージ10の11行になっていること
		assertThat(getTableRowSize(By.id("app")), is(11));
	}
}
