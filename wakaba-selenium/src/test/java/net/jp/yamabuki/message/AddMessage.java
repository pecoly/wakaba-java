package net.jp.yamabuki.message;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AddMessage extends BaseMessage {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testAddMessage() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));
		
		// メッセージを追加
		this.addModel(new Object[] {"a", "ja", "abc"});
		this.addModel(new Object[] {"b", "ja", "def"});
		this.addModel(new Object[] {"c", "ja", "ghi"});
		
		// ヘッダ行とメッセージ3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
	}
}
