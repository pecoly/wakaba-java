package net.jp.yamabuki.message;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RemoveMessage extends BaseMessage {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	public void testRemoveMessage() {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/message/list");
		waitForTitle(this.getListTitle());

		this.removeAllModels();
		
		// ヘッダ行のみ（メッセージが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// メッセージを追加
		this.addModel(new Object[] {"xxy", "ja", "sst"});
		this.addModel(new Object[] {"xyz", "en", "stu"});
		this.addModel(new Object[] {"yzx", "ja", "tuv"});

		this.removeModels(new Object[] {"yzx", "", ""});
		
		this.findModels(new Object[] {"", "", ""} );
		
		// ヘッダ行とユーザー2人の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));

		List<WebElement> rows = driver.findElement(By.id("app"))
				.findElements(By.tagName("tr"));

		// ヘッダ行とメッセージの3行になっていること
		assertThat(rows.size(), is(3));
		
		List<String> list = this.getKeyList();
		assertThat(list.get(0), is("xxy"));
		assertThat(list.get(1), is("xyz"));
	}
}
