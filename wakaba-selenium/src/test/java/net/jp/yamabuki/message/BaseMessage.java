package net.jp.yamabuki.message;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.jp.yamabuki.Base;

public class BaseMessage extends Base {
	@Override
	protected String getListTitle() {
		return "Messages";
	}

	@Override
	protected String getAddTitle() {
		return "New message";
	}

	@Override
	protected String getUpdateTitle() {
		return "Update message";
	}

	@Override
	protected String getTypeName() {
		return "message";
	}
	
	@Override
	protected int getColSize() {
		return 4;
	}
	
	@Override
	protected void findAllModels() {
		this.findModels(new Object[] {
				"", "", "" });
	}
	
	@Override
	protected void setFindParameters(Object[] parameters) {
		String m = String.format(
				"Find %s. { code : %s, localeId : %s, message : %s }",
				this.getTypeName(),
				(String)parameters[0],
				(String)parameters[1],
				(String)parameters[2]);
		System.out.println(m);

		setInputText(By.id("code"), (String)parameters[0]);
		setInputText(By.id("localeId"), (String)parameters[1]);
		setInputText(By.id("message"), (String)parameters[2]);
		
	}
	
	@Override
	public void setModelParameters(Object[] parameters) {
		String m = String.format(
				"Add %s. { code : %s, localeId : %s, message : %s }",
				this.getTypeName(),
				(String)parameters[0],
				(String)parameters[1],
				(String)parameters[2]);
		System.out.println(m);
		
		setInputText(By.id("message_code"), (String)parameters[0]);
		setInputText(By.id("message_localeId"), (String)parameters[1]);
		setInputText(By.id("message_message"), (String)parameters[2]);
	}
	
	@Override
	protected String getKey(List<WebElement> columns) {
		return columns.get(0).getText();		
	}
}
