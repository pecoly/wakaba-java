package net.jp.yamabuki.user;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AddUser extends BaseUser {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testNewUser() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/user/list");
		waitForTitle(this.getListTitle());

		this.removeAllModels();
		
		// ヘッダ行のみ（ユーザーが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));
		
		// ユーザーを追加
		this.addModel(new Object[] {"a"});
		this.addModel(new Object[] {"b"});
		this.addModel(new Object[] {"c"});
		
		// ヘッダ行とユーザー3人の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
	}
}
