package net.jp.yamabuki.user;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FindUser extends BaseUser {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/user/list");
		waitForTitle("Users");

		this.removeAllModels();
		
		// ヘッダ行のみ（ユーザーが0）になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));

		// ユーザーを追加
		this.addModel(new Object[] {"aab"});
		this.addModel(new Object[] {"abc"});
		this.addModel(new Object[] {"cba"});
		
		// ヘッダ行とユーザー3人の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));

		this.findModels(new Object[] {"ab"});
		
		// ヘッダ行とユーザー2人の3行になっていること
		assertThat(getTableRowSize(By.id("app")), is(3));
	}	
}
