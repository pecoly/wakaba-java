package net.jp.yamabuki.user;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.jp.yamabuki.Base;

public class BaseUser extends Base {
	@Override
	protected String getListTitle() {
		return "Users";
	}

	@Override
	protected String getAddTitle() {
		return "New user";
	}

	@Override
	protected String getUpdateTitle() {
		return "Update user";
	}

	@Override
	protected String getTypeName() {
		return "user";
	}
	
	@Override
	protected int getColSize() {
		return 3;
	}
	
	@Override
	protected void findAllModels() {
		this.findModels(new Object[] {
				"", "", "" });
	}
	
	@Override
	protected void setFindParameters(Object[] parameters) {
		String m = String.format(
				"Find %s. loginId : %s",
				this.getTypeName(),
				(String)parameters[0]);
		System.out.println(m);

		setInputText(By.id("loginId"), (String)parameters[0]);
		
	}
	
	@Override
	public void setModelParameters(Object[] parameters) {
		String m = String.format(
				"Add %s. { loginId : %s",
				this.getTypeName(),
				(String)parameters[0]);
		System.out.println(m);
		
		setInputText(By.id("user_loginId"), (String)parameters[0]);
		setInputText(By.id("user_loginPassword1"), "bcde");
		setInputText(By.id("user_loginPassword2"), "bcde");
		driver.findElement(By.id("user_manager")).click();
	}
	
	@Override
	protected String getKey(List<WebElement> columns) {
		return columns.get(0).getText();		
	}
}
