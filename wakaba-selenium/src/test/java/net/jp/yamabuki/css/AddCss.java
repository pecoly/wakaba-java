package net.jp.yamabuki.css;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AddCss extends BaseCss {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:9080";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void testAddCss() throws Exception {
		driver.get(baseUrl + "/wakaba-server/sample/sample_1/manager/view/css/list");
		waitForTitle(this.getListTitle());
    
		this.removeAllModels();
		
		// ヘッダ行のみ(Cssが0)になっていること
		assertThat(getTableRowSize(By.id("app")), is(1));
		
		// Cssを追加
		this.addModel(new Object[] {"a", "abc"});
		this.addModel(new Object[] {"b", "def"});
		this.addModel(new Object[] {"c", "ghi"});
		
		// ヘッダ行とCss3の4行になっていること
		assertThat(getTableRowSize(By.id("app")), is(4));
	}
}
