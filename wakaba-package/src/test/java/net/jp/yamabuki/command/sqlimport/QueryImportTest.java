package net.jp.yamabuki.command.sqlimport;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.eventFrom;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.hasXPath;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.typeCompatibleWith;
import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.sql.mapper.StringMapper;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import org.junit.*;


public class QueryImportTest {
	static DataSource dataSource;

	static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";
	static final String USERNAME = "TEST";
	static final String PASSWORD = "TEST";
	static final String URL = "jdbc:postgresql://192.168.33.10/TEST";

	@BeforeClass
	public static void beforeClass() {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(DRIVER_CLASS_NAME);
		ds.setUsername(USERNAME);
		ds.setPassword(PASSWORD);
		ds.setUrl(URL);
		dataSource = ds;
	}

	/*
	@Test
	public void getFilePathList() {
		SqlImport i = new SqlImport();
		List<Path> filePathList = i.getFilePathList();

	}
	*/

	@AfterClass
	public static void afterClass() {
		dataSource = null;
	}

	void removeAllQueries() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.update("DELETE FROM QUERY_LIST");
	}

	List<Map<String, String>> getAllQueries() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		return jdbc.query("SELECT * FROM QUERY_LIST", new StringMapper());
	}

	@Test
	public void importQuery() {
		removeAllQueries();

		String userId = "suzuki";
		String appId = "jiro";
		QueryImport.QueryFileVisitor visitor = new QueryImport.QueryFileVisitor(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				new JdbcTemplate(dataSource));

		String name = "abc";
		String xml = "def";
		visitor.importQuery(new UserIdImpl(userId), new AppIdImpl(appId), name, xml);

		List<Map<String, String>> list = getAllQueries();
		assertThat(list.size(), is(1));

		Map<String, String> map = list.get(0);
		assertThat(map.get("name"), is("abc"));
		assertThat(map.get("xml"), is("def"));

		removeAllQueries();
	}
}
