package net.jp.yamabuki.command.viewimport;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;

import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.PathUtils;

import org.springframework.jdbc.core.JdbcTemplate;

public class ViewImport {
	private final UserId userId;

	private final AppId appId;

	private JdbcTemplate jdbc;

	private String dirPath;

	public ViewImport(UserId userId, AppId appId, String dirPath, DataSource dataSource) {
		this.userId = userId;
		this.appId = appId;
		this.dirPath = PathUtils.combine(dirPath, "view");
		this.jdbc = new JdbcTemplate(dataSource);
	}

	public void execute() {
		this.removeViews();
		this.importViews();
	}

	void importViews() {
		ViewFileVisitor visitor = new ViewFileVisitor(
				this.userId, this.appId, this.dirPath, jdbc);
		try {
			Path path = Paths.get(dirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	void removeViews() {
		String sql = "DELETE FROM VIEW_LIST "
				+ "WHERE NAME LIKE ? || '/' || ? || '/%' ";

		this.jdbc.update(
				sql,
				new Object[] {
						this.userId.getValue(),
						this.appId.getValue(),
				});
	}

	static class ViewFileVisitor extends SimpleFileVisitor<Path> {
		private final UserId userId;

		private final AppId appId;

		private String dirPath;
		private JdbcTemplate jdbc;

		public ViewFileVisitor(UserId userId, AppId appId, String dirPath, JdbcTemplate jdbc) {
			this.userId = userId;
			this.appId = appId;
			this.dirPath = dirPath;
			this.jdbc = jdbc;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			System.out.println("Visit File: " + file);
			int dirPathLength = this.dirPath.length();
			String name = this.userId.getValue() + "/"
					+ this.appId.getValue() + "/"
					+ file.toString().substring(dirPathLength + 1).replace("\\", "/").replace("@", "/");
			String content = new String(Files.readAllBytes(file));
			System.out.println("name:" + name);
			this.importView(name, content);
			return FileVisitResult.CONTINUE;
		}

		void importView(String name, String content) {
			String sql
					= "INSERT INTO VIEW_LIST( "
					+ "NAME, CONTENT "
					+ ") VALUES(?, ?)";

			this.jdbc.update(
					sql,
					new Object[] {
							name,
							content,
					});
		}
	}
}
