package net.jp.yamabuki.command.sqlimport;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.PropertiesConfig;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.FileUtils;
import net.jp.yamabuki.util.PathUtils;

import org.springframework.jdbc.core.JdbcTemplate;

public class ConnectionImport{
	private ConnectionFileVisitor visitor;

	private final UserId userId;

	private final AppId appId;

	private JdbcTemplate jdbc;

	public ConnectionImport(UserId userId, AppId appId,
			JdbcTemplate jdbc) {
		this.userId = userId;
		this.appId = appId;
		this.jdbc = jdbc;
		this.visitor = new ConnectionFileVisitor(userId, appId, jdbc);
	}

	public void importConnections(String dirPath) {
		Path path = Paths.get(PathUtils.combine(dirPath, "connection"));
		if (!Files.exists(path)) {
			return;
		}

		try {
			Files.walkFileTree(path, this.visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	public void removeConnections() {
		String sql = "DELETE FROM JDBC_CONNECTION_LIST "
				+ "WHERE USER_ID = ? "
				+ "AND APP_ID = ? ";

		this.jdbc.update(
				sql,
				new Object[] {
						this.userId.getValue(),
						this.appId.getValue(),
				});
	}

	static class ConnectionFileVisitor extends SimpleFileVisitor<Path> {
		private UserId userId;
		private AppId appId;
		private JdbcTemplate jdbc;

		public ConnectionFileVisitor(UserId userId, AppId appId, JdbcTemplate jdbc) {
			this.userId = userId;
			this.appId = appId;
			this.jdbc = jdbc;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			System.out.println("Visit File: " + file);

			PropertiesConfig p = new PropertiesConfig();
			try {
				p.load(new FileInputStream(file.toFile()));
			}
			catch (FileNotFoundException e) {
				throw new WakabaException(WakabaErrorCode.PARSE_ERROR,
						"file : " + file.toString(), e);
			}

			String name = FileUtils.removeExtension(file.getFileName().toString());
			String driver = p.getString("driver");
			String url = p.getString("url");
			String username = p.getString("username");
			String password = p.getString("password");

			this.importQuery(this.userId, this.appId,
					name, driver, url, username, password);
			return FileVisitResult.CONTINUE;
		}

		void importQuery(UserId userId, AppId appId,
				String name, String driver, String url,
				String username, String password) {
			String sql
					= "INSERT INTO JDBC_CONNECTION_LIST( "
					+ "USER_ID, APP_ID, NAME, DRIVER, URL, USERNAME, PASSWORD "
					+ ") VALUES(?, ?, ?, ?, ?, ?, ?)";

			this.jdbc.update(
					sql,
					new Object[] {
							userId.getValue(),
							appId.getValue(),
							name,
							driver,
							url,
							username,
							password,
					});
		}
	}
}
