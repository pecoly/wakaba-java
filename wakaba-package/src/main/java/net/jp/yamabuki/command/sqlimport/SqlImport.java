package net.jp.yamabuki.command.sqlimport;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppId;

import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.PathUtils;

import org.springframework.jdbc.core.JdbcTemplate;

public class SqlImport {
	private final UserId userId;

	private final AppId appId;

	private JdbcTemplate jdbc;

	private String dirPath;

	public SqlImport(UserId userId, AppId appId, String dirPath, DataSource dataSource) {
		this.userId = userId;
		this.appId = appId;
		this.dirPath = PathUtils.combine(dirPath, "sql");
		this.jdbc = new JdbcTemplate(dataSource);
	}

	public void execute() {
		this.importQuery();
		this.importConnection();
	}

	void importQuery() {
		QueryImport queryImport = new QueryImport(
				this.userId, this.appId, this.jdbc);
		queryImport.removeQueries();
		queryImport.importQueries(this.dirPath);
	}

	void importConnection() {
		ConnectionImport ci = new ConnectionImport(
				this.userId, this.appId, this.jdbc);
		ci.removeConnections();
		ci.importConnections(this.dirPath);
	}
}
