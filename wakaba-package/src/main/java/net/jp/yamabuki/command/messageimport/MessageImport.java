package net.jp.yamabuki.command.messageimport;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.MessagePropertiesFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.LocaleId;

import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.PathUtils;

import org.springframework.jdbc.core.JdbcTemplate;

public class MessageImport {
	private final UserId userId;

	private final AppId appId;

	private JdbcTemplate jdbc;

	private String dirPath;

	public MessageImport(UserId userId, AppId appId, String dirPath, DataSource dataSource) {
		this.userId = userId;
		this.appId = appId;
		this.dirPath = PathUtils.combine(dirPath, "message");
		this.jdbc = new JdbcTemplate(dataSource);
	}

	public void execute() {
		this.removeMessages();
		this.importMessages();
	}

	void importMessages() {
		Path path = Paths.get(dirPath);
		if (!Files.exists(path)) {
			return;
		}

		try {
			MessageFileVisitor visitor = new MessageFileVisitor(
					this.userId, this.appId, this.jdbc);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	void removeMessages() {
		String sql = "DELETE FROM MESSAGE_LIST "
				+ "WHERE USER_ID = ? "
				+ "AND APP_ID = ?";

		this.jdbc.update(
				sql,
				new Object[] {
						this.userId.getValue(),
						this.appId.getValue(),
				});
	}

	static class MessageFileVisitor extends SimpleFileVisitor<Path> {
		private final UserId userId;

		private final AppId appId;

		private JdbcTemplate jdbc;

		public MessageFileVisitor(UserId userId, AppId appId, JdbcTemplate jdbc) {
			this.userId = userId;
			this.appId = appId;
			this.jdbc = jdbc;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			MessagePropertiesFile messageFile = new MessagePropertiesFile();
			messageFile.load(file.toString());
			LocaleId localeId = messageFile.getLocaleId();
			Map<String, String> messageList = messageFile.getMessageList();
			for (Entry<String, String> x : messageList.entrySet()) {
				this.importMessage(this.userId, this.appId,
						x.getKey(), localeId, x.getValue());
			}

			return FileVisitResult.CONTINUE;
		}

		void importMessage(UserId userId, AppId appId,
				String code, LocaleId localeId, String message) {
			String sql
					= "INSERT INTO MESSAGE_LIST( "
					+ "USER_ID, APP_ID, CODE, LOCALE_ID, MESSAGE "
					+ ") VALUES(?, ?, ?, ?, ?)";

			this.jdbc.update(
					sql,
					new Object[] {
							userId.getValue(),
							appId.getValue(),
							code,
							localeId.getValue(),
							message,
					});
		}
	}
}
