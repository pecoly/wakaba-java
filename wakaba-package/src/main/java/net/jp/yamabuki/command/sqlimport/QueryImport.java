package net.jp.yamabuki.command.sqlimport;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.PathUtils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.jdbc.core.JdbcTemplate;

public class QueryImport{
	private QueryFileVisitor visitor;

	private final UserId userId;

	private final AppId appId;

	private JdbcTemplate jdbc;

	public QueryImport(UserId userId, AppId appId,
			JdbcTemplate jdbc) {
		this.userId = userId;
		this.appId = appId;
		this.jdbc = jdbc;
		this.visitor = new QueryFileVisitor(userId, appId, jdbc);
	}

	public void importQueries(String dirPath) {
		Path path = Paths.get(PathUtils.combine(dirPath, "query"));
		if (!Files.exists(path)) {
			return;
		}

		try {
			Files.walkFileTree(path, this.visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	public void removeQueries() {
		String sql = "DELETE FROM QUERY_LIST "
				+ "WHERE USER_ID = ? "
				+ "AND APP_ID = ? ";

		this.jdbc.update(
				sql,
				new Object[] {
						this.userId.getValue(),
						this.appId.getValue(),
				});
	}

	static class QueryFileVisitor extends SimpleFileVisitor<Path> {
		private UserId userId;
		private AppId appId;
		private JdbcTemplate jdbc;

		public QueryFileVisitor(UserId userId, AppId appId, JdbcTemplate jdbc) {
			this.userId = userId;
			this.appId = appId;
			this.jdbc = jdbc;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			System.out.println("Visit File: " + file);
			String name = FilenameUtils.removeExtension(file.getFileName().toString());
			String content = new String(Files.readAllBytes(file));
			this.importQuery(this.userId, this.appId, name, content);
			return FileVisitResult.CONTINUE;
		}

		void importQuery(UserId userId, AppId appId,
				String name, String xml) {
			String sql
					= "INSERT INTO QUERY_LIST( "
					+ "USER_ID, APP_ID, QUERY_TYPE, NAME, XML "
					+ ") VALUES(?, ?, ?, ?, ?)";

			this.jdbc.update(
					sql,
					new Object[] {
							userId.getValue(),
							appId.getValue(),
							"query",
							name,
							xml,
					});
		}
	}
}
