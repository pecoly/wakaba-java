package net.jp.yamabuki;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import net.jp.yamabuki.app.exp.postgresql.AppFileSqlImpl;
import net.jp.yamabuki.command.messageimport.MessageImport;
import net.jp.yamabuki.command.sqlimport.SqlImport;
import net.jp.yamabuki.command.viewimport.ViewImport;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.ConfigPropertiesFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.pkg.app.exp.AppExport;
import net.jp.yamabuki.pkg.app.exp.GetAppFile;
import net.jp.yamabuki.pkg.app.exp.GetAppFileImpl;
import net.jp.yamabuki.pkg.app.exp.OutputAppFile;
import net.jp.yamabuki.pkg.app.exp.OutputAppFileForPlain;
import net.jp.yamabuki.pkg.app.exp.OutputAppFileForZip;
import net.jp.yamabuki.pkg.app.imp.AddAppFileImpl;
import net.jp.yamabuki.pkg.app.imp.AppImport;
import net.jp.yamabuki.pkg.app.imp.InputAppFile;
import net.jp.yamabuki.pkg.app.imp.InputAppFileForPlain;
import net.jp.yamabuki.util.PathUtils;

public class WakabaPackage {
	public static void main(String[] args) {
		WakabaPackage w = new WakabaPackage();
		try {
			w.start(args);
			return;
		}
		catch (NoSuchAlgorithmException ex) {
			System.out.println(ex.getMessage());
		}
		catch (ParseException ex) {
			System.out.println(ex.getMessage());
		}
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		System.exit(1);
	}

	private String userHomeDirPath;
	private String wakabaDirPath;

	public WakabaPackage() {
		this.userHomeDirPath = System.getProperty("user.home");
		this.wakabaDirPath = PathUtils.combine(userHomeDirPath, "wakaba");
	}

	public void start(String ... args) throws NoSuchAlgorithmException, ParseException, IOException {
		/*
		Path wakabaConfigFilePath = Paths.get(PathUtil.combine(userHomeDirPath, ".wakaba", "wakaba.config"));
		if (!Files.exists(wakabaConfigFilePath)) {
			throw new FileNotFoundException(
					"File not found. file : " + wakabaConfigFilePath.toString());
		}
		*/
		String configFilePath = PathUtils.combine(this.wakabaDirPath, "wakaba.config");

		ConfigPropertiesFile configFile = new ConfigPropertiesFile();
		configFile.load(configFilePath);

		String driver = configFile.getDbDriver();
		String url = configFile.getDbUrl();
		String username = configFile.getDbUsername();
		String password = configFile.getDbPassword();

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);

		Options opt = new Options();
		opt.addOption("import", true, "");
		opt.addOption("export", false, "");
		opt.addOption("userId", true, "");
		opt.addOption("appId", true, "");

		BasicParser parser = new BasicParser();
		CommandLine cl = parser.parse(opt, args);

		boolean hasImport = cl.hasOption("import");
		boolean hasExport = cl.hasOption("export");
		boolean hasUserId = cl.hasOption("userId");
		boolean hasAppId = cl.hasOption("appId");

		boolean success = false;
		if (hasImport) {
			String packageName = cl.getOptionValue("import");
			success = importPackage(dataSource, packageName);
		}
		else if (hasExport && hasUserId && hasAppId) {
			UserId userId = new UserIdImpl(cl.getOptionValue("userId"));
			AppId appId = new AppIdImpl(cl.getOptionValue("appId"));
			//success = exportPackage(dataSource, userId, appId);
			success = exportZip(dataSource, userId, appId);
		}

		if (!success) {
			System.out.println("usage: wakaba-package [options]");
			System.out.println(" -add         ユーザーを追加します。");
			System.out.println();
			System.out.println(" -id <arg>    ユーザーID(半角英数字 最大20文字)");
			System.out.println();
			System.out.println("例) java -jar wakaba-package.jar -import sample.sample2.zip");
			System.out.println();
			return;
		}
	}

	boolean importPackage(DataSource dataSource, String packageName) {
		Pattern p = Pattern.compile("(.+)@(.+)");

		Matcher m = p.matcher(packageName);
		if (!m.matches()) {
			throw new WakabaException(WakabaErrorCode.INVALID_PACKAGE_NAME);
		}

		if (m.groupCount() != 2) {
			throw new WakabaException(WakabaErrorCode.INVALID_PACKAGE_NAME);
		}

		UserId userId = new UserIdImpl(m.group(1));
		AppId appId = new AppIdImpl(m.group(2));

		System.out.println(m.group(1));
		System.out.println(m.group(2));

		String dirPath = this.wakabaDirPath + "/" + packageName;

		// Sql Import
		SqlImport s = new SqlImport(
				userId,
				appId,
				dirPath,
				dataSource);
		s.execute();

		// View Import
		ViewImport v = new ViewImport(
				userId,
				appId,
				dirPath,
				dataSource);
		v.execute();

		// Message Import
		MessageImport mi = new MessageImport(
				userId,
				appId,
				dirPath,
				dataSource);
		mi.execute();

		// App Import
		AddAppFileImpl addAppFile = new AddAppFileImpl();
		addAppFile.setNamedParameterJdbcTemplate(
				new NamedParameterJdbcTemplate(dataSource));
		addAppFile.setAppFileSql(new AppFileSqlImpl());

		InputAppFile inputAppFile = new InputAppFileForPlain(dirPath);
		AppImport ai = new AppImport(inputAppFile, addAppFile);
		ai.execute();


		return true;
	}

	boolean exportPackage(DataSource dataSource, UserId userId, AppId appId) {

		String packageName = userId + "@" + appId;
		String dirPath = this.wakabaDirPath + "/" + packageName;

		// App Export
		GetAppFileImpl getAppFile = new GetAppFileImpl();
		getAppFile.setNamedParameterJdbcTemplate(
				new NamedParameterJdbcTemplate(dataSource));
		getAppFile.setAppFileSql(new AppFileSqlImpl());

		OutputAppFile outputAppFile = new OutputAppFileForPlain(dirPath);
		AppExport ai = new AppExport(getAppFile, outputAppFile);
		ai.execute(userId, appId);

		return true;
	}

	boolean exportZip(DataSource dataSource, UserId userId, AppId appId) {

		String packageName = userId + "@" + appId;
		String filePath = this.wakabaDirPath + "/" + packageName + ".zip";


		try (ZipArchiveOutputStream zip
				= new ZipArchiveOutputStream(new FileOutputStream(filePath))) {

			zip.setEncoding(System.getProperty("file.encoding"));

			// App Export
			GetAppFileImpl getAppFile = new GetAppFileImpl();
			getAppFile.setNamedParameterJdbcTemplate(
					new NamedParameterJdbcTemplate(dataSource));
			getAppFile.setAppFileSql(new AppFileSqlImpl());

			OutputAppFile outputAppFile = new OutputAppFileForZip(zip);
			AppExport ai = new AppExport(getAppFile, outputAppFile);
			ai.execute(userId, appId);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}

		return true;
	}
}
