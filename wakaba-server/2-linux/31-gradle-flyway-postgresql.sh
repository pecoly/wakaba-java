cd ..
gradle flywayClean -Penv=postgresql
gradle flywayInit -Penv=postgresql
gradle flywayMigrate -Penv=postgresql
gradle flywayClean -Penv=postgresql-sample
gradle flywayInit -Penv=postgresql-sample
gradle flywayMigrate -Penv=postgresql-sample

