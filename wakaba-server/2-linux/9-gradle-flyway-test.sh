cd ..
gradle flywayClean -Penv=postgresql-test
gradle flywayInit -Penv=postgresql-test
gradle flywayMigrate -Penv=postgresql-test
