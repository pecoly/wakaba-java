
DELETE FROM VIEW_QUERY;
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/list', 'pecoly/myapp/query/getRecordList', 'recordList', 'ym=200001');
	
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/update', 'pecoly/myapp/query/getRecord', 'record', '');
	
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/update', 'pecoly/myapp/query/getItemList', 'item1List', 'itemCategory=1');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/update', 'pecoly/myapp/query/getItemList', 'item2List', 'itemCategory=2');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/update', 'pecoly/myapp/query/getItemList', 'item3List', 'itemCategory=3');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/update', 'pecoly/myapp/query/getItemList', 'item4List', 'itemCategory=4');
	
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/add', 'pecoly/myapp/query/getItemList', 'item1List', 'itemCategory=1');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/add', 'pecoly/myapp/query/getItemList', 'item2List', 'itemCategory=2');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/add', 'pecoly/myapp/query/getItemList', 'item3List', 'itemCategory=3');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/record/add', 'pecoly/myapp/query/getItemList', 'item4List', 'itemCategory=4');
	
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/category/list', 'pecoly/myapp/query/getCategoryList', 'categoryList', '');

INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/category/update', 'pecoly/myapp/query/getCategory', 'category', '');
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/category/update', 'pecoly/myapp/query/getItemList', 'itemList', '');
	
INSERT INTO VIEW_QUERY (VIEW_NAME, QUERY_NAME, MODEL_NAME, PARAMETER) VALUES
	('pecoly/myapp/users/view/item/update', 'pecoly/myapp/query/getItem', 'item', '');

DROP SEQUENCE IF EXISTS RECORD_LIST_ID_SEQ;
CREATE SEQUENCE RECORD_LIST_ID_SEQ;
DROP TABLE IF EXISTS RECORD_LIST;
CREATE TABLE IF NOT EXISTS RECORD_LIST (
	ID INT DEFAULT NEXTVAL('RECORD_LIST_ID_SEQ')
,	RECORD_DATE DATE NOT NULL
,	ITEM_01_ID INT
,	ITEM_02_ID INT
,	ITEM_03_ID INT
,	ITEM_04_ID INT
,	AMOUNT INT NOT NULL
,	PLUS_MINUS INT NOT NULL
,	COMMENT VARCHAR(256)
,	UPDATE_DATE DATE NOT NULL
,	PRIMARY KEY (ID)
);


DROP TABLE IF EXISTS CATEGORY_LIST;
CREATE TABLE IF NOT EXISTS CATEGORY_LIST (
	ID INT NOT NULL
,	NAME VARCHAR(256) NOT NULL
,	ENABLED INT NOT NULL
,	PARENT_ID INT
,	PRIMARY KEY (ID)
);


DROP SEQUENCE IF EXISTS ITEM_LIST_ID_SEQ;
CREATE SEQUENCE ITEM_LIST_ID_SEQ;

DROP TABLE IF EXISTS ITEM_LIST;
CREATE TABLE IF NOT EXISTS ITEM_LIST (
	ID INT DEFAULT NEXTVAL('ITEM_LIST_ID_SEQ')
,	CATEGORY INT NOT NULL
,	NAME VARCHAR(256) NOT NULL
,	PRIMARY KEY (ID)
);


