@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])

def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importUserService() {
	deleteSql = "DELETE FROM USER_SERVICE"
	sql.execute(deleteSql)
	db.user.drop()

	insertSql = "INSERT INTO USER_SERVICE (LOGIN_ID, LOGIN_PASSWORD, MAIN_AUTHORITY, SUB_AUTHORITY, " \
		+ "USER_ID, APP_ID, DB_USERNAME, DB_PASSWORD) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )"
	filePath = "../src/main/resources/userlist.txt"
	new File(filePath).eachLine{
		words = it.split("/", 8)
		if (words.length == 8) {
			sql.execute(insertSql, words)

			db.user.insert(loginId : words[0], loginPassword : words[1],
				mainAuthority : words[2], subAuthority : words[3],
				userIdAuthority : words[4], appIdAuthority : words[5],
				dbUsername : words[6], dbPassword : words[7])
		}
		else {
			println "not imported $it $words.length" 
			for (i in words) {
				println(i)
			}
		}
	}
	
}

importUserService()
