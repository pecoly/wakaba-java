//@Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.18')
@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab('com.gmongo:gmongo:1.0')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql
import java.nio.file.Files
import java.nio.file.Paths
import com.gmongo.GMongo

def mongo = new GMongo('192.168.33.10', 27017)
db = mongo.getDB('WAKABA')


/*
sql = Sql.newInstance(
	"jdbc:mysql://localhost:3306/wakaba",
	"WAKABA", "WAKABA",
	"com.mysql.jdbc.Driver")
*/
sql = Sql.newInstance(
	"jdbc:postgresql://192.168.33.10/WAKABA",
	"WAKABA", "WAKABA",
	"org.postgresql.Driver")
def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def exportVelocity(datetime) {
	selectSql = "SELECT * FROM VELOCITY"
	sql.eachRow(selectSql) {
		//println it.NAME
		pos = it.NAME.lastIndexOf('/')
		name = ""
		dir = ""
		if (pos == -1) {
			name = it.NAME
		}
		else {
			dir = it.NAME.substring(0, pos)
			name = it.NAME.substring(pos + 1)
		}

//		println "name = ${name}"
//		println "dir = ${dir}"
		dirPath = "export/${datetime}/${dir}/view"
		filePath = dirPath + "/${name}.vm"
		Files.createDirectories(Paths.get(dirPath))

		writer = new OutputStreamWriter(new FileOutputStream(filePath),"UTF-8"); 
		writer.write(it.CONTENT)
		writer.close()
	}
}

def exportQueryService(datetime) {
	selectSql = "SELECT * FROM QUERY_SERVICE"
	sql.eachRow(selectSql) {
		pos = it.NAME.lastIndexOf('/')
		name = ""
		dir = ""
		if (pos == -1) {
			name = it.NAME
		}
		else {
			dir = it.NAME.substring(0, pos)
			name = it.NAME.substring(pos + 1)
		}

		dirPath = "export/${datetime}/${dir}/query"
		filePath = dirPath + "/${name}.xml"
		Files.createDirectories(Paths.get(dirPath))

		writer = new OutputStreamWriter(new FileOutputStream(filePath),"UTF-8"); 
		writer.write(it.XML)
		writer.close()
	}
}

def exportUpdateService(datetime) {
	selectSql = "SELECT * FROM UPDATE_SERVICE"
	sql.eachRow(selectSql) {
		pos = it.NAME.lastIndexOf('/')
		name = ""
		dir = ""
		if (pos == -1) {
			name = it.NAME
		}
		else {
			dir = it.NAME.substring(0, pos)
			name = it.NAME.substring(pos + 1)
		}

		dirPath = "export/${datetime}/${dir}/update"
		filePath = dirPath + "/${name}.xml"
		Files.createDirectories(Paths.get(dirPath))

		writer = new OutputStreamWriter(new FileOutputStream(filePath),"UTF-8"); 
		writer.write(it.XML)
		writer.close()
	}
}

def exportMessageFromPostgresql(datetime) {
	selectSql = "SELECT * FROM MESSAGE_SERVICE"
	sql.eachRow(selectSql) {
		dirPath = "export/${datetime}/message/postgresql/${it.USER_ID}/${it.APP_ID}"
		if (it.USER_ID.equals("*") || it.APP_ID.equals("*")) {
			dirPath =  "export/${datetime}/message/mongodb"
		}
		
		filePath = ""
		if (it.LOCALE.equals("*")) {
			filePath = dirPath + "/messages.properties"
		}
		else {
			filePath = dirPath + "/messages_${it.LOCALE}.properties"
		}
		Files.createDirectories(Paths.get(dirPath))

		writer = new OutputStreamWriter(new FileOutputStream(filePath, true),"UTF-8"); 
		writer.write("${it.CODE}=${it.MESSAGE}\r\n")
		writer.close()
	}
}
def exportMessageFromMongoDb(datetime) {
	db.message.find().each {
		dirPath = "export/${datetime}/message/mongodb/${it.userId}/${it.appId}"
		if (it.userId.equals("*") || it.appId.equals("*")) {
			dirPath =  "export/${datetime}/message/mongodb"
		}

		filePath = ""
		if (it.locale.equals("*")) {
			filePath = dirPath + "/messages.properties"
		}
		else {
			filePath = dirPath + "/messages_${it.locale}.properties"
		}

		Files.createDirectories(Paths.get(dirPath))

		writer = new OutputStreamWriter(new FileOutputStream(filePath, true),"UTF-8"); 
		writer.write("${it.code}=${it.message}\r\n")
		writer.close()
	}
}

datetime = new Date().format("yyyyMMdd-HHmmss")

exportVelocity(datetime)
exportQueryService(datetime)
exportUpdateService(datetime)
exportMessageFromPostgresql(datetime)
exportMessageFromMongoDb(datetime)
