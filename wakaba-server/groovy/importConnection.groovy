@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])

def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importConnectionService() {
	deleteSql = "DELETE FROM JDBC_CONNECTION_SERVICE"
	sql.execute(deleteSql)

	insertSql = "INSERT INTO JDBC_CONNECTION_SERVICE (USER_ID, APP_ID, NAME, DRIVER, URL, USERNAME, PASSWORD) VALUES ( ?, ?, ?, ?, ?, ?, ? )"
	basePath = "../src/main/resources/sql"
	basePathLength = basePath.length()
	new File(basePath).eachFileRecurse {
		filePath = it.getPath().replace("\\", "/")
		pathList = filePath.split("/")
		//println filePath

		if (it.isFile() && filePath.endsWith(".properties")) {
			if (pathList.length >= 8 && pathList[7].equals("connection")) {
				name = toKey(pathList[8], ".properties")
				def props = new Properties()
				new File(filePath).withInputStream { 
					stream -> props.load(stream) 
				}
				userId = pathList[5]
				appId = pathList[6]

				println "import connection service ${name}"
				sql.execute(insertSql, [userId, appId,
					name, props["driver"],
					props["url"], props["username"], props["password"]])
			}
		}
	}
}

importConnectionService()
