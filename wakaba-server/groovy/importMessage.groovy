@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])

def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importMessageToPostgres(userId, appId, code, locale, message) {
	insertSql = "INSERT INTO MESSAGE_LIST (USER_ID, APP_ID, CODE, LOCALE_ID, MESSAGE) VALUES ( ?, ?, ?, ?, ? )"
	sql.execute(insertSql, [userId, appId, code, locale, message])
}

/*
def importMessageToMongoDb(userId, appId, code, locale, message) {
	db.message.insert(userId : userId, appId : appId, code : code, locale : locale, message : message)
}
*/
def importMessage() {
	deleteSql = "DELETE FROM MESSAGE_LIST WHERE USER_ID = '*' AND APP_ID = '*'"
	sql.execute(deleteSql)

	basePath = "../src/main/webapp/WEB-INF/message"
	basePathLength = basePath.length()
	new File(basePath).eachFileRecurse {
		filePath = it.getPath().replace("\\", "/")
		pathList = filePath.split("/")
		println filePath

		if (it.isFile() && filePath.endsWith(".properties")
			&& pathList.length >= 8) {
			path = pathList[pathList.length - 1]
			locale = path[8] == '_' ? path.substring(9, 11) : "*"
			userId = pathList[6]
			appId = pathList.length == 9 ? pathList[7] : "*"


			properties = new Properties();
			new File(filePath).withInputStream { input ->
				properties.load(input)
			}

			println locale
			println userId
			println appId

			properties.stringPropertyNames().each { code ->
				message = properties.getProperty(code)
				importMessageToPostgres(userId, appId, code, locale, message)
				//importMessageToMongoDb(userId, appId, code, locale, message)
			}
		}
	}

	new File(basePath).eachFile {
		filePath = it.getPath().replace("\\", "/")
		pathList = filePath.split("/")

		if (it.isFile() && filePath.endsWith(".properties")
			&& pathList.length == 7) {
			path = pathList[6]
			locale = path[8] == '_' ? path.substring(9, 11) : "*"

			println filePath + ", " +  pathList.length;
			
			properties = new Properties();
			new File(filePath).withInputStream { input ->
				properties.load(input)
			}

			println locale
			
			properties.stringPropertyNames().each { code ->
				message = properties.getProperty(code)
				importMessageToPostgres("*", "*", code, locale, message)
				//importMessageToMongoDb("*", "*", code, locale, message)

			}
		}
	}
}

importMessage()
