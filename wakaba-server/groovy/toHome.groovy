def userHomeDir = System.getProperty("user.home")
def wakabaDir = userHomeDir + "/wakaba"

new AntBuilder().copy(todir: wakabaDir) {
    fileset(dir : "../../sample-package") {
        include(name:"**/*.*")
    }
}

new File(wakabaDir + "/wakaba-package.jar") << new File("../../wakaba-package/build/libs/wakaba-package.jar")

