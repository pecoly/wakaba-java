@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])

def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importQueryService() {
	deleteSql = "DELETE FROM QUERY_SERVICE"
	sql.execute(deleteSql)

	insertSql = "INSERT INTO QUERY_SERVICE (USER_ID, APP_ID, QUERY_TYPE, NAME, XML) VALUES ( ?, ?, ?, ?, ? )"
	basePath = "../src/main/resources/sql"
	basePathLength = basePath.length()
	new File(basePath).eachFileRecurse {
		filePath = it.getPath().replace("\\", "/")
		pathList = filePath.split("/")
		//println filePath

		if (it.isFile() && filePath.endsWith(".xml")) {
			def userId = ""
			def appId = ""
			def type = ""
			def name = ""
			if (pathList.length >= 7) {
				userId = pathList[5]
				appId = pathList[6]
			}

			if (pathList[7].equals("query")) {
				if (pathList.length == 9) {
					type = "query"
					name = toKey(pathList[8], ".xml")
				}
			}
			else if (pathList.length == 10) {
				if ((pathList[7] == "collection"
					|| pathList[7] == "model"
					|| pathList[7] == "count")
					&& pathList[8] == "get") {
					type = pathList[7] + "/" + pathList[8] 
					name = toKey(pathList[9], ".xml");
				}
			}

			xml = readFile(filePath)
			if (name != "") {
				sql.execute(insertSql, [userId, appId, type, name, xml])
			}
			if (name != "") {
				println "import query service $userId $appId $name"
			}
		}
	}
}

importQueryService()
