@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])

def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importVelocity() {
	deleteSql = "DELETE FROM VIEW_LIST WHERE PATH LIKE 'manager/%'"
	sql.execute(deleteSql)


	sql.execute("DELETE FROM VIEW_LIST WHERE PATH = '404.vm'")
	sql.execute("DELETE FROM VIEW_LIST WHERE PATH = 'login.vm'")

	insertSql = "INSERT INTO VIEW_LIST (NAME, PATH, USER_ID, APP_ID, CONTENT) VALUES ( ?, ?, ?, ?, ? )"
	basePath = "../src/main/webapp/WEB-INF/view"
	basePathLength = basePath.length()
	new File(basePath).eachFileRecurse {
		filePath = it.getPath().replace("\\", "/")

		if (it.isFile() && filePath.endsWith(".vm")) {
			path = toKey(filePath.substring(basePathLength + 1), ".vm") + ".vm"

			pathList = path.split("/")
			if (pathList.length >= 3) {
				appId = pathList[0]
				view = pathList[1]
				name = toKey(path.substring(appId.length() + 1 + view.length() + 1), ".vm")

				content = readFile(filePath)
				println "1" + name + " " + path

				sql.execute(insertSql, [name, path, "*", appId, content])
			}
			else {
				path = toKey(filePath.substring(basePathLength + 1), ".vm") + ".vm"
				content = readFile(filePath)
				name = toKey(path, ".vm")
				println "2" + name + " " + path
				sql.execute(insertSql, [name, path, "*", "*", content])
			}
		}
	}

	sql.execute("DELETE FROM VIEW_LIST WHERE NAME = 'spring'")
	sql.execute("DELETE FROM VIEW_LIST WHERE NAME = 'ds-custom'")

	sql.execute(insertSql, ["spring","spring", "*", "*", readFile("../src/main/webapp/WEB-INF/spring.vm")])
	sql.execute(insertSql, ["ds-custom", "ds-custom", "*", "*", readFile("../src/main/webapp/WEB-INF/ds-custom.vm")])
}

importVelocity()
