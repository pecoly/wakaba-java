@Grab(group = 'postgresql', module = 'postgresql', version = '9.1-901.jdbc4')
@Grab(group = 'com.h2database', module = 'h2', version = '1.3.173')

@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def env = args[0]
def envProperties = new Properties()

new File(env + ".properties").withInputStream { input ->
	envProperties.load(input)
}

println "url : " + envProperties["dbUrl"]

def userHome = System.getProperty("user.home")
def dbUrl = envProperties["dbUrl"]
dbUrl = dbUrl.replace("\${user.home}", userHome)
dbUrl = dbUrl.replace("\\", "/")

sql = Sql.newInstance(
	dbUrl,
	envProperties["dbUsername"],
	envProperties["dbPassword"],
	envProperties["dbDriver"])


def readFile(filePath) {
	return new File(filePath).getText("UTF-8")
}

def toKey(filePath, ext) {
	extLength = ext.length()
	if (filePath.length() <= extLength) {
		return ""
	}

	return filePath.substring(0, filePath.length() - extLength)
}

def importViewTemplate() {
	deleteSql = "DELETE FROM VIEW_TEMPLATE_LIST"
	sql.execute(deleteSql)
	deleteSql = "DELETE FROM USER_TEMPLATE_LIST"
	sql.execute(deleteSql)

	insertSql1 = "INSERT INTO VIEW_TEMPLATE_LIST (NAME, USER_ID, APP_ID, CONTENT) VALUES ( ?, ?, ?, ? )"
	insertSql2 = "INSERT INTO USER_TEMPLATE_LIST (NAME, USER_ID, APP_ID, DESCRIPTION) VALUES ( ?, ?, ?, ? )"
	insertSql3 = "INSERT INTO VIEW_LIST (USER_ID, APP_ID, NAME, PATH, CONTENT) VALUES ( ?, ?, ?, ?, ? )"
	basePath = "../src/main/webapp/WEB-INF/template"
	basePathLength = basePath.length()
	new File(basePath).eachFileRecurse {
		filePath = it.getPath().replace("\\", "/")
		if (it.isFile() && filePath.endsWith(".html")) {
			name = toKey(filePath.substring(basePathLength + 1), ".html")
			content = readFile(filePath)
			println name
			sql.execute(insertSql1, [name, '*', '*', content])
		}
		else if (it.isFile() && filePath.endsWith(".properties")) {
			println name

			def props = new Properties()
			new File(filePath).withInputStream { 
				stream -> props.load(stream) 
			}

			sql.execute(insertSql2, props["name"], '*', '*', props["description"])
		}
		else if (it.isFile() && filePath.endsWith(".vm")) {
			name = "template/" + toKey(filePath.substring(basePathLength + 1), ".vm") + ".vm"
			
			deleteSql = "DELETE FROM VIEW_LIST WHERE NAME = ?"
			sql.execute(deleteSql, [name])

			content = readFile(filePath)
			userId = "*"
			appId = "*"
			path = name
			println "3 " +  ", " + userId + ", " + appId + ", " + name + ", " + ", " + path
			sql.execute(insertSql3, [userId, appId, name, path, content])
		}
	}
}

importViewTemplate()
