PUSHD ..\groovy

REM CALL groovy exportView.groovy postgresql

CALL groovy importView.groovy postgresql
CALL groovy importViewTemplate.groovy postgresql
CALL groovy importQuery.groovy postgresql
CALL groovy importUpdate.groovy postgresql
CALL groovy importMessage.groovy postgresql
REM CALL groovy importUser.groovy postgresql
CALL groovy importConnection.groovy postgresql

POPD

