PUSHD ..

CALL gradle flywayClean -Penv=postgresql
CALL gradle flywayInit -Penv=postgresql
CALL gradle flywayMigrate -Penv=postgresql

CALL gradle flywayClean -Penv=postgresql-sample
CALL gradle flywayInit -Penv=postgresql-sample
CALL gradle flywayMigrate -Penv=postgresql-sample

POPD
