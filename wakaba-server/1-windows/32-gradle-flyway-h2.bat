PUSHD ..

CALL gradle flywayClean -Penv=h2
CALL gradle flywayInit -Penv=h2
CALL gradle flywayMigrate -Penv=h2

CALL gradle flywayClean -Penv=h2-sample
CALL gradle flywayInit -Penv=h2-sample
CALL gradle flywayMigrate -Penv=h2-sample

POPD
