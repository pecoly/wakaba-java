PUSHD ..\groovy

REM CALL groovy exportView.groovy h2

CALL groovy importView.groovy h2
CALL groovy importViewTemplate.groovy h2
CALL groovy importQuery.groovy h2
CALL groovy importUpdate.groovy h2
CALL groovy importMessage.groovy h2
REM CALL groovy importUser.groovy h2
CALL groovy importConnection.groovy h2

POPD

