PUSHD ..

CALL gradle flywayClean -Penv=postgresql-test
CALL gradle flywayInit -Penv=postgresql-test
CALL gradle flywayMigrate -Penv=postgresql-test

POPD
