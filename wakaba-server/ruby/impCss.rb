require "find"
require "pg"

begin
	con = PGconn.connect("192.168.33.10", 5432, "", "", "WAKABA", "WAKABA", "WAKABA")

	deleteSql = "DELETE FROM CSS_LIST WHERE USER_ID = $1 AND APP_ID = $2"
	insertSql = "INSERT INTO CSS_LIST (USER_ID, APP_ID, NAME, CONTENT) VALUES ( $1, $2, $3, $4 )"
	basePath = "../src/main/webapp/resources"
	basePathLength = basePath.length

	con.exec(deleteSql, ['*', '*'])
	Find.find(basePath) { |f|
		next unless FileTest.file?(f)
		filePath = f
		if f.end_with?(".css") then
			puts filePath
			name = File.basename(filePath, ".css")
			content = File.read(filePath)
			con.exec(insertSql, ['*', '*', name, content])
		end
	}
end

