require "find"
require "pg"

begin
	con = PGconn.connect("192.168.33.10", 5432, "", "", "WAKABA", "WAKABA", "WAKABA")

	con.exec("DELETE FROM VIEW_LIST WHERE PATH LIKE 'manager/%'")
	con.exec("DELETE FROM VIEW_LIST WHERE PATH = '404.vm'")
	con.exec("DELETE FROM VIEW_LIST WHERE PATH = 'login.vm'")

	insertSql = "INSERT INTO VIEW_LIST (USER_ID, APP_ID, NAME, PATH, CONTENT) VALUES ( $1, $2, $3, $4, $5 )"
	basePath = "../src/main/webapp/WEB-INF/view"
	basePathLength = basePath.length

	Find.find(basePath) { |f|
		next unless FileTest.file?(f)
		filePath = f
		if f.end_with?(".vm") then
			puts(f)
#			name = File.basename(filePath, ".vm")
			path = filePath[basePathLength + 1..filePath.length]
			pathList = path.split("/")
			if pathList.length >= 3 then
				appId = pathList[0]
				view = pathList[1]
				name = path[appId.length + 1 + view.length + 1..path.length - 4]
#				puts name + " " + path

				con.exec(insertSql, ["*", "manager", name, path, File.read(filePath)])
			else
				name = path[0..path.length - 4]
				puts name
				puts path
				con.exec(insertSql, ["*", "*", name, path, File.read(filePath)])
			end
		end
	}

	con.exec("DELETE FROM VIEW_LIST WHERE PATH = 'spring'")
	con.exec("DELETE FROM VIEW_LIST WHERE PATH = 'ds-custom'")
	con.exec(insertSql, ["*", "*", "spring", "spring", File.read("../src/main/webapp/WEB-INF/spring.vm")])
	con.exec(insertSql, ["*", "*", "ds-custom", "ds-custom", File.read("../src/main/webapp/WEB-INF/ds-custom.vm")])
end

