#
# Cookbook Name:: dev_mongodb
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "mongodb::default"
#include_recipe "mongodb::10gen_repo"

#mongodb_instance "mongodb" do
#  port node['application']['port']
#end

service "mongodb" do
	supports restart: true
	action [:enable, :start]
end


template "/etc/mongodb.conf" do
	source "mongodb.conf.erb"
	group "root"
	owner "root"
	mode "0644"
	notifies :restart, "service[mongodb]"
end
