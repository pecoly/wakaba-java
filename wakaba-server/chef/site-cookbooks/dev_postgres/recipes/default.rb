package "postgresql-server" do
    action :install
end

ENV['LANGUAGE'] = ENV['LANG'] = ENV['LC_ALL'] = "en_US.UTF-8"
execute "/sbin/service postgresql initdb --encoding=UTF8" do
  not_if { ::FileTest.exist?("/var/lib/pgsql/data/PG_VERSION") }
end

cookbook_file "/var/lib/pgsql/data/pg_hba.conf" do
  source "pg_hba.conf"
end
cookbook_file "/var/lib/pgsql/data/postgresql.conf" do
  source "postgresql.conf"
end

service "postgresql" do
    action :stop
end

service "postgresql" do
	action [:enable, :start]
end

#
# WAKABA
#
execute "create-user" do
	user "postgres"
	command "createuser -a WAKABA"
	not_if "psql -U postgres -c \"select * from pg_user where usename='WAKABA'\" | grep -q WAKABA", :user => "postgres"
end

execute "set-password" do
	user "postgres"
	command "psql -U postgres -c \"alter user \\\"WAKABA\\\" WITH PASSWORD 'WAKABA'\""
end

execute "createdb" do
	user "postgres"
	command "createdb WAKABA"
	not_if "psql -U postgres -c \"select * from pg_database WHERE datname='WAKABA'\" | grep -q WAKABA", :user => "postgres"
end


#
# TEST
#
execute "create-user" do
  user "postgres"
  command "createuser -a TEST"
  not_if "psql -U postgres -c \"select * from pg_user where usename='TEST'\" | grep -q TEST", :user => "postgres"
end

execute "set-password" do
  user "postgres"
  command "psql -U postgres -c \"alter user \\\"TEST\\\" WITH PASSWORD 'TEST'\""
end

execute "createdb" do
  user "postgres"
  command "createdb TEST"
  not_if "psql -U postgres -c \"select * from pg_database WHERE datname='TEST'\" | grep -q TEST", :user => "postgres"
end
