#
# Cookbook Name:: dev_wakaba
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
fileName = "wakaba.war"

#cookbook_file "/tmp/#{fileName}" do
#	source "#{fileName}"
#end

#script "install_wakaba" do
#	interpreter "bash"
#	user "root"
#	code <<-EOL
#		mv /tmp/#{fileName} /usr/local/jetty/webapps/
#	EOL
#end

#script "install_wakaba" do
#	interpreter "bash"
#	user "root"
#	code <<-EOL
#		cd /usr/local/jetty
#		java -jar start.jar
#	EOL
#end

#script "install_wakaba2" do
#	interpreter "bash"
#	user "root"
#	code <<-EOL
#		service jetty restart
#		/etc/init.d/jetty restart
#	EOL
#end

service "jetty" do
	action [:enable, :start]
#	notifies :restart, "service[jetty]", :immediately
	supports :start => true, :restart => true
end

package "nginx" do
  action :install
end

service "nginx" do
  supports :status => true, :restart => true, :reload => true
  action [:enable, :start]
end
