#
# Cookbook Name:: dev_jetty
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
dirName = "jetty-distribution-7.6.13.v20130916"
fileName = "jetty-distribution-7.6.13.v20130916.tar.gz"

cookbook_file "/tmp/#{fileName}" do
	source "#{fileName}"
end

cookbook_file "/tmp/jetty" do
	source "jetty"
end

script "install_jetty0" do
	interpreter "bash"
	user "root"
	code <<-EOL
		mv /tmp/jetty /etc/init.d/jetty
		chmod 755 /etc/init.d/jetty
	EOL
end

#script "stop_jetty1" do
#	interpreter "bash"
#	user "root"
#	code <<-EOL
#		pkill -f 'java -jar start.jar'
#	EOL
#end

script "install_jetty1" do
	interpreter "bash"
	user "root"
	code <<-EOL
		rm -rf /usr/local/jetty
	EOL
end

script "install_jetty2" do
	interpreter "bash"
	user "root"
	code <<-EOL
		tar xvzf /tmp/#{fileName}
		rm /tmp/#{fileName}
		rm /usr/local/jetty
		mv #{dirName} /usr/local/jetty
	EOL
end


#service "jetty" do
#	action [ :enable, :start ]
#	#subscribes :restart, resources(:template => "/usr/local/jetty/webapps/wakaba.war")
#	supports :start => true, :restart => true
#end
