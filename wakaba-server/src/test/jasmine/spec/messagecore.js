describe("MessageCore", function() {
	var message;

	beforeEach(function() {
		message = new yamabuki.message.Message({
			urlRoot : "message",
		});
		
		yamabuki.message.Message.setMessage({
			code_notEmpty : "code_notEmpty",
			locale_notEmpty : "locale_notEmpty",
			locale_maxLength_2 : "locale_maxLength_2",
			message_notEmpty : "message_notEmpty"
		});
		/*
		player = new Player();
		song = new Song();
		*/
	});

	it("code is empty", function() {
		
		expect(message.get("id")).toEqual(null);
		var attr = {
			mode : "add",
			code : "",
			locale : "",
			message : "",
		};
		
		var errors = message.validate(attr);
		expect(errors["code"]).toEqual("code_notEmpty");
		
		/*
		player.play(song);
		expect(player.currentlyPlayingSong).toEqual(song);

		//demonstrates use of custom matcher
		expect(player).toBePlaying(song);
		*/
	});
	
/*
	describe("when song has been paused", function() {
		beforeEach(function() {
			player.play(song);
			player.pause();
		});

		it("should indicate that the song is currently paused", function() {
			expect(player.isPlaying).toBeFalsy();

			// demonstrates use of 'not' with a custom matcher
			expect(player).not.toBePlaying(song);
		});

		it("should be possible to resume", function() {
			player.resume();
			expect(player.isPlaying).toBeTruthy();
			expect(player.currentlyPlayingSong).toEqual(song);
		});
	});

	// demonstrates use of spies to intercept and test method calls
	it("tells the current song if the user has made it a favorite", function() {
		spyOn(song, 'persistFavoriteStatus');

		player.play(song);
		player.makeFavorite();

		expect(song.persistFavoriteStatus).toHaveBeenCalledWith(true);
	});

	//demonstrates use of expected exceptions
	describe("#resume", function() {
		it("should throw an exception if song is already playing", function() {
			player.play(song);

			expect(function() {
				player.resume();
			}).toThrow("song is already playing");
		});
	});
*/
});