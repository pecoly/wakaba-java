package net.jp.yamabuki.io;

import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.EmptyXmlValueException;
import net.jp.yamabuki.exception.XmlNodeNotFoundException;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.model.query.config.QueryConfigColumn;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.util.StringUtils;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class QueryConfigXmlReaderTest {
	public static class Base {
		QueryConfigXmlReader reader;

		XPathParser parser;

		DataSource createDataSource() {
			BasicDataSource dataSource = new BasicDataSource();
			dataSource.setUsername("root");
			dataSource.setPassword("root");
			dataSource.setUrl("jdbc:mysql://localhost/test");
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");

			return dataSource;
		}

		Configuration createConfiguration(DataSource dataSource) {
			TransactionFactory transactionFactory = new JdbcTransactionFactory();
			Environment environment = new Environment(
					"development", transactionFactory, dataSource);
			Configuration configuration = new Configuration(environment);
			return configuration;
		}

		public void initialize(String xml) {
			Configuration configuration = this.createConfiguration(this.createDataSource());
			InputStream inputStream = StringUtils.toByteArrayInputStream(xml);
			this.reader = new QueryConfigXmlReader(configuration, inputStream);
			this.parser = this.reader.createParser(inputStream);
		}
	}

	public static class Sql extends Base {
		String xml1
				= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<!DOCTYPE query>"
				+ "<query>";
		String xml2
				= "<connection>hoge</connection>"
				+ "<columns></columns>"
				+ "<parameters></parameters>"
				+ "<validator></validator>"
				+ "</query>";

		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void ok() {
			String xml
					= xml1
					+ "<sql>hoge</sql>"
					+ xml2;

			this.initialize(xml);
			XNode node = this.reader.readSql(this.parser);
			assertThat(node.getStringBody(), is("hoge"));
		}

		@Test
		public void ng_empty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/sql");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<sql></sql>"
					+ xml2;

			this.initialize(xml);
			this.reader.readSql(this.parser);
		}

		@Test
		public void ng_null() {
			thrown.expectMessage("10016 XML node not found. node : /query/sql");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ xml2;

			this.initialize(xml);
			this.reader.readSql(this.parser);
		}
	}

	public static class ColumnList extends Base {
		String xml1
				= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<!DOCTYPE query>"
				+ "<query>"
				+ "<sql></sql>"
				+ "<connection>hoge</connection>";

		String xml2
				= "<parameters></parameters>"
				+ "<validator></validator>"
				+ "</query>";

		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void ok_columnsIsEmpty() {
			String xml
					= xml1
					+ "<columns></columns>"
					+ xml2;

			this.initialize(xml);
			List<QueryConfigColumn> list = ImmutableList.copyOf(
					this.reader.readColumnList(this.parser));
			assertThat(list.isEmpty(), is(true));
		}

		@Test
		public void ok_columnsIsNull() {
			String xml
					= xml1
					+ xml2;

			this.initialize(xml);
			List<QueryConfigColumn> list = ImmutableList.copyOf(
					this.reader.readColumnList(this.parser));
			assertThat(list.isEmpty(), is(true));
		}

		@Test
		public void ok() {
			String xml
					= xml1
					+ "<columns>"
					+ "<column><name>a</name><accessor>b</accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			List<QueryConfigColumn> list = ImmutableList.copyOf(
					this.reader.readColumnList(this.parser));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("a"));
			assertThat(list.get(0).getAccessor(), is("b"));
			assertThat(list.get(1).getName(), is("c"));
			assertThat(list.get(1).getAccessor(), is("d"));
		}

		@Test
		public void ng_nameIsNull() {
			thrown.expectMessage("10016 XML node not found. node : /query/columns/column/name");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><accessor>b</accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_nameIsEmpty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/columns/column/name");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name></name><accessor>b</accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_accessorIsNull() {
			thrown.expectMessage("10016 XML node not found. node : /query/columns/column/accessor");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name>a</name></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_accessorIsEmpty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/columns/column/accessor");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name>a</name><accessor></accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}
	}

	public static class ParameterList extends Base {
		String xml1
				= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<!DOCTYPE query>"
				+ "<query>"
				+ "<sql></sql>"
				+ "<connection>hoge</connection>"
				+ "<columns></columns>";

		String xml2
				= "<validator></validator>"
				+ "</query>";

		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void ok_parametersIsEmpty() {
			String xml
					= xml1
					+ "<parameters></parameters>"
					+ xml2;

			this.initialize(xml);
			List<ParameterConfig> list = ImmutableList.copyOf(
					this.reader.readParameterList(this.parser));
			assertThat(list.isEmpty(), is(true));
		}

		@Test
		public void ok_parametersIsNull() {
			String xml
					= xml1
					+ xml2;

			this.initialize(xml);
			List<ParameterConfig> list = ImmutableList.copyOf(
					this.reader.readParameterList(this.parser));
			assertThat(list.isEmpty(), is(true));
		}

		@Test
		public void ok() {
			String xml
					= xml1
					+ "<parameters>"
					+ "<parameter>"
					+ "<name>a</name><converter>b</converter>"
					+ "<validators>"
					+ "<validator><name>x</name><hoge>S</hoge><fuga>T</fuga></validator>"
					+ "<validator><name>y</name></validator>"
					+ "</validators>"
					+ "</parameter>"
					+ "<parameter><name>c</name><converter>d</converter></parameter>"
					+ "</parameters>"
					+ xml2;

			this.initialize(xml);
			List<ParameterConfig> list = ImmutableList.copyOf(
					this.reader.readParameterList(this.parser));
			assertThat(list.size(), is(2));

			// list 0
			assertThat(list.get(0).getName(), is("a"));
			assertThat(list.get(0).getConverter(), is("b"));

			// validator 0
			List<ValidatorConfig> validatorList0 = ImmutableList.copyOf(
					list.get(0).getValidatorList());
			assertThat(validatorList0.size(), is(2));

			ValidatorConfig validator00 = validatorList0.get(0);
			Map<String, String> argList00 = toMap(validator00.getArgumentList());
			assertThat(argList00.size(), is(2));
			assertThat(argList00.get("hoge"), is("S"));
			assertThat(argList00.get("fuga"), is("T"));

			ValidatorConfig validator01 = validatorList0.get(1);
			Map<String, String> argList01 = toMap(validator01.getArgumentList());
			assertThat(argList01.size(), is(0));

			// list 1
			assertThat(list.get(1).getName(), is("c"));
			assertThat(list.get(1).getConverter(), is("d"));

			// validator 1
			List<ValidatorConfig> validatorList1 = ImmutableList.copyOf(
					list.get(1).getValidatorList());
			assertThat(validatorList1.size(), is(0));
		}

		Map<String, String> toMap(Iterable<Entry<String, String>> itr) {
			Map<String, String> map = new HashMap<>();
			for (Entry<String, String> x : itr) {
				map.put(x.getKey(), x.getValue());
			}

			return map;
		}

		@Test
		public void ng_nameIsNull() {
			thrown.expectMessage("10016 XML node not found. node : /query/columns/column/name");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><accessor>b</accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_nameIsEmpty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/columns/column/name");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name></name><accessor>b</accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_accessorIsNull() {
			thrown.expectMessage("10016 XML node not found. node : /query/columns/column/accessor");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name>a</name></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}

		@Test
		public void ng_accessorIsEmpty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/columns/column/accessor");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<columns>"
					+ "<column><name>a</name><accessor></accessor></column>"
					+ "<column><name>c</name><accessor>d</accessor></column>"
					+ "</columns>"
					+ xml2;

			this.initialize(xml);
			this.reader.readColumnList(this.parser);
		}
	}










	public static class JdbcConnection extends Base {
		String xml1
				= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<!DOCTYPE query>"
				+ "<query>"
				+ "<sql>"
				+ "SELECT COUNT(*)"
				+ "FROM QUERY_LIST T1"
				+ "WHERE QUERY_TYPE = '_listview'"
				+ "</sql>";
		String xml2
				= "<columns></columns>"
				+ "<parameters></parameters>"
				+ "<validator></validator>"
				+ "</query>";

		@Rule
		public ExpectedException thrown = ExpectedException.none();

		@Test
		public void ok_system() {
			String xml
					= xml1
					+ "<connection>*</connection>"
					+ xml2;

			this.initialize(xml);
			String jdbc = this.reader.readConnection(this.parser);
			assertThat(jdbc, is("*"));
		}

		@Test
		public void ok_user() {
			String xml
					= xml1
					+ "<connection>hoge</connection>"
					+ xml2;

			this.initialize(xml);
			String jdbc = this.reader.readConnection(this.parser);
			assertThat(jdbc, is("hoge"));
		}

		@Test
		public void ng_empty() {
			thrown.expectMessage("10017 XML value is empty. node : /query/connection");
			thrown.expect(EmptyXmlValueException.class);

			String xml
					= xml1
					+ "<connection></connection>"
					+ xml2;

			this.initialize(xml);
			this.reader.readConnection(this.parser);
		}

		@Test
		public void ng_null() {
			thrown.expectMessage("10016 XML node not found. node : /query/connection");
			thrown.expect(XmlNodeNotFoundException.class);

			String xml
					= xml1
					+ xml2;

			this.initialize(xml);
			this.reader.readConnection(this.parser);
		}
	}

}
