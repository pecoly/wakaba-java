package net.jp.yamabuki.controller.manager.message;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.result.SuccessResult;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.message.GetMessageByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.viewmodel.VmMessage;
import net.jp.yamabuki.viewmodel.VmMessageMatcher;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.util.ReflectionTestUtils;

public class MessageServiceControllerTest {
	static MessageBuilder builder1 = new MessageBuilder(
			"123", "suzuki", "jiro",
			"abc", "ja", "def", "20010203102030");

	static MessageBuilder builder2 = new MessageBuilder(
			"124", "suzuki", "jiro",
			"ghi", "ja", "jkl", "20010203102030");

	public static class Base {
		MessageServiceController ctr = new MessageServiceController();

		ServiceController mock = mock(ServiceController.class);

		MockHttpSession session = new MockHttpSession();

		MockHttpServletRequest request = new MockHttpServletRequest();
	}

	public static class Get extends Base {
		GetAppModelById<Message> getById = mock(GetAppModelById.class);

		VmMessage result = null;

		@Before
		public void before() {
			Message message = builder1.build();

			when(mock
					.get(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", new ModelIdImpl("123"),
							session, getById, "message"))
					.thenReturn(message);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getMessageById", getById);

			result = ctr.get("suzuki", "jiro", "abcd", "123", session);
		}

		@Test
		public void getCalled() {
			verify(mock).get(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", new ModelIdImpl("123"), session, getById, "message");
		}

		@Test
		public void result() {
			VmMessage expected = new VmMessage(
					"123", "suzuki", "jiro",
					"abc", "ja", "def", "20010203040506");
			assertThat(result, VmMessageMatcher.matches(expected));
		}
	}

	public static class GetList extends Base {
		GetAppModelList<Message> getList = mock(GetAppModelList.class);

		List<VmMessage> result = null;

		@Before
		public void before() {
			Message message1 = builder1.build();
			Message message2 = builder2.build();
			List<Message> messageList = Arrays.asList(new Message[] {
				message1, message2,
			});

			when(mock
					.getList(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", 3, request, session, getList, "message"))
					.thenReturn(messageList);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getMessageList", getList);

			// コントローラのgetList呼び出し
			result = ctr.getList("suzuki", "jiro", "abcd", 3, request, session);
		}

		@Test
		public void getListCalled() {
			// モックのgetListが呼び出されていること
			verify(mock).getList(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", 3, request, session, getList, "message");
		}

		@Test
		public void result() {
			// getListの戻り値にmessageListが設定されていること
			assertThat(result.size(), is(2));
			VmMessage expected1 = new VmMessage(
					"123", "suzuki", "jiro",
					"abc", "ja", "def", "20010203040506");
			VmMessage expected2 = new VmMessage(
					"124", "suzuki", "jiro",
					"ghi", "ja", "jkl", "20010203040506");

			assertThat(result.get(0), VmMessageMatcher.matches(expected1));
			assertThat(result.get(1), VmMessageMatcher.matches(expected2));
		}
	}

	public static class GetCount extends Base {
		GetAppModelCount getCount = mock(GetAppModelCount.class);

		Map<String, Object> result = null;

		@Before
		public void before() {
			Map<String, Object> map = new HashMap<>();
			map.put("count", 12L);
			when(mock
					.getCount(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", request, session, getCount, "message"))
					.thenReturn(map);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getMessageCount", getCount);

			// コントローラのgetCount呼び出し
			result = ctr.getCount("suzuki", "jiro", "abcd", request, session);
		}

		@Test
		public void getCountCalled() {
			// モックのgetCountが呼び出されていること
			verify(mock).getCount(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", request, session, getCount, "message");
		}

		@Test
		public void result() {
			// getCountの戻り値にmapが設定されていること
			assertThat(result, is(not(nullValue())));
			assertThat(result.size(), is(1));
			assertThat((Long)result.get("count"), is(12L));
		}
	}

	public static class Add extends Base {
		AddAppModel<Message> add = mock(AddAppModel.class);

		GetMessageByKey getByKey = mock(GetMessageByKey.class);

		VmMessage result;

		@Before
		public void before() {
			Map<String, String> map = new HashMap<>();
			map.put("code", "abc");
			map.put("localeId", "ja");
			map.put("message", "def");

			Message message = builder1.build();

			when(mock
					.add(
							(Message)any(), (String)any(), (HttpSession)any(),
							(AddAppModel)any(), (GetMessageByKey)any(), (String)any()))
					.thenReturn(message);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "addMessage", add);
			ReflectionTestUtils.setField(ctr, "getMessageByKey", getByKey);

			// コントローラのadd呼び出し
			result = ctr.add("suzuki", "jiro", "abcd",
					map, session);
		}

		@Test
		public void addCalled() {
			// モックのaddが呼び出されていること
			String id = null;
			Message message = new MessageBuilder(builder1).withId(id).build();

			verify(mock).add(message, "abcd", session,
					add, getByKey, "message");
		}

		@Test
		public void result() {
			// addの戻り値にmessageが設定されていること
			VmMessage expected = new VmMessage(
					"123", "suzuki", "jiro",
					"abc", "ja", "def", "20010203040506");
			assertThat(result, VmMessageMatcher.matches(expected));
		}
	}

	public static class Update extends Base {
		UpdateAppModel<Message> update = mock(UpdateAppModel.class);

		GetAppModelById<Message> getById = mock(GetAppModelById.class);

		VmMessage result;

		@Before
		public void before() {
			Map<String, String> map = new HashMap<>();
			map.put("code", "ABC");
			map.put("localeId", "JA");
			map.put("message", "DEF");
			map.put("lastModified", "20000101010101");

			// 更新後モデルを作成
			Message message = new MessageBuilder(builder1)
					.withCode("ABC")
					.withLocaleId("JA")
					.withMessage("DEF")
					.withLastModified("20000101010101")
					.build();

			when(mock
					.update(
							message, "abcd", session,
							update, getById, "message"))
					.thenReturn(message);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "updateMessage", update);
			ReflectionTestUtils.setField(ctr, "getMessageById", getById);

			// コントローラのupdate呼び出し
			result = ctr.update("suzuki", "jiro", "abcd",
					"123", map, session);
		}

		@Test
		public void updateCalled() {
			// モックのupdateが呼び出されること
			Message message = new MessageBuilder(builder1)
					.withCode("ABC")
					.withLocaleId("JA")
					.withMessage("DEF")
					.withLastModified("20000101010101")
					.build();

			verify(mock).update(message, "abcd", session,
					update, getById, "message");
		}

		@Test
		public void result() {
			// updateの戻り値に更新後のmessageが設定されていること
			VmMessage expected = new VmMessage(
					"123", "suzuki", "jiro",
					"ABC", "JA", "DEF", "20000101010101");
			assertThat(result, VmMessageMatcher.matches(expected));
		}
	}

	public static class Remove extends Base {
		RemoveAppModel<Message> remove = mock(RemoveAppModel.class);

		Result result = new SuccessResult();

		@Before
		public void before() {
			HttpServletRequest request = mock(HttpServletRequest.class);
			when(request
					.getHeader("lastModified"))
					.thenReturn("20001011202122");

			when(mock
					.remove(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							new ModelIdImpl("123"),
							DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
									"20001011202122"),
							"abcd", session, remove, "message"))
					.thenReturn(result);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "removeMessage", remove);

			// コントローラのremove呼び出し
			result = ctr.remove("suzuki", "jiro", "abcd",
					"123", request, session);
		}

		@Test
		public void removeCalled() {
			// モックのremoveが呼び出されること
			verify(mock).remove(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new ModelIdImpl("123"),
					DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
							"20001011202122"),
					"abcd", session, remove, "message");
		}

		@Test
		public void result() {
			// removeの戻り値が設定されていること
			assertThat(result instanceof SuccessResult, is(true));
		}
	}
}
