package net.jp.yamabuki.controller.manager.css;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.CssBuilder;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.result.SuccessResult;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.css.GetCssByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.viewmodel.VmCss;
import net.jp.yamabuki.viewmodel.VmCssMatcher;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.util.ReflectionTestUtils;

public class CssServiceControllerTest {
	static CssBuilder builder1 = new CssBuilder(
			"123", "suzuki", "jiro",
			"abc", "def", "20010203102030");

	static CssBuilder builder2 = new CssBuilder(
			"124", "suzuki", "jiro",
			"ghi", "jkl", "20010203102030");

	public static class Base {
		CssServiceController ctr = new CssServiceController();

		ServiceController mock = mock(ServiceController.class);

		MockHttpSession session = new MockHttpSession();

		MockHttpServletRequest request = new MockHttpServletRequest();
	}

	public static class Get extends Base {
		GetAppModelById<Css> getById = mock(GetAppModelById.class);

		VmCss result = null;

		@Before
		public void before() {
			Css css = builder1.build();

			when(mock
					.get(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", new ModelIdImpl("123"),
							session, getById, "css"))
					.thenReturn(css);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getCssById", getById);

			result = ctr.get("suzuki", "jiro", "abcd", "123", session);
		}

		@Test
		public void getCalled() {
			verify(mock).get(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", new ModelIdImpl("123"), session, getById, "css");
		}

		@Test
		public void result() {
			VmCss expected = new VmCss(
					"123", "suzuki", "jiro",
					"abc", "def", "20010203040506");
			assertThat(result, VmCssMatcher.matches(expected));
		}
	}

	public static class GetList extends Base {
		GetAppModelList<Css> getList = mock(GetAppModelList.class);

		List<VmCss> result = null;

		@Before
		public void before() {
			Css css1 = builder1.build();
			Css css2 = builder2.build();
			List<Css> cssList = Arrays.asList(new Css[] {
				css1, css2,
			});

			when(mock
					.getList(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", 3, request, session, getList, "css"))
					.thenReturn(cssList);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getCssList", getList);

			// コントローラのgetList呼び出し
			result = ctr.getList("suzuki", "jiro", "abcd", 3, request, session);
		}

		@Test
		public void getListCalled() {
			// モックのgetListが呼び出されていること
			verify(mock).getList(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", 3, request, session, getList, "css");
		}

		@Test
		public void result() {
			// getListの戻り値にcssListが設定されていること
			assertThat(result.size(), is(2));
			VmCss expected1 = new VmCss(
					"123", "suzuki", "jiro",
					"abc", "def", "20010203040506");
			VmCss expected2 = new VmCss(
					"124", "suzuki", "jiro",
					"ghi", "jkl", "20010203040506");

			assertThat(result.get(0), VmCssMatcher.matches(expected1));
			assertThat(result.get(1), VmCssMatcher.matches(expected2));
		}
	}

	public static class GetCount extends Base {
		GetAppModelCount getCount = mock(GetAppModelCount.class);

		Map<String, Object> result = null;

		@Before
		public void before() {
			Map<String, Object> map = new HashMap<>();
			map.put("count", 12L);
			when(mock
					.getCount(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							"abcd", request, session, getCount, "css"))
					.thenReturn(map);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "getCssCount", getCount);

			// コントローラのgetCount呼び出し
			result = ctr.getCount("suzuki", "jiro", "abcd", request, session);
		}

		@Test
		public void getCountCalled() {
			// モックのgetCountが呼び出されていること
			verify(mock).getCount(new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					"abcd", request, session, getCount, "css");
		}

		@Test
		public void result() {
			// getCountの戻り値にmapが設定されていること
			assertThat(result, is(not(nullValue())));
			assertThat(result.size(), is(1));
			assertThat((Long)result.get("count"), is(12L));
		}
	}

	public static class Add extends Base {
		AddAppModel<Css> add = mock(AddAppModel.class);

		GetCssByKey getByKey = mock(GetCssByKey.class);

		VmCss result;

		@Before
		public void before() {
			Map<String, String> map = new HashMap<>();
			map.put("name", "abc");
			map.put("content", "def");

			Css css = builder1.build();

			when(mock
					.add(
							(Css)any(), (String)any(), (HttpSession)any(),
							(AddAppModel)any(), (GetCssByKey)any(), (String)any()))
					.thenReturn(css);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "addCss", add);
			ReflectionTestUtils.setField(ctr, "getCssByKey", getByKey);

			// コントローラのadd呼び出し
			result = ctr.add("suzuki", "jiro", "abcd",
					map, session);
		}

		@Test
		public void addCalled() {
			// モックのaddが呼び出されていること
			String id = null;
			Css css = new CssBuilder(builder1).withId(id).build();

			verify(mock).add(css, "abcd", session,
					add, getByKey, "css");
		}

		@Test
		public void result() {
			// addの戻り値にcssが設定されていること
			VmCss expected = new VmCss(
					"123", "suzuki", "jiro",
					"abc", "def", "20010203040506");
			assertThat(result, VmCssMatcher.matches(expected));
		}
	}

	public static class Update extends Base {
		UpdateAppModel<Css> update = mock(UpdateAppModel.class);

		GetAppModelById<Css> getById = mock(GetAppModelById.class);

		VmCss result;

		@Before
		public void before() {
			Map<String, String> map = new HashMap<>();
			map.put("name", "ABC");
			map.put("content", "DEF");
			map.put("lastModified", "20000101010101");

			// 更新後モデルを作成
			Css css = new CssBuilder(builder1)
					.withName("ABC")
					.withContent("DEF")
					.withLastModified("20000101010101")
					.build();

			when(mock
					.update(
							css, "abcd", session,
							update, getById, "css"))
					.thenReturn(css);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "updateCss", update);
			ReflectionTestUtils.setField(ctr, "getCssById", getById);

			// コントローラのupdate呼び出し
			result = ctr.update("suzuki", "jiro", "abcd",
					"123", map, session);
		}

		@Test
		public void updateCalled() {
			// モックのupdateが呼び出されること
			Css css = new CssBuilder(builder1)
					.withName("ABC")
					.withContent("DEF")
					.withLastModified("20000101010101")
					.build();

			verify(mock).update(css, "abcd", session,
					update, getById, "css");
		}

		@Test
		public void result() {
			// updateの戻り値に更新後のcssが設定されていること
			VmCss expected = new VmCss(
					"123", "suzuki", "jiro",
					"ABC", "DEF", "20000101010101");
			assertThat(result, VmCssMatcher.matches(expected));
		}
	}

	public static class Remove extends Base {
		RemoveAppModel<Css> remove = mock(RemoveAppModel.class);

		Result result = new SuccessResult();

		@Before
		public void before() {
			HttpServletRequest request = mock(HttpServletRequest.class);
			when(request
					.getHeader("lastModified"))
					.thenReturn("20001011202122");

			when(mock
					.remove(
							new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
							new ModelIdImpl("123"),
							DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
									"20001011202122"),
							"abcd", session, remove, "css"))
					.thenReturn(result);

			ReflectionTestUtils.setField(ctr, "controller", mock);
			ReflectionTestUtils.setField(ctr, "removeCss", remove);

			// コントローラのremove呼び出し
			result = ctr.remove("suzuki", "jiro", "abcd",
					"123", request, session);
		}

		@Test
		public void removeCalled() {
			// モックのremoveが呼び出されること
			verify(mock).remove(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new ModelIdImpl("123"),
					DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
							"20001011202122"),
					"abcd", session, remove, "css");
		}

		@Test
		public void result() {
			// removeの戻り値が設定されていること
			assertThat(result instanceof SuccessResult, is(true));
		}
	}
}
