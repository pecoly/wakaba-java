package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class VmMessageMatcher extends TypeSafeMatcher<VmMessage> {
	private final VmMessage expected;

	public VmMessageMatcher(VmMessage expected) {
		this.expected = expected;
		Argument.isNotNull(expected, "expected");
	}

	public static VmMessageMatcher matches(VmMessage message) {
		return new VmMessageMatcher(message);
	}

	@Override
	protected boolean matchesSafely(VmMessage actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof VmMessage)) {
			return false;
		}

		VmMessage obj = (VmMessage)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			return false;
		}

		// Code
		if (!this.expected.getCode().equals(obj.getCode())) {
			return false;
		}

		// Locale
		if (!this.expected.getLocaleId().equals(obj.getLocaleId())) {
			return false;
		}

		// Message
		if (!this.expected.getMessage().equals(obj.getMessage())) {
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
