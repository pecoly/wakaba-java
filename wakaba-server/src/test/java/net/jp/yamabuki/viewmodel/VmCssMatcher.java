package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class VmCssMatcher extends TypeSafeMatcher<VmCss> {
	private final VmCss expected;

	public VmCssMatcher(VmCss expected) {
		this.expected = expected;
		Argument.isNotNull(expected, "expected");
	}

	public static VmCssMatcher matches(VmCss css) {
		return new VmCssMatcher(css);
	}

	@Override
	protected boolean matchesSafely(VmCss actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof VmCss)) {
			return false;
		}

		VmCss obj = (VmCss)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			return false;
		}

		// Content
		if (!this.expected.getContent().equals(obj.getContent())) {
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
