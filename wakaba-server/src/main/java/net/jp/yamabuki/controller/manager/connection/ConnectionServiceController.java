package net.jp.yamabuki.controller.manager.connection;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.result.SuccessResult;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.jdbcconnection.GetJdbcConnectionByKey;
import net.jp.yamabuki.service.manager.jdbcconnection.sql.TestJdbcConnection;
import net.jp.yamabuki.util.AuthUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.StringUtils;
import net.jp.yamabuki.viewmodel.VmJdbcConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * コネクションサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/connection")
public final class ConnectionServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "connection";

	private Function<JdbcConnection, VmJdbcConnection> toViewModel
			= new Function<JdbcConnection, VmJdbcConnection>() {
				@Override
				public VmJdbcConnection apply(JdbcConnection in) {
					return new VmJdbcConnection(in);
				}
			};

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getJdbcConnectionById")
	private GetAppModelById<JdbcConnection> getJdbcConnectionById;

	@Autowired
	private GetJdbcConnectionByKey getJdbcConnectionByKey;

	@Autowired
	@Qualifier("getJdbcConnectionList")
	private GetAppModelList<JdbcConnection> getJdbcConnectionList;

	@Autowired
	@Qualifier("getJdbcConnectionCount")
	private GetAppModelCount getJdbcConnectionCount;

	@Autowired
	@Qualifier("addJdbcConnection")
	private AddAppModel<JdbcConnection> addJdbcConnection;

	@Autowired
	@Qualifier("updateJdbcConnection")
	private UpdateAppModel<JdbcConnection> updateJdbcConnection;

	@Autowired
	@Qualifier("removeJdbcConnection")
	private RemoveAppModel<JdbcConnection> removeJdbcConnection;

	@Autowired
	private TestJdbcConnection testJdbcConnection;

	@Autowired
	private SuccessResult successResult;

	/**
	 * コネクションを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id コネクションId
	 * @param session セッション
	 * @return コネクション
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmJdbcConnection get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getJdbcConnectionById, TYPE_NAME));
	}

	/**
	 * コネクションの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return コネクションの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmJdbcConnection> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<JdbcConnection> list
				= this.controller.getList(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, pageNumber, request,
						session, this.getJdbcConnectionList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * コネクションの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return コネクションの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getJdbcConnectionCount, TYPE_NAME);
	}

	/**
	 * コネクションを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したコネクション
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmJdbcConnection add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(JdbcConnection.KEY_NAME);
		String driver = map.get(JdbcConnection.KEY_DRIVER);
		String url = map.get(JdbcConnection.KEY_URL);
		String username = map.get(JdbcConnection.KEY_USERNAME);
		String password = map.get(JdbcConnection.KEY_PASSWORD);

		Argument.isNotNull(name, JdbcConnection.KEY_NAME);
		Argument.isNotNull(driver, JdbcConnection.KEY_DRIVER);
		Argument.isNotNull(url, JdbcConnection.KEY_URL);
		Argument.isNotNull(username, JdbcConnection.KEY_USERNAME);
		Argument.isNotNull(password, JdbcConnection.KEY_PASSWORD);

		JdbcConnection con = new JdbcConnection(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				driver,
				url,
				username,
				password);

		return this.toViewModel.apply(
				this.controller.add(
						con, csrfToken, session,
						this.addJdbcConnection, this.getJdbcConnectionByKey, TYPE_NAME));
	}

	/**
	 * コネクションを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id コネクションId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のコネクション
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmJdbcConnection update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(JdbcConnection.KEY_NAME);
		String driver = map.get(JdbcConnection.KEY_DRIVER);
		String url = map.get(JdbcConnection.KEY_URL);
		String username = map.get(JdbcConnection.KEY_USERNAME);
		String password = map.get(JdbcConnection.KEY_PASSWORD);
		String lastModified = map.get(JdbcConnection.KEY_LAST_MODIFIED);

		Argument.isNotNull(name, JdbcConnection.KEY_NAME);
		Argument.isNotNull(driver, JdbcConnection.KEY_DRIVER);
		Argument.isNotNull(url, JdbcConnection.KEY_URL);
		Argument.isNotNull(username, JdbcConnection.KEY_USERNAME);
		Argument.isNotNull(password, JdbcConnection.KEY_PASSWORD);
		Argument.isNotNull(lastModified, JdbcConnection.KEY_LAST_MODIFIED);

		JdbcConnection con1 = this.getJdbcConnectionById.execute(
				new UserIdImpl(userId), new AppIdImpl(appId), new ModelIdImpl(id));
		if (StringUtils.isBlank(password)) {
			password = con1.getPassword();
		}

		JdbcConnection con2 = new JdbcConnection(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				driver,
				url,
				username,
				password,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert con2.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						con2, csrfToken, session,
						this.updateJdbcConnection, this.getJdbcConnectionById, TYPE_NAME));
	}

	/**
	 * コネクションを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id コネクションId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeJdbcConnection, TYPE_NAME);
	}

	/**
	 * コネクションの接続をテストします。
	 * 失敗時は例外を投げます。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "test", method = RequestMethod.POST)
	public Result test(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String driver = map.get(JdbcConnection.KEY_DRIVER);
		String url = map.get(JdbcConnection.KEY_URL);
		String username = map.get(JdbcConnection.KEY_USERNAME);
		String password = map.get(JdbcConnection.KEY_PASSWORD);

		Argument.isNotNull(driver, JdbcConnection.KEY_DRIVER);
		Argument.isNotNull(url, JdbcConnection.KEY_URL);
		Argument.isNotNull(username, JdbcConnection.KEY_USERNAME);
		Argument.isNotNull(password, JdbcConnection.KEY_PASSWORD);

		AuthUtils.checkCsrfToken(session, csrfToken);

		JdbcConnection con = new JdbcConnection(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				"testconnection",
				driver,
				url,
				username,
				password);

		try {
			this.testJdbcConnection.execute(con);
		}
		catch (CannotGetJdbcConnectionException ex) {
			Throwable t = ex.getCause();
			if (t != null) {
				throw new WakabaException(WakabaErrorCode.CONNECTION_ERROR, t.getMessage());
			}
		}

		return this.successResult;
	}
}
