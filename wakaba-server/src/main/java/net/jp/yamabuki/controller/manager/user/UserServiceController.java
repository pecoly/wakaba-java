package net.jp.yamabuki.controller.manager.user;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.appuser.GetAppUserByKey;
import net.jp.yamabuki.util.CollectionUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.StringUtils;
import net.jp.yamabuki.util.Transformer;
import net.jp.yamabuki.viewmodel.VmAppUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * ユーザーサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/user")
public final class UserServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "user";

	private Function<AppUser, VmAppUser> toViewModel
			= new Function<AppUser, VmAppUser>() {
				@Override
				public VmAppUser apply(AppUser in) {
					return new VmAppUser(in);
				}
			};

	@Autowired
	private ServiceController controller;

	@Autowired
	private GetAppUserByKey getAppUserByKey;

	@Autowired
	@Qualifier("getAppUserById")
	private GetAppModelById<AppUser> getAppUserById;

	@Autowired
	@Qualifier("getAppUserList")
	private GetAppModelList<AppUser> getAppUserList;

	@Autowired
	@Qualifier("getAppUserCount")
	private GetAppModelCount getAppUserCount;

	@Autowired
	@Qualifier("addAppUser")
	private AddAppModel<AppUser> addAppUser;

	@Autowired
	@Qualifier("updateAppUser")
	private UpdateAppModel<AppUser> updateAppUser;

	@Autowired
	private RemoveAppModel<AppUser> removeAppUser;

	/**
	 * アプリケーションユーザーを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションユーザーId
	 * @param session セッション
	 * @return アプリケーションユーザー
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmAppUser get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getAppUserById, TYPE_NAME));
	}

	/**
	 * アプリケーションユーザーの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return アプリケーションユーザーの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmAppUser> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<AppUser> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId), csrfToken,
				pageNumber, request, session, this.getAppUserList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * アプリケーションユーザーの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return アプリケーションユーザーの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getAppUserCount, TYPE_NAME);
	}

	/**
	 * アプリケーションユーザーを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したアプリケーションユーザー
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmAppUser add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		AppUser appUser = new AppUser(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				map.get("loginId"),
				map.get("loginPassword"),
				map.get("mainAuthority"),
				"");

		return this.toViewModel.apply(
				this.controller.add(
						appUser, csrfToken, session,
						this.addAppUser, this.getAppUserByKey, TYPE_NAME));
	}

	/**
	 * アプリケーションユーザーを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションユーザーId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のアプリケーションユーザー
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmAppUser update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		AppUser appUser1 = this.getAppUserById.execute(
				new UserIdImpl(userId), new AppIdImpl(appId), new ModelIdImpl(id));
		if (appUser1 == null) {
			throw new ResourceNotFoundException("");
		}

		String password = map.get("loginPassword");
		if (StringUtils.isBlank(password)) {
			password = appUser1.getLoginPassword();
		}

		AppUser appUser2 = new AppUser(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				map.get("loginId"),
				password,
				map.get("mainAuthority"),
				"",
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						map.get("lastModified")));
		//assert appUser2.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						appUser2, csrfToken, session,
						this.updateAppUser, this.getAppUserById, TYPE_NAME));
	}

	/**
	 * アプリケーションユーザーを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションユーザーId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeAppUser, TYPE_NAME);
	}
}
