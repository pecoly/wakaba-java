package net.jp.yamabuki.controller.manager.js;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.js.GetJsByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmJs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Jsサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/js")
public final class JsServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "js";

	private Function<Js, VmJs> toViewModel
			= new Function<Js, VmJs>() {
				@Override
				public VmJs apply(Js in) {
					return new VmJs(in);
				}
			};

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getJsById")
	private GetAppModelById<Js> getJsById;

	@Autowired
	private GetJsByKey getJsByKey;

	@Autowired
	@Qualifier("getJsList")
	private GetAppModelList<Js> getJsList;

	@Autowired
	@Qualifier("getJsCount")
	private GetAppModelCount getJsCount;

	@Autowired
	@Qualifier("addJs")
	private AddAppModel<Js> addJs;

	@Autowired
	@Qualifier("updateJs")
	private UpdateAppModel<Js> updateJs;

	@Autowired
	@Qualifier("removeJs")
	private RemoveAppModel<Js> removeJs;

	/**
	 * Jsを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id JsId
	 * @param session セッション
	 * @return Js
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmJs get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, new ModelIdImpl(id), session,
				this.getJsById, TYPE_NAME));
	}

	/**
	 * Jsの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return Jsの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmJs> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<Js> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, pageNumber, request,
				session, this.getJsList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * Jsの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return Jsの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getJsCount, TYPE_NAME);
	}

	/**
	 * Jsを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したJs
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmJs add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(Js.KEY_NAME);
		String content = map.get(Js.KEY_CONTENT);

		Argument.isNotNull(name, Js.KEY_NAME);
		Argument.isNotNull(content, Js.KEY_CONTENT);

		Js css = new Js(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content);

		return this.toViewModel.apply(
				this.controller.add(
						css, csrfToken, session,
						this.addJs, this.getJsByKey, TYPE_NAME));
	}

	/**
	 * Jsを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id JsId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のJs
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmJs update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(Css.KEY_NAME);
		String content = map.get(Css.KEY_CONTENT);
		String lastModified = map.get(Css.KEY_LAST_MODIFIED);

		Argument.isNotNull(name, Css.KEY_NAME);
		Argument.isNotNull(content, Css.KEY_CONTENT);
		Argument.isNotNull(lastModified, Css.KEY_LAST_MODIFIED);

		Js css = new Js(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert css.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						css, csrfToken, session,
						this.updateJs, this.getJsById, TYPE_NAME));
	}

	/**
	 * Jsを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id JsId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeJs, TYPE_NAME);
	}
}
