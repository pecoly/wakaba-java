package net.jp.yamabuki.controller.top;

import javax.servlet.http.HttpServletRequest;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.resource.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping(value = "/{userId}/{appId}")
public class LoginController {

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String show(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpServletRequest request) {
		model.addAttribute("name", "xyz");
		//return "a";
		model.addAttribute("USER_ID", userId);
		model.addAttribute("APP_ID", appId);

		model.addAttribute("MESSAGE", this.messageService.getMessageQuery(
				new UserIdImpl(userId), new AppIdImpl(appId), request));

//		return userId + "/" + appId + "/login";
		return "login";
	}

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			SessionStatus sessionStatus) {
		System.out.println("logout");

		// ここで全ての情報を削除
		//sessionStatus.setComplete();

		return "redirect:/j_spring_security_logout?userId="
				+ userId
				+ "&appId="
				+ appId;
	}
	/*
	public String show(
			@PathVariable("appId") String appId) {
		return appId + "/login";
	}
	*/


	/*
	@RequestMapping(value = "{error}", method = RequestMethod.GET)
	public String displayLoginform(Model model, RedirectAttributes redirectAttributes,
			@PathVariable String error) {
		if (error.equals("error01")) {
			redirectAttributes.addFlashAttribute("error_message", "ログインIDまたはパスワードが正しくありません。");
		}

		return "redirect:/";
	}
	*/

//	@Autowired
//	private Hoge hoge;
}
