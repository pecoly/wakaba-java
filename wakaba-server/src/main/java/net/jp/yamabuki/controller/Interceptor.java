package net.jp.yamabuki.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.GetUserModelList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class Interceptor implements HandlerInterceptor {

	@Autowired
	@Qualifier("getAppList")
	private GetUserModelList<App> getAppList;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub

		String[] pathList = request.getServletPath().split("/");
		String userId = "";
		String appId = "";
		String main = "";
		if (pathList.length >= 4) {
			userId = pathList[1];
			appId = pathList[2];
			main = pathList[3];
		}

		String type = "";
		String content = "";
		String process = "";
		if (pathList.length >= 6) {
			type = pathList[4];
			content = pathList[5];
		}

		if (pathList.length >= 7) {
			process = pathList[6];
		}

		if (userId.equals("resources")) {
			return true;
		}

		System.out.println("pre handle interceptor " + userId + " " + appId + " " + main
				+ " " + type + " " + content + " " + process);

		if (type.equals("service")) {
			return true;
		}

		if (appId.equals("manager")) {
			if (type.equals("view")) {
				if (!content.equals("app") && !content.equals("")) {
					System.out.println("red1 " + content);
					response.sendRedirect(request.getContextPath() + "/" + userId + "/manager/manager/view");
					return false;
				}
				else if (content.equals("")) {
					return true;
				}

				return true;
			}
		}

		if (!userId.equals("")) {
			List<App> appList = this.getAppList.execute(new UserIdImpl(userId),
					new PageRequest(0, 1000), new HashMap<String, String>());
			if (appList.isEmpty()) {
				System.out.println("red3 " + content);
				response.sendRedirect(request.getContextPath() + "/" + userId + "/manager/manager/view/app/add");
				return false;
			}
		}

		//return false;
		return true;
	}

	boolean containsByAppId(List<App> appList, String appId) {
		try {
			AppId a = new AppIdImpl(appId);

			for (App x : appList) {
				if (x.getAppId().equals(a)) {
					return true;
				}
			}

			return false;
		}
		catch (Exception ex) {
			return false;
		}
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		/*
		System.out.println("post handle interceptor");

		String[] pathList = request.getServletPath().split("/");
		String userId = "";
		String appId = "";
		String main = "";
		if (pathList.length >= 4) {
			userId = pathList[1];
			appId = pathList[2];
			main = pathList[3];
		}

		System.out.println(userId + " " + appId + " " + main);

		if (modelAndView == null) {
			System.out.println("mav is null.");
			return;
		}

		System.out.println(modelAndView.getViewName());


		List<App> appList = this.getAppList.execute(new UserIdImpl(userId),
				new PageRequest(0, 1000), new HashMap<String, String>());
		if (appList.isEmpty()) {
			// アプリケーションが0の場合は、アプリケーション作成用の画面を表示する
			//modelAndView.setViewName("redirect:/" + userId + "/_/manager/view/app/add");
			//modelAndView.setViewName("redirect:/" + userId + "/manager/app/add");
		}
		*/
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}



}
