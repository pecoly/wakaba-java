package net.jp.yamabuki.controller.manager.viewtemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ManagerAppId;
import net.jp.yamabuki.model.ManagerUserId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.viewtemplate.GetViewTemplateByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmViewTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ビューテンプレートサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/viewtemplate")
public final class ViewTemplateServiceController extends BaseServiceController {
	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getViewTemplateById")
	private GetAppModelById<ViewTemplate> getViewTemplateById;

	@Autowired
	private GetViewTemplateByKey getViewTemplateByKey;

	@Autowired
	@Qualifier("getViewTemplateList")
	private GetAppModelList<ViewTemplate> getViewTemplateList;

	@Autowired
	@Qualifier("getViewTemplateCount")
	private GetAppModelCount getViewTemplateCount;

	@Autowired
	@Qualifier("addViewTemplate")
	private AddAppModel<ViewTemplate> addViewTemplate;

	@Autowired
	@Qualifier("updateViewTemplate")
	private UpdateAppModel<ViewTemplate> updateViewTemplate;

	@Autowired
	@Qualifier("removeViewTemplate")
	private RemoveAppModel<ViewTemplate> removeViewTemplate;

	private static final String TYPE_NAME = "viewTemplate";

	/**
	 * メッセージを取得します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param name メッセージ名
	 * @param session セッション
	 * @return メッセージ
	 */
	@ResponseBody
	@RequestMapping(value = "{name}", method = RequestMethod.GET)
	public VmViewTemplate get1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("name") String name,
			HttpSession session) {
		ViewTemplate viewTemplate = new ViewTemplate(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(),
				name,
				"dummy");

		return new VmViewTemplate(
				this.controller.getByKey(viewTemplate, csrfToken, session,
				this.getViewTemplateByKey, TYPE_NAME));
	}

	@ResponseBody
	@RequestMapping(value = "{name1}/{name2}", method = RequestMethod.GET)
	public VmViewTemplate get2(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("name1") String name1,
			@PathVariable("name2") String name2,
			HttpSession session) {
		ViewTemplate viewTemplate = new ViewTemplate(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(),
				name1 + "/" + name2,
				"dummy");

		return new VmViewTemplate(
				this.controller.getByKey(viewTemplate, csrfToken, session,
				this.getViewTemplateByKey, TYPE_NAME));
	}

	/**
	 * メッセージの一覧を取得します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmViewTemplate> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {

		List<ViewTemplate> list = this.controller.getList(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(),
				csrfToken, pageNumber, request,
				session, this.getViewTemplateList, TYPE_NAME);

		List<VmViewTemplate> result = new ArrayList<>(list.size());
		for (ViewTemplate x : list) {
			result.add(new VmViewTemplate(x));
		}

		return result;
	}

	/**
	 * メッセージの件数を取得します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getViewTemplateCount, TYPE_NAME);
	}

	/**
	 * メッセージを追加します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したメッセージ
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmViewTemplate add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		ViewTemplate viewTemplate = new ViewTemplate(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				map.get("name"),
				map.get("content"));

		return new VmViewTemplate(
				this.controller.add(
						viewTemplate, csrfToken, session,
						this.addViewTemplate, this.getViewTemplateByKey, TYPE_NAME));
	}

	/**
	 * メッセージを更新します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param id メッセージID
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のメッセージ
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmViewTemplate update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		ViewTemplate viewTemplate = new ViewTemplate(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				map.get("name"),
				map.get("content"),
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						map.get(ViewTemplate.KEY_LAST_MODIFIED)));
		assert viewTemplate.getId().getValue().equals(id);

		return new VmViewTemplate(
				this.controller.update(
						viewTemplate, csrfToken, session,
						this.updateViewTemplate, this.getViewTemplateById, TYPE_NAME));
	}

	/**
	 * ビューテンプレートを削除します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param csrfToken CSRFトークン
	 * @param id ビューテンプレートID
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{name}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeViewTemplate, TYPE_NAME);
	}
}
