package net.jp.yamabuki.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/")
public class ViewController {

	@RequestMapping(method = RequestMethod.GET)
	public String show() {
		return "redirect:admin/manager/manager/view/app/list";
	}
}
