package net.jp.yamabuki.controller.manager.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.view.GetViewByKey;
import net.jp.yamabuki.util.CollectionUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * ビューサービスクラス。
 *
 */
@Controller("managerViewServiceController")
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/view")
public final class ViewServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "view";

	private Function<View, VmView> toViewModel
			= new Function<View, VmView>() {
				@Override
				public VmView apply(View in) {
					return new VmView(in);
				}
			};
	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getViewById")
	private GetAppModelById<View>getViewById;

	@Autowired
	private GetViewByKey getViewByKey;

	@Autowired
	@Qualifier("getViewList")
	private GetAppModelList<View> getViewList;

	@Autowired
	@Qualifier("getViewCount")
	private GetAppModelCount getViewCount;

	@Autowired
	@Qualifier("addView")
	private AddAppModel<View> addView;

	@Autowired
	@Qualifier("updateView")
	private UpdateAppModel<View> updateView;

	@Autowired
	@Qualifier("removeView")
	private RemoveAppModel<View> removeView;

	/**
	 * ビューを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id ビューId
	 * @param session セッション
	 * @return ビュー
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmView get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getViewById, TYPE_NAME));
	}

	/**
	 * ビューの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmView> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<View> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, pageNumber, request,
				session, this.getViewList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * ビューの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return ビューの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getViewCount, TYPE_NAME);
	}

	/**
	 * ビューを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したビュー
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmView add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(View.KEY_NAME);
		String content = map.get(View.KEY_CONTENT);

		Argument.isNotNull(name, View.KEY_NAME);
		Argument.isNotNull(content, View.KEY_CONTENT);

		View view = new View(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content);

		return this.toViewModel.apply(
				this.controller.add(
						view, csrfToken, session,
						this.addView, this.getViewByKey, TYPE_NAME));
	}

	/**
	 * ビューを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id ビューId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のビュー
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmView update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(View.KEY_NAME);
		String content = map.get(View.KEY_CONTENT);
		String lastModified = map.get(View.KEY_LAST_MODIFIED);

		Argument.isNotNull(name, View.KEY_NAME);
		Argument.isNotNull(content, View.KEY_CONTENT);
		Argument.isNotNull(lastModified, View.KEY_LAST_MODIFIED);

		View view = new View(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert view.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						view, csrfToken, session,
						this.updateView, this.getViewById, TYPE_NAME));
	}

	/**
	 * ビューを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id ビューId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeView, TYPE_NAME);
	}
}
