package net.jp.yamabuki.controller.manager.plugin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.manager.BaseManagerViewController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * プラグインビュークラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/view/plugin")
public class PluginViewController extends BaseManagerViewController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@RequestMapping(value = "{pluginName}/{viewName}", method = RequestMethod.GET)
	public String name(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("pluginName") String pluginName,
			@PathVariable("viewName") String viewName,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("list");

		String path = "manager/view/plugin/" + pluginName;
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}

	@RequestMapping(value = "execute/{pluginName}", method = RequestMethod.GET)
	public String execute0(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("pluginName") String pluginName,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("execute0");

		String path = "manager/view/plugin/execute";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		model.addAttribute("PLUGIN_NAME", pluginName);
		model.addAttribute("VIEW_PATH", "view");

		return path;
	}

	@RequestMapping(value = "execute/{pluginName}/{viewName1}", method = RequestMethod.GET)
	public String execute1(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("pluginName") String pluginName,
			@PathVariable("viewName1") String viewName1,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("execute1");

		String path = "manager/view/plugin/execute";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		model.addAttribute("PLUGIN_NAME", pluginName);
		model.addAttribute("VIEW_PATH", viewName1 + "/view");

		return path;
	}

	@RequestMapping(value = "execute/{pluginName}/{viewName1}/{viewName2}",
			method = RequestMethod.GET)
	public String execute2(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("pluginName") String pluginName,
			@PathVariable("viewName1") String viewName1,
			@PathVariable("viewName2") String viewName2,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("execute2");

		String path = "manager/view/plugin/execute";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		model.addAttribute("PLUGIN_NAME", pluginName);
		model.addAttribute("VIEW_PATH", viewName1 + "/" + viewName2 + "/view");

		return path;
	}

	/**
	 * 一覧画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 一覧画面のパス
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String list(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("list");

		String path = "manager/view/plugin/list";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}

	/**
	 * 追加画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 追加画面のパス
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("add");

		String path = "manager/view/plugin/add";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}

	/**
	 * 更新画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id プラグインId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 更新画面のパス
	 */
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public String update(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@RequestParam("id") String id,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("update");

		String path = "manager/view/plugin/update";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}
}
