package net.jp.yamabuki.controller.manager.update;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.update.GetUpdateByKey;
import net.jp.yamabuki.util.CollectionUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.Transformer;
import net.jp.yamabuki.viewmodel.VmUpdate;

import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * アップデートサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/update")
public final class UpdateServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "update";

	private Function<Update, VmUpdate> toViewModel;

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getUpdateById")
	private GetAppModelById<Update> getUpdateById;

	@Autowired
	private GetUpdateByKey getUpdateByKey;

	@Autowired
	@Qualifier("getUpdateList")
	private GetAppModelList<Update> getUpdateList;

	@Autowired
	@Qualifier("getUpdateCount")
	private GetAppModelCount getUpdateCount;

	@Autowired
	@Qualifier("addUpdate")
	private AddAppModel<Update> addUpdate;

	@Autowired
	@Qualifier("updateUpdate")
	private UpdateAppModel<Update> updateUpdate;

	@Autowired
	@Qualifier("removeUpdate")
	private RemoveAppModel<Update> removeUpdate;

	@Autowired
	private YamabukiContext yamabukiContext;

	/**
	 * クエリの変換クラス。
	 *
	 */
	private static class FunctionEx implements Function<Update, VmUpdate> {
		private Configuration configuration;

		/**
		 * コンストラクタ。
		 * @param configuration mybatisに使用する設定
		 */
		public FunctionEx(Configuration configuration) {
			this.configuration = configuration;
		}

		@Override
		public VmUpdate apply(Update in) {
			return new VmUpdate(in, this.configuration);
		}
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.toViewModel = new FunctionEx(this.yamabukiContext.getConfiguration());
	}

	/**
	 * アップデートを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アップデートId
	 * @param session セッション
	 * @return アップデート
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmUpdate get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getUpdateById, TYPE_NAME));
	}

	/**
	 * アップデートの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return アップデートの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmUpdate> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<Update> list
				= this.controller.getList(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, pageNumber, request,
						session, this.getUpdateList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * アップデートの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return アップデートの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getUpdateCount, TYPE_NAME);
	}

	/**
	 * アップデートを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param vmUpdate アップデート
	 * @param session セッション
	 * @return 追加したアップデート
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmUpdate add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody VmUpdate vmUpdate,
			HttpSession session) {

		String type = vmUpdate.getType();
		String name = vmUpdate.getName();
		String xml = vmUpdate.toXml();

		Argument.isNotNull(type, "type");
		Argument.isNotNull(name, "name");
		Argument.isNotNull(xml, "xml");

		Update update = new Update(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				type,
				name,
				xml);

		return this.toViewModel.apply(
				this.controller.add(
						update, csrfToken, session,
						this.addUpdate, this.getUpdateByKey, TYPE_NAME));
	}

	/**
	 * アップデートを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アップデートId
	 * @param vmUpdate アップデート
	 * @param session セッション
	 * @return 更新後のアップデート
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmUpdate update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody VmUpdate vmUpdate,
			HttpSession session) {

		String type = vmUpdate.getType();
		String name = vmUpdate.getName();
		String xml = vmUpdate.toXml();
		String lastModified = vmUpdate.getLastModified();

		Argument.isNotNull(type, "type");
		Argument.isNotNull(name, "name");
		Argument.isNotNull(xml, "xml");
		Argument.isNotNull(lastModified, "lastModified");

		Update update2 = new Update(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				type,
				name,
				xml,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert update2.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						update2, csrfToken, session,
						this.updateUpdate, this.getUpdateById, TYPE_NAME));
	}

	/**
	 * アップデートを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アップデートId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeUpdate, TYPE_NAME);
	}
}
