package net.jp.yamabuki.controller.manager.message;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.message.GetMessageByKey;
import net.jp.yamabuki.util.CollectionUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * メッセージサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/message")
public final class MessageServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "message";

	private Function<Message, VmMessage> toViewModel
			= new Function<Message, VmMessage>() {
				@Override
				public VmMessage apply(Message in) {
					return new VmMessage(in);
				}
			};

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getMessageById")
	private GetAppModelById<Message> getMessageById;

	@Autowired
	private GetMessageByKey getMessageByKey;

	@Autowired
	@Qualifier("getMessageList")
	private GetAppModelList<Message> getMessageList;

	@Autowired
	@Qualifier("getMessageCount")
	private GetAppModelCount getMessageCount;

	@Autowired
	@Qualifier("addMessage")
	private AddAppModel<Message> addMessage;

	@Autowired
	@Qualifier("updateMessage")
	private UpdateAppModel<Message> updateMessage;

	@Autowired
	@Qualifier("removeMessage")
	private RemoveAppModel<Message> removeMessage;

	/**
	 * メッセージを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id メッセージId
	 * @param session セッション
	 * @return メッセージ
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmMessage get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getMessageById, TYPE_NAME));
	}

	/**
	 * メッセージの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmMessage> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<Message> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, pageNumber, request,
				session, this.getMessageList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * メッセージの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getMessageCount, TYPE_NAME);
	}

	/**
	 * メッセージを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したメッセージ
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmMessage add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String code = map.get(Message.KEY_CODE);
		String localeId = map.get(Message.KEY_LOCALE_ID);
		String m = map.get(Message.KEY_MESSAGE);

		Argument.isNotNull(code, Message.KEY_CODE);
		Argument.isNotNull(localeId, Message.KEY_LOCALE_ID);
		Argument.isNotNull(m, Message.KEY_MESSAGE);

		Message message = new Message(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				code,
				new LocaleIdImpl(localeId),
				m);

		return this.toViewModel.apply(
				this.controller.add(
						message, csrfToken, session,
						this.addMessage, this.getMessageByKey, TYPE_NAME));
	}

	/**
	 * メッセージを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id メッセージId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のメッセージ
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmMessage update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String code = map.get(Message.KEY_CODE);
		String localeId = map.get(Message.KEY_LOCALE_ID);
		String m = map.get(Message.KEY_MESSAGE);
		String lastModified = map.get(Message.KEY_LAST_MODIFIED);

		Argument.isNotNull(code, Message.KEY_CODE);
		Argument.isNotNull(localeId, Message.KEY_LOCALE_ID);
		Argument.isNotNull(m, Message.KEY_MESSAGE);
		Argument.isNotNull(lastModified, Message.KEY_LAST_MODIFIED);

		Message message = new Message(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				code,
				new LocaleIdImpl(localeId),
				m,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert message.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						message, csrfToken, session,
						this.updateMessage, this.getMessageById, TYPE_NAME));
	}

	/**
	 * メッセージを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id メッセージId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeMessage, TYPE_NAME);
	}
}
