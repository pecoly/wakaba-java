package net.jp.yamabuki.controller.manager;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetUserModelList;
import net.jp.yamabuki.service.resource.CssService;
import net.jp.yamabuki.service.resource.JsService;
import net.jp.yamabuki.service.resource.MessageService;
import net.jp.yamabuki.util.HttpUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;

public abstract class BaseManagerViewController {

	@Autowired
	private YamabukiContext context;

	@Autowired
	private MessageService messageService;

	@Autowired
	private CssService cssService;

	@Autowired
	private JsService jsService;

	@Autowired
	@Qualifier("getAppList")
	private GetUserModelList<App> getAppList;

	/**
	 * モデルとセッションに属性を設定します。
	 * @param model モデル
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param path 部分URL
	 * @param session セッション
	 * @param request リクエスト
	 */
	protected void addAttributes(Model model,
			UserId userId, AppId appId,
			String path,
			HttpSession session,
			HttpServletRequest request) {
		this.addModelAttributes(model, userId, appId, session, request);
		this.addSessionAttributes(path, session, request);
	}

	/**
	 * モデルに属性を設定します。
	 * @param model モデル
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param session セッション
	 * @param request リクエスト
	 */
	void addModelAttributes(Model model,
			UserId userId, AppId appId,
			HttpSession session,
			HttpServletRequest request) {

		model.addAttribute("USER_ID", userId);
		model.addAttribute("APP_ID", appId);
		model.addAttribute("BASE_PATH", this.context.getBasePath());
		model.addAttribute("USER_BASE_PATH", this.context.getUserBasePath(userId));
		model.addAttribute("APP_BASE_PATH", this.context.getAppBasePath(userId, appId));
		model.addAttribute("MANAGER_VIEW_PATH", this.context.getManagerViewPath(userId, appId));
		model.addAttribute("MANAGER_SERVICE_PATH",
				this.context.getManagerServiceBasePath(userId, appId) + "/" + session.getId());
		model.addAttribute("USERS_VIEW_PATH", this.context.getUsersViewPath(userId, appId));
		model.addAttribute("USERS_SERVICE_PATH",
				this.context.getUsersServicePath(userId, appId) + "/" + session.getId());
		model.addAttribute("MESSAGE", this.messageService.getMessageQuery(userId, appId, request));
		model.addAttribute("CSS", this.cssService.getCssQuery(userId, appId));
		model.addAttribute("JS", this.jsService.getJsQuery(userId, appId));
		model.addAttribute("APP_LIST",
				this.getAppList.execute(userId, new PageRequest(0, 1000),
						new HashMap<String, String>()));
		model.addAttribute("USER_MODE", "single");

		model.addAttribute("CSS_NOT_FOUND", WakabaErrorCode.CSS_NOT_FOUND.getNumber());

		Map<String, String> map = HttpUtils.getStringMap(request);
		model.addAllAttributes(map);

		Map<String, String> x = HttpUtils.getStringMap(session);
		model.addAllAttributes(x);
	}

	/**
	 * セッションにクエリ文字列を設定します。
	 * @param path 部分URL
	 * @param session セッション
	 * @param request リクエスト
	 */
	void addSessionAttributes(
			String path,
			HttpSession session,
			HttpServletRequest request) {

		String queryString = HttpUtils.getQueryString(request);
		session.setAttribute(path, queryString);
	}
}
