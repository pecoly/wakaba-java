package net.jp.yamabuki.controller.manager.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ManagerAppId;
import net.jp.yamabuki.model.ManagerUserId;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmPlugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * プラグインサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/plugin")
public final class PluginServiceController extends BaseServiceController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getPluginList")
	private GetAppModelList<Plugin> getUserPluginList;

	@Autowired
	@Qualifier("getPluginCount")
	private GetAppModelCount getUserPluginCount;

	@Autowired
	private UsersQueryService queryService;

	private static final String TYPE_NAME = "plugin";

	/**
	 * プラグイン一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return プラグインの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmPlugin> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {

		List<Plugin> list = this.controller.getList(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(),
				csrfToken, pageNumber, request,
				session, this.getUserPluginList, TYPE_NAME);

		List<VmPlugin> result = new ArrayList<>(list.size());
		for (Plugin x : list) {
			result.add(new VmPlugin(x));
		}

		return result;
	}

	/**
	 * プラグインの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return プラグインの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getUserPluginCount, TYPE_NAME);
	}

	@ResponseBody
	@RequestMapping(value = "querylist/{path0}/{path1}", method = RequestMethod.GET)
	public List<Map<String, Object>> queryList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("path1") String path1,
			HttpServletRequest request,
			HttpSession session) {
		logger.debug("query1");

		QueryObject query = this.queryService.findQuery(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(), path0, path1);

		Map<String, String> map = HttpUtils.getStringMap(request);
		return query.execute(map);
	}

	@ResponseBody
	@RequestMapping(value = "queryobject/{path0}/{path1}", method = RequestMethod.GET)
	public Map<String, Object> queryObject(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("path1") String path1,
			HttpServletRequest request,
			HttpSession session) {
		logger.debug("query1");

		QueryObject query = this.queryService.findQuery(
				ManagerUserId.getInstance(),
				ManagerAppId.getInstance(), path0, path1);

		Map<String, String> map = HttpUtils.getStringMap(request);
		List<Map<String, Object>> result = query.execute(map);
		if (result.isEmpty()) {
			throw new ResourceNotFoundException(path0 + "/" + path1);
		}

		return result.get(0);
	}
}
