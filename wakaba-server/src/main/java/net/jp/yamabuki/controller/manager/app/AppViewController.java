package net.jp.yamabuki.controller.manager.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.manager.BaseManagerViewController;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.pkg.exp.PackageExport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * アプリケーションビュークラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/manager/manager/view/app")
public final class AppViewController extends BaseManagerViewController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Autowired
	private PackageExport packageExport;

	@RequestMapping(value = "download/{appId}", method = RequestMethod.GET)
	public void download(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpServletRequest request,
			HttpServletResponse response) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		this.packageExport.executeForZip(
				new UserIdImpl(userId), new AppIdImpl(appId), out);
		byte[] byteArray = out.toByteArray();
		response.setContentType("application/octet-stream");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-disposition",
				"attachment; filename=\"" + userId + "@" + appId + ".zip" + "\"");

		try {
			FileCopyUtils.copy(byteArray, response.getOutputStream());
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	/**
	 * 一覧画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 一覧画面のパス
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String list(Model model,
			@PathVariable("userId") String userId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("list");

		String path = "manager/view/app/list";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl("manager"), path, session, request);

		return path;
	}

	/**
	 * 追加画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 追加画面のパス
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(Model model,
			@PathVariable("userId") String userId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("add");

		String path = "manager/view/app/add";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl("manager"), path, session, request);

		return path;
	}

	/**
	 * 更新画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param id アプリケーションId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 更新画面のパス
	 */
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public String update(Model model,
			@PathVariable("userId") String userId,
			@RequestParam("id") String id,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("update");

		String path = "manager/view/app/update";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl("manager"), path, session, request);

		return path;
	}
}
