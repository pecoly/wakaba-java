package net.jp.yamabuki.controller.users;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.util.HttpUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/{userId}/{appId}/users/service/{csrfToken}/collection")
public class UsersCollectionServiceController extends BaseServiceController {
	@ResponseBody
	@RequestMapping(value = "{path0}", method = RequestMethod.GET)
	public List<Map<String, Object>> get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			HttpServletRequest request,
			HttpSession session) {
		logger.debug("get");

		QueryObject query = this.queryService.findQuery(
				new UserIdImpl(userId), new AppIdImpl(appId), "collection/get", path0);

		Map<String, String> map = HttpUtils.getStringMap(request);
		return query.execute(map);
	}

	@Autowired
	private UsersQueryService queryService;

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
}
