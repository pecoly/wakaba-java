package net.jp.yamabuki.controller.resources;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.service.manager.css.GetCssByKey;
import net.jp.yamabuki.service.manager.js.GetJsByKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * リソースビュークラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/resources")
public class ResourcesController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");


	@Autowired
	private YamabukiContext context;

	@Autowired
	private GetCssByKey getCssByKey;

	@Autowired
	private GetJsByKey getJsByKey;

	/**
	 * Cssの内容を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param session セッション
	 * @param request リクエスト
	 * @param response レスポンス
	 * @return Cssの内容
	 */
	@ResponseBody
	@RequestMapping(value = "/css/{name}",
		method = RequestMethod.GET, produces = "text/css;charset=UTF-8")
	public String css1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("name") String name,
			HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("css1");
		logger.debug(name + " date = " + request.getDateHeader("If-Modified-Since"));
		long dateTime = request.getDateHeader("If-Modified-Since");
		if (dateTime != -1) {
			System.out.println("css not modified.");
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return null;
		}

		Css css = this.getCssByKey.execute(
				new UserIdImpl(userId), new AppIdImpl(appId), name);
		if (css == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}

		System.out.println("css last modified : " + css.getLastModified().toString());
		response.setDateHeader("Last-Modified", css.getLastModified().getMillis());
		return css.getContent();
	}

	@ResponseBody
	@RequestMapping(value = "/js/{name1}",
		method = RequestMethod.GET, produces = "text/css;charset=UTF-8")
	public String js1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("name1") String name1,
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("js1");
		return this.js(new UserIdImpl(userId), new AppIdImpl(appId),
				name1, request, response);
	}

	@ResponseBody
	@RequestMapping(value = "/js/{name1}/{name2}",
		method = RequestMethod.GET, produces = "text/css;charset=UTF-8")
	public String js2(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("name1") String name1,
			@PathVariable("name2") String name2,
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("js2");
		return this.js(new UserIdImpl(userId), new AppIdImpl(appId),
				name1 + "/" + name2, request, response);
	}

	@ResponseBody
	@RequestMapping(value = "/js/{name1}/{name2}/{name3}",
		method = RequestMethod.GET, produces = "text/css;charset=UTF-8")
	public String js3(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("name1") String name1,
			@PathVariable("name2") String name2,
			@PathVariable("name3") String name3,
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("js3");
		return this.js(
				new UserIdImpl(userId), new AppIdImpl(appId),
				name1 + "/" + name2 + "/" + name3,
				request, response);
	}

	String js(UserId userId, AppId appId,
			String name,
			HttpServletRequest request,
			HttpServletResponse response) {
		Js js = this.getJsByKey.execute(userId, appId, name);
		if (js == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}

		return js.getContent();
	}

	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public void handleException(final Exception e,
			/*final HttpServletRequest request,*/
			HttpServletResponse response,
			Writer writer) throws IOException {

		if (e instanceof RuntimeException) {
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
		}
		else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}
}
