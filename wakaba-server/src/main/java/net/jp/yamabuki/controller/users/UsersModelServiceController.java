package net.jp.yamabuki.controller.users;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.result.SuccessResult;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.service.update.users.UsersUpdateService;
import net.jp.yamabuki.util.AuthUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/{userId}/{appId}/users/service/{csrfToken}/model")
public class UsersModelServiceController extends BaseServiceController {
	/**
	 * モデルオブジェクトを取得します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param path0 モデル名
	 * @param csrfToken CSRFトークン
	 * @param id モデルID
	 * @param session セッション
	 * @return モデルオブジェクト
	 */
	@ResponseBody
	@RequestMapping(value = "{path0}/{id}", method = RequestMethod.GET)
	public Map<String, Object> get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		logger.debug("get1");

		AuthUtils.checkCsrfToken(session, csrfToken);

		String name = path0;

		return this.getModelById(new UserIdImpl(userId), new AppIdImpl(appId), "model/get", name, id);
	}

	/**
	 * モデルオブジェクトを更新します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param path0 モデル名
	 * @param csrfToken CSRFトークン
	 * @param id モデルID
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のモデルオブジェクト
	 */
	@ResponseBody
	@RequestMapping(value = "{path0}/{id}", method = RequestMethod.PUT)
	public Map<String, Object> put(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		logger.debug("put");

		AuthUtils.checkCsrfToken(session, csrfToken);

		UpdateObject update = this.updateService.find(
				new UserIdImpl(userId), new AppIdImpl(appId), "model/put", path0);

		Map<String, String> tmp = new HashMap<>();
		tmp.putAll(map);
		tmp.put("id", id);

		int result = update.execute(tmp);
		if (result != 1) {
			throw new UpdateException(WakabaErrorCode.UNDEFINIED, "not 1");
		}

		return this.getModelById(new UserIdImpl(userId), new AppIdImpl(appId), "model/get", path0, id);
	}

	/**
	 * モデルオブジェクトを作成します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param path0 モデル名
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 作成したモデルオブジェクト
	 */
	@ResponseBody
	@RequestMapping(value = "{path0}", method = RequestMethod.POST)
	public Map<String, Object> post(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		logger.debug("post");

		AuthUtils.checkCsrfToken(session, csrfToken);

		Map<String, Object> found = this.getModelByKey(
				new UserIdImpl(userId), new AppIdImpl(appId), "model/key", path0, map);
		if (found != null) {
			throw new UpdateException(WakabaErrorCode.UNDEFINIED, "Failed to add. '" + path0 + "' already exists.");
		}

		UpdateObject update = this.updateService.find(
				new UserIdImpl(userId), new AppIdImpl(appId), "model/post", path0);

		int result = update.execute(map);
		if (result != 1) {
			throw new UpdateException(WakabaErrorCode.UNDEFINIED, "not 1");
		}

		return this.getModelByKey(new UserIdImpl(userId), new AppIdImpl(appId), "model/key", path0, map);
	}

	/**
	 * モデルオブジェクトを削除します。
	 * @param userId ユーザーID
	 * @param appId アプリケーションID
	 * @param path0 モデル名
	 * @param csrfToken CSRFトークン
	 * @param id モデルID
	 * @param session セッション
	 * @return 成否
	 */
	@ResponseBody
	@RequestMapping(value = "{path0}/{id}", method = RequestMethod.DELETE)
	public Result delete(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		logger.debug("delete");

		AuthUtils.checkCsrfToken(session, csrfToken);

		UpdateObject update = this.updateService.find(
				new UserIdImpl(userId), new AppIdImpl(appId), "model/delete", path0);

		Map<String, String> map = new HashMap<>();
		map.put("id", id);

		int result = update.execute(map);
		if (result != 1) {
			throw new UpdateException(WakabaErrorCode.UNDEFINIED, "not 1");
		}

		return successResult;
	}

	Map<String, Object> getModelById(UserId userId, AppId appId,
			String type, String name, String id) {
		QueryObject query = this.queryService.findQuery(
				userId, appId, type, name);

		Map<String, String> map = new HashMap<>();
		map.put("id", id);

		List<Map<String, Object>> result = query.execute(map);
		if (result.isEmpty()) {
			return null;
		}

		return result.get(0);
	}

	Map<String, Object> getModelByKey(UserId userId, AppId appId,
			String type, String name, Map<String, String> map) {
		QueryObject query = this.queryService.findQuery(
				userId, appId, type, name);

		List<Map<String, Object>> result = query.execute(map);
		if (result.isEmpty()) {
			return null;
		}

		return result.get(0);
	}

	@Autowired
	private UsersQueryService queryService;

	@Autowired
	private UsersUpdateService updateService;

	@Autowired
	private SuccessResult successResult;

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
}
