package net.jp.yamabuki.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserModel;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.result.SuccessResult;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.AddUserModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetUserModelById;
import net.jp.yamabuki.service.manager.GetUserModelByKey;
import net.jp.yamabuki.service.manager.GetUserModelCount;
import net.jp.yamabuki.service.manager.GetUserModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.RemoveUserModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.UpdateUserModel;
import net.jp.yamabuki.util.AuthUtils;
import net.jp.yamabuki.util.HttpUtils;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

@Component
public class ServiceController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Autowired
	private SuccessResult successResult;

	public <T extends AppModel> T get(
			UserId userId, AppId appId, String csrfToken,
			ModelId id, HttpSession session,
			GetAppModelById<T> getModelById, String typeName) {
		logger.debug("get " + typeName + ". id : " + id);

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			T found = getModelById.execute(userId, appId, id);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " id : " + id);
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + ". id : " + id, ex);
			throw ex;
		}
	}

	public <T extends UserModel> T get(
			UserId userId, String csrfToken,
			ModelId id, HttpSession session,
			GetUserModelById<T> getModelById, String typeName) {
		logger.debug("get " + typeName + ". id : " + id);

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			T found = getModelById.execute(userId, id);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. id : " + id);
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + ". id : " + id, ex);
			throw ex;
		}
	}



	public <T extends AppModel> T getByKey(
			T model, String csrfToken, HttpSession session,
			GetModelByKey<T> getModelByKey, String typeName) {
		logger.debug("get " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			T found = getModelByKey.execute(model);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. " + model.toString());
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + ".");
			throw ex;
		}
	}

	public <T extends AppModel> List<T> getList(
			UserId userId, AppId appId, String csrfToken,
			int pageNumber, HttpServletRequest request,
			HttpSession session,
			GetAppModelList<T> getModelList, String typeName) {
		logger.debug("get " + typeName + " list.");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			Map<String, String> map = HttpUtils.getStringMap(request);
			return getModelList.execute(userId, appId,
					this.createPageRequest(pageNumber), map);
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + " list.", ex);
			throw ex;
		}
	}

	public <T extends UserModel> List<T> getList(
			UserId userId, String csrfToken,
			int pageNumber, HttpServletRequest request,
			HttpSession session,
			GetUserModelList<T> getModelList, String typeName) {
		logger.debug("get " + typeName + " list.");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			Map<String, String> map = HttpUtils.getStringMap(request);
			return getModelList.execute(userId,
					this.createPageRequest(pageNumber), map);
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + " list.", ex);
			throw ex;
		}
	}

	public <T extends AppModel> Map<String, Object> getCount(
			UserId userId, AppId appId, String csrfToken,
			HttpServletRequest request,
			HttpSession session,
			GetAppModelCount getModelCount, String typeName) {
		logger.debug("get " + typeName + " count.");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			Map<String, String> map = HttpUtils.getStringMap(request);
			long count = getModelCount.execute(userId, appId, map);
			Map<String, Object> result = new HashMap<>();
			result.put("count", count);
			return result;
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + " count.", ex);
			throw ex;
		}
	}

	public <T extends UserModel> Map<String, Object> getCount(
			UserId userId, String csrfToken,
			HttpServletRequest request,
			HttpSession session,
			GetUserModelCount getModelCount, String typeName) {
		logger.debug("get " + typeName + " count.");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			Map<String, String> map = HttpUtils.getStringMap(request);
			long count = getModelCount.execute(userId, map);
			Map<String, Object> result = new HashMap<>();
			result.put("count", count);
			return result;
		}
		catch (Exception ex) {
			logger.warn("Failed to get " + typeName + " count.", ex);
			throw ex;
		}
	}

	public <T extends AppModel> T add(T model, String csrfToken,
			HttpSession session,
			AddAppModel<T> addModel,
			GetModelByKey<T> getModelByKey, String typeName) {
		Argument.isNotNull(model, "model");
		Argument.isNotNull(csrfToken, "csrfToken");
		Argument.isNotNull(addModel, "addModel");
		Argument.isNotNull(getModelByKey, "getModelByKey");
		Argument.isNotNull(typeName, "typeName");

		logger.debug("add " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);
			addModel.execute(model);

			T found = getModelByKey.execute(model);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. " + model.toString());
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to add " + typeName + ".", ex);
			throw ex;
		}
	}

	public <T extends UserModel> T add(T model, String csrfToken,
			HttpSession session,
			AddUserModel<T> addModel,
			GetUserModelByKey<T> getModelByKey, String typeName) {
		Argument.isNotNull(model, "model");
		Argument.isNotNull(csrfToken, "csrfToken");
		Argument.isNotNull(addModel, "addModel");
		Argument.isNotNull(getModelByKey, "getModelByKey");
		Argument.isNotNull(typeName, "typeName");

		logger.debug("add " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);
			addModel.execute(model);

			T found = getModelByKey.execute(model);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. " + model.toString());
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to add " + typeName + ".", ex);
			throw ex;
		}
	}


	public <T extends AppModel> T update(T model, String csrfToken,
			HttpSession session, UpdateAppModel<T> updateModel,
			GetAppModelById<T> getModelById, String typeName) {
		logger.debug("update " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);
			updateModel.execute(model);

			T found = getModelById.execute(model);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. id : " + model.getId());
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to update " + typeName + ". id : " + model.getId(), ex);
			throw ex;
		}
	}

	public <T extends UserModel> T update(T model, String csrfToken,
			HttpSession session, UpdateUserModel<T> updateModel,
			GetUserModelById<T> getModelById, String typeName) {
		logger.debug("update " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);
			updateModel.execute(model);

			T found = getModelById.execute(model);
			if (found == null) {
				throw new ResourceNotFoundException(typeName + " not found. id : " + model.getId());
			}

			return found;
		}
		catch (Exception ex) {
			logger.warn("Failed to update " + typeName + ". id : " + model.getId(), ex);
			throw ex;
		}
	}

	/**
	 * モデルを削除します。
	 * @param <T> モデルの型
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id モデルId
	 * @param lastModified 最終更新日時
	 * @param csrfToken Csrfトークン
	 * @param session セッション
	 * @param removeModel 削除処理
	 * @param typeName モデル種別
	 * @return 削除結果
	 */
	public <T extends AppModel> Result remove(
			UserId userId, AppId appId,
			ModelId id, DateTime lastModified,
			String csrfToken, HttpSession session,
			RemoveAppModel<T> removeModel, String typeName) {
		Argument.isNotNull(lastModified, "lastModified");

		logger.debug("remove " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			removeModel.execute(userId, appId, id, lastModified);
			return this.successResult;
		}
		catch (Exception ex) {
			logger.warn("Failed to remove " + typeName + ". id : " + id, ex);
			throw ex;
		}
	}

	public <T extends UserModel> Result remove(
			UserId userId,
			ModelId id, DateTime lastModified,
			String csrfToken, HttpSession session,
			RemoveUserModel<T> removeModel, String typeName) {
		logger.debug("remove " + typeName + ".");

		try {
			AuthUtils.checkCsrfToken(session, csrfToken);

			removeModel.execute(userId, id, lastModified);
			return this.successResult;
		}
		catch (Exception ex) {
			logger.warn("Failed to remove " + typeName + ". id : " + id, ex);
			throw ex;
		}
	}

	PageRequest createPageRequest(int pageNumber) {
		Argument.isGreaterThanOrEqualTo(pageNumber, "pageNumber", 1);

		return new PageRequest(pageNumber - 1, 10);
	}
}
