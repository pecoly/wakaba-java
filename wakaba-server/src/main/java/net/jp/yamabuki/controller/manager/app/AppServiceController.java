package net.jp.yamabuki.controller.manager.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddUserModel;
import net.jp.yamabuki.service.manager.GetUserModelById;
import net.jp.yamabuki.service.manager.GetUserModelCount;
import net.jp.yamabuki.service.manager.GetUserModelList;
import net.jp.yamabuki.service.manager.RemoveUserModel;
import net.jp.yamabuki.service.manager.UpdateUserModel;
import net.jp.yamabuki.service.manager.app.GetAppByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * アプリケーションサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/app")
public final class AppServiceController extends BaseServiceController {
	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getAppById")
	private GetUserModelById<App>getAppById;

	@Autowired
	private GetAppByKey getAppByKey;

	@Autowired
	@Qualifier("getAppList")
	private GetUserModelList<App> getAppList;

	@Autowired
	@Qualifier("getAppCount")
	private GetUserModelCount getAppCount;

	@Autowired
	@Qualifier("addApp")
	private AddUserModel<App> addApp;

	@Autowired
	@Qualifier("updateApp")
	private UpdateUserModel<App> updateApp;

	@Autowired
	@Qualifier("removeApp")
	private RemoveUserModel<App> removeApp;

	private static final String TYPE_NAME = "app";

	/**
	 * アプリケーションを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションId
	 * @param session セッション
	 * @return アプリケーション
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmApp get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return new VmApp(
				this.controller.get(new UserIdImpl(userId),
				csrfToken, new ModelIdImpl(id), session,
				this.getAppById, TYPE_NAME));
	}

	/**
	 * アプリケーションの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return アプリケーションの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmApp> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<App> list = this.controller.getList(
				new UserIdImpl(userId),
				csrfToken, pageNumber, request,
				session, this.getAppList, TYPE_NAME);

		List<VmApp> result = new ArrayList<>(list.size());
		for (App x : list) {
			result.add(new VmApp(x));
		}

		return result;
	}

	/**
	 * アプリケーションの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return アプリケーションの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId),
				csrfToken, request, session,
				this.getAppCount, TYPE_NAME);
	}

	/**
	 * アプリケーションを追加します。
	 * @param userId ユーザーId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したアプリケーション
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmApp add(
			@PathVariable("userId") String userId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		App app = new App(
				new UserIdImpl(userId),
				new AppIdImpl(map.get("appId")),
				map.get(App.KEY_APP_NAME));

		return new VmApp(
				this.controller.add(
						app, csrfToken, session,
						this.addApp, this.getAppByKey, TYPE_NAME));
	}

	/**
	 * アプリケーションを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のアプリケーション
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmApp update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		App app1 = this.getAppById.execute(
				new UserIdImpl(userId), new ModelIdImpl(id));
		if (app1 == null) {
			throw new ResourceNotFoundException(TYPE_NAME + " not found. id : " + id);
		}

		Argument.isNotNull(map.get(App.KEY_APP_NAME), App.KEY_APP_NAME);
		Argument.isNotNull(map.get(App.KEY_LAST_MODIFIED), App.KEY_LAST_MODIFIED);

		App app2 = new App(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				app1.getAppId(),
				map.get(App.KEY_APP_NAME),
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						map.get(App.KEY_LAST_MODIFIED)));
		assert app2.getId().getValue().equals(id);

		return new VmApp(
				this.controller.update(
						app2, csrfToken, session,
						this.updateApp, this.getAppById, TYPE_NAME));
	}

	/**
	 * アプリケーションを削除します。
	 * @param userId ユーザーId
	 * @param csrfToken CSRFトークン
	 * @param id アプリケーションId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		App app1 = this.getAppById.execute(
				new UserIdImpl(userId), new ModelIdImpl(id));
		if (app1 == null) {
			throw new ResourceNotFoundException(TYPE_NAME + " not found. id : " + id);
		}

		return this.controller.remove(
				new UserIdImpl(userId), new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeApp, TYPE_NAME);
	}
}
