package net.jp.yamabuki.controller.manager.query;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.query.GetQueryByKey;
import net.jp.yamabuki.util.CollectionUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.Transformer;
import net.jp.yamabuki.viewmodel.VmQuery;

import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * クエリサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/query")
public final class QueryServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "query";

	private Function<Query, VmQuery> toViewModel;

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getQueryById")
	private GetAppModelById<Query> getQueryById;

	@Autowired
	private GetQueryByKey getQueryByKey;

	@Autowired
	@Qualifier("getQueryList")
	private GetAppModelList<Query> getQueryList;

	@Autowired
	@Qualifier("getQueryCount")
	private GetAppModelCount getQueryCount;

	@Autowired
	@Qualifier("addQuery")
	private AddAppModel<Query> addQuery;

	@Autowired
	@Qualifier("updateQuery")
	private UpdateAppModel<Query> updateQuery;

	@Autowired
	@Qualifier("removeQuery")
	private RemoveAppModel<Query> removeQuery;

	@Autowired
	private YamabukiContext yamabukiContext;

	/**
	 * クエリの変換クラス。
	 *
	 */
	private static class FunctionEx implements Function<Query, VmQuery> {
		private Configuration configuration;

		/**
		 * コンストラクタ。
		 * @param configuration mybatisに使用する設定
		 */
		public FunctionEx(Configuration configuration) {
			this.configuration = configuration;
		}

		@Override
		public VmQuery apply(Query in) {
			return new VmQuery(in, this.configuration);
		}
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.toViewModel = new FunctionEx(this.yamabukiContext.getConfiguration());
	}

	/**
	 * クエリを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id クエリId
	 * @param session セッション
	 * @return クエリ
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmQuery get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getQueryById, TYPE_NAME));
	}

	/**
	 * クエリの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return クエリの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmQuery> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<Query> list
				= this.controller.getList(
						new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, pageNumber, request,
						session, this.getQueryList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * クエリの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return クエリの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getQueryCount, TYPE_NAME);
	}

	/**
	 * クエリを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param vmQuery クエリ
	 * @param session セッション
	 * @return 追加したクエリ
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmQuery add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody VmQuery vmQuery,
			HttpSession session) {

		String type = vmQuery.getType();
		String name = vmQuery.getName();
		String xml = vmQuery.toXml();

		Argument.isNotNull(type, "type");
		Argument.isNotNull(name, "name");
		Argument.isNotNull(xml, "xml");

		Query query = new Query(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				type,
				name,
				xml);

		return this.toViewModel.apply(
				this.controller.add(
						query, csrfToken, session,
						this.addQuery, this.getQueryByKey, TYPE_NAME));
	}

	/**
	 * クエリを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id メッセージId
	 * @param vmQuery クエリ
	 * @param session セッション
	 * @return 更新後のクエリ
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmQuery update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody VmQuery vmQuery,
			HttpSession session) {

		String type = vmQuery.getType();
		String name = vmQuery.getName();
		String xml = vmQuery.toXml();
		String lastModified = vmQuery.getLastModified();

		Argument.isNotNull(type, "type");
		Argument.isNotNull(name, "name");
		Argument.isNotNull(xml, "xml");
		Argument.isNotNull(lastModified, "lastModified");

		Query query2 = new Query(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				type,
				name,
				xml,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert query2.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						query2, csrfToken, session,
						this.updateQuery, this.getQueryById, TYPE_NAME));
	}

	/**
	 * クエリを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id クエリId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeQuery, TYPE_NAME);
	}
}
