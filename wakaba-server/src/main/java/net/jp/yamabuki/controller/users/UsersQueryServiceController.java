package net.jp.yamabuki.controller.users;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.service.update.users.UsersUpdateService;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/{userId}/{appId}/users/service/{csrfToken}/query")
public class UsersQueryServiceController extends BaseServiceController {
	@ResponseBody
	@RequestMapping(value = "{path0}", method = RequestMethod.GET)
	public List<Map<String, Object>> query1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			HttpServletRequest request,
			HttpSession session) {
		logger.debug("query1");

		String name = path0;

		QueryObject query = this.queryService.findQuery(new UserIdImpl(userId), new AppIdImpl(appId), "query", name);

		Map<String, String> map = HttpUtils.getStringMap(request);
		return query.execute(map);
	}

	@ResponseBody
	@RequestMapping(value = "{path0}/{path1}", method = RequestMethod.GET)
	public List<Map<String, Object>> query2(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("path1") String path1,
			HttpServletRequest request,
			HttpSession session) {
		logger.debug("query2");

		String name = StringUtils.join(
				new String[] {path0, path1},
				"/");

		QueryObject query = this.queryService.findQuery(new UserIdImpl(userId), new AppIdImpl(appId), "query", name);

		Map<String, String> map = HttpUtils.getStringMap(request);
		return query.execute(map);
	}

	/*
	@RequestMapping(value = "/download/{path0}", method = RequestMethod.POST)
	public void downloadGet1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		logger.debug("downloadGet1");

		String name = StringUtils.join(
				new String[] {userId, appId, "query", path0},
				"/");

		QueryObject query = this.queryService.find(userId, name);

		Map<String, String> map = HttpUtil.getStringMap(request);


		QueryResult result = query.execute(map);
		try {
			byte[] byteArray = new String("abc").getBytes("UTF-8");
			response.setContentType("application/octet-stream");
			response.setContentLength(byteArray.length);
			response.setHeader("Content-disposition",
					"attachment; filename=\"" + "def.txt" + "\"");
			FileCopyUtils.copy(byteArray, response.getOutputStream());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/

	@Autowired
	private UsersQueryService queryService;

	@Autowired
	private UsersUpdateService updateService;

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
}
