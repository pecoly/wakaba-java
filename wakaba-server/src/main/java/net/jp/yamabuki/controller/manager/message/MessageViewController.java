package net.jp.yamabuki.controller.manager.message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.manager.BaseManagerViewController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * メッセージビュークラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/view/message")
public class MessageViewController extends BaseManagerViewController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	/**
	 * 一覧画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 一覧画面のパス
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String list(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("list");

		String path = "manager/view/message/list";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}

	/**
	 * 追加画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 追加画面のパス
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("add");

		String path = "manager/view/message/add";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}

	/**
	 * 更新画面を表示します。
	 * @param model モデル
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id メッセージId
	 * @param session セッション
	 * @param request リクエスト
	 * @return 更新画面のパス
	 */
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public String update(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@RequestParam("id") String id,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("update");

		String path = "manager/view/message/update";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}
}
