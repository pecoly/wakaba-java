package net.jp.yamabuki.controller.manager.test;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.TestUpdate;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.testupdate.GetTestUpdateByKey;
import net.jp.yamabuki.service.update.users.UsersUpdateService;
import net.jp.yamabuki.viewmodel.VmUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * テストアップデートサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/testupdate")
public class TestUpdateServiceController extends BaseServiceController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	private static final String TYPE_NAME = "testupdate";

	@Autowired
	private ServiceController controller;

	@Autowired
	private GetTestUpdateByKey getTestUpdateByKey;

	@Autowired
	@Qualifier("addTestUpdate")
	private AddAppModel<TestUpdate> addTestUpdate;

	@Autowired
	private UsersUpdateService updateService;

	/**
	 * テストアップデートを実行します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param bindingUpdate アップデート
	 * @param session セッション
	 * @return 実行結果(件数)
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public Map<String, Object> update1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody VmUpdate bindingUpdate,
			HttpSession session) {
		logger.debug("update1");

		TestUpdate update1 = new TestUpdate(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				bindingUpdate.toXml());

		this.controller.add(update1, csrfToken, session,
				this.addTestUpdate, this.getTestUpdateByKey, TYPE_NAME);

		UpdateObject update = this.updateService.findTestUpdate(
				new UserIdImpl(userId), new AppIdImpl(appId));

		Map<String, String> map = new HashMap<>();
		int count = update.execute(map);

		Map<String, Object> result = new HashMap<>();
		result.put("count", count);
		return result;
	}
}
