package net.jp.yamabuki.controller.users;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.service.resource.MessageService;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.util.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/{userId}/{appId}/users/view")
public class UsersViewController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Autowired
	private YamabukiContext context;

	@Autowired
	private UsersQueryService queryService;

	@Autowired
	private MessageService messageService;

	@RequestMapping(method = RequestMethod.GET)
	public String path0(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("path0");

		commonProcess(model, "", session, request);
		addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), session, request);

		return userId + "/" + appId + "/users/view/home";
	}

	@RequestMapping(value = "/{path0}", method = RequestMethod.GET)
	public String path1(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@RequestParam(required = false, value = "PAGE") String page,
			HttpSession session,
			HttpServletRequest request) {
		System.out.println("appId : " + appId);

		String subPath = path0;

		String result = executeCommand(
				new UserIdImpl(userId), new AppIdImpl(appId), subPath, page, session);
		if (result != null) {
			return result;
		}

		String path = StringUtils.join(
				new String[] {userId, appId, "users", "view", subPath},
				"/");
/*
		Map<String, String> map = HttpUtil.getStringMap(request);
		for (ViewQueryMapping x : this.viewQueryService.findViewQueryListByViewName(path)) {
			QueryObject query = this.queryService.find(userId, x.getQueryName());

			Map<String, String> tmp = x.getParameterList();
			tmp.putAll(map);

			QueryResult queryResult = query.execute(tmp);

			model.addAttribute(x.getModelName(), queryResult);
		}
	*/
		commonProcess(model, subPath, session, request);
		addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), session, request);

		return path;
	}

	@RequestMapping(value = "/{path0}/{path1}", method = RequestMethod.GET)
	public String path2(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@PathVariable("path1") String path1,
			@RequestParam(required = false, value = "PAGE") String page,
			HttpSession session,
			HttpServletRequest request) {
		System.out.println("path2");

		String subPath = path0 + "/" + path1;

		String result = executeCommand(new UserIdImpl(userId), new AppIdImpl(appId), subPath, page, session);
		if (result != null) {
			return result;
		}

		String path = StringUtils.join(
				new String[] {userId, appId, "users", "view", subPath},
				"/");

		commonProcess(model, subPath, session, request);
		addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), session, request);

		return path;
	}

	private String executeCommand(UserId userId, AppId appId,
			String subPath, String page, HttpSession session) {
		if (page != null && page.equals("LAST")) {
			String last = (String)session.getAttribute(subPath);
			if (last != null) {
				return "redirect:/" + userId.getValue() + "/" + appId.getValue() + "/" + subPath + "?" + last;
			}
		}

		return null;
	}

	private void addAttributes(Model model,
			UserId userId, AppId appId,
			HttpSession session,
			HttpServletRequest request) {

		model.addAttribute("USER_ID", userId);
		model.addAttribute("APP_ID", appId);
		model.addAttribute("SESSION_ID", session.getId());
		model.addAttribute("BASE_PATH", this.context.getAppBasePath(userId, appId));
		model.addAttribute("USERS_VIEW_PATH", this.context.getUsersViewPath(userId, appId));
		model.addAttribute("USERS_SERVICE_PATH", this.context.getUsersServicePath(userId, appId) + "/" + session.getId());
		model.addAttribute("MESSAGE", this.messageService.getMessageQuery(userId, appId, request));

	}

	private void commonProcess(Model model,
			String subPath,
			HttpSession session,
			HttpServletRequest request) {

		Map<String, String> map = HttpUtils.getStringMap(request);
		model.addAllAttributes(map);

		Map<String, String> x = HttpUtils.getStringMap(session);
		model.addAllAttributes(x);

		String queryString = HttpUtils.getQueryString(request);
		session.setAttribute("users/view/" + subPath, queryString);
	}
}
