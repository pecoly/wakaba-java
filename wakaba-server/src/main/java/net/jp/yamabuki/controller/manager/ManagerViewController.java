package net.jp.yamabuki.controller.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * アプリケーションのホームページビュークラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/view")
public final class ManagerViewController extends BaseManagerViewController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@RequestMapping(method = RequestMethod.GET)
	public String dashboard(Model model,
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			HttpSession session,
			HttpServletRequest request) {
		logger.debug("dashboard");

		String path = "manager/view/dashboard";
		this.addAttributes(model, new UserIdImpl(userId), new AppIdImpl(appId), path, session, request);

		return path;
	}
}
