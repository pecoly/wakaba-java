package net.jp.yamabuki.controller.users.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.view.GetViewByKey;
import net.jp.yamabuki.viewmodel.VmView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ビューサービスクラス。
 *
 */
@Controller("usersViewServiceController")
@RequestMapping(value = "/{userId}/{appId}/users/service/{csrfToken}/view")
public final class ViewServiceController extends BaseServiceController {
	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getViewById")
	private GetAppModelById<View>getViewById;

	@Autowired
	private GetViewByKey getViewByKey;

	@Autowired
	@Qualifier("getViewList")
	private GetAppModelList<View> getViewList;

	@Autowired
	@Qualifier("getViewCount")
	private GetAppModelCount getViewCount;

	@Autowired
	@Qualifier("addView")
	private AddAppModel<View> addView;

	@Autowired
	@Qualifier("updateView")
	private UpdateAppModel<View> updateView;

	@Autowired
	@Qualifier("removeView")
	private RemoveAppModel<View> removeView;

	private static final String TYPE_NAME = "view";

	/**
	 * ビューの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return メッセージの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmView> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<View> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, pageNumber, request,
				session, this.getViewList, TYPE_NAME);

		List<VmView> result = new ArrayList<>(list.size());
		for (View x : list) {
			result.add(new VmView(x));
		}

		return result;
	}

	/**
	 * ビューの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return ビューの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getViewCount, TYPE_NAME);
	}
}
