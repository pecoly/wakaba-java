package net.jp.yamabuki.controller.manager.css;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.result.Result;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.service.manager.css.GetCssByKey;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.HttpUtils;
import net.jp.yamabuki.viewmodel.VmCss;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Cssサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/css")
public final class CssServiceController extends BaseServiceController {

	private static final String TYPE_NAME = "css";

	private Function<Css, VmCss> toViewModel
			= new Function<Css, VmCss>() {
				@Override
				public VmCss apply(Css in) {
					return new VmCss(in);
				}
			};

	@Autowired
	private ServiceController controller;

	@Autowired
	@Qualifier("getCssById")
	private GetAppModelById<Css> getCssById;

	@Autowired
	private GetCssByKey getCssByKey;

	@Autowired
	@Qualifier("getCssList")
	private GetAppModelList<Css> getCssList;

	@Autowired
	@Qualifier("getCssCount")
	private GetAppModelCount getCssCount;

	@Autowired
	@Qualifier("addCss")
	private AddAppModel<Css> addCss;

	@Autowired
	@Qualifier("updateCss")
	private UpdateAppModel<Css> updateCss;

	@Autowired
	@Qualifier("removeCss")
	private RemoveAppModel<Css> removeCss;

	/**
	 * Cssを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id CssId
	 * @param session セッション
	 * @return Css
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public VmCss get(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpSession session) {
		return this.toViewModel.apply(
				this.controller.get(new UserIdImpl(userId), new AppIdImpl(appId),
						csrfToken, new ModelIdImpl(id), session,
						this.getCssById, TYPE_NAME));
	}

	/**
	 * Cssの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param pageNumber ページ番号
	 * @param request リクエスト
	 * @param session セッション
	 * @return Cssの一覧
	 */
	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<VmCss> getList(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestParam("pageNumber") int pageNumber,
			HttpServletRequest request,
			HttpSession session) {
		List<Css> list = this.controller.getList(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, pageNumber, request,
				session, this.getCssList, TYPE_NAME);

		return Lists.transform(list, this.toViewModel);
	}

	/**
	 * Cssの件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param request リクエスト
	 * @param session セッション
	 * @return Cssの件数
	 */
	@ResponseBody
	@RequestMapping(value = "count", method = RequestMethod.GET)
	public Map<String, Object> getCount(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.getCount(
				new UserIdImpl(userId), new AppIdImpl(appId),
				csrfToken, request, session,
				this.getCssCount, TYPE_NAME);
	}

	/**
	 * Cssを追加します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param map パラメータ
	 * @param session セッション
	 * @return 追加したCss
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public VmCss add(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(Css.KEY_NAME);
		String content = map.get(Css.KEY_CONTENT);

		Argument.isNotNull(name, Css.KEY_NAME);
		Argument.isNotNull(content, Css.KEY_CONTENT);

		Css css = new Css(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content);

		return this.toViewModel.apply(
				this.controller.add(
						css, csrfToken, session,
						this.addCss, this.getCssByKey, TYPE_NAME));
	}

	/**
	 * Cssを更新します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id CssId
	 * @param map パラメータ
	 * @param session セッション
	 * @return 更新後のCss
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public VmCss update(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			@RequestBody Map<String, String> map,
			HttpSession session) {

		String name = map.get(Css.KEY_NAME);
		String content = map.get(Css.KEY_CONTENT);
		String lastModified = map.get(Css.KEY_LAST_MODIFIED);

		Argument.isNotNull(name, Css.KEY_NAME);
		Argument.isNotNull(content, Css.KEY_CONTENT);
		Argument.isNotNull(lastModified, Css.KEY_LAST_MODIFIED);

		Css css = new Css(
				new ModelIdImpl(id),
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				name,
				content,
				DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
						lastModified));
		//assert css.getId().getValue().equals(id);

		return this.toViewModel.apply(
				this.controller.update(
						css, csrfToken, session,
						this.updateCss, this.getCssById, TYPE_NAME));
	}

	/**
	 * Cssを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param id CssId
	 * @param request リクエスト
	 * @param session セッション
	 * @return 成功結果
	 */
	@ResponseBody
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Result remove(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@PathVariable("id") String id,
			HttpServletRequest request,
			HttpSession session) {
		return this.controller.remove(
				new UserIdImpl(userId), new AppIdImpl(appId),
				new ModelIdImpl(id),
				HttpUtils.getLastModified(request),
				csrfToken, session,
				this.removeCss, TYPE_NAME);
	}
}
