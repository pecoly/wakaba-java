package net.jp.yamabuki.controller.manager.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.controller.ServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.testquery.GetTestQueryByKey;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.util.AuthUtils;
import net.jp.yamabuki.viewmodel.VmQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * テストクエリサービスクラス。
 *
 */
@Controller
@RequestMapping(value = "/{userId}/{appId}/manager/service/{csrfToken}/testquery")
public class TestQueryServiceController extends BaseServiceController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	private static final String TYPE_NAME = "testquery";

	@Autowired
	private ServiceController controller;

	@Autowired
	private GetTestQueryByKey getTestQueryByKey;

	@Autowired
	@Qualifier("addTestQuery")
	private AddAppModel<TestQuery> addTestQuery;

	@Autowired
	private UsersQueryService queryService;

	/**
	 * テストクエリを実行します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param csrfToken CSRFトークン
	 * @param bindingQuery クエリ
	 * @param session セッション
	 * @return 実行結果
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public List<Map<String, Object>> query1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("csrfToken") String csrfToken,
			@RequestBody VmQuery bindingQuery,
			HttpSession session) {
		logger.debug("query1");
		AuthUtils.checkCsrfToken(session, csrfToken);

		TestQuery query1 = new TestQuery(
				new UserIdImpl(userId),
				new AppIdImpl(appId),
				bindingQuery.toXml());

		this.controller.add(query1, csrfToken, session,
				this.addTestQuery, this.getTestQueryByKey, TYPE_NAME);

		QueryObject query = this.queryService.findTestQuery(
				new UserIdImpl(userId), new AppIdImpl(appId));

		Map<String, String> map = new HashMap<>();
		return query.execute(map);
	}
}
