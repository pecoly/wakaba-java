package net.jp.yamabuki.controller.users;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.jp.yamabuki.controller.BaseServiceController;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.service.query.users.UsersQueryService;
import net.jp.yamabuki.service.update.users.UsersUpdateService;
import net.jp.yamabuki.util.AuthUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/{userId}/{appId}/users/service/{csrfToken}/update")
public class UsersUpdateServiceController extends BaseServiceController {

	@ResponseBody
	@RequestMapping(value = "{path0}", method = RequestMethod.POST)
	public Map<String, Object> updatePost1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			@RequestBody Map<String, String> map,
			HttpSession session) {
		logger.debug("updatePost1");

		AuthUtils.checkCsrfToken(session, map);

		UpdateObject update = this.updateService.find(
				new UserIdImpl(userId), new AppIdImpl(appId), "update", path0);

		int count = update.execute(map);
		Map<String, Object> result = new HashMap<>();
		result.put("count", count);
		return result;
	}
	/*
	@RequestMapping(value = "/download/{path0}", method = RequestMethod.POST)
	public void downloadGet1(
			@PathVariable("userId") String userId,
			@PathVariable("appId") String appId,
			@PathVariable("path0") String path0,
			HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		logger.debug("downloadGet1");

		String name = StringUtils.join(
				new String[] {userId, appId, "query", path0},
				"/");

		QueryObject query = this.queryService.find(userId, name);

		Map<String, String> map = HttpUtil.getStringMap(request);


		QueryResult result = query.execute(map);
		try {
			byte[] byteArray = new String("abc").getBytes("UTF-8");
			response.setContentType("application/octet-stream");
			response.setContentLength(byteArray.length);
			response.setHeader("Content-disposition",
					"attachment; filename=\"" + "def.txt" + "\"");
			FileCopyUtils.copy(byteArray, response.getOutputStream());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/

	@Autowired
	private UsersQueryService queryService;

	@Autowired
	private UsersUpdateService updateService;

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
}
