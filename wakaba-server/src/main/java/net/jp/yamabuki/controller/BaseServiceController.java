package net.jp.yamabuki.controller;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.exception.QueryNotFoundException;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.exception.UpdateNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class BaseServiceController {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");


	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public void handleException(final Exception e,
			/*final HttpServletRequest request,*/
			HttpServletResponse response,
			Writer writer) throws IOException {

		if (e instanceof QueryNotFoundException) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
		else if (e instanceof UpdateNotFoundException) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
		else if (e instanceof ResourceNotFoundException) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
		else if (e instanceof QueryException) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		else if (e instanceof UpdateException) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}

		logger.warn(e.getMessage(), e);

		writer.write(e.getMessage());
	}
}
