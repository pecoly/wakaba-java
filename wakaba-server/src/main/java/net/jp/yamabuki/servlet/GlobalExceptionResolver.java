package net.jp.yamabuki.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.exception.QueryNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class GlobalExceptionResolver implements HandlerExceptionResolver {
	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
 
	@Override
	public ModelAndView resolveException(
			HttpServletRequest request,
			HttpServletResponse response,
			Object object,
			Exception ex) {
		logger.error(ex.getMessage(), ex);
		
		if (ex instanceof QueryNotFoundException) {
			throw (QueryNotFoundException)ex;
		}
		else if (ex instanceof NoSuchBeanDefinitionException) {
			throw new RuntimeException(ex.getMessage(), ex.getCause());
		}
		else if (ex instanceof RuntimeException) {
			throw (RuntimeException)ex;
		}

		ModelAndView mav = new ModelAndView();
		return mav;
	}
}
