package net.jp.yamabuki;

import javax.servlet.ServletContext;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

import org.apache.ibatis.session.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
public class YamabukiContext {

	private String dbDriver;

	private String dbBaseUrl;

	private String dbType;

	private String homeDirectoryPath;

	private Configuration configuration;

	private String contextPath;

	private WebApplicationContext springContext;

	private ServletContext servletContext;

	public String getHomeDirectoryPath() {
		return this.homeDirectoryPath;
	}

	public void setHomeDirectoryPath(String homeDirectoryPath) {
		this.homeDirectoryPath = homeDirectoryPath;
	}

	public Configuration getConfiguration() {
		return this.configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public String getBasePath() {
		return this.contextPath;
	}

	public String getUserBasePath(UserId userId) {
		return this.contextPath + "/" + userId.getValue();
	}

	public String getAppBasePath(UserId userId, AppId appId) {
		return this.contextPath + "/" + userId.getValue() + "/" + appId.getValue();
	}

	public String getUsersBasePath(UserId userId, AppId appId) {
		return this.getAppBasePath(userId, appId) + "/users";
	}

	public String getUsersServicePath(UserId userId, AppId appId) {
		return this.getUsersBasePath(userId, appId) + "/service";
	}

	public String getUsersViewPath(UserId userId, AppId appId) {
		return this.getUsersBasePath(userId, appId) + "/view";
	}

	public String getManagerServiceBasePath(UserId userId, AppId appId) {
		return contextPath + "/" + userId.getValue() + "/" + appId.getValue() + "/manager/service";
	}

	public String getManagerViewPath(UserId userId, AppId appId) {
		return contextPath + "/" + userId.getValue() + "/" + appId.getValue() + "/manager/view";
	}

	public WebApplicationContext getSpringContext() {
		return this.springContext;
	}

	public void setSpringContext(WebApplicationContext springContext) {
		this.springContext = springContext;
	}

	public ServletContext getServletContext() {
		return this.servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public String getDbDriver() {
		return this.dbDriver;
	}

	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}

	public String getDbBaseUrl() {
		return this.dbBaseUrl;
	}

	public void setDbBaseUrl(String dbBaseUrl) {
		this.dbBaseUrl = dbBaseUrl;
	}

	public String getDbType() {
		return this.dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
}
