package net.jp.yamabuki.security;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.util.HttpUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 * 認証成功時の処理を行うクラス。
 *
 */
public class YamabukiAuthenticationSuccessHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		logger.debug("onAuthenticationSuccess");


		System.out.println(request.getServletPath());
		String[] pathList = request.getServletPath().split("/");
		String userId = "";
		String appId = "";
		if (pathList.length >= 3) {
			userId = pathList[1];
			appId = pathList[2];
		}

		for (Entry<String, String> kv : HttpUtils.getStringMap(request).entrySet()) {
			if (kv.getKey().equals("userId")) {
				userId = kv.getValue();
			}
			else if (kv.getKey().equals("appId")) {
				appId = kv.getValue();
			}
		}

		logger.debug("userId : " + userId);
		logger.debug("appId : " + appId);

		RedirectStrategy redirectStrategy = getRedirectStrategy();
		//redirectStrategy.sendRedirect(request, response, "/" + userId + "/" + appId + "/users/view");
		redirectStrategy.sendRedirect(request, response, "/" + userId + "/" + appId + "/manager/view");

		return;//super.onAuthenticationSuccess(request, response, authentication);
	}

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");
}
