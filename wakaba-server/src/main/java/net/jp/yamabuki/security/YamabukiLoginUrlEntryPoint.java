package net.jp.yamabuki.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.util.HttpUtils;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

public class YamabukiLoginUrlEntryPoint
	extends LoginUrlAuthenticationEntryPoint {

	private static final String AJAX_REQUEST_IDENTIFIER = "XMLHttpRequest";

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
		throws IOException, ServletException {

		if (AJAX_REQUEST_IDENTIFIER.equals(request.getHeader("X-Requested-With"))) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		super.commence(request, response, authException);
	}

	@Override
	protected String buildRedirectUrlToLoginPage(
		HttpServletRequest request, HttpServletResponse response,
		AuthenticationException authException) {
		System.out.println(request.getServletPath());

		String[] pathList = HttpUtils.getPathList(request.getServletPath());
		String userId = pathList[0];
		String appId = pathList[1];
		String role = pathList[2];

		String redirectUrl = super.buildRedirectUrlToLoginPage(request, response, authException);
		if (userId.equals("") || appId.equals("")) {
			return redirectUrl;
		}

		return redirectUrl + userId + "/" + appId + "/login";
	}


}