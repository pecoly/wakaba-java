package net.jp.yamabuki.security;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jp.yamabuki.util.HttpUtils;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * 認証成功時の処理を行うクラス。
 *
 */
public class YamabukiAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		String userId = "";
		String appId = "";

		request.getParameterMap();

		for (Entry<String, String> kv : HttpUtils.getStringMap(request).entrySet()) {
			if (kv.getKey().equals("userId")) {
				userId = kv.getValue();
			}
			else if (kv.getKey().equals("appId")) {
				appId = kv.getValue();
			}
		}

		/*
		System.out.println(userId);
		System.out.println(appId);
		*/

		String message = "";
		if (exception instanceof BadCredentialsException) {
			message = "error01";
		}
		else {
			message = "unknown";
		}

		RedirectStrategy redirectStrategy = getRedirectStrategy();
		redirectStrategy.sendRedirect(
				request, response, "/" + userId + "/" + appId + "/login?error=" + message);

	}
}
