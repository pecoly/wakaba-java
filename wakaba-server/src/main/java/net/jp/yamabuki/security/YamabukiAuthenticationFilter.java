package net.jp.yamabuki.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import net.jp.yamabuki.security.authority.AppIdAuthority;
import net.jp.yamabuki.security.authority.MainAuthority;
import net.jp.yamabuki.security.authority.SubAuthority;
import net.jp.yamabuki.security.authority.UserIdAuthority;
import net.jp.yamabuki.util.HttpUtils;

import org.springframework.web.filter.GenericFilterBean;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.access.AccessDeniedException;

public class YamabukiAuthenticationFilter extends GenericFilterBean {
	@Override
	public void doFilter(ServletRequest request,
	ServletResponse response,
	FilterChain chain) throws IOException,
	ServletException {

		HttpServletRequest httpreq = (HttpServletRequest)request;

		String[] pathList = HttpUtils.getPathList(
				((HttpServletRequest)request).getServletPath());
		String userId = pathList[0];
		String appId = pathList[1];
		String role = pathList[2];

		/*
		System.out.println(userId);
		System.out.println(appId);
		System.out.println(main);
		*/

		boolean userIdSuccess = false;
		boolean appIdSuccess = false;
		boolean roleSuccess = false;
		Authentication auth = (Authentication) SecurityContextHolder
				.getContext().getAuthentication();

		for (GrantedAuthority x : auth.getAuthorities()) {
			String y = x.getAuthority();

			if (x instanceof MainAuthority) {
				for (String z : y.split(",")) {
					if (z.equals(role)) {
						roleSuccess = true;
						break;
					}
				}
			}
			else if (x instanceof SubAuthority) {

			}
			else if (x instanceof UserIdAuthority) {
				if (y.equals(userId)) {
					userIdSuccess = true;
				}
			}
			else if (x instanceof AppIdAuthority) {
				if (y.equals(appId)) {
					appIdSuccess = true;
				}
			}
		}

		if (!userIdSuccess || !appIdSuccess || !roleSuccess) {
			throw new AccessDeniedException("");
		}

		chain.doFilter(request, response);
	}
}
