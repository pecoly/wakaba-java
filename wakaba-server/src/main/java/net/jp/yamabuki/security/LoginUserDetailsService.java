package net.jp.yamabuki.security;

import net.jp.yamabuki.service.manager.GetLoginUserByKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class LoginUserDetailsService implements UserDetailsService {
	@Override
	public UserDetails loadUserByUsername(String loginId) {
		User user = this.getLoginUserByKey.execute(loginId);
		if (user == null) {
			throw new UsernameNotFoundException("User not found.");
		}
		
		return user;
	}
	
	@Autowired
	private GetLoginUserByKey getLoginUserByKey;
}
