package net.jp.yamabuki;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import net.jp.yamabuki.io.Config;
import net.jp.yamabuki.io.ConfigPropertiesFile;
import net.jp.yamabuki.io.DefaultConfig;
import net.jp.yamabuki.service.initializer.Initializer;
import net.jp.yamabuki.util.FileUtils;
import net.jp.yamabuki.util.PathUtils;
import net.jp.yamabuki.util.StringUtils;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * リスナークラス。
 * 以下が指定できるオプションです。
 * ・wakaba.home 指定しない場合は${user.home}/wakabaとなります。
 *
 *
 */
public class ContextListener implements ServletContextListener {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Override
	public void contextInitialized(ServletContextEvent event) {
		String wakabaHomeDirPath = System.getProperty("wakaba.home");
		if (wakabaHomeDirPath == null) {
			wakabaHomeDirPath = PathUtils.combine(System.getProperty("user.home"), "wakaba");
			System.setProperty("wakaba.home", wakabaHomeDirPath);
		}

		File wakabaHomeDir = new File(wakabaHomeDirPath);
		if (!wakabaHomeDir.exists()) {
			wakabaHomeDir.mkdirs();
		}

		ServletContext servletContext = event.getServletContext();
		WebApplicationContext springContext
				= WebApplicationContextUtils.getWebApplicationContext(servletContext);

		YamabukiContext context = springContext.getBean(YamabukiContext.class);

		context.setServletContext(servletContext);
		context.setSpringContext(springContext);
		context.setContextPath(servletContext.getContextPath());

		Config config = this.initializeConfig(wakabaHomeDirPath);
		this.initializeDataSource(springContext, config);

		context.setDbType(config.getDbType());
		context.setDbBaseUrl(config.getDbBaseUrl());
		context.setDbDriver(config.getDbDriver());
		/*
		System.setProperty("dbDriver", config.getDbDriver());
		System.setProperty("dbUrl", config.getDbUrl());
		System.setProperty("dbUsername", config.getDbUsername());
		System.setProperty("dbPassword", config.getDbPassword());
		*/

		this.inititalizeLogback(wakabaHomeDirPath, config);

		logger.debug("debug");
		logger.info("info");
		logger.warn("warn");
		logger.error("error");

		this.initializeDatabase(springContext);

		initializeJdbc(springContext);

		logger.info("wakaba.home : " + wakabaHomeDirPath);
		context.setHomeDirectoryPath(wakabaHomeDirPath);

		logger.info("db.driver : " + config.getDbDriver());
		logger.info("db.url : " + config.getDbUrl());
		logger.info("db.username : " + config.getDbUsername());
		logger.info("db.type : " + config.getDbType());

		DataSource dataSource = this.createDataSource();
		Configuration configuration = this.createConfiguration(dataSource);
		context.setConfiguration(configuration);
	}

	void inititalizeLogback(String wakabaHomeDirPath, Config config) {
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(context);
		context.reset();

		String logBackFilePath = PathUtils.combine(wakabaHomeDirPath, "logback.xml");

		// ログ設定ファイルが存在しないときは初期処理を行わない
		File file = new File(logBackFilePath);
		if (!file.exists()) {
			return;
		}

		String s = FileUtils.readFileToString(logBackFilePath);
		s = s.replace("${dbDriver}", config.getDbDriver());
		s = s.replace("${dbUrl}", config.getDbUrl());
		s = s.replace("${dbUsername}", config.getDbUsername());
		s = s.replace("${dbPassword}", config.getDbPassword());

		try (InputStream is = StringUtils.toByteArrayInputStream(s)) {
			try {
				configurator.doConfigure(is);
			}
			catch (JoranException ex) {
				ex.printStackTrace();
			}
		}
		catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

		StatusPrinter.printInCaseOfErrorsOrWarnings(context);
	}

	Config initializeConfig(String wakabaHomeDirPath) {
		String configFilePath = PathUtils.combine(wakabaHomeDirPath, "wakaba.config");
		File file = new File(configFilePath);
		if (!file.exists()) {
			return new DefaultConfig(wakabaHomeDirPath);
		}

		ConfigPropertiesFile configFile = new ConfigPropertiesFile();
		configFile.load(configFilePath);
		return configFile;
	}

	void initializeDataSource(WebApplicationContext springContext, Config config) {
		BasicDataSource dataSource = springContext.getBean(BasicDataSource.class);
		dataSource.setDriverClassName(config.getDbDriver());
		dataSource.setUrl(config.getDbUrl());
		dataSource.setUsername(config.getDbUsername());
		dataSource.setPassword(config.getDbPassword());
	}

	void initializeDatabase(WebApplicationContext springContext) {
		Initializer initializer = springContext.getBean(Initializer.class);
		initializer.initialize();
	}

	void initializeJdbc(WebApplicationContext springContext) {
		DataSourceConfig dataSourceConfig = springContext.getBean(DataSourceConfig.class);

		// JdbcTemplate初期設定
		JdbcTemplate jdbc = springContext.getBean(JdbcTemplate.class);
		jdbc.setMaxRows(dataSourceConfig.getMaxRows());

		// NamedParameterJdbcTemplate初期設定
		NamedParameterJdbcTemplate namedParmaeterJdbc
				= springContext.getBean(NamedParameterJdbcTemplate.class);
		((JdbcTemplate)namedParmaeterJdbc.getJdbcOperations())
				.setMaxRows(dataSourceConfig.getMaxRows());
	}

	DataSource createDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		dataSource.setUrl("jdbc:mysql://localhost/test");
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");

		return dataSource;
	}

	Configuration createConfiguration(DataSource dataSource) {
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		Environment environment = new Environment("development", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);
		return configuration;
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}
}
