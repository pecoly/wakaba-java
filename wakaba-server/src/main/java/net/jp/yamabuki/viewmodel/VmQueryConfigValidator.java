package net.jp.yamabuki.viewmodel;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class VmQueryConfigValidator {
	public VmQueryConfigValidator() {
	}
	
	public VmQueryConfigValidator(String name, Map<String, String> argumentList) {
		this.name = name;
		this.argumentList = argumentList;
	}

	public String getName() {
		return this.name;
	}
	private String name;
	
	public Set<Entry<String, String>> getArgumentList() {
		return this.argumentList.entrySet();
	}
	
	public String getArgument(String name) {
		return this.argumentList.get(name);
	}
	
	private Map<String, String> argumentList;
}
