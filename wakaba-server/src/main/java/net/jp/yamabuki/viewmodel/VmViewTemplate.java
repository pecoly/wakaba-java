package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.ViewTemplate;

public class VmViewTemplate {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String content;

	public VmViewTemplate() {

	}

	public VmViewTemplate(ViewTemplate viewTemplate) {
		Argument.isNotNull(viewTemplate, "viewTemplate");

		this.id = viewTemplate.getId().getValue();
		this.userId = viewTemplate.getUserId().getValue();
		this.appId = viewTemplate.getAppId().getValue();
		this.name = viewTemplate.getName();
		this.content = viewTemplate.getContent();
	}

	public String getId() {
		return this.id;
	}

	public String getUserId() {
		return this.userId;
	}

	public String getAppId() {
		return this.appId;
	}

	public String getName() {
		return this.name;
	}

	public String getContent() {
		return this.content;
	}
}
