package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.Plugin;

public class VmPlugin {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String description;

	public VmPlugin() {

	}

	public VmPlugin(Plugin template) {
		this.id = template.getId().getValue();
		this.userId = template.getUserId().getValue();
		this.appId = template.getAppId().getValue();
		this.name = template.getName();
		this.description = template.getDescription();
	}

	public String getId() {
		return this.id;
	}

	public String getUserId() {
		return this.userId;
	}

	public String getAppId() {
		return this.appId;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}
}
