package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.App;

public class VmApp extends VmUserModel {
	private String appId;
	private String appName;

	public VmApp() {
	}

	public VmApp(App app) {
		super(app);

		this.appId = app.getAppId().getValue();
		this.appName = app.getAppName();
	}

	public String getAppId() {
		return this.appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return this.appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
}
