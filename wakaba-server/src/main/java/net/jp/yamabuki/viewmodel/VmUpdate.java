package net.jp.yamabuki.viewmodel;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.UpdateConfigXmlReader;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.update.config.UpdateConfig;
import net.jp.yamabuki.util.StringUtils;

import org.apache.ibatis.session.Configuration;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl;

@JsonIgnoreProperties(ignoreUnknown=true)
public class VmUpdate {

	private String id;

	private String name = "";

	private String type = "";

	private String sql = "";

	private String connection = "";

	private List<VmUpdateConfigParameter> parameterList = new ArrayList<>();

	private String lastModified;

	public VmUpdate() {
	}

	public VmUpdate(Update update, Configuration configuration) {
		InputStream inputStream = StringUtils.toByteArrayInputStream(
				update.getXml());

		UpdateConfigXmlReader reader = new UpdateConfigXmlReader(
				configuration, inputStream);
		UpdateConfig config = reader.read();

		this.id = update.getId().getValue();
		this.type = update.getType();
		this.name = update.getName();
		this.sql = config.getNode().getStringBody();
		this.connection = config.getConnection();

		for (ParameterConfig x : config.getParameterList()) {
			List<VmUpdateConfigValidator> validatorList = new ArrayList<>();

			this.parameterList.add(
					new VmUpdateConfigParameter(x.getName(), x.getConverter(),
							validatorList));
		}
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<VmUpdateConfigParameter> getParameterList() {
		return this.parameterList;
	}

	public void setParameterList(List<VmUpdateConfigParameter> parameterList) {
		this.parameterList = parameterList;
	}


	public String getSql() {
		return this.sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getConnection() {
		return this.connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	public String toXml() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		}
		catch (ParserConfigurationException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}

		Document doc = db.newDocument();

		Element queryE = doc.createElement("update");

		// connection
		Element connectionE = doc.createElement("connection");
		Text connectionT = doc.createTextNode(this.connection);
		connectionE.appendChild(connectionT);
		queryE.appendChild(connectionE);

		// sql
		Element sqlE = doc.createElement("sql");
		Text sqlT = doc.createTextNode(this.sql);
		sqlE.appendChild(sqlT);
		queryE.appendChild(sqlE);

		// parameters
		Element parametersE = this.createParametersElement(doc);
		queryE.appendChild(parametersE);

		// validator
		Element validatorE = doc.createElement("validator");
		queryE.appendChild(validatorE);

		doc.appendChild(queryE);
		doc.setXmlStandalone(true);

		TransformerFactory tf = TransformerFactoryImpl.newInstance();

		Transformer transformer = null;
		try {
			transformer = tf.newTransformer();
		}
		catch (TransformerConfigurationException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}

		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);

		DOMSource source = new DOMSource(doc);
		try {
			transformer.transform(source, result);
		}
		catch (TransformerException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}

		return writer.toString();
	}

	Element createParametersElement(Document doc) {
		Element parametersE = doc.createElement("parameters");
		for (VmUpdateConfigParameter x : this.parameterList) {
			Element parameterE = doc.createElement("parameter");

			Element nameE = doc.createElement("name");
			Text nameT = doc.createTextNode(x.getName());
			nameE.appendChild(nameT);
			parameterE.appendChild(nameE);

			Element converterE = doc.createElement("converter");
			Text converterT = doc.createTextNode(x.getConverter());
			converterE.appendChild(converterT);
			parameterE.appendChild(converterE);

			parametersE.appendChild(parameterE);
		}

		return parametersE;
	}

	@Override
	public String toString() {
		return this.toXml();
	}
}
