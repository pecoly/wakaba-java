package net.jp.yamabuki.viewmodel;

import java.util.List;

public class VmQueryConfigParameter {
	public VmQueryConfigParameter() {
	}
	
	public VmQueryConfigParameter(String name, String converter,
			List<VmQueryConfigValidator> validatorList) {
		this.name = name;
		this.converter = converter;
		this.validatorList = validatorList;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private String name;
	
	public String getConverter() {
		return this.converter;
	}
	
	public void setConverter(String converter) {
		this.converter = converter;
	}
	
	private String converter;
	
	public List<VmQueryConfigValidator> getValidatorList() {
		return this.validatorList;
	}
	
	public void setValidatorList(List<VmQueryConfigValidator> validatorList) {
		this.validatorList = validatorList;
	}
	
	private List<VmQueryConfigValidator> validatorList;
}
