package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.resource.Js;

public class VmJs extends VmAppModel {

	private String name;

	private String content;

	public VmJs() {
	}

	public VmJs(String id, String userId, String appId,
			String name, String content, String lastModified) {
		super(id, userId, appId, lastModified);

		this.name = name;
		this.content = content;
	}

	public VmJs(Js js) {
		super(js);

		this.name = js.getName();
		this.content = js.getContent();
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
