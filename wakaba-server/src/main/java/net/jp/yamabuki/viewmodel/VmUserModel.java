package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.UserModel;
import net.jp.yamabuki.util.DateTimeUtils;

public class VmUserModel {
	private String id;

	private String userId;

	private String lastModified;

	/**
	 * デフォルトコンストラクタ。
	 * 削除しないこと。
	 */
	public VmUserModel() {
	}

	public VmUserModel(String id, String userId,
			String lastModified) {
		this.id = id;
		this.userId = userId;
		this.lastModified = lastModified;
	}

	public VmUserModel(UserModel model) {
		Argument.isNotNull(model, "model");

		this.id = model.getId().getValue();
		this.userId = model.getUserId().getValue();
		this.lastModified = DateTimeUtils.toStringYYYYMMDDHHMISS(
				model.getLastModified());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
}
