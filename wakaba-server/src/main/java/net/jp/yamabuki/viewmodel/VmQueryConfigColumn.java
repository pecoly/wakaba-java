package net.jp.yamabuki.viewmodel;

public class VmQueryConfigColumn {
	public VmQueryConfigColumn() {
	}
		
	public VmQueryConfigColumn(String name, String accessor) {
		this.name = name;
		this.accessor = accessor;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private String name;
	
	public String getAccessor() {
		return this.accessor;
	}
	
	public void setAccessor(String accessor) {
		this.accessor = accessor;
	}
	
	private String accessor;
}
