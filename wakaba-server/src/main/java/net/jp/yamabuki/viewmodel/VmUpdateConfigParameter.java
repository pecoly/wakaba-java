package net.jp.yamabuki.viewmodel;

import java.util.List;

public class VmUpdateConfigParameter {

	private String name;

	private String converter;

	private List<VmUpdateConfigValidator> validatorList;

	public VmUpdateConfigParameter() {
	}

	public VmUpdateConfigParameter(String name, String converter,
			List<VmUpdateConfigValidator> validatorList) {
		this.name = name;
		this.converter = converter;
		this.validatorList = validatorList;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConverter() {
		return this.converter;
	}

	public void setConverter(String converter) {
		this.converter = converter;
	}

	public List<VmUpdateConfigValidator> getValidatorList() {
		return this.validatorList;
	}

	public void setValidatorList(List<VmUpdateConfigValidator> validatorList) {
		this.validatorList = validatorList;
	}
}
