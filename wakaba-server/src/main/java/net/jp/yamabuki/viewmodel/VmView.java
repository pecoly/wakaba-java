package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.View;

public class VmView extends VmAppModel {

	private String name;

	private String content;

	public VmView(String id, String userId, String appId,
			String name, String content, String lastModified) {
		super(id, userId, appId, lastModified);

		this.name = name;
		this.content = content;
	}

	public VmView(View view) {
		super(view);
	}

	public String getName() {
		return this.name;
	}

	public String getContent() {
		return this.content;
	}
}