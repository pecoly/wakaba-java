package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.util.StringUtils;

public class VmJdbcConnection {

	private String id;

	private String name;

	private String driver;

	private String url;

	private String username;

	/**
	 * デフォルトコンストラクタ。
	 * Springが必要とするため削除しないこと。
	 */
	public VmJdbcConnection() {
	}

	/**
	 * コンストラクタ。
	 * @param jdbcConnection Jdbcコネクション
	 */
	public VmJdbcConnection(JdbcConnection jdbcConnection) {
		this.id = jdbcConnection.getId().getValue();
		this.name = jdbcConnection.getName();
		this.driver = jdbcConnection.getDriver();
		this.url = jdbcConnection.getUrl();
		this.username = jdbcConnection.getUsername();
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getDriver() {
		return this.driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/*
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String password;
	*/

	public boolean isValid() {
		return !StringUtils.isBlank(this.driver);
	}

	/*
	public Element createElement(Document doc) {
		Element connectionE = doc.createElement("connection");

		// driver
		Element driverE = doc.createElement("driver");
		if (!StringUtility.isBlank(this.driver)) {
			Text driverT = doc.createTextNode(this.driver);
			driverE.appendChild(driverT);
		}

		connectionE.appendChild(driverE);

		// url
		Element urlE = doc.createElement("url");
		if (!StringUtility.isBlank(this.url)) {
			Text urlT = doc.createTextNode(this.url);
			urlE.appendChild(urlT);
		}

		connectionE.appendChild(urlE);

		// username
		Element usernameE = doc.createElement("username");
		if (!StringUtility.isBlank(this.username)) {
			Text usernameT = doc.createTextNode(this.username);
			usernameE.appendChild(usernameT);
		}

		connectionE.appendChild(usernameE);

		// password
		Element passwordE = doc.createElement("password");
		if (!StringUtility.isBlank(this.password)) {
			Text passwordT = doc.createTextNode(this.password);
			passwordE.appendChild(passwordT);
		}

		connectionE.appendChild(passwordE);

		return connectionE;
	}
	*/
}
