package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.util.DateTimeUtils;

public class VmAppModel {
	private String id;

	private String userId;

	private String appId;

	private String lastModified;

	/**
	 * デフォルトコンストラクタ。
	 * 削除しないこと。
	 */
	public VmAppModel() {
	}

	public VmAppModel(String id, String userId, String appId, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.lastModified = lastModified;
	}

	public VmAppModel(AppModel model) {
		Argument.isNotNull(model, "model");

		this.id = model.getId().getValue();
		this.userId = model.getUserId().getValue();
		this.appId = model.getAppId().getValue();
		this.lastModified = DateTimeUtils.toStringYYYYMMDDHHMISS(
				model.getLastModified());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAppId() {
		return this.appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
}
