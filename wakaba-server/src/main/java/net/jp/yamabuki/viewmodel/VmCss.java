package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.resource.Css;

/**
 * ビューに表示するCssクラス。
 *
 */
public class VmCss extends VmAppModel {
	private String name;

	private String content;

	/**
	 * デフォルトコンストラクタ。
	 * 削除しないこと。
	 */
	public VmCss() {
	}

	/**
	 * コンストラクタ。
	 * @param id CssId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param content 内容
	 * @param lastModified 最終更新日時
	 */
	public VmCss(String id, String userId, String appId,
			String name, String content, String lastModified) {
		super(id, userId, appId, lastModified);

		this.name = name;
		this.content = content;
	}

	/**
	 * コンストラクタ。
	 * @param css Css
	 */
	public VmCss(Css css) {
		super(css);

		this.name = css.getName();
		this.content = css.getContent();
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
