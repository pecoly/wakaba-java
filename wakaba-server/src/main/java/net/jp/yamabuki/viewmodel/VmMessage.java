package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.resource.Message;

public class VmMessage extends VmAppModel {

	private String code;

	private String localeId;

	private String message;

	public VmMessage(String id, String userId, String appId,
			String code, String localeId, String message, String lastModified) {
		super(id, userId, appId, lastModified);

		this.code = code;
		this.localeId = localeId;
		this.message = message;
	}

	public VmMessage(Message message) {
		super(message);

		this.code = message.getCode();
		this.localeId = message.getLocaleId().getValue();
		this.message = message.getMessage();
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLocaleId() {
		return this.localeId;
	}

	public void setLocaleId(String localeId) {
		this.localeId = localeId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
