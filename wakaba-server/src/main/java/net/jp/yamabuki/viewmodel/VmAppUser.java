package net.jp.yamabuki.viewmodel;

import net.jp.yamabuki.model.AppUser;

public class VmAppUser {
	public VmAppUser() {

	}

	public VmAppUser(String id, String loginId,
			String mainAuthority) {
		this.id = id;
		this.loginId = loginId;
		this.mainAuthority = mainAuthority;
	}

	public VmAppUser(AppUser appUser) {
		this(appUser.getId().getValue(),
			appUser.getLoginId(),
			appUser.getMainAuthority());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	private String loginId;


	public String getMainAuthority() {
		return this.mainAuthority;
	}

	public void setMainAuthority(String mainAuthority) {
		this.mainAuthority = mainAuthority;
	}

	private String mainAuthority;

}
