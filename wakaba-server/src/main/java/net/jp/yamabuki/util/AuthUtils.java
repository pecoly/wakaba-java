package net.jp.yamabuki.util;

import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 認証ユーティリティクラス。
 *
 */
public final class AuthUtils {
	/**
	 * コンストラクタ。
	 */
	private AuthUtils() {
	}

	public static void checkCsrfToken(HttpSession session, Map<String, String> map) {
		String sessionId = map.get("CSRF_TOKEN");
		if (sessionId == null) {
			throw new IllegalStateException("'CSRF_TOKEN' should not be null.");
		}

		checkCsrfToken(session, sessionId);
	}

	public static void checkCsrfToken(HttpSession session, String sessionId) {
		if (!sessionId.equals(session.getId())) {
			throw new IllegalStateException("Session invalid.");
		}
	}
}
