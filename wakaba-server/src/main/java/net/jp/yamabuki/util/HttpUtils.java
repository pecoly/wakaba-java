package net.jp.yamabuki.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

/**
 * Httpユーティリティクラス。
 *
 */
public final class HttpUtils {
	/**
	 * コンストラクタ。
	 */
	private HttpUtils() {
	}

	/**
	 * リクエストパラメータを文字列マップで取得します。
	 * @param request リクエスト
	 * @return リクエストパラメータの文字列マップ
	 */
	public static Map<String, String> getStringMap(HttpServletRequest request) {
		Map<String, String> map = new LinkedHashMap<>();

		for (Iterator<?> i = request.getParameterMap().entrySet().iterator(); i.hasNext();) {
			Map.Entry<?, ?> kv = (Map.Entry<?, ?>)i.next();
			String key = (String)kv.getKey();
			String[] values = (String[])kv.getValue();
			map.put(key, values[0]);
		}

		return map;
	}

	public static String getQueryString(HttpServletRequest request) {
		Map<String, String> map = getStringMap(request);

		StringBuilder sb = new StringBuilder();

		for (Entry<String, String> kv : map.entrySet()) {
			String key = kv.getKey();
			if (!key.toUpperCase().equals(key)) {
				sb.append(key);
				sb.append("=");
				sb.append(kv.getValue());
			}
		}

		return sb.toString();
	}

	public static Map<String, String> getStringMap(HttpSession session) {
		Map<String, String> map = new LinkedHashMap<>();
		for (Enumeration<?> e = session.getAttributeNames(); e.hasMoreElements();) {
			String name = (String)e.nextElement();
			Object value = session.getAttribute(name);
			if (value != null && value instanceof String) {
				map.put(name, (String)value);
			}
		}
		return map;
	}

	/**
	 * 最終更新日時を取得します。
	 * @param request リクエスト
	 * @return 最終更新日時
	 */
	public static DateTime getLastModified(HttpServletRequest request) {
		return DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(
				request.getHeader("lastModified"));
	}

	public static String[] getPathList(String servletPath) {
		String[] pathList = servletPath.split("/");
		if (pathList.length >= 4) {
			return new String[] {
					pathList[1], pathList[2], pathList[3],
			};
		}

		return new String[] {
				"", "", "",
		};
	}

}
