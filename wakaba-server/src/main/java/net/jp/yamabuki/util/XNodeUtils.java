package net.jp.yamabuki.util;

import net.jp.yamabuki.exception.EmptyXmlValueException;
import net.jp.yamabuki.exception.XmlNodeNotFoundException;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;

public final class XNodeUtils {


	public static String readNodeWithEmptyCheck(XPathParser parser, String name) {

		XNode targetNode = parser.evalNode(name);
		if (targetNode == null) {
			throw new XmlNodeNotFoundException(name);
		}

		String body = targetNode.getStringBody();
		if (body == null) {
			throw new EmptyXmlValueException(name);
		}

		return body;
	}

	public static String readNodeWithNullCheck(XPathParser parser, String name) {

		XNode targetNode = parser.evalNode(name);
		if (targetNode == null) {
			throw new XmlNodeNotFoundException(name);
		}

		String body = targetNode.getStringBody();

		return body == null ? "" : body;
	}

	public static String readNodeWithEmptyCheck(XNode parentNode, String parentName, String targetName) {

		XNode targetNode = parentNode.evalNode(targetName);
		if (targetNode == null) {
			throw new XmlNodeNotFoundException(parentName + "/" + targetName);
		}

		String body = targetNode.getStringBody();
		if (body == null) {
			throw new EmptyXmlValueException(parentName + "/" + targetName);
		}

		return body;
	}

	public static String readNodeWithNullCheck(XNode parentNode, String parentName, String targetName) {

		XNode targetNode = parentNode.evalNode(targetName);
		if (targetNode == null) {
			throw new XmlNodeNotFoundException(parentName + "/" + targetName);
		}

		String body = targetNode.getStringBody();

		return body == null ? "" : body;
	}

	public static String toStringWithEmptyCheck(XNode node, String name) {
		if (node == null) {
			throw new XmlNodeNotFoundException(name);
		}

		String body = node.getStringBody();
		if (body == null) {
			throw new EmptyXmlValueException(name);
		}

		return body;
	}
}
