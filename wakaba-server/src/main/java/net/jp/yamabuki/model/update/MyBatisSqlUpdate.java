package net.jp.yamabuki.model.update;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.mybatis.MyBatisNode;
import net.jp.yamabuki.model.validator.MapValidator;
import net.jp.yamabuki.model.validator.UpdateParameterListValidator;

import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class MyBatisSqlUpdate implements UpdateObject {
	private UpdateParameterListValidator validator1;

	private MapValidator validator2;

	private MyBatisNode myBatisNode;

	private NamedParameterJdbcTemplate jdbc;

	public void initialize(DataSource dataSource,
			MyBatisNode myBatisNode,
			List<UpdateParameter> parameterList, MapValidator validator) {
		Argument.isNotNull(dataSource, "dataSource");
		Argument.isNotNull(myBatisNode, "myBatisNode");
		Argument.isNotNull(parameterList, "parameterList");
		Argument.isNotNull(validator, "validator");

		System.out.println("MyBatisSqlUpdate");

		this.myBatisNode = myBatisNode;
		this.validator1 = new UpdateParameterListValidator(parameterList);
		this.validator2 = validator;

		this.jdbc = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public int execute(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		String error = this.validator1.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		error = this.validator2.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		Map<String, Object> objectParameterList = this.validator1.getObjectParameterList();
		String sql = this.myBatisNode.getSql(objectParameterList);

		try {
			return this.jdbc.update(sql, objectParameterList);
		}
		catch (BadSqlGrammarException ex) {
			throw new QueryException(WakabaErrorCode.BAD_SQL, ex);
		}
	}
}