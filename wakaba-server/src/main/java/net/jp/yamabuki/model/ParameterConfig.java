package net.jp.yamabuki.model;

import net.jp.yamabuki.check.Argument;

public class ParameterConfig {

	private final String name;

	private final String converter;

	private final Iterable<ValidatorConfig> validatorList;

	public ParameterConfig(String name, String converter,
			Iterable<ValidatorConfig> validatorList) {
		Argument.isNotBlank(name, "name");
		Argument.isNotNull(converter, "converter");
		Argument.isNotNull(validatorList, "validatorList");

		this.name = name;
		this.converter = converter;
		this.validatorList = validatorList;
	}

	public String getName() {
		return this.name;
	}

	public String getConverter() {
		return this.converter;
	}

	public Iterable<ValidatorConfig> getValidatorList() {
		return this.validatorList;
	}
}
