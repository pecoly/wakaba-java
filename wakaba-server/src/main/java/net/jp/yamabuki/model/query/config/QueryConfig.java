package net.jp.yamabuki.model.query.config;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.ParameterConfig;

import org.apache.ibatis.parsing.XNode;

/**
 * クエリの設定クラス。
 *
 */
public class QueryConfig {

	private final XNode node;

	private final String connection;

	private final Iterable<QueryConfigColumn> columnList;

	private final Iterable<ParameterConfig> parameterList;

	private final String validator;

	/**
	 * コンストラクタ。
	 * @param node Sqlノード
	 * @param connection コネクション名
	 * @param columnList カラム一覧
	 * @param parameterList パラメータ一覧
	 * @param validator 全体バリデータ
	 */
	public QueryConfig(XNode node, String connection,
			Iterable<QueryConfigColumn> columnList,
			Iterable<ParameterConfig> parameterList, String validator) {
		Argument.isNotNull(node, "node");
		Argument.isNotBlank(connection, "connection");
		Argument.isNotNull(columnList, "columnList");
		Argument.isNotNull(parameterList, "parameterList");
		Argument.isNotNull(validator, "validator");

		this.node = node;
		this.connection = connection;
		this.columnList = columnList;
		this.parameterList = parameterList;
		this.validator = validator;
	}

	/**
	 * Sqlノードを取得します。
	 * @return Sqlノード
	 */
	public XNode getNode() {
		return this.node;
	}

	/**
	 * コネクション名を取得します。
	 * @return コネクション名
	 */
	public String getConnection() {
		return this.connection;
	}

	/**
	 * カラム一覧を取得します。
	 * @return カラム一覧
	 */
	public Iterable<QueryConfigColumn> getColumnList() {
		return this.columnList;
	}

	/**
	 * パラメータ一覧を取得します。
	 * @return パラメータ一覧
	 */
	public Iterable<ParameterConfig> getParameterList() {
		return this.parameterList;
	}

	/**
	 * 全体バリデータを取得します。
	 * @return 全体バリデータ
	 */
	public String getValidator() {
		return this.validator;
	}
}
