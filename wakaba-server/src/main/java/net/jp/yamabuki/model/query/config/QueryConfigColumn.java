package net.jp.yamabuki.model.query.config;

import net.jp.yamabuki.check.Argument;

public class QueryConfigColumn {

	private final String name;

	private final String accessor;

	public QueryConfigColumn(String name, String accessor) {
		Argument.isNotBlank(name, "name");
		Argument.isNotBlank(accessor, "accessor");

		this.name = name;
		this.accessor = accessor;
	}

	public String getName() {
		return this.name;
	}

	public String getAccessor() {
		return this.accessor;
	}
}
