package net.jp.yamabuki.model.result;

/**
 * 結果を表すインターフェース。
 *
 */
public interface Result {
	/**
	 * 結果を取得します。
	 * @return 結果
	 */
	String getResult();
}
