package net.jp.yamabuki.model.query;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.DataSourceConfig;
import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.sql.mapper.ObjectMapper;
import net.jp.yamabuki.model.validator.QueryParameterListValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class SimpleSqlQuery implements QueryObject {

	private String sql;

	private QueryParameterListValidator validator1;

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private DataSourceConfig dataSourceConfig;

	public void initialize(DataSource dataSource,
			String sql, List<QueryParameter> parameterList) {
		Argument.isNotNull(dataSource, "dataSource");
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(parameterList, "parameterList");

		this.sql = sql;
		this.validator1 = new QueryParameterListValidator(parameterList);

		this.jdbc = new NamedParameterJdbcTemplate(dataSource);
		((JdbcTemplate)this.jdbc.getJdbcOperations())
				.setMaxRows(this.dataSourceConfig.getMaxRows());
	}

	@Override
	public List<Map<String, Object>> execute(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		String error = this.validator1.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		Map<String, Object> objectParameterList = this.validator1.getObjectParameterList();

		try {
			return this.jdbc.query(this.sql, objectParameterList, this.mapper);
		}
		catch (BadSqlGrammarException ex) {
			throw new QueryException(WakabaErrorCode.BAD_SQL, ex);
		}
	}
}