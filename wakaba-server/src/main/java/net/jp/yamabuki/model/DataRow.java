package net.jp.yamabuki.model;


public interface DataRow {
	Object get(String key);
}
