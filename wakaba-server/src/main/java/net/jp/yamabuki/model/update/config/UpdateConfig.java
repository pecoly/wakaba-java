package net.jp.yamabuki.model.update.config;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.ParameterConfig;

import org.apache.ibatis.parsing.XNode;

public class UpdateConfig {

	private final XNode node;

	private final String connection;

	private final Iterable<ParameterConfig> parameterList;

	private final String validator;

	public UpdateConfig(XNode node, String connection,
			Iterable<ParameterConfig> parameterList, String validator) {
		Argument.isNotNull(node, "node");
		Argument.isNotBlank(connection, "connection");
		Argument.isNotNull(parameterList, "parameterList");
		Argument.isNotNull(validator, "validator");

		this.node = node;
		this.connection = connection;
		this.parameterList = parameterList;
		this.validator = validator;
	}

	public XNode getNode() {
		return this.node;
	}

	public String getConnection() {
		return this.connection;
	}

	public Iterable<ParameterConfig> getParameterList() {
		return this.parameterList;
	}

	public String getValidator() {
		return this.validator;
	}
}
