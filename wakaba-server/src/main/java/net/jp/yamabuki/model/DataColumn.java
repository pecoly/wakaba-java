package net.jp.yamabuki.model;

public class DataColumn {
	public DataColumn(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	private String name;
	
	public String toString() {
		return this.name;
	}
}
