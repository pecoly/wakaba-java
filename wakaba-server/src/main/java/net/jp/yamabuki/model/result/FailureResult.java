package net.jp.yamabuki.model.result;

import org.springframework.stereotype.Component;

/**
 * 失敗を表すインターフェース。
 *
 */
@Component
public class FailureResult implements Result {
	@Override
	public String getResult() {
		return "failure";
	}
}
