package net.jp.yamabuki.model.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.DataColumn;
import net.jp.yamabuki.model.DataRow;

public class QueryResult {
	public QueryResult(
			Map<String, String> validationResult) {
		this.result = false;
		this.validationResult = validationResult;
	}
	
	public QueryResult(
			List<DataColumn> columnList,
			List<DataRow> rowList) {
		this.result = true;
		this.columnList = columnList;
		this.rowList = rowList;
	}
	
	public QueryResult(String error) {
		this.error = error;
	}
	
	public boolean getResult() {
		return this.result;
	}
	
	private boolean result;
	
	public List<DataColumn> getColumnList() {
		return this.columnList;
	}
	
	private List<DataColumn> columnList = new ArrayList<>();
	
	public List<DataRow> getRowList() {
		return this.rowList;
	}
	
	private List<DataRow> rowList = new ArrayList<>();
	
	public Map<String, String> getValidationResult() {
		return this.validationResult;
	}
	
	private Map<String, String> validationResult = new HashMap<>();

	public String getError() {
		return this.error;
	}
	
	private String error = "";
	
	public String toString() {
		if (this.columnList.size() == 1 && this.rowList.size() == 1) {
			return this.rowList.get(0).toString();
		}
		
		return "";
	}
}
