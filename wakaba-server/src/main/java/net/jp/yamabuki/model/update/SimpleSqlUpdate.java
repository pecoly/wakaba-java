package net.jp.yamabuki.model.update;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.UpdateParameterListValidator;

import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class SimpleSqlUpdate implements UpdateObject {

	private String sql;

	private UpdateParameterListValidator validator1;

	private NamedParameterJdbcTemplate jdbc;

	public void initialize(DataSource dataSource,
			String sql,
			List<UpdateParameter> parameterList) {
		Argument.isNotNull(dataSource, "dataSource");
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(parameterList, "parameterList");

		this.sql = sql;
		this.validator1 = new UpdateParameterListValidator(parameterList);
		this.jdbc = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public int execute(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		String error = this.validator1.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		Map<String, Object> objectParameterList = this.validator1.getObjectParameterList();

		try {
			return this.jdbc.update(this.sql, objectParameterList);
		}
		catch (BadSqlGrammarException ex) {
			throw new QueryException(WakabaErrorCode.BAD_SQL, ex);
		}
	}
}