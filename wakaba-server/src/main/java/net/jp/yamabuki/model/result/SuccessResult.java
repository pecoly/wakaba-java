package net.jp.yamabuki.model.result;

import org.springframework.stereotype.Component;

/**
 * 成功を表すクラス。
 *
 */
@Component
public class SuccessResult implements Result {
	@Override
	public String getResult() {
		return "success";
	}
}
