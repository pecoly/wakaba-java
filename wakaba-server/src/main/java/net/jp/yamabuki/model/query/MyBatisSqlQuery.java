package net.jp.yamabuki.model.query;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.QueryColumn;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.mybatis.MyBatisNode;
import net.jp.yamabuki.model.validator.MapValidator;
import net.jp.yamabuki.model.validator.QueryParameterListValidator;
import net.jp.yamabuki.service.query.NamedMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class MyBatisSqlQuery implements QueryObject {

	private MyBatisNode myBatisNode;

	private QueryParameterListValidator validator1;

	private MapValidator validator2;

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	private NamedMapper mapper;

	public void initialize(DataSource dataSource,
			MyBatisNode myBatisNode, List<QueryColumn> columnList,
			List<QueryParameter> parameterList, MapValidator validator,
			int maxRows) {
		Argument.isNotNull(dataSource, "dataSource");
		Argument.isNotNull(myBatisNode, "myBatisNode");
		Argument.isNotNull(columnList, "columnList");
		Argument.isNotNull(parameterList, "parameterList");
		Argument.isNotNull(validator, "validator");
		Argument.isGreaterThanOrEqualTo(maxRows, "maxRows", 1);

		System.out.println("MyBatisSqlQuery");

		this.myBatisNode = myBatisNode;
		this.validator1 = new QueryParameterListValidator(parameterList);
		this.validator2 = validator;
		this.mapper.initialize(columnList);

		this.jdbc = new NamedParameterJdbcTemplate(dataSource);
		((JdbcTemplate)this.jdbc.getJdbcOperations()).setMaxRows(maxRows);
	}

	@Override
	public List<Map<String, Object>> execute(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		String error = this.validator1.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		error = this.validator2.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		Map<String, Object> objectParameterList = this.validator1.getObjectParameterList();
		String sql = this.myBatisNode.getSql(objectParameterList);

		try {
			return this.jdbc.query(sql, objectParameterList, this.mapper);
		}
		catch (BadSqlGrammarException ex) {
			throw new QueryException(WakabaErrorCode.BAD_SQL, ex);
		}
	}
}