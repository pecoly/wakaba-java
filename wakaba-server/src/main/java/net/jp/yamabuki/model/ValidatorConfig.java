package net.jp.yamabuki.model;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.jp.yamabuki.check.Argument;

public class ValidatorConfig {
	private final String name;

	private final Map<String, String> argumentList;

	public ValidatorConfig(String name, Map<String, String> argumentList) {
		Argument.isNotBlank(name, "name");
		Argument.isNotNull(argumentList, "argumentList");

		this.name = name;
		this.argumentList = argumentList;
	}

	public String getName() {
		return this.name;
	}

	public Iterable<Entry<String, String>> getArgumentList() {
		return this.argumentList.entrySet();
	}

	public String getArgument(String name) {
		return this.argumentList.get(name);
	}
}
