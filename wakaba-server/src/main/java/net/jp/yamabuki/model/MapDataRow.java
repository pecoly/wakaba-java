package net.jp.yamabuki.model;

import java.util.Map;

public class MapDataRow implements DataRow {
	public MapDataRow(Map<String, Object> map) {
		this.map = map;
	}
	public Object get(String key) {
		return this.map.get(key);
	}
	
	private Map<String, Object> map;
}
