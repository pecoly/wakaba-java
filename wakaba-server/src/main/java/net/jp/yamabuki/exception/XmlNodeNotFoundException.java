package net.jp.yamabuki.exception;

@SuppressWarnings("serial")
public class XmlNodeNotFoundException extends WakabaException {
	public XmlNodeNotFoundException(String message) {
		super(WakabaErrorCode.XML_NODE_NOT_FOUND, "node : " + message);
	}
}
