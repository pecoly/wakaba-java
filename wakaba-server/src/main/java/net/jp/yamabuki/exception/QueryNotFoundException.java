package net.jp.yamabuki.exception;

@SuppressWarnings("serial")
public class QueryNotFoundException extends WakabaException {
	public QueryNotFoundException(String message) {
		super(WakabaErrorCode.NO_SUCH_QUERY, "Query not found : " + message);
	}
}
