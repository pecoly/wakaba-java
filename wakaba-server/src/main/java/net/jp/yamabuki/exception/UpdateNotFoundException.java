package net.jp.yamabuki.exception;

@SuppressWarnings("serial")
public class UpdateNotFoundException extends WakabaException {
	public UpdateNotFoundException(String message) {
		super(WakabaErrorCode.NO_SUCH_UPDATE, "Update not found : " + message);
	}
}
