package net.jp.yamabuki.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public final class ResourceNotFoundException extends WakabaException {
	public ResourceNotFoundException(String message) {
		super(WakabaErrorCode.RESOURCE_NOT_FOUND, message);
	}
}

