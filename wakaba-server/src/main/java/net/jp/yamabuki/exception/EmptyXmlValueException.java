package net.jp.yamabuki.exception;

@SuppressWarnings("serial")
public class EmptyXmlValueException extends WakabaException {
	public EmptyXmlValueException(String message) {
		super(WakabaErrorCode.EMPTY_XML_VALUE, "node : " + message);
	}
}
