package net.jp.yamabuki.service.initializer;

import javax.sql.DataSource;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.check.Argument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.googlecode.flyway.core.Flyway;

@Component
public class TableInitializer {
	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Autowired
	private DataSource dataSource;

	@Autowired
	private YamabukiContext yamabukiContext;

	public void initialize() {
		logger.info("begin initialize");
		String dbType = this.yamabukiContext.getDbType();
		this.initializeFlyway(dbType);
		logger.info("end initialize");
	}

	void initializeFlyway(String dbType) {
		Argument.isNotBlank(dbType, "dbType");

		Flyway flyway = new Flyway();
		flyway.setDataSource(this.dataSource);
		flyway.setLocations("flyway." + dbType + ".db.migration");

		logger.info("falyway clean start.");
		flyway.clean();
		logger.info("falyway clean done.");
		logger.info("falyway init start.");
		flyway.init();
		logger.info("falyway init done.");
		logger.info("falyway migrate start.");
		flyway.migrate();
		logger.info("falyway migrate done.");
	}
}
