package net.jp.yamabuki.service.update.users;

import net.jp.yamabuki.exception.UpdateNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.UpdateObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersUpdateService {

	@Autowired
	private UsersUpdateDataSourceManager updateDataSourceManager;

	public UpdateObject find(UserId userId, AppId appId, String type, String name) {
		UpdateObject update = null;

		update = this.updateDataSourceManager.findUpdate(userId, appId, type, name);
		if (update != null) {
			return update;
		}

		throw new UpdateNotFoundException(type + "/" + name);
	}

	public UpdateObject findTestUpdate(UserId userId, AppId appId) {
		UpdateObject update = null;

		update = this.updateDataSourceManager.findTestUpdate(userId, appId);
		if (update != null) {
			return update;
		}

		throw new UpdateNotFoundException("testupdate");
	}
}
