package net.jp.yamabuki.service.query.users;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.query.BaseQueryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class UsersQueryDataSourceManager extends BaseQueryManager {

	@Autowired
	private JdbcTemplate jdbc;

	public QueryObject findQuery(UserId userId, AppId appId, String type, String name) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject(
					"SELECT XML FROM QUERY_LIST "
					+ "WHERE USER_ID = ? "
					+ "AND APP_ID = ? "
					+ "AND NAME = ? ",
					String.class,
					new Object[] { userId.getValue(), appId.getValue(), name });
		}
		catch (EmptyResultDataAccessException ex) {
			return null;
		}

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.createQuery(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	public QueryObject findTestQuery(UserId userId, AppId appId) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject(
					"SELECT XML FROM TEST_QUERY_LIST "
					+ "WHERE USER_ID = ? "
					+ "AND APP_ID = ?",
					String.class,
					new Object[] { userId.getValue(), appId.getValue() });
		}
		catch (EmptyResultDataAccessException ex) {
			return null;
		}

		System.out.println(xml);

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.createTestQuery(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
