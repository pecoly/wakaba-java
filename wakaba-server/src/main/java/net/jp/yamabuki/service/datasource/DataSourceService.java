package net.jp.yamabuki.service.datasource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.WakabaUser;
import net.jp.yamabuki.service.user.WakabaUserService;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataSourceService {

	private Map<String, DataSource> userIdDataSourceList = new ConcurrentHashMap<>();

	private Map<JdbcConnection, DataSource> connectionDataSourceList = new ConcurrentHashMap<>();

	@Autowired
	private WakabaUserService wakabaUserService;

	@Autowired
	private YamabukiContext yamabukiContext;

	public DataSource getDataSource(String userId) {
		if (!this.userIdDataSourceList.containsKey(userId)) {
			BasicDataSource dataSource = new BasicDataSource();

			WakabaUser user = this.wakabaUserService.getUser(userId);

			dataSource.setDriverClassName(this.yamabukiContext.getDbDriver());
			dataSource.setUrl(this.yamabukiContext.getDbBaseUrl() + user.getDbUserName());
			dataSource.setUsername(user.getDbUserName());
			dataSource.setPassword(user.getDbPassword());
			dataSource.setInitialSize(2);
			dataSource.setMaxActive(10);
			dataSource.setMaxIdle(5);
			dataSource.setMaxWait(-1);

			this.userIdDataSourceList.put(userId,  dataSource);
		}

		return this.userIdDataSourceList.get(userId);
	}

	public DataSource getDataSource(JdbcConnection connection) {
		if (!this.connectionDataSourceList.containsKey(connection)) {
			BasicDataSource dataSource = new BasicDataSource();

			dataSource.setDriverClassName(connection.getDriver());
			dataSource.setUrl(connection.getUrl());
			dataSource.setUsername(connection.getUsername());
			dataSource.setPassword(connection.getPassword());
			dataSource.setInitialSize(2);
			dataSource.setMaxActive(10);
			dataSource.setMaxIdle(5);
			dataSource.setMaxWait(-1);

			this.connectionDataSourceList.put(connection, dataSource);
		}

		return this.connectionDataSourceList.get(connection);
	}
}
