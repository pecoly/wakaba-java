package net.jp.yamabuki.service.update.manager;

import net.jp.yamabuki.exception.UpdateNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.UpdateObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManagerUpdateService {

	@Autowired
	private ManagerUpdateDataSourceManager updateDataSourceManager;

	public UpdateObject find(UserId userId, AppId appId, String key) {
		UpdateObject update = null;

		update = this.updateDataSourceManager.find(userId, appId, key);
		if (update != null) {
			return update;
		}

		throw new UpdateNotFoundException(key);
	}
}
