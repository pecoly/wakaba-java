package net.jp.yamabuki.service.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.jp.yamabuki.model.accessor.Accessor;
import net.jp.yamabuki.model.datasource.QueryColumn;
import net.jp.yamabuki.model.sql.mapper.ObjectMapper;

public class NamedMapper extends ObjectMapper {

	private Map<String, Accessor> getObjectList1 = new LinkedHashMap<>();

	private Map<String, Accessor> getObjectList2 = new LinkedHashMap<>();

	private boolean initialized;

	public void initialize(List<QueryColumn> getObjectList) {
		for (QueryColumn x : getObjectList) {
			this.getObjectList1.put(x.getName(), x.getAccessor());
		}
	}

	@Override
	public Map<String, Object> mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Map<String, Object> map = new LinkedHashMap<>();

		if (!this.initialized) {
			this.initialized = true;
			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
				String columnName = rs.getMetaData().getColumnLabel(i + 1).toLowerCase();
				if (this.getObjectList1.containsKey(columnName)) {
					this.getObjectList2.put(columnName, this.getObjectList1.get(columnName));
				}
				else {
					int type = rs.getMetaData().getColumnType(i + 1);
					Accessor accessor = this.getAccessor(type);
					this.getObjectList2.put(columnName, accessor);
				}
			}
		}

		for (Entry<String, Accessor> x : this.getObjectList2.entrySet()) {
			map.put(x.getKey(), x.getValue().getObject(rs, x.getKey()));
		}

		return map;
	}
}
