package net.jp.yamabuki.service.query.users;

import net.jp.yamabuki.exception.QueryNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * クエリサービスクラス。
 *
 */
@Component
public class UsersQueryService {

	@Autowired
	private UsersQueryDataSourceManager queryDataSourceManager;

	/**
	 * クエリを検索します。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param type クエリ種別
	 * @param name 名前
	 * @return クエリ
	 */
	public QueryObject findQuery(UserId userId, AppId appId, String type, String name) {
		QueryObject query = null;

		query = this.queryDataSourceManager.findQuery(userId, appId, type, name);
		if (query != null) {
			return query;
		}

		throw new QueryNotFoundException(type + "/" + name);
	}

	/**
	 * テストクエリを検索します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return テストクエリ
	 */
	public QueryObject findTestQuery(UserId userId, AppId appId) {
		QueryObject query = null;

		query = this.queryDataSourceManager.findTestQuery(userId, appId);
		if (query != null) {
			return query;
		}

		throw new QueryNotFoundException("testquery");
	}

}
