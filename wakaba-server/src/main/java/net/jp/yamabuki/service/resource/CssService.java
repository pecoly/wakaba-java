package net.jp.yamabuki.service.resource;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.CssQuery;

@Component
public class CssService {
	private Map<String, CssQuery> cssQueryList = new ConcurrentHashMap<>();

	@Autowired
	private YamabukiContext yamabukiContext;

	public CssQuery getCssQuery(UserId userId, AppId appId) {
		String key = userId.getValue() + "/" + appId.getValue();

		if (!this.cssQueryList.containsKey(key)) {
			CssQuery css
					= this.yamabukiContext.getSpringContext().getBean(CssQuery.class);
			css.initialize(userId, appId);

			this.cssQueryList.put(key, css);
		}

		return this.cssQueryList.get(key);
	}
}
