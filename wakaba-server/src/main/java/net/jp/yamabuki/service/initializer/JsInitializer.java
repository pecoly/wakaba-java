package net.jp.yamabuki.service.initializer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.service.initializer.Initializer;
import net.jp.yamabuki.util.FileUtils;

@Component
public class JsInitializer {

	@Autowired
	private Initializer initializer;

	@Autowired
	private YamabukiContext yamabukiContext;

	@Autowired
	private JdbcTemplate jdbc;

	public void initialize() {
		ServletContext sc = this.yamabukiContext.getServletContext();

		String messageDirPath = sc.getRealPath("/WEB-INF/js/");

		JsFileVisitor visitor = new JsFileVisitor(
				this.jdbc, sc.getRealPath("/WEB-INF/js/"));
		try {
			Path path = Paths.get(messageDirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	static class JsFileVisitor extends SimpleFileVisitor<Path> {
		private JdbcTemplate jdbc;

		private String dirPath;

		public JsFileVisitor(JdbcTemplate jdbc, String dirPath) {
			this.jdbc = jdbc;
			this.dirPath = dirPath;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			int dirPathLength = this.dirPath.length();
			String path = file.toString().substring(dirPathLength + 1).replace("\\", "/");

			String name = FileUtils.removeExtension(path);
			String content = new String(Files.readAllBytes(file));
			this.addJs(name, content);

			return FileVisitResult.CONTINUE;
		}

		public void addJs(String name, String content) {
			System.out.println("js name : " + name);

			String deleteSql
					= "DELETE FROM JS_LIST"
					+ " WHERE NAME = ?";

			String insertSql
					= "INSERT INTO JS_LIST"
					+ "(USER_ID, APP_ID, NAME, CONTENT)"
					+ " VALUES('*', '*', ?, ?)";

			this.jdbc.update(deleteSql, new Object[] {
					name,
			});

			this.jdbc.update(insertSql, new Object[] {
					name, content,
			});
		}
	}
}
