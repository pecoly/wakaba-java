package net.jp.yamabuki.service.validator;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.model.validator.MapValidator;
import net.jp.yamabuki.model.validator.NullMapValidator;
import net.jp.yamabuki.model.validator.NullValidator;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ValidatorService {

	@Autowired
	private YamabukiContext yamabukiContext;

	public MapValidator getMapValidator(String name) {
		if (StringUtils.isBlank(name)) {
			return this.yamabukiContext
					.getSpringContext().getBean(NullMapValidator.class);
		}

		return (MapValidator)this.yamabukiContext
				.getSpringContext().getBean(name);
	}

	public Validator getValidator(ValidatorConfig config) {
		if (config == null) {
			return this.yamabukiContext
					.getSpringContext().getBean(NullValidator.class);
		}

		String name = config.getName();
		if (StringUtils.isBlank(name)) {
			return this.yamabukiContext
					.getSpringContext().getBean(NullValidator.class);
		}

		Validator validator = (Validator)this.yamabukiContext
				.getSpringContext().getBean(name);
		validator.initialize(config.getArgumentList());

		return validator;
	}
}
