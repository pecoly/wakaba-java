package net.jp.yamabuki.service.resource;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.MessageQuery;

@Component
public class MessageService {

	private Map<String, MessageQuery> messageQueryList = new HashMap<>();

	@Autowired
	private YamabukiContext yamabukiContext;

	public MessageQuery getMessageQuery(UserId userId, AppId appId,
			HttpServletRequest request) {
		String language = "";
		Locale locale = request.getLocale();
		if (locale != null) {
			language = locale.getLanguage();
		}

		String key = userId.getValue() + "/" + appId.getValue() + "/" + language;

		if (!this.messageQueryList.containsKey(key)) {
			MessageQuery message
					= this.yamabukiContext.getSpringContext().getBean(MessageQuery.class);
			message.initialize(userId, appId, new LocaleIdImpl(language));

			this.messageQueryList.put(key, message);
		}

		return this.messageQueryList.get(key);
	}
}
