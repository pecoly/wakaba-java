package net.jp.yamabuki.service.query.manager;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.QueryNotFoundException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManagerQueryService {

	@Autowired
	private ManagerQueryDataSourceManager queryDataSourceManager;

	public QueryObject find(UserId userId, AppId appId, String key) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(key, "key");

		QueryObject query = null;

		query = this.queryDataSourceManager.find(userId, appId, key);
		if (query != null) {
			return query;
		}

		throw new QueryNotFoundException(key);
	}
}
