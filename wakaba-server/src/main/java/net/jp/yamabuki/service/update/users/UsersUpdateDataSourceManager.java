package net.jp.yamabuki.service.update.users;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.service.update.BaseUpdateManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class UsersUpdateDataSourceManager extends BaseUpdateManager {

	@Autowired
	private JdbcTemplate jdbc;

	private static final String SQL
			= "SELECT XML FROM UPDATE_LIST "
			+ "WHERE USER_ID = ? "
			+ "AND APP_ID = ? "
			+ "AND QUERY_TYPE = ? "
			+ "AND NAME = ? ";

	private static final String SQL_2
			= "SELECT XML FROM TEST_UPDATE_LIST"
			+ " WHERE USER_ID = ?"
			+ " AND APP_ID = ? ";

	public UpdateObject findUpdate(UserId userId, AppId appId, String type, String name) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject(SQL,
					String.class,
					new Object[] { userId, appId, type, name });
		}
		catch (EmptyResultDataAccessException ex) {
			throw new ResourceNotFoundException(name);
		}

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.find(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}


	public UpdateObject findTestUpdate(UserId userId, AppId appId) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject(SQL_2,
					String.class,
					new Object[] { userId, appId });
		}
		catch (EmptyResultDataAccessException ex) {
			throw new ResourceNotFoundException("testupdate");
		}

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.find(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
