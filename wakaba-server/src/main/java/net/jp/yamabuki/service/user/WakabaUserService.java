package net.jp.yamabuki.service.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.jp.yamabuki.model.WakabaUser;
import net.jp.yamabuki.security.authority.AppIdAuthority;
import net.jp.yamabuki.security.authority.MainAuthority;
import net.jp.yamabuki.security.authority.SubAuthority;
import net.jp.yamabuki.security.authority.UserIdAuthority;
import net.jp.yamabuki.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class WakabaUserService {

	@Autowired
	private JdbcTemplate jdbc;

	public WakabaUser getUser(String userId) {
		UserMapper mapper = new UserMapper();
		List<WakabaUser> userList = this.jdbc.query(
				"SELECT * FROM USER_LIST WHERE LOGIN_ID = ?", mapper, userId);
		if (userList.isEmpty()) {
			return null;
		}

		return userList.get(0);
	}

	private static class UserMapper implements RowMapper<WakabaUser> {
		@Override
		public WakabaUser mapRow(ResultSet rs, int number) throws SQLException {
			String loginId = rs.getString("LOGIN_ID");
			String loginPassword = rs.getString("LOGIN_PASSWORD");
			String mainAuthority = rs.getString("MAIN_AUTHORITY");
			String subAuthority = rs.getString("SUB_AUTHORITY");
			String userIdAuthority = rs.getString("USER_ID_AUTHORITY");
			String appIdAuthority = rs.getString("APP_ID_AUTHORITY");
			String dbUserName = rs.getString("DB_USERNAME");
			String dbPassword = rs.getString("DB_PASSWORD");

			List<GrantedAuthority> authorityList = new ArrayList<>();

			if (!StringUtils.isBlank(mainAuthority)) {
				authorityList.add(new MainAuthority(mainAuthority));
			}

			if (!StringUtils.isBlank(subAuthority)) {
				authorityList.add(new SubAuthority(subAuthority));
			}

			if (!StringUtils.isBlank(userIdAuthority)) {
				authorityList.add(new UserIdAuthority(userIdAuthority));
			}

			if (!StringUtils.isBlank(appIdAuthority)) {
				authorityList.add(new AppIdAuthority(appIdAuthority));
			}

			User loginUser = new User(loginId, loginPassword, true, true, true, true,
					authorityList);
			return new WakabaUser(loginUser, dbUserName, dbPassword);
		}
	}
}
