package net.jp.yamabuki.service.initializer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.TemplatePropertiesFile;
import net.jp.yamabuki.util.FileUtils;

@Component
public class PluginInitializer {
	@Autowired
	private JdbcTemplate jdbc;

	@Autowired
	private YamabukiContext yamabukiContext;

	public void initialize() {
		ServletContext sc = this.yamabukiContext.getServletContext();

		String templateDirPath = sc.getRealPath("/WEB-INF/plugin/");
		TemplateFileVisitor visitor = new TemplateFileVisitor(
				jdbc,
				sc.getRealPath("/WEB-INF/plugin/"));
		try {
			Path path = Paths.get(templateDirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	static class TemplateFileVisitor extends SimpleFileVisitor<Path> {
		@Autowired
		private JdbcTemplate jdbc;

		private String dirPath;

		public TemplateFileVisitor(JdbcTemplate jdbc, String dirPath) {
			this.jdbc = jdbc;
			this.dirPath = dirPath;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			String filePath = file.toString();

			int dirPathLength = this.dirPath.length();
			String name = file.toString().substring(dirPathLength + 1).replace("\\", "/");
			String content = new String(Files.readAllBytes(file));
			String[] list = name.split("/");

			String pluginName = list[0];

			if (filePath.endsWith(".tmp")) {
				this.addViewTemplate(FileUtils.removeExtension(name), content);
			}
			else if (filePath.endsWith(".properties")) {
				TemplatePropertiesFile props = new TemplatePropertiesFile();
				props.load(filePath);
				this.addUserTemplate(props.getName(), props.getDescription());
			}
			else if (filePath.endsWith(".vm")) {
				this.addView("plugin/" + name, content);
			}
			else if (filePath.endsWith(".query")) {
				String queryName = FileUtils.removeExtension(name.substring(pluginName.length() + 1));
				this.addQuery(pluginName, queryName, content);
			}

			return FileVisitResult.CONTINUE;
		}

		public void addQuery(String type, String name, String content) {
			String deleteSql
					= "DELETE FROM QUERY_LIST"
					+ " WHERE QUERY_TYPE = ?"
					+ " AND NAME = ?";

			String insertSql
					= "INSERT INTO QUERY_LIST ("
					+ " USER_ID, APP_ID,"
					+ " QUERY_TYPE, NAME, XML)"
					+ "VALUES (?, ?, ?, ?, ?)";

			this.jdbc.update(deleteSql, new Object[] {
					type, name,
			});

			this.jdbc.update(insertSql, new Object[] {
					"*", "*", type, name, content
			});
		}

		public void addView(String name, String content) {
			String deleteSql
					= "DELETE FROM VIEW_LIST"
					+ " WHERE NAME = ?";

			String insertSql
					= "INSERT INTO VIEW_LIST ("
					+ " USER_ID, APP_ID,"
					+ " NAME, PATH, CONTENT)"
					+ "VALUES (?, ?, ?, ?, ?)";

			this.jdbc.update(deleteSql, new Object[] {
					name,
			});

			this.jdbc.update(insertSql, new Object[] {
					"*", "*", name, name, content
			});
		}

		public void addViewTemplate(String name, String content) {
			String deleteSql
					= "DELETE FROM VIEW_TEMPLATE_LIST"
					+ " WHERE NAME = ?";

			String insertSql
					= "INSERT INTO VIEW_TEMPLATE_LIST("
					+ " NAME, CONTENT, USER_ID, APP_ID"
					+ " ) VALUES ("
					+ " ?, ?, '*', '*'"
					+ " )";

			this.jdbc.update(deleteSql, new Object[] {
					name,
			});

			this.jdbc.update(insertSql, new Object[] {
					name, content,
			});
		}

		public void addUserTemplate(String name, String description) {
			String deleteSql
					= "DELETE FROM PLUGIN_LIST"
					+ " WHERE NAME = ?";

			String insertSql
					= "INSERT INTO PLUGIN_LIST("
					+ " NAME, DESCRIPTION, USER_ID, APP_ID"
					+ " ) VALUES ("
					+ " ?, ?, '*', '*'"
					+ " )";

			this.jdbc.update(deleteSql, new Object[] {
					name,
			});

			this.jdbc.update(insertSql, new Object[] {
					name, description,
			});
		}
	}
}
