package net.jp.yamabuki.service.initializer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.BindingParameterExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.delete.DeleteSql;
import net.jp.yamabuki.model.sql.statement.delete.DeleteStatement;
import net.jp.yamabuki.model.sql.statement.insert.InsertSql;
import net.jp.yamabuki.model.sql.statement.insert.InsertStatement;
import net.jp.yamabuki.model.sql.statement.insert.ValuesStatement;
import net.jp.yamabuki.service.initializer.Initializer;
import net.jp.yamabuki.service.manager.css.sql.CssSqlConstants;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.FileUtils;

@Component
public class CssInitializer {

	@Autowired
	private Initializer initializer;

	@Autowired
	private YamabukiContext yamabukiContext;

	@Autowired
	private JdbcTemplate jdbc;

	public void initialize() {
		ServletContext sc = this.yamabukiContext.getServletContext();

		String messageDirPath = sc.getRealPath("/WEB-INF/css/");

		CssFileVisitor visitor = new CssFileVisitor(
				this.jdbc, sc.getRealPath("/WEB-INF/css/"));
		try {
			Path path = Paths.get(messageDirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	static class CssFileVisitor extends SimpleFileVisitor<Path> {
		private JdbcTemplate jdbc;

		private String dirPath;

		public CssFileVisitor(JdbcTemplate jdbc, String dirPath) {
			this.jdbc = jdbc;
			this.dirPath = dirPath;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			int dirPathLength = this.dirPath.length();
			String path = file.toString().substring(dirPathLength + 1).replace("\\", "/");

			String name = FileUtils.removeExtension(path);
			String[] nameList = name.split("/");
			if (nameList.length >= 3) {
				String content = new String(Files.readAllBytes(file));
				this.addCss(nameList[0], nameList[1], nameList[2], content);
			}
			else {
				String content = new String(Files.readAllBytes(file));
				this.addCss("*", "*", name, content);
			}

			return FileVisitResult.CONTINUE;
		}

		public void addCss(String userId, String appId, String name, String content) {
			System.out.println("css name : " + name);

			String tableName = CssSqlConstants.TABLE;
			String param = "?";

			String deleteSql = new DeleteSql(
					new DeleteStatement(tableName),
					new WhereStatement(new AndExpression(new BaseExpression[] {
							new EqualExpression(AppModelSqlConstants.COL_USER_ID, param),
							new EqualExpression(AppModelSqlConstants.COL_APP_ID, param),
							new EqualExpression(CssSqlConstants.COL_NAME, param),
					}))).toString();

			String insertSql = new InsertSql(
					new InsertStatement(tableName, new String[] {
							AppModelSqlConstants.COL_USER_ID,
							AppModelSqlConstants.COL_APP_ID,
							CssSqlConstants.COL_NAME,
							CssSqlConstants.COL_CONTENT,
					}),
					new ValuesStatement(new ValueExpression[] {
							BindingParameterExpression.getInstance(),
							BindingParameterExpression.getInstance(),
							BindingParameterExpression.getInstance(),
							BindingParameterExpression.getInstance(),
					})).toString();

			this.jdbc.update(deleteSql, new Object[] {
					userId, appId, name,
			});

			this.jdbc.update(insertSql, new Object[] {
					userId, appId, name, content,
			});
		}
	}
}
