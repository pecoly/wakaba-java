package net.jp.yamabuki.service.update.manager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.service.update.BaseUpdateManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class ManagerUpdateDataSourceManager extends BaseUpdateManager {

	@Autowired
	private JdbcTemplate jdbc;

	public UpdateObject find(UserId userId, AppId appId, String name) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject("SELECT XML FROM UPDATE_LIST WHERE NAME = ?",
					String.class,
					new Object[] { name });
		}
		catch (EmptyResultDataAccessException ex) {
			throw new ResourceNotFoundException(name);
		}

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.find(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
