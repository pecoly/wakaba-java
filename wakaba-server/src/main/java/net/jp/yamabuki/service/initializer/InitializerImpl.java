package net.jp.yamabuki.service.initializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.jp.yamabuki.service.initializer.Initializer;

@Component
public class InitializerImpl implements Initializer {
	@Autowired
	private JdbcTemplate jdbc;

	//@Autowired
	//private InitializerSql initializerSql;

	@Autowired
	private TableInitializer tableInitializer;

	@Autowired
	private ViewInitializer viewInitializer;

	@Autowired
	private MessageInitializer messageInitializer;

	@Autowired
	private CssInitializer cssInitializer;

	@Autowired
	private JsInitializer jsInitializer;

	@Autowired
	private PluginInitializer templateInitializer;

	@Override
	public boolean isInitialized() {
		String selectSql1 = "SELECT COUNT(*)"
				+ " FROM information_schema.tables"
				+ " WHERE table_schema = 'public'";
		int result1 = this.jdbc.queryForObject(selectSql1, Integer.class);
		if (result1 == 0) {
			return false;
		}

		String selectSql2
				= "SELECT COUNT(*)"
				+ " FROM information_schema.tables"
				+ " WHERE table_schema = 'public'"
				+ " AND table_name = 'system_info'";
		int result2 = this.jdbc.queryForObject(selectSql2, Integer.class);
		if (result2 == 0) {
			return false;
		}

		String selectSql3
				= "SELECT COUNT(*)"
				+ " FROM SYSTEM_INFO"
				+ " WHERE NAME = 'INITIALIZED'"
				+ " AND VALUE = '1'";

		int result3 = this.jdbc.queryForObject(selectSql3, Integer.class);
		if (result3 == 0) {
			return false;
		}

		//return true;
		return false;
	}

	@Override
	public void setInitialized() {
		String insertSql
				= "INSERT INTO SYSTEM_INFO"
				+ " VALUES ('INITIALIZED', '1')";
		this.jdbc.update(insertSql);
	}

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	@Override
	public void initialize() {
		if (!this.isInitialized()) {
			logger.warn("System initializing.");
			this.initialize1();
			this.initialize2();
		}
	}

	void initialize1() {
		this.tableInitializer.initialize();
	}

	@Transactional(rollbackFor = Exception.class)
	void initialize2() {
		this.viewInitializer.initialize();
		this.messageInitializer.initialize();
		this.cssInitializer.initialize();
		//this.jsInitializer.initialize();
		this.templateInitializer.initialize();

		this.jdbc.execute("INSERT INTO APP_LIST (USER_ID, APP_ID, APP_NAME)"
				+ " VALUES ('admin', 'MyApp', 'MyApp')");

		this.jdbc.execute("INSERT INTO JDBC_CONNECTION_LIST"
				+ " (USER_ID, APP_ID, NAME, DRIVER, URL, USERNAME, PASSWORD)"
				+ " VALUES ('admin', 'MyApp', 'ora', 'oracle.jdbc.driver.OracleDriver',"
				+ " 'jdbc:oracle:thin:@192.168.56.202:1521:orcl10gUTF8',"
				+ " 'p1', 'p1')");
		this.setInitialized();
	}
}
