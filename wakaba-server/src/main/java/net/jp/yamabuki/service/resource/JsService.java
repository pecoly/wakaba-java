package net.jp.yamabuki.service.resource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.JsQuery;

@Component
public class JsService {
	private Map<String, JsQuery> jsQueryList = new ConcurrentHashMap<>();

	@Autowired
	private YamabukiContext yamabukiContext;

	public JsQuery getJsQuery(UserId userId, AppId appId) {
		String key = userId.getValue() + "/" + appId.getValue();

		if (!this.jsQueryList.containsKey(key)) {
			JsQuery js
					= this.yamabukiContext.getSpringContext().getBean(JsQuery.class);
			js.initialize(userId, appId);

			this.jsQueryList.put(key, js);
		}

		return this.jsQueryList.get(key);
	}
}
