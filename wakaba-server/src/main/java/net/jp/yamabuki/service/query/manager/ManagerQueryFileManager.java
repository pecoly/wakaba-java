package net.jp.yamabuki.service.query.manager;

import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ResourceNotFoundException;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.query.BaseQueryManager;
import net.jp.yamabuki.util.PathUtils;

public class ManagerQueryFileManager extends BaseQueryManager {

	public QueryObject find(UserId userId, AppId appId, String name) {
		String filePath = PathUtils.combine(
				"sql",
				name + ".xml");
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(filePath)) {
			if (is == null) {
				throw new ResourceNotFoundException("");
			}

			return this.createQuery(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
