package net.jp.yamabuki.service.initializer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.MessagePropertiesFile;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.service.initializer.Initializer;

@Component
public class MessageInitializer {

	@Autowired
	private Initializer initializer;

	@Autowired
	private YamabukiContext yamabukiContext;

	@Autowired
	private JdbcTemplate jdbc;

	public void initialize() {
		ServletContext sc = this.yamabukiContext.getServletContext();

		String messageDirPath = sc.getRealPath("/WEB-INF/message/");

		MessageFileVisitor visitor = new MessageFileVisitor(this.jdbc);
		try {
			Path path = Paths.get(messageDirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	static class MessageFileVisitor extends SimpleFileVisitor<Path> {
		private JdbcTemplate jdbc;

		public MessageFileVisitor(JdbcTemplate jdbc) {
			this.jdbc = jdbc;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			MessagePropertiesFile messageFile = new MessagePropertiesFile();
			messageFile.load(file.toString());
			LocaleId localeId = messageFile.getLocaleId();
			Map<String, String> messageList = messageFile.getMessageList();
			for (Entry<String, String> x : messageList.entrySet()) {
				this.addMessage(x.getKey(), localeId, x.getValue());
			}

			return FileVisitResult.CONTINUE;
		}

		public void addMessage(String code, LocaleId localeId, String message) {
			String deleteSql
					= "DELETE FROM MESSAGE_LIST"
					+ " WHERE CODE = ?"
					+ " AND LOCALE_ID = ?";

			String insertSql
					= "INSERT INTO MESSAGE_LIST"
					+ "(USER_ID, APP_ID, CODE, LOCALE_ID, MESSAGE)"
					+ " VALUES('*', '*', ?, ?, ?)";

			this.jdbc.update(deleteSql, new Object[] {
					code, localeId.getValue(),
			});

			this.jdbc.update(insertSql, new Object[] {
					code, localeId.getValue(), message
			});
		}
	}
}
