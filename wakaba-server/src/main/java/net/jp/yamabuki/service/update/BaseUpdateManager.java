package net.jp.yamabuki.service.update;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.io.UpdateConfigXmlReader;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.model.converter.Converter;
import net.jp.yamabuki.model.datasource.UpdateObject;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.mybatis.MyBatisNode;
import net.jp.yamabuki.model.update.MyBatisSqlUpdate;
import net.jp.yamabuki.model.update.config.UpdateConfig;
import net.jp.yamabuki.model.validator.MapValidator;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.datasource.DataSourceService;
import net.jp.yamabuki.service.manager.jdbcconnection.GetJdbcConnectionByKey;
import net.jp.yamabuki.service.validator.ValidatorService;

import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;

public abstract class BaseUpdateManager {

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private ValidatorService validatorService;

	@Autowired
	private GetJdbcConnectionByKey getJdbcConnectionByKey;

	@Autowired
	private YamabukiContext yamabukiContext;

	protected UpdateObject find(UserId userId, AppId appId, InputStream inputStream) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(inputStream, "inputStream");

		Configuration configuration = this.yamabukiContext.getConfiguration();

		UpdateConfigXmlReader reader = new UpdateConfigXmlReader(configuration, inputStream);
		UpdateConfig updateConfig = reader.read();

		JdbcConnection jdbcConnection
				= this.getJdbcConnectionByKey.execute(
						userId, appId, updateConfig.getConnection());
		if (jdbcConnection == null) {
			throw new RuntimeException("not found jdbcConnection");
		}

		WebApplicationContext springContext = this.yamabukiContext.getSpringContext();
		MyBatisSqlUpdate update = springContext.getBean(MyBatisSqlUpdate.class);
		MyBatisNode myBatisNode = new MyBatisNode(configuration, updateConfig.getNode());

		update.initialize(
				this.getDataSource(userId, jdbcConnection),
				myBatisNode,
				this.getParameterList(updateConfig.getParameterList()),
				this.getMapValidator(updateConfig.getValidator()));
		return update;
	}

	DataSource getDataSource(UserId userId, JdbcConnection connection) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(connection, "connection");

		return this.dataSourceService.getDataSource(connection);
	}

	List<UpdateParameter> getParameterList(Iterable<ParameterConfig> parameterConfigList) {
		Argument.isNotNull(parameterConfigList, "parameterConfigList");

		List<UpdateParameter> parameterList = new ArrayList<>();

		WebApplicationContext springContext = this.yamabukiContext.getSpringContext();
		for (ParameterConfig x : parameterConfigList) {
			String name = x.getName();
			String converterName = x.getConverter();

			Converter converter = null;
			if (converterName != null) {
				converter = (Converter)springContext.getBean(converterName);
			}

			List<Validator> validatorList = new ArrayList<>();
			for (ValidatorConfig y : x.getValidatorList()) {
				validatorList.add(this.validatorService.getValidator(y));
			}

			parameterList.add(new UpdateParameter(name, converter, validatorList));
		}

		return parameterList;
	}

	MapValidator getMapValidator(String validatorName) {
		Argument.isNotNull(validatorName, "validatorName");

		return this.validatorService.getMapValidator(validatorName);
	}
}
