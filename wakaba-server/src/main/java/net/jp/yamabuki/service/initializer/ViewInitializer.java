package net.jp.yamabuki.service.initializer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.BindingParameterExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.delete.DeleteSql;
import net.jp.yamabuki.model.sql.statement.delete.DeleteStatement;
import net.jp.yamabuki.model.sql.statement.insert.InsertSql;
import net.jp.yamabuki.model.sql.statement.insert.InsertStatement;
import net.jp.yamabuki.model.sql.statement.insert.ValuesStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.service.manager.view.sql.ViewSqlConstants;
import net.jp.yamabuki.util.FileUtils;

@Component
public class ViewInitializer {
	@Autowired
	private YamabukiContext yamabukiContext;

	@Autowired
	private JdbcTemplate jdbc;

	public void initialize() {
		ServletContext sc = this.yamabukiContext.getServletContext();

		addView(this.jdbc, "spring", "spring",
				FileUtils.readFileToString(sc.getRealPath("/WEB-INF/spring.vm")));
		addView(this.jdbc, "ds-custom", "ds-custom",
				FileUtils.readFileToString(sc.getRealPath("/WEB-INF/ds-custom.vm")));

		String viewDirPath = sc.getRealPath("/WEB-INF/view/");
		ViewFileVisitor visitor = new ViewFileVisitor(
				this.jdbc, sc.getRealPath("/WEB-INF/view/"));
		try {
			Path path = Paths.get(viewDirPath);
			Files.walkFileTree(path, visitor);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}

	public static void addView(JdbcTemplate jdbc, String name, String path, String content) {
		String tableName = ViewSqlConstants.TABLE;
		String param = "?";

		String deleteSql = new DeleteSql(
				new DeleteStatement(tableName),
				new WhereStatement(new AndExpression(new BaseExpression[] {
						new EqualExpression(ViewSqlConstants.COL_NAME, param),
				}))).toString();

		String insertSql = new InsertSql(
				new InsertStatement(tableName, new String[] {
						AppModelSqlConstants.COL_USER_ID,
						AppModelSqlConstants.COL_APP_ID,
						ViewSqlConstants.COL_NAME,
						ViewSqlConstants.COL_PATH,
						ViewSqlConstants.COL_CONTENT,
				}),
				new ValuesStatement(new ValueExpression[] {
						BindingParameterExpression.getInstance(),
						BindingParameterExpression.getInstance(),
						BindingParameterExpression.getInstance(),
						BindingParameterExpression.getInstance(),
						BindingParameterExpression.getInstance(),
				})).toString();

		jdbc.update(deleteSql, new Object[] {
				name,
		});

		jdbc.update(insertSql, new Object[] {
				"*", "*", name, path, content
		});
	}

	static class ViewFileVisitor extends SimpleFileVisitor<Path> {
		private JdbcTemplate jdbc;

		private String dirPath;

		public ViewFileVisitor(JdbcTemplate jdbc, String dirPath) {
			this.jdbc = jdbc;
			this.dirPath = dirPath;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			int dirPathLength = this.dirPath.length();
			String path = file.toString().substring(dirPathLength + 1).replace("\\", "/");
			String[] pathList = path.split("/");

			if (pathList.length >= 3) {
				String name = FileUtils.removeExtension(
						path.substring(pathList[0].length() + pathList[1].length() + 2));
				String content = new String(Files.readAllBytes(file));
				ViewInitializer.addView(this.jdbc, name, path, content);
			}

			return FileVisitResult.CONTINUE;
		}
	}
}
