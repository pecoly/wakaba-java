package net.jp.yamabuki.service.query.manager;

import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.service.query.BaseQueryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class ManagerQueryDataSourceManager extends BaseQueryManager {

	@Autowired
	private JdbcTemplate jdbc;

	public QueryObject find(UserId userId, AppId appId, String name) {
		String xml = null;
		try {
			xml = this.jdbc.queryForObject("SELECT XML FROM QUERY_LIST WHERE NAME = ?",
					String.class,
					new Object[] { name });
		}
		catch (EmptyResultDataAccessException ex) {
			return null;
		}

		try (InputStream is = new ByteArrayInputStream(xml.getBytes())) {
			return this.createQuery(userId, appId, is);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
