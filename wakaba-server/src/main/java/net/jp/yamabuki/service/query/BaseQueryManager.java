package net.jp.yamabuki.service.query;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import net.jp.yamabuki.DataSourceConfig;
import net.jp.yamabuki.YamabukiContext;
import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.QueryConfigXmlReader;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.accessor.Accessor;
import net.jp.yamabuki.model.converter.Converter;
import net.jp.yamabuki.model.datasource.QueryColumn;
import net.jp.yamabuki.model.datasource.QueryObject;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.mybatis.MyBatisNode;
import net.jp.yamabuki.model.query.MyBatisSqlQuery;
import net.jp.yamabuki.model.query.config.QueryConfig;
import net.jp.yamabuki.model.query.config.QueryConfigColumn;
import net.jp.yamabuki.model.validator.MapValidator;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.datasource.DataSourceService;
import net.jp.yamabuki.service.manager.jdbcconnection.GetJdbcConnectionByKey;
import net.jp.yamabuki.service.validator.ValidatorService;

import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
public abstract class BaseQueryManager {

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private ValidatorService validatorService;

	@Autowired
	private GetJdbcConnectionByKey getJdbcConnectionByKey;

	@Autowired
	private YamabukiContext yamabukiContext;

	@Autowired
	private DataSourceConfig dataSourceConfig;

	@Autowired
	private DataSource dataSource;

	protected QueryObject createQuery(UserId userId, AppId appId, InputStream inputStream) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(inputStream, "inputStream");

		return this.createQuery(userId, appId, inputStream, this.dataSourceConfig.getMaxRows());
	}

	protected QueryObject createTestQuery(UserId userId, AppId appId, InputStream inputStream) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(inputStream, "inputStream");

		return this.createQuery(userId, appId, inputStream, this.dataSourceConfig.getTestMaxRows());
	}

	QueryObject createQuery(UserId userId, AppId appId, InputStream inputStream, int maxRows) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(inputStream, "inputStream");

		Configuration configuration = this.yamabukiContext.getConfiguration();

		QueryConfigXmlReader reader = new QueryConfigXmlReader(configuration, inputStream);
		QueryConfig queryConfig = reader.read();

		String jdbcConnectionName = queryConfig.getConnection();
		DataSource ds = null;
		if (jdbcConnectionName.equals("*")) {
			ds = this.dataSource;
		}
		else {
			JdbcConnection jdbcConnection
					= this.getJdbcConnectionByKey.execute(
							userId, appId, jdbcConnectionName);
			if (jdbcConnection == null) {
				throw new RuntimeException("not found jdbcConnection");
			}

			ds = this.getDataSource(userId, jdbcConnection);
		}

		WebApplicationContext springContext = this.yamabukiContext.getSpringContext();
		MyBatisSqlQuery query = springContext.getBean(MyBatisSqlQuery.class);
		MyBatisNode myBatisNode = new MyBatisNode(configuration, queryConfig.getNode());

		query.initialize(
				ds,
				myBatisNode,
				this.getColumnList(queryConfig.getColumnList()),
				this.getParameterList(queryConfig.getParameterList()),
				this.getMapValidator(queryConfig.getValidator()),
				maxRows);
		return query;
	}

	DataSource getDataSource(UserId userId, JdbcConnection connection) {
		return this.dataSourceService.getDataSource(connection);
	}

	List<QueryColumn> getColumnList(Iterable<QueryConfigColumn> columnConfigList) {
		Argument.isNotNull(columnConfigList, "columnConfigList");

		List<QueryColumn> columnList = new ArrayList<>();

		WebApplicationContext springContext = this.yamabukiContext.getSpringContext();
		for (QueryConfigColumn x: columnConfigList) {
			String name = x.getName();
			String accessorName = x.getAccessor();
			Accessor accessor = (Accessor)springContext.getBean(accessorName);

			columnList.add(new QueryColumn(name, accessor));
		}

		return columnList;
	}

	List<QueryParameter> getParameterList(Iterable<ParameterConfig> parameterConfigList) {
		Argument.isNotNull(parameterConfigList, "parameterConfigList");

		List<QueryParameter> parameterList = new ArrayList<>();

		WebApplicationContext springContext = this.yamabukiContext.getSpringContext();
		for (ParameterConfig x : parameterConfigList) {
			String name = x.getName();
			String converterName = x.getConverter();

			Converter converter = null;
			if (converterName != null) {
				converter = (Converter)springContext.getBean(converterName);
			}

			List<Validator> validatorList = new ArrayList<>();
			for (ValidatorConfig y : x.getValidatorList()) {
				validatorList.add(this.validatorService.getValidator(y));
			}

			parameterList.add(new QueryParameter(name, converter, true, validatorList));
		}

		return parameterList;
	}

	MapValidator getMapValidator(String validatorName) {
		return this.validatorService.getMapValidator(validatorName);
	}
}
