package net.jp.yamabuki.io;

import java.io.InputStream;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.EmptyXmlValueException;
import net.jp.yamabuki.exception.XmlNodeNotFoundException;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.query.config.QueryConfigColumn;
import net.jp.yamabuki.model.query.config.QueryConfig;
import net.jp.yamabuki.util.XNodeUtils;

import org.apache.ibatis.builder.xml.XMLMapperEntityResolver;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.session.Configuration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class QueryConfigXmlReader {

	private Configuration configuration;

	private InputStream inputStream;

	public QueryConfigXmlReader(Configuration configuration, InputStream inputStream) {
		Argument.isNotNull(configuration, "configuration");
		Argument.isNotNull(inputStream, "inputStream");

		this.configuration = configuration;
		this.inputStream = inputStream;
	}
;
	public QueryConfig read() {
		XPathParser parser = this.createParser(this.inputStream);
		XNode node = this.readSql(parser);

		String connection = this.readConnection(parser);
		Iterable<QueryConfigColumn> columnList = this.readColumnList(parser);
		Iterable<ParameterConfig> parameterList = this.readParameterList(parser);
		String validator = this.readValidator(parser);

		return new QueryConfig(node, connection, columnList, parameterList, validator);
	}

	/**
	 * パーサーを生成します。
	 * @param inputStream ストリーム
	 * @return パーサー
	 */
	XPathParser createParser(InputStream inputStream) {
		Argument.isNotNull(inputStream, "inputStream");

		return new XPathParser(inputStream, false,
				this.configuration.getVariables(),
				new XMLMapperEntityResolver());
	}

	/**
	 * Sqlを取得します。
	 * @param parser パーサー
	 * @return Sql
	 */
	XNode readSql(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		String nodeName = "/query/sql";
		XNode node = parser.evalNode(nodeName);
		if (node == null) {
			throw new XmlNodeNotFoundException(nodeName);
		}

		String body = node.getStringBody();
		if (body == null) {
			throw new EmptyXmlValueException(nodeName);
		}

		return node;
	}

	/**
	 * コネクション設定を取得します。
	 * @param parser パーサー
	 * @return コネクション設定
	 */
	String readConnection(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		return XNodeUtils.readNodeWithEmptyCheck(parser, "/query/connection");
	}

	/**
	 * カラム一覧を取得します。
	 * @param parser パーサー
	 * @return カラム一覧
	 */
	Iterable<QueryConfigColumn> readColumnList(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		final String nodeName = "/query/columns/column";
		XNodeToColumnConfig toColumnConfig = new XNodeToColumnConfig(nodeName);

		return ImmutableList.copyOf(
				Lists.transform(parser.evalNodes(nodeName), toColumnConfig));
	}

	/**
	 * パラメータ一覧を取得します。
	 * @param parser パーサー
	 * @return パラメータ一覧
	 */
	Iterable<ParameterConfig> readParameterList(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		final String nodeName1 = "/query/parameters/parameter";
		final String nodeName2 = "validators/validator";

		XNodeToParameterConfig toParameterConfig
				= new XNodeToParameterConfig(nodeName1, nodeName2);

		return ImmutableList.copyOf(
				Lists.transform(
						parser.evalNodes(nodeName1), toParameterConfig));
	}

	/**
	 * バリデータを取得します。
	 * @param parser パーサー
	 * @return バリデータ
	 */
	String readValidator(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		return XNodeUtils.readNodeWithNullCheck(parser, "/query/validator");
	}
}
