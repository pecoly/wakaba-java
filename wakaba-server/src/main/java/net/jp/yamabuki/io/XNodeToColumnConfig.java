package net.jp.yamabuki.io;

import net.jp.yamabuki.model.query.config.QueryConfigColumn;
import net.jp.yamabuki.util.XNodeUtils;

import org.apache.ibatis.parsing.XNode;

import com.google.common.base.Function;

public class XNodeToColumnConfig implements Function<XNode, QueryConfigColumn> {
	private final String name;

	public XNodeToColumnConfig(String name) {
		this.name = name;
	}

	@Override
	public QueryConfigColumn apply(XNode i) {
		return new QueryConfigColumn(
				XNodeUtils.readNodeWithEmptyCheck(i, this.name, "name"),
				XNodeUtils.readNodeWithEmptyCheck(i, this.name, "accessor"));
	}
}
