package net.jp.yamabuki.io;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.model.update.config.UpdateConfig;

import org.apache.ibatis.builder.xml.XMLMapperEntityResolver;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.session.Configuration;

public class UpdateConfigXmlReader {
	public UpdateConfigXmlReader(Configuration configuration, InputStream inputStream) {
		Argument.isNotNull(configuration, "configuration");
		Argument.isNotNull(inputStream, "inputStream");

		this.configuration = configuration;
		this.inputStream = inputStream;
	}


	public UpdateConfig read() {
		XNode node = null;
		XPathParser parser = this.getParser(this.inputStream);
		node = this.readNode(parser);

		String connection = this.readConnection(parser);
		List<ParameterConfig> parameterList = this.readParameterList(parser);
		String validator = this.readValidator(parser);

		return new UpdateConfig(node, connection, parameterList, validator);
	}

	XPathParser getParser(InputStream inputStream) {
		Argument.isNotNull(inputStream, "inputStream");

		return new XPathParser(inputStream, false,
				this.configuration.getVariables(),
				new XMLMapperEntityResolver());
	}

	String readConnection(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		XNode node = parser.evalNode("/update/connection");
		String name = node.getStringBody();

		return name;
	}

	List<ParameterConfig> readParameterList(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		List<ParameterConfig> parameterList = new ArrayList<>();

		for (XNode x : parser.evalNodes("/update/parameters/parameter")) {
			XNode name = x.evalNode("name");
			if (name == null) {
				throw new RuntimeException();
			}

			XNode converter = x.evalNode("converter");
			if (converter == null) {
				throw new RuntimeException();
			}

			List<ValidatorConfig> validatorList = new ArrayList<>();
			for (XNode y : x.evalNodes("validators/validator")) {
				String validatorName = null;
				Map<String, String> argumentList = new HashMap<>();
				for (XNode z : y.getChildren()) {
					if (z.getName().equals("name")) {
						validatorName = z.getStringBody();
					}
					else {
						argumentList.put(z.getName(), z.getStringBody());
					}
				}

				validatorList.add(
						new ValidatorConfig(validatorName, argumentList));
			}

			parameterList.add(new ParameterConfig(
					name.getStringBody(), converter.getStringBody(),
					validatorList));
		}

		return parameterList;
	}

	XNode readNode(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		XNode node = parser.evalNode("/update/sql");
		if (node == null) {
			return null;
		}

		return node;
	}

	String readValidator(XPathParser parser) {
		Argument.isNotNull(parser, "parser");

		XNode node = parser.evalNode("/update/validator");
		if (node == null) {
			return null;
		}

		return node.getStringBody();
	}

	private Configuration configuration;

	private InputStream inputStream;
}
