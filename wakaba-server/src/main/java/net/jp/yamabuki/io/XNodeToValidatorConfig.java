package net.jp.yamabuki.io;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.ValidatorConfig;
import net.jp.yamabuki.util.XNodeUtils;

import org.apache.ibatis.parsing.XNode;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

public class XNodeToValidatorConfig implements Function<XNode, ValidatorConfig> {
	private final Predicate<XNode> isName = new Predicate<XNode>() {
		@Override
		public boolean apply(XNode i) {
			return i.getName().equals("name");
		}
	};

	private final Predicate<XNode> isNotName = Predicates.not(isName);

	private final String name;

	/**
	 * コンストラクタ。
	 * @param name ノード名
	 */
	public XNodeToValidatorConfig(String name) {
		this.name = name;
	}

	@Override
	public ValidatorConfig apply(XNode node) {
		Iterable<XNode> nameNodeList
				= Iterables.filter(node.getChildren(), isName);

		Iterable<XNode> notNameNodeList
				= Iterables.filter(node.getChildren(), isNotName);

		XNode validatorNode = Iterables.getFirst(nameNodeList, null);
		String validatorName = XNodeUtils.toStringWithEmptyCheck(validatorNode, this.name);

		Map<String, String> argumentList = new HashMap<>();
		for (XNode x : notNameNodeList) {
			argumentList.put(x.getName(), x.getStringBody());
		}

		return new ValidatorConfig(validatorName, argumentList);
	}
}