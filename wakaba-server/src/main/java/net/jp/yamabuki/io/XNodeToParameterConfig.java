package net.jp.yamabuki.io;

import net.jp.yamabuki.model.ParameterConfig;
import net.jp.yamabuki.util.XNodeUtils;

import org.apache.ibatis.parsing.XNode;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

public class XNodeToParameterConfig implements Function<XNode, ParameterConfig> {
	private final String name1;

	private final String name2;

	public XNodeToParameterConfig(String name1, String name2) {
		this.name1 = name1;
		this.name2 = name2;
	}

	@Override
	public ParameterConfig apply(XNode i) {
		return new ParameterConfig(
				XNodeUtils.readNodeWithEmptyCheck(i, this.name1, "name"),
				XNodeUtils.readNodeWithNullCheck(i, this.name1, "converter"),
				Lists.transform(
						i.evalNodes(this.name2),
						new XNodeToValidatorConfig(this.name1 + "/" + this.name2)));
	}

}
