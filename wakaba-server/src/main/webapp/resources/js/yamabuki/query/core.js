var yamabuki = yamabuki || {};
yamabuki.query = {};
yamabuki.query.Query = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"type" : null,
			"name" : null,
			"sql" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["query_name", "query_type", "query_sql"]);
		var message = yamabuki.query.Query.message;

		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["query_name"] = message["name_notEmpty"];
			}

			// type
			if (_.isEmpty(attr.type)) {
				errors["query_type"] = message["type_notEmpty"];
			}

			// connection
			if (false) {
				errors["query_name"] = "";
			}

			// sql
			if (_.isEmpty(attr.sql)) {
				errors["query_sql"] = message["sql_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// type
			if (_.isEmpty(attr.type)) {
				errors["query_type"] = message["type_notEmpty"];
			}

			// sql
			if (_.isEmpty(attr.sql)) {
				errors["query_sql"] = message["sql_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.query.QueryList = Backbone.Collection.extend({
	model: yamabuki.query.Query,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(query) {
		return query.get("name");
	}
});

yamabuki.query.Column = Backbone.Model.extend({
	defaults : function() {
		return {
			"columnName" : "",
			"columnAccessor" : ""
		};
	},
});

yamabuki.query.ColumnList = Backbone.Collection.extend({
	model : yamabuki.query.Column,
});

yamabuki.query.Parameter = Backbone.Model.extend({
	defaults : function() {
		return {
			"parameterName" : "",
			"parameterConverter" : "",
		};
	},
});

yamabuki.query.ParameterList = Backbone.Collection.extend({
	model : yamabuki.query.Parameter,
});

yamabuki.query.getColumnListFromView = function() {
	var columnList = new Array();
	$("*[name=query_column]").each(function() {
		var columnName = $(this).find("*[name=query_columnName]");
		var columnAccessor = $(this).find("*[name=query_columnAccessor]");
		var column = {
				"name" : columnName.val(),
				"accessor": columnAccessor.val(),
		};
		columnList.push(column);
	});

	return columnList;
}

yamabuki.query.getParameterListFromView = function() {
	var parameterList = new Array();
	$("*[name=query_parameter]").each(function() {
		var parameterName = $(this).find("*[name=query_parameterName]");
		var parameterConverter = $(this).find("*[name=query_parameterConverter]");
		var parameter = {
				"name" : parameterName.val(),
				"converter": parameterConverter.val(),
		};
		parameterList.push(parameter);
	});

	return parameterList;
}

