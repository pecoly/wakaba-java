var yamabuki = yamabuki || {};
yamabuki.query = yamabuki.query || {};
yamabuki.query.update = {};
yamabuki.query.update.initialize = function() {

	yamabuki.query.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.query.Query,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var columnList = yamabuki.query.getColumnListFromView();

			var parameterList = yamabuki.query.getParameterListFromView();

			this.model.set({
				"name" : $("#query_name").val(),
				"sql" : $("#query_sql").val(),
				"type" : $("#query_type").val(),
				"connection" : $("#query_connection").val(),
				"validator" : "",
				"mode" : "update"
			});
			this.model.set("columnList", columnList);
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.query.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.query.Query,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.query.update.ColumnView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#columnTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "column");
			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});

	yamabuki.query.update.ParameterView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#parameterTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var parameterConverter = this.model.get("parameterConverter");

			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "parameter");

			this.$el.find("*[name=parameterConverter]").val(parameterConverter);

			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});


	yamabuki.query.update.AppView = Backbone.View.extend({
		el: $("#app"),
		initialize : function(options) {
			this.query = options.query;
			this.columnList = options.columnList;
			this.parameterList = options.parameterList;
			this.connectionList = options.connectionList;
			this.testQueryUrl = options.testQueryUrl;

			this.listenTo(this.columnList, "add", this.addColumn);
			this.listenTo(this.columnList, "remove", this.removeColumn);

			this.listenTo(this.parameterList, "add", this.addParameter);
			this.listenTo(this.parameterList, "remove", this.removeParameter);

			this.listenTo(this.connectionList, "add", this.addConnection);

			this.connectionTemplate = _.template(
					"<option value=\"<%= name %>\"><%= name %></option>");

			yamabuki.ajax.setAsync(false);
			this.connectionList.fetch({
				data : {
					pageNumber : 1,
					limit : 1000
				},
				error : yamabuki.ajax.showError
			});
			var that = this;
			this.query.fetch({
				success : function(query, resp) {
					var name = query.get("name");
					var sql = query.get("sql");
					var connection = query.get("connection");

					$("#query_modelName").html(name);
					$("#query_name").val(name);
					$("#query_connection").val(connection);
					$("#query_sql").val(sql);

					var list = query.get("parameterList");
					for (var i = 0; i < list.length; i++) {
						var parameter = list[i];
						var x = new yamabuki.query.Parameter({
							"parameterName" : parameter["name"],
							"parameterConverter" : parameter["converter"],
						});

						that.parameterList.add(x);
					}
				},
				error : yamabuki.ajax.showError
			});
			yamabuki.ajax.setAsync(true);
		},
		events : {
			"click #addColumnButton" : "newColumn",
			"click #addParameterButton" : "newParameter",
			"click #testButton" : "testQuery",
		},
		newColumn : function() {
			this.columnList.add(new yamabuki.query.Column());
		},
		newParameter : function() {
			this.parameterList.add(new yamabuki.query.Parameter());
		},
		addColumn : function(column) {
			var view = new yamabuki.query.update.ColumnView({model : column});
			$("#query_columnList").append(view.render().el);
			if (this.columnList.length == 1) {
				$("#addColumnButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeColumn : function() {
			if (this.columnList.length == 0) {
				$("#addColumnButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addParameter : function(parameter) {
			var view = new yamabuki.query.update.ParameterView({model : parameter});
			$("#query_parameterList").append(view.render().el);
			if (this.parameterList.length == 1) {
				$("#addParameterButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeParameter : function() {
			if (this.parameterList.length == 0) {
				$("#addParameterButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addConnection : function(connection) {
			var html = this.connectionTemplate(connection.attributes);
			$("#query_connection").append(html);
		},
		testQuery : function() {
			yamabuki.ajax.resetError();
			$("#tableHead").children().remove();
			$("#tableBody").children().remove();

			var columnList = yamabuki.query.getColumnListFromView();

			var parameterList = yamabuki.query.getParameterListFromView();

			var json = {
				"name" : $("#query_name").val(),
				"sql" : $("#query_sql").val(),
				"connection" : $("#query_connection").val(),
				"mode" : "add",
				"columnList" : columnList,
				"parameterList" : parameterList
			};

			yamabuki.ajax.postJson(this.testQueryUrl,
					JSON.stringify(json), this.testSuccess);
		},
		testSuccess : function(result) {
			var initialized = false;
			var keys = new Array();
			var tbody = "";
			$.each(result, function(i) {
				if (!initialized) {
					initialized = true;
					$("#tableHead").append(yamabuki.html.defaultCreateHeader(result[i]));
				}

				tbody += yamabuki.html.defaultCreateRow(i, result[i]);
			});

			$("#tableBody").append(tbody);
		}
	});
}
