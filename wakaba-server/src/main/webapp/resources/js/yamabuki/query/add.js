var yamabuki = yamabuki || {};
yamabuki.query = yamabuki.query || {};
yamabuki.query.add = {};
yamabuki.query.add.initialize = function() {
	yamabuki.query.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.query.Query,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var columnList = yamabuki.query.getColumnListFromView();

			var parameterList = yamabuki.query.getParameterListFromView();

			this.model.set({
				"name" : $("#query_name").val(),
				"sql" : $("#query_sql").val(),
				"type" : $("#query_type").val(),
				"connection" : $("#query_connection").val(),
				"validator" : "",
				"mode" : "add"
			});
			this.model.set("columnList", columnList);
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.query.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.query.Query,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var columnList = yamabuki.query.getColumnListFromView();

			var parameterList = yamabuki.query.getParameterListFromView();

			this.model.set({
				"name" : $("#query_name").val(),
				"sql" : $("#query_sql").val(),
				"type" : $("#query_type").val(),
				"connection" : $("#query_connection").val(),
				"validator" : "",
				"mode" : "add"
			});
			this.model.set("columnList", columnList);
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: function(model, resp) {
					$("#name").val("");
					$("#sql").val("");
				},
				error : yamabuki.ajax.showError
			});
		},
		getColumnList : function() {

		console.log($("select[name=columnAccessor]").val());
		}
	});

	yamabuki.query.add.ColumnView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#query_columnTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "column");
			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});

	yamabuki.query.add.ParameterView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#query_parameterTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "parameter");
			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});


	yamabuki.query.add.AppView = Backbone.View.extend({
		el: $("#app"),
		initialize : function(options) {
			this.columnList = options.columnList;
			this.parameterList = options.parameterList;
			this.connectionList = options.connectionList;
			this.testQueryUrl = options.testQueryUrl;

			this.listenTo(this.columnList, "add", this.addColumn);
			this.listenTo(this.columnList, "remove", this.removeColumn);

			this.listenTo(this.parameterList, "add", this.addParameter);
			this.listenTo(this.parameterList, "remove", this.removeParameter);

			this.listenTo(this.connectionList, "add", this.addConnection);

			this.connectionTemplate = _.template(
					"<option value=\"<%= name %>\"><%= name %></option>");

			yamabuki.ajax.setAsync(false);
			this.connectionList.fetch({
				data : {
					pageNumber : 1,
					limit : 1000
				},
				error : yamabuki.ajax.showError
			});
			yamabuki.ajax.setAsync(true);
		},
		events : {
			"click #addColumnButton" : "newColumn",
			"click #addParameterButton" : "newParameter",
			"click #testButton" : "testQuery",
		},
		newColumn : function() {
			this.columnList.add(new yamabuki.query.Column());
		},
		newParameter : function() {
			this.parameterList.add(new yamabuki.query.Parameter());
		},
		addColumn : function(column) {
			var view = new yamabuki.query.add.ColumnView({model : column});
			$("#query_columnList").append(view.render().el);
			if (this.columnList.length == 1) {
				$("#addColumnButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeColumn : function() {
			if (this.columnList.length == 0) {
				$("#addColumnButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addParameter : function(parameter) {
			var view = new yamabuki.query.add.ParameterView({model : parameter});
			$("#query_parameterList").append(view.render().el);
			if (this.parameterList.length == 1) {
				$("#addParameterButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeParameter : function() {
			if (this.parameterList.length == 0) {
				$("#addParameterButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addConnection : function(connection) {
			var html = this.connectionTemplate(connection.attributes);
			$("#query_connection").append(html);
		},
		testQuery : function() {
			yamabuki.ajax.resetError();
			$("#tableHead").children().remove();
			$("#tableBody").children().remove();

			var columnList = yamabuki.query.getColumnListFromView();

			var parameterList = yamabuki.query.getParameterListFromView();

			var json = {
				"name" : $("#query_name").val(),
				"sql" : $("#query_sql").val(),
				"connection" : $("#query_connection").val(),
				"mode" : "add",
				"validator" : "",
				"columnList" : columnList,
				"parameterList" : parameterList
			};

			yamabuki.ajax.postJson(this.testQueryUrl,
					JSON.stringify(json), this.testSuccess);
		},
		testSuccess : function(result) {
			var initialized = false;
			var keys = new Array();
			var tbody = "";
			$.each(result, function(i) {
				if (!initialized) {
					initialized = true;
					$("#tableHead").append(yamabuki.html.defaultCreateHeader(result[i]));
				}

				tbody += yamabuki.html.defaultCreateRow(i, result[i]);
			});

			$("#tableBody").append(tbody);
		}
	});
}
