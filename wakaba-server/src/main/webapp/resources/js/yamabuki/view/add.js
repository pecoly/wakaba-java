var yamabuki = yamabuki || {};
yamabuki.view = yamabuki.view || {};
yamabuki.view.add = {};
yamabuki.view.add.initialize = function() {

	yamabuki.view.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.view.View,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#view_name").val(),
				"content" : $("#view_content").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.view.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.view.View,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#view_name").val(),
				"content" : $("#view_content").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success : function(model, resp) {
					$("#view_name").val("");
					$("#view_content").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});
}
