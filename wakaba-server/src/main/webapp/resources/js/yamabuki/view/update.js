var yamabuki = yamabuki || {};
yamabuki.view = yamabuki.view || {};
yamabuki.view.update = {};
yamabuki.view.update.initialize = function() {

	yamabuki.view.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.view.View,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#view_name").val(),
				"content" : $("#view_content").val(),
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.view.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.view.View,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});
}
