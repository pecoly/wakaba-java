var yamabuki = yamabuki || {};
yamabuki.view = {};
yamabuki.view.View = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"name" : null,
			"content" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["view_name", "view_content"]);
		var message = yamabuki.view.View.message;

		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["view_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["view_content"] = message["content_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["view_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["view_content"] = message["content_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.view.ViewList = Backbone.Collection.extend({
	model: yamabuki.view.View,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(view) {
		return view.get("name");
	}
});
