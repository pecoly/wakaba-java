var yamabuki = yamabuki || {};
yamabuki.user= yamabuki.user || {};
yamabuki.user.update = {};
yamabuki.user.update.initialize = function() {

	yamabuki.user.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.user.User,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"loginId" : $("#user_modelName").html(),
				"loginPassword" : $("#user_loginPassword1").val(),
				"loginPassword1" : $("#user_loginPassword1").val(),
				"loginPassword2" : $("#user_loginPassword2").val(),
				"mainAuthority" : yamabuki.html.getCheckedValueListByName("user_mainAuthority").join(","),
				"subAuthority" : "",
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.user.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.user.User,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});
}
