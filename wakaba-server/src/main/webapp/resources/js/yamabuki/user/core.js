var yamabuki = yamabuki || {};
yamabuki.user = {};
yamabuki.user.User = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"loginId" : null,
			"loginPassword" : null,
			"loginPassword1" : null,
			"loginPassword2" : null,
			"mainAuthority" : null,
			"subAuthority" : null,
			"mode" : null,
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["user_loginId", "user_loginPassword1", "user_mainAuthority"]);
		var message = yamabuki.user.User.message;

		if (attr.mode == "add") {
			// loginId
			if (_.isEmpty(attr.loginId)) {
				errors["user_loginId"] = message["loginId_notEmpty"];
			}

			// loginPassword
			if (_.isEmpty(attr.loginPassword1)) {
				errors["user_loginPassword1"] = message["password_notEmpty"];
			}
			else if (_.isEmpty(attr.loginPassword2)) {
				errors["user_loginPassword1"] = message["confirmPassword_notEmpty"];
			}
			else if (attr.loginPassword1 != attr.loginPassword2) {
				errors["user_loginPassword1"] = message["password_notMatch"];
			}

			// mainAuthority
			if (_.isEmpty(attr.mainAuthority)) {
				errors["user_mainAuthority"] = message["role_notSelected"];
			}
		}
		else if (attr.mode == "update") {
			// loginPassword
			if (_.isEmpty(attr.loginPassword1) && !_.isEmpty(attr.loginPassword2)) {
				errors["user_loginPassword1"] = message["password_notEmpty"];
			}
			else if (!_.isEmpty(attr.loginPassword1) && _.isEmpty(attr.loginPassword2)) {
				errors["user_loginPassword1"] = message["confirmPassword_notEmpty"];
			}
			else if (attr.loginPassword1 != attr.loginPassword2) {
				errors["user_loginPassword1"] = message["password_notMatch"];
			}

			// mainAuthority
			if (_.isEmpty(attr.mainAuthority)) {
				errors["user_mainAuthority"] = message["role_notSelected"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.user.UserList = Backbone.Collection.extend({
	model: yamabuki.user.User,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(user) {
		return user.get("loginId");
	}
});
