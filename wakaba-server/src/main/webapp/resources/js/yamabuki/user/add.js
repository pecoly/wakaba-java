var yamabuki = yamabuki || {};
yamabuki.user = yamabuki.user || {};
yamabuki.user.add = {};
yamabuki.user.add.initialize = function() {
	yamabuki.user.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.user.User,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"loginId" : $("#user_loginId").val(),
				"loginPassword" : $("#user_loginPassword1").val(),
				"loginPassword1" : $("#user_loginPassword1").val(),
				"loginPassword2" : $("#user_loginPassword2").val(),
				"mainAuthority" : yamabuki.html.getCheckedValueListByName("user_mainAuthority").join(","),
				"subAuthority" : "",
				"mode" : "add"
			});
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.user.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.user.User,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"loginId" : $("#user_loginId").val(),
				"loginPassword" : $("#user_loginPassword1").val(),
				"loginPassword1" : $("#user_loginPassword1").val(),
				"loginPassword2" : $("#user_loginPassword2").val(),
				"mainAuthority" : yamabuki.html.getCheckedValueListByName("user_mainAuthority").join(","),
				"subAuthority" : "",
				"mode" : "add"
			});
			this.model.save(null, {
				success : function(model, resp) {
					$("#user_loginId").val("");
					$("#user_loginPassword").val("");
					$("#user_loginPassword1").val("");
					$("#user_loginPassword2").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});
}
