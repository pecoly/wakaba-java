var yamabuki = yamabuki || {};
yamabuki.css = yamabuki.css || {};
yamabuki.css.update = {};
yamabuki.css.update.initialize = function() {

	yamabuki.css.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.css.Css,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
			if (options.failureFunction) {
				this.failureFunction = options.failureFunction;
			}
			else {
				this.failureFunction = yamabuki.ajax.showError;
			}
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#css_name").val(),
				"content" : $("#css_content").val(),
				"lastModified" : $("#css_lastModified").val(),
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : this.failureFunction
			});
		}
	});

	yamabuki.css.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.css.Css,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
			if (options.failureFunction) {
				this.failureFunction = options.failureFunction;
			}
			else {
				this.failureFunction = yamabuki.ajax.showError;
			}
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				headers : { "lastModified" : this.model.get("lastModified") },
				success: this.successFunction,
				error : this.failureFunction
			});
		}
	});
}
