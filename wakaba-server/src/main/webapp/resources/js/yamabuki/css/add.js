var yamabuki = yamabuki || {};
yamabuki.css = yamabuki.css || {};
yamabuki.css.add = {};
yamabuki.css.add.initialize = function() {

	yamabuki.css.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.css.Css,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#css_name").val(),
				"content" : $("#css_content").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.css.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.css.Css,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#css_name").val(),
				"content" : $("#css_content").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success : function(model, resp) {
					$("#css_name").val("");
					$("#css_content").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});
}
