var yamabuki = yamabuki || {};
yamabuki.css = {};
yamabuki.css.Css = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"name" : null, // 名前
			"content" : null, // 内容
			"lastModified" : null, // 最終更新日時
			"mode" : null // 追加 / 更新
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["css_name", "css_content"]);
		var message = yamabuki.css.Css.message;

		// 追加時
		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["css_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["css_content"] = message["content_notEmpty"];
			}
		}
		// 更新時
		else if (attr.mode == "update") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["css_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["css_content"] = message["content_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.css.CssList = Backbone.Collection.extend({
	model: yamabuki.css.Css,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(css) {
		return css.get("name");
	}
});
