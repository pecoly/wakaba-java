var yamabuki = yamabuki || {};
yamabuki.app = yamabuki.app || {};
yamabuki.app.add = {};
yamabuki.app.add.initialize = function() {

	yamabuki.app.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.app.App,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"appId" : $("#app_appId").val(),
				"appName" : $("#app_appName").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});
}
