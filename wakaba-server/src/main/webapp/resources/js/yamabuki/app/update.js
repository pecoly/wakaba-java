var yamabuki = yamabuki || {};
yamabuki.app = yamabuki.app || {};
yamabuki.app.update = {};
yamabuki.app.update.initialize = function() {

	yamabuki.app.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.app.App,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"appId" : $("#app_appId").val(),
				"appName" : $("#app_appName").val(),
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.app.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.app.App,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});
}
