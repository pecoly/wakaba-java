var yamabuki = yamabuki || {};
yamabuki.app = {};
yamabuki.app.App = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"appId" : null,
			"appName" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["app_appId", "app_appName"]);
		var message = yamabuki.app.App.message;

		if (attr.mode == "add") {
			// appId
			if (_.isEmpty(attr.appId)) {
				errors["app_appId"] = message["appId_notEmpty"];
			}

			// appName
			if (_.isEmpty(attr.appName)) {
				errors["app_appName"] = message["appName_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// appName
			if (_.isEmpty(attr.appName)) {
				errors["app_appName"] = message["appName_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.app.AppList = Backbone.Collection.extend({
	model: yamabuki.app.App,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(message) {
		return message.get("appId");
	}
});
