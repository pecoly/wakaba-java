var yamabuki = yamabuki || {};
yamabuki.update = yamabuki.update || {};
yamabuki.update.update = {};
yamabuki.update.update.initialize = function() {

	yamabuki.update.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.update.Update,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var parameterList = yamabuki.update.getParameterListFromView();

			this.model.set({
				"name" : $("#update_name").val(),
				"sql" : $("#update_sql").val(),
				"connection" : $("#update_connection").val(),
				"mode" : "update"
			});
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.update.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.update.Update,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.update.update.ParameterView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#parameterTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var parameterConverter = this.model.get("parameterConverter");

			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "parameter");

			this.$el.find("*[name=parameterConverter]").val(parameterConverter);

			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});


	yamabuki.update.update.AppView = Backbone.View.extend({
		el: $("#app"),
		initialize : function(options) {
			this.update = options.update;
			this.parameterList = options.parameterList;
			this.connectionList = options.connectionList;
			this.testUpdateUrl = options.testUpdateUrl;

			this.listenTo(this.parameterList, "add", this.addParameter);
			this.listenTo(this.parameterList, "remove", this.removeParameter);

			this.listenTo(this.connectionList, "add", this.addConnection);

			this.connectionTemplate = _.template(
					"<option value=\"<%= name %>\"><%= name %></option>");

			yamabuki.ajax.setAsync(false);
			this.connectionList.fetch({
				data : {
					pageNumber : 1,
					limit : 1000
				},
				error : yamabuki.ajax.showError
			});
			var that = this;
			this.update.fetch({
				success : function(update, resp) {
					var name = update.get("name");
					var sql = update.get("sql");
					var connection = update.get("connection");

					$("#update_modelName").html(name);
					$("#update_name").val(name);
					$("#update_connection").val(connection);
					$("#update_sql").val(sql);

					var list = update.get("parameterList");
					for (var i = 0; i < list.length; i++) {
						var parameter = list[i];
						var x = new yamabuki.update.Parameter({
							"parameterName" : parameter["name"],
							"parameterConverter" : parameter["converter"],
						});

						that.parameterList.add(x);
					}
				},
				error : yamabuki.ajax.showError
			});
			yamabuki.ajax.setAsync(true);
		},
		events : {
			"click #addParameterButton" : "newParameter",
			"click #testButton" : "testUpdate",
		},
		newParameter : function() {
			this.parameterList.add(new yamabuki.update.Parameter());
		},
		addParameter : function(parameter) {
			var view = new yamabuki.update.update.ParameterView({model : parameter});
			$("#update_parameterList").append(view.render().el);
			if (this.parameterList.length == 1) {
				$("#addParameterButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeParameter : function() {
			if (this.parameterList.length == 0) {
				$("#addParameterButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addConnection : function(connection) {
			var html = this.connectionTemplate(connection.attributes);
			$("#update_connection").append(html);
		},
		testUpdate : function() {
			yamabuki.ajax.resetError();
			$("#tableHead").children().remove();
			$("#tableBody").children().remove();

			var parameterList = yamabuki.update.getParameterListFromView();

			var json = {
				"name" : $("#update_name").val(),
				"sql" : $("#update_sql").val(),
				"connection" : $("#update_connection").val(),
				"mode" : "add",
				"parameterList" : parameterList
			};

			yamabuki.ajax.postJson(this.testUpdateUrl,
					JSON.stringify(json), this.testSuccess);
		},
		testSuccess : function(result) {
			var initialized = false;
			var keys = new Array();
			var tbody = "";
			$.each(result, function(i) {
				if (!initialized) {
					initialized = true;
					$("#tableHead").append(yamabuki.html.defaultCreateHeader(result[i]));
				}

				tbody += yamabuki.html.defaultCreateRow(i, result[i]);
			});

			$("#tableBody").append(tbody);
		}
	});
}
