var yamabuki = yamabuki || {};
yamabuki.update = yamabuki.update || {};
yamabuki.update.add = {};
yamabuki.update.add.initialize = function() {
	yamabuki.update.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.update.Update,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var parameterList = yamabuki.update.getParameterListFromView();

			this.model.set({
				"name" : $("#update_name").val(),
				"sql" : $("#update_sql").val(),
				"connection" : $("#update_connection").val(),
				"mode" : "add"
			});
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.update.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.update.Update,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var parameterList = yamabuki.update.getParameterListFromView();

			this.model.set({
				"name" : $("#update_name").val(),
				"sql" : $("#update_sql").val(),
				"connection" : $("#update_connection").val(),
				"mode" : "add"
			});
			this.model.set("parameterList", parameterList);
			this.model.save(null, {
				success: function(model, resp) {
					$("#update_name").val("");
					$("#update_sql").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.update.add.ParameterView = Backbone.View.extend({
		tagName : "div",
		template : _.template($("#update_parameterTemplate").html()),
		events : {
			"click button.remove" : "clear"
		},
		initialize : function() {
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "remove", this.remove);
		},
		render : function() {
			var html = this.template(this.model.attributes);
			this.$el.html(html);
			this.$el.addClass("form-group");
			this.$el.attr("name", "parameter");
			return this;
		},
		clear : function() {
			this.model.destroy();
		}
	});


	yamabuki.update.add.AppView = Backbone.View.extend({
		el: $("#app"),
		initialize : function(options) {
			this.parameterList = options.parameterList;
			this.connectionList = options.connectionList;
			this.testUpdateUrl = options.testUpdateUrl;

			this.listenTo(this.parameterList, "add", this.addParameter);
			this.listenTo(this.parameterList, "remove", this.removeParameter);

			this.listenTo(this.connectionList, "add", this.addConnection);

			this.connectionTemplate = _.template(
					"<option value=\"<%= name %>\"><%= name %></option>");

			yamabuki.ajax.setAsync(false);
			this.connectionList.fetch({
				data : {
					pageNumber : 1,
					limit : 1000
				},
				error : yamabuki.ajax.showError
			});
			yamabuki.ajax.setAsync(true);
		},
		events : {
			"click #addParameterButton" : "newParameter",
			"click #testButton" : "testUpdate",
		},
		newParameter : function() {
			this.parameterList.add(new yamabuki.update.Parameter());
		},
		addParameter : function(parameter) {
			var view = new yamabuki.update.add.ParameterView({model : parameter});
			$("#update_parameterList").append(view.render().el);
			if (this.parameterList.length == 1) {
				$("#addParameterButton").parent("div").addClass("col-md-offset-2");
			}
		},
		removeParameter : function() {
			if (this.parameterList.length == 0) {
				$("#addParameterButton").parent("div").removeClass("col-md-offset-2");
			}
		},
		addConnection : function(connection) {
			var html = this.connectionTemplate(connection.attributes);
			$("#update_connection").append(html);
		},
		testUpdate : function() {
			yamabuki.ajax.resetError();
			$("#tableHead").children().remove();
			$("#tableBody").children().remove();

			var parameterList = yamabuki.update.getParameterListFromView();

			var json = {
				"name" : $("#update_name").val(),
				"sql" : $("#update_sql").val(),
				"connection" : $("#update_connection").val(),
				"mode" : "add",
				"parameterList" : parameterList
			};

			yamabuki.ajax.postJson(this.testUpdateUrl,
					JSON.stringify(json), this.testSuccess);
		},
		testSuccess : function(result) {
			var initialized = false;
			var keys = new Array();
			var tbody = "";
			$.each(result, function(i) {
				if (!initialized) {
					initialized = true;
					$("#tableHead").append(yamabuki.html.defaultCreateHeader(result[i]));
				}

				tbody += yamabuki.html.defaultCreateRow(i, result[i]);
			});

			$("#tableBody").append(tbody);
		}
	});
}
