var yamabuki = yamabuki || {};
yamabuki.update = {};
yamabuki.update.Update = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"type" : "update",
			"name" : null,
			"sql" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["update_name", "update_sql"]);
		var message = yamabuki.update.Update.message;

		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["update_name"] = message["name_notEmpty"];
			}

			// connection
			if (false) {
				errors["update_name"] = "";
			}

			// sql
			if (_.isEmpty(attr.sql)) {
				errors["update_sql"] = message["sql_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// sql
			if (_.isEmpty(attr.sql)) {
				errors["update_sql"] = message["sql_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.update.UpdateList = Backbone.Collection.extend({
	model: yamabuki.update.Update,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(update) {
		return update.get("name");
	}
});

yamabuki.update.Parameter = Backbone.Model.extend({
	defaults : function() {
		return {
			"parameterName" : "",
			"parameterConverter" : "",
		};
	},
});

yamabuki.update.ParameterList = Backbone.Collection.extend({
	model : yamabuki.update.Parameter,
});

yamabuki.update.getParameterListFromView = function() {
	var parameterList = new Array();
	$("*[name=update_parameter]").each(function() {
		var parameterName = $(this).find("*[name=update_parameterName]");
		var parameterConverter = $(this).find("*[name=update_parameterConverter]");
		var parameter = {
				"name" : parameterName.val(),
				"converter": parameterConverter.val(),
		};
		parameterList.push(parameter);
	});

	return parameterList;
}

