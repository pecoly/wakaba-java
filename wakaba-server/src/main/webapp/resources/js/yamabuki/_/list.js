var yamabuki = yamabuki || {};
yamabuki._ = yamabuki._ || {};

yamabuki._.list = {};

yamabuki._.list.initialize = function() {
	yamabuki._.list.AppRouter = Backbone.Router.extend({
		initialize : function(options) {
			this.appView = options.appView;
		},
		routes : {
			":pageNumber" : "showPage",
			"" : "showFirst",
		},
		showPage : function(pageNumber) {
			this.appView.changePage(pageNumber);
		},
		showFirst : function() {
			this.showPage(1);
		}
	});
	
	yamabuki._.list.AppView = Backbone.View.extend({
		pageNumber : 1,
		initialize : function(options) {
			this.key = options.key;
			this.storage = options.storage;
			this.countUrl = options.countUrl;
			this.list = options.list;
			this.getDataFromView = options.getDataFromView;
			this.setDataFromStorageToView = options.setDataFromStorageToView;
			this.setDataToStorage = options.setDataToStorage;
			this.clearListView = options.clearListView;
			this.listenTo(this.list, "add", options.addView);
			if (this.getPageNumber() != null) {
				this.pageNumber = this.getPageNumber();
			}

			this.setDataFromStorageToView(this.storage);

			//this.fetch();
		},
		events : {
			"click .findButton" : "find",
//			"click a.page" : "clickPage",
		},
		find : function() {
			this.pageNumber = 1;
			this.reset();
		},
		clickPage : function(e) {
			this.changePage($(e.target).attr("pageNumber"));
		},
		changePage : function(pageNumber) {
			this.pageNumber = pageNumber;
			this.reset();
		},
		reset : function() {
			while (model = this.list.first()) {
				this.list.remove(model);
			}
			this.clearListView();
			this.fetch();
		},
		fetch : function() {
			var data = this.getDataFromView();
			data.pageNumber = this.pageNumber;

			// ストレージに保存
			this.setDataToStorage(this.storage, data);
			this.setPageNumber(this.pageNumber);

			// 一覧を取得
			yamabuki.ajax.setAsync(false);
			this.list.fetch({
				data : data,
				error : yamabuki.ajax.showError
			});
			yamabuki.ajax.setAsync(true);

			// ページ描画
			var view = new yamabuki._.pagination.PaginationView({
				countUrl : this.countUrl,
				data : data,
				pageNumber : Number(this.pageNumber),

			});
		},
		getPageNumber : function() {
			return this.storage.getItem(this.key + ".pageNubmer");
		},
		setPageNumber : function(pageNumber) {
			this.storage.setItem(this.key + ".pageNumber", pageNumber);
		}
	});
}

yamabuki._.list.ListView = Backbone.View.extend({
	tagName : "tr",
	events : {
		"click a.remove" : "clear",
	},
	initialize : function(options) {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.destroy);
	},
	render : function() {
		var html = yamabuki._.list.ListView.template(this.model.attributes);
		this.$el.html(html);
		return this;
	},
	clear : function() {
		this.model.destroy();
	},
	destroy : function() {
		this.remove();
	}
},
{
	template : {},
	setTemplate : function(templateId) {
		this.template = _.template($(templateId).html());
	}
});