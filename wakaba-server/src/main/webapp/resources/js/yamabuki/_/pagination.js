var yamabuki = yamabuki || {};
yamabuki._ = yamabuki._ || {};
yamabuki._.pagination = {};

yamabuki._.pagination.PaginationView = Backbone.View.extend({
	tagName : "ul",
	initialize : function(options) {
		this.data = options.data;
		this.pageNumber = options.pageNumber;

		yamabuki.ajax.setAsync(false);
		var count;
		yamabuki.ajax.getJson(options.countUrl + "?" + $.param(options.data),
			null,
			function(data) {
				count = data["count"];
			});
		this.count = count;
		yamabuki.ajax.setAsync(true);

		$(".pageList").children().remove();
		if (this.count != 0) {
			$(".pageList").append(this.render().el);
		}
	},
	render : function() {
		var html = yamabuki._.pagination.PaginationView.template({
			count : this.count,
			pageNumber : this.pageNumber
		});
		this.$el.html(html);
		return this;
	},
},
{
	template : {},
	setTemplate : function(templateId) {
		this.template = _.template($(templateId).html());
	}
});
