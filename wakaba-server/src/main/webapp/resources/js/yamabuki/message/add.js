var yamabuki = yamabuki || {};
yamabuki.message = yamabuki.message || {};
yamabuki.message.add = {};
yamabuki.message.add.initialize = function() {

	yamabuki.message.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.message.Message,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"code" : $("#message_code").val(),
				"localeId" : $("#message_localeId").val(),
				"message" : $("#message_message").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.message.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.message.Message,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"code" : $("#message_code").val(),
				"localeId" : $("#message_localeId").val(),
				"message" : $("#message_message").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success : function(model, resp) {
					$("#message_code").val("");
					$("#message_localeId").val("");
					$("#message_message").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});
}
