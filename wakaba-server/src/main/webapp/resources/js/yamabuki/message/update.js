var yamabuki = yamabuki || {};
yamabuki.message = yamabuki.message || {};
yamabuki.message.update = {};
yamabuki.message.update.initialize = function() {

	yamabuki.message.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.message.Message,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"code" : $("#modelName").html(),
				"localeId" : $("#message_localeId").val(),
				"message" : $("#message_message").val(),
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.message.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.message.Message,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});
}
