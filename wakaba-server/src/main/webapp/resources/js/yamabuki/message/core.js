var yamabuki = yamabuki || {};
yamabuki.message = {};
yamabuki.message.Message = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"code" : null,
			"localeId" : null,
			"message" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["message_code", "message_locale", "message_message"]);
		var message = yamabuki.message.Message.message;

		if (attr.mode == "add") {
			// code
			if (_.isEmpty(attr.code)) {
				errors["message_code"] = message["code_notEmpty"];
			}

			// localeId
			if (_.isEmpty(attr.localeId)) {
				errors["message_locale"] = message["localeId_notEmpty"];
			}
			else if (2 < attr.localeId.length) {
				errors["message_locale"] = message["localeId_maxLength_2"];
			}

			// message
			if (_.isEmpty(attr.message)) {
				errors["message_message"] = message["message_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// localeId
			if (_.isEmpty(attr.localeId)) {
				errors["message_locale"] = message["localeId_notEmpty"];
			}
			else if (2 < attr.localeId.length) {
				errors["message_locale"] = message["localeId_maxLength_2"];
			}

			// message
			if (_.isEmpty(attr.message)) {
				errors["message_message"] = message["message_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.message.MessageList = Backbone.Collection.extend({
	model: yamabuki.message.Message,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(message) {
		return message.get("code");
	}
});
