var yamabuki = yamabuki || {};
$(document).ready(function(){

  $(window).resize(function()
  {
    if($(window).width() >= 991){
      $(".sidey").slideDown(350);
    }                
  });

});

$(document).ready(function(){

  $(".has_submenu > a").click(function(e){
    e.preventDefault();
    var menu_li = $(this).parent("li");
    var menu_ul = $(this).next("ul");

    if(menu_li.hasClass("open")){
      menu_ul.slideUp(350);
      menu_li.removeClass("open")
    }
    else{
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      menu_ul.slideDown(350);
      menu_li.addClass("open");
    }
  });
  
});
//--------------------------------------------------------------------------
//yamabuki util
//--------------------------------------------------------------------------
yamabuki.util= yamabuki.util || {};

function formatCurrency(value) {
	return String(value).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' ) + "円";
}

function isBlank(obj) {
	return (!obj || $.trim(obj) === "");
}

yamabuki.util.zeroLeftPadding2 = function(value) {
	return ("00" + value).slice(-2);
}

//--------------------------------------------------------------------------
//yamabuki html
//--------------------------------------------------------------------------
yamabuki.html = yamabuki.html || {};

yamabuki.html.defaultCreateHeader = function(result) {
	var tr = "<tr>";
	for (var key in result) {
		var th = "<th>" + key + "</th>";
		tr += th;
	}

	tr += "</tr>";
	return tr;
}

yamabuki.html.createHeaderWithNumber = function(result) {
	var tr = "<tr>";
	tr += "<th>No</th>"
	for (var key in result) {
		var th = "<th>" + key + "</th>";
		tr += th;
	}

	tr += "</tr>";
	return tr;
}

yamabuki.html.defaultCreateRow = function(i, result) {
	var tr = "<tr>";
	for (var key in result) {
		tr += "<td>" + result[key] + "</td>";
	}

	tr += "</tr>";
	return tr;
}

yamabuki.html.createRowWithNumber = function(i, result) {
	var tr = "<tr>";
	tr += "<th>" + (i + 1) + "</th>"
	for (var key in result) {
		tr += "<td>" + result[key] + "</td>";
	}

	tr += "</tr>";
	return tr;
}

yamabuki.html.createOption = function(value, name) {
	return "<option value=\"" + value + "\">" + name + "</option>";
}

yamabuki.html.createLink = function(url, value) {
	return "<a href=\"" + url + "\">" + value + "</a>";
}

yamabuki.html.createLinkWithClickFunction = function(clickFunction, value) {
	return "<a href=\"javascript:void(0); return false;\" onclick=\"" + clickFunction + "\">" + value + "</a>";
}

yamabuki.html.getRadioValueByName = function(name) {
	return $("input[name='" + name + "']:checked").val();
}

yamabuki.html.setRadioValueByName = function(name, value) {
	$("input[name='" + name + "']").val([value]);
}

yamabuki.html.getCheckedValueListByName = function(name) {
	var valueList = [];
	$('[name="' + name + '"]:checked').each(function(){
		valueList.push($(this).val());
	});

	return valueList;
}

yamabuki.html.setCheckValueById = function(id) {
	$("#" + id).attr("checked", true);
}

yamabuki.html.unsetCheckValueById = function(id) {
	$("#" + id).attr("checked", fasle);
}

yamabuki.html.getSelectValueByName = function(name) {
	return $("select[name='" + name + "']").val();
}

yamabuki.html.getSelectValueById = function(id) {
	return $("#" + id).val();
}

yamabuki.html.showValidation = function(model, errors) {
	for (key in errors) {
		$("#" + key + "Error").html(errors[key]);
		$("#" + key + "Group").addClass("has-error");
	}
}

yamabuki.html.resetValidation = function(errors) {
	for (var i = 0; i < errors.length; i++) {
		$("#" + errors[i] + "Error").html("");
		$("#" + errors[i] + "Group").removeClass("has-error");
	}
}


//--------------------------------------------------------------------------
// yamabuki ajax
//--------------------------------------------------------------------------
yamabuki.ajax = yamabuki.ajax || {};

yamabuki.ajax.setAsync = function(enabled) {
	$.ajaxSetup({
		async: enabled
	});
}
yamabuki.ajax.getJson = function(getUrl, params, successFunction, failureFunction) {
	var successFunc = {};
	if (successFunction) {
		successFunc = successFunction;
	}
	else {
		successFunc = yamabuki.ajax.defaultSuccessFunction;
	}

	var failureFunc = {};
	if (failureFunction) {
		failureFunc = failureFunction;
	}
	else {
		failureFunc = yamabuki.ajax.defaultFailureFunction;
	}

	$.getJSON(
		getUrl, params
	)
	.success(function(data) {
		successFunc(data);
	})
	.error(function(jqXHR, textStatus, errorThrown) {
		failureFunc(jqXHR, textStatus, errorThrown);
	});
}

yamabuki.ajax.getListJson = function(getUrl, tableHeadId, tableBodyId,
		createHeaderFunction, createRowFunction, failureFunction) {
	var createHeaderFunc = function(){}
	if (createHeaderFunction) {
		createHeaderFunc = createHeaderFunction;
	}
	else{
		createHeaderFunc = yamabuki.html.defaultCreateHeader;
	}

	var createRowFunc = function(){}
	if (createRowFunction) {
		createRowFunc = createRowFunction;
	}
	else{
		createRowFunc = yamabuki.html.defaultCreateRow;
	}

	var successFunction = function(result) {
		var initialized = false;
		var keys = new Array();
		var tbody = "";

		$.each(result, function(i) {
			if (!initialized) {
				initialized = true;
				$(tableHeadId).append(createHeaderFunc(result[i]));
			}

			tbody += createRowFunc(i, result[i]);
		});

		$(tableBodyId).append(tbody);
	}

	yamabuki.ajax.getJson(getUrl, null, successFunction, failureFunction);
}

yamabuki.ajax.getPageJson = function(
		getUrl, pageNumber, templateId, pageListId, failureFunction) {
	var successFunction = function(result) {
		var count = result[0]["count"];
		var pageTemplate = _.template($(templateId).html());
		var html = pageTemplate({
			count : count,
			pageNumber : pageNumber
		});

		$(pageListId).append(html);
	}

	yamabuki.ajax.getJson(getUrl, null, successFunction, failureFunction);
}

yamabuki.ajax.postJson = function(
		postUrl, postData, successFunction, failureFunction) {
	var successFunc = {};
	if (successFunction) {
		successFunc = successFunction;
	}
	else {
		successFunc = yamabuki.ajax.defaultSuccessFunction;
	}

	var failureFunc = {};
	if (failureFunction) {
		failureFunc = failureFunction;
	}
	else {
		failureFunc = yamabuki.ajax.defaultFailureFunction;
	}

	$.ajax({
		dataType: 'json',
		contentType: "application/json",
		url: postUrl,
		type: 'POST',
		data: postData,
		success: successFunc,
		error: failureFunc,
	});
}

yamabuki.ajax.defaultSuccessFunction = function(data) {
}

yamabuki.ajax.defaultFailureFunction = function(jqXHR, textStatus, errorThrown) {
	yamabuki.ajax.showError("", jqXHR);
}

yamabuki.ajax.showError = function(model, resp) {
	var message = yamabuki.ajax.getRespText(resp);
	yamabuki.ajax.setError(message);
}

yamabuki.ajax.showCodeError = function(resp, codeList) {
	var message = yamabuki.ajax.getRespText(resp);
	if (message.length >= 5) {
		var code = message.substr(0, 5);
		if (code in codeList) {
			yamabuki.ajax.setError(codeList[code]);
			return;
		}
	}

	yamabuki.ajax.setError(message);
}

yamabuki.ajax.getRespText = function(resp) {
	var message = resp.responseText;
	if (isBlank(message)) {
		return resp.statusText;
	}
	else {
		return message;
	}
}

yamabuki.ajax.resetError = function() {
	yamabuki.ajax.setError("");
}

yamabuki.ajax.getError = function() {
	return $("#errorMessage").html();
}

yamabuki.ajax.setError = function(message) {
	$("#errorMessage").html(message);
}

yamabuki.ajax.setSuccess = function(message) {
	$("#successMessage").html(message);
}

yamabuki.ajax.resetSuccess = function() {
	$("#successMessage").html("");
}
