var yamabuki = yamabuki || {};
yamabuki.plugin = {};
yamabuki.plugin.Plugin = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.ajax.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"name" : null,
			"content" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.ajax.resetValidation(["plugin_name", "plugin_content"]);
		var message = yamabuki.plugin.Plugin.message;

		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["plugin_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["plugin_content"] = message["content_notEmpty"];
			}
		}
		else if (attr.mode == "update") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["plugin_name"] = message["name_notEmpty"];
			}

			// content
			if (_.isEmpty(attr.content)) {
				errors["plugin_content"] = message["content_notEmpty"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.plugin.PluginList = Backbone.Collection.extend({
	model: yamabuki.plugin.Plugin,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(view) {
		return view.get("name");
	}
});
