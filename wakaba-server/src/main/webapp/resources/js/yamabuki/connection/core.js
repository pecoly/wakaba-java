var yamabuki = yamabuki || {};
yamabuki.connection = {};
yamabuki.connection.Connection = Backbone.Model.extend({
	initialize : function(options) {
		this.urlRoot = options.urlRoot;
		this.set("id", options.id);
		this.on("invalid", yamabuki.html.showValidation);
	},
	defaults : function() {
		return {
			"id" : null,
			"name" : null,
			"driver" : null,
			"url" : null,
			"username" : null,
			"password" : null,
			"password1" : null,
			"password2" : null,
			"mode" : null
		};
	},
	validate: function (attr) {
		errors = {};
		yamabuki.html.resetValidation(["connection_name", "connection_driver", "connection_url", "connection_username", "connection_password1"]);
		var message = yamabuki.connection.Connection.message;

		if (attr.mode == "add") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["connection_name"] = message["name_notEmpty"];
			}

			// driver
			if (_.isEmpty(attr.driver)) {
				errors["connection_driver"] = message["driver_notEmpty"];
			}

			// url
			if (_.isEmpty(attr.url)) {
				errors["connection_url"] = message["url_notEmpty"];
			}

			// password
			if (attr.password1 != attr.password2) {
				errors["connection_password1"] = message["password_notMatch"];
			}
		}
		else if (attr.mode == "update") {
			// name
			if (_.isEmpty(attr.name)) {
				errors["connection_name"] = message["name_notEmpty"];
			}

			// driver
			if (_.isEmpty(attr.driver)) {
				errors["connection_driver"] = message["driver_notEmpty"];
			}

			// url
			if (_.isEmpty(attr.url)) {
				errors["connection_url"] = message["url_notEmpty"];
			}

			// password
			if (attr.password1 != attr.password2) {
				errors["connection_password1"] = message["password_notMatch"];
			}
		}
		else {
			alert("error");
		}

		if (!_.isEmpty(errors)) {
			return errors;
		}
	}
},
{
	message : {},
	setMessage : function(message) {
		this.message = message;
	}
});

yamabuki.connection.ConnectionList = Backbone.Collection.extend({
	model: yamabuki.connection.Connection,
	initialize : function(options) {
		this.url = options.url;
	},
	comparator : function(connection) {
		return connection.get("name");
	}
});
