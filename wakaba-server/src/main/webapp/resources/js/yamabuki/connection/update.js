var yamabuki = yamabuki || {};
yamabuki.connection = yamabuki.connection || {};
yamabuki.connection.update = {};
yamabuki.connection.update.initialize = function() {

	yamabuki.connection.update.UpdateButton = Backbone.View.extend({
		el : $("#updateButton"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#connection_name").val(),
				"driver" : $("#connection_driver").val(),
				"url" : $("#connection_url").val(),
				"username" : $("#connection_username").val(),
				"password" : $("#connection_password1").val(),
				"password1" : $("#connection_password1").val(),
				"password2" : $("#connection_password2").val(),
				"mode" : "update"
			});
			this.model.save(null, {
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.connection.update.RemoveButton = Backbone.View.extend({
		el : $("#removeButton"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
			this.successFunction = options.successFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.destroy({
				success: this.successFunction,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.connection.update.TestButton = Backbone.View.extend({
		el : $("#testButton"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
			this.testUrl = options.testUrl;
			this.successFunction = options.successFunction;
			this.failureFunction = options.failureFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var json = {
					"driver" : $("#connection_driver").val(),
					"url" : $("#connection_url").val(),
					"username" : $("#connection_username").val(),
					"password" : $("#connection_password1").val(),
				};
				yamabuki.ajax.postJson(this.testUrl, JSON.stringify(json),
						this.successFunction, this.failureFunction);
		}
	});
}
