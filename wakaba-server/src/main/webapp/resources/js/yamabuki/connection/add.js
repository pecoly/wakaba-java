var yamabuki = yamabuki || {};
yamabuki.connection = yamabuki.connection || {};
yamabuki.connection.add = {};
yamabuki.connection.add.initialize = function() {

	yamabuki.connection.add.AddButton1 = Backbone.View.extend({
		el : $("#addButton1"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#connection_name").val(),
				"driver" : $("#connection_driver").val(),
				"url" : $("#connection_url").val(),
				"username" : $("#connection_username").val(),
				"password" : $("#connection_password1").val(),
				"password1" : $("#connection_password1").val(),
				"password2" : $("#connection_password2").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success: redirectList,
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.connection.add.AddButton2 = Backbone.View.extend({
		el : $("#addButton2"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			this.model.set({
				"name" : $("#connection_name").val(),
				"driver" : $("#connection_driver").val(),
				"url" : $("#connection_url").val(),
				"username" : $("#connection_username").val(),
				"password" : $("#connection_password1").val(),
				"password1" : $("#connection_password1").val(),
				"password2" : $("#connection_password2").val(),
				"mode" : "add"
			});
			this.model.save(null, {
				success : function(model, resp) {
					$("#connection_name").val("");
					$("#connection_driver").val("");
					$("#connection_url").val("");
					$("#connection_username").val("");
					$("#connection_password1").val("");
					$("#connection_password1").val("");
					$("#connection_password2").val("");
				},
				error : yamabuki.ajax.showError
			});
		}
	});

	yamabuki.connection.add.TestButton = Backbone.View.extend({
		el : $("#testButton"),
		model : yamabuki.connection.Connection,
		initialize : function(options) {
			this.model = options.model;
			this.testUrl = options.testUrl;
			this.successFunction = options.successFunction;
			this.failureFunction = options.failureFunction;
		},
		events : {
			"click" : "click"
		},
		click : function(e) {
			var json = {
				"driver" : $("#connection_driver").val(),
				"url" : $("#connection_url").val(),
				"username" : $("#connection_username").val(),
				"password" : $("#connection_password1").val(),
			};
			yamabuki.ajax.postJson(this.testUrl, JSON.stringify(json),
					this.successFunction, this.failureFunction);
		}
	});
}

