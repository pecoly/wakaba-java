●アプリケーションのビルド
	1.準備
		・JDK7
		・Gradle1.7
		
	2.方法
		$ cd wakaba
		$ gradle war
		
		build/libs/wakaba.warが作成される
		
●アプリケーションのデバッグ
	1.準備
		・JDK7
		・Eclipse
		・Jetty
		・Postgresql
	2.方法
		$ cd wakaba
		$ gradle eclipse
		
		Eclipseを実行してwakabaを既存プロジェクトとしてインポート
		
●アプリケーション実行用のサーバー
	1.準備
		・Ruby
		・Vagrant
		・Chef
		・Berkshelf
		・Cygwin
		・Devkit-tdm
	2.方法
		Cygwin、rsyncをインストール
		以下のファイルをパスの通った場所にコピー
		cygiconv-2.dll
		cygintl-8.dll
		cygpopt-0.dll
		cygwin1.dll
		rsync.exe
		
		DevKit-tdmを解凍してDevKitフォルダを作成
		$ cd DevKit
		$ ruby dk.rb init
		$ ruby dk.rb install
		
		$ gem install vagrant
		$ gem install berkshelf
		
		$ cd wakaba/vagrant
		$ vagrant box add ubuntu http://files.vagrantup.com/precise64.box
		$ vagrant init ubuntu
		
		$ cd wakaba/vagrant/ubuntu
		$ vagnrat up
		
		$ cd wakaba/chef
		$ berks install --path=chef-repo/cookbooks
		
		$ cd wakaba/chef/chef-repo
		$ knife solo prepare 192.168.33.10
		$ knife solo cook 192.168.33.10
		
		
●Heroku

