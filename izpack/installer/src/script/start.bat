SET P_JAVA_HOME=C:\Program Files\Java\jdk1.7.0_01
SET P_PORT=9872
SET P_JETTY_HOME=C:\app\jetty
SET P_APP_DATA=%P_JETTY_HOME%\AppData

"%P_JAVA_HOME%\bin\java" -Xms128m -Xmx512m -XX:MaxPermSize=128m -jar start.jar  -Djetty.port=%P_PORT% -Dwakaba.home=%P_APP_DATA%
PAUSE
