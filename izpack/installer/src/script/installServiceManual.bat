
SET P_SERVICE_NAME=WakabaServer3

SET P_PORT=9872
SET P_JETTY_HOME="C:\Program Files\wakaba-server\jetty"
SET P_APP_DATA=%P_JETTY_HOME%\AppData

PUSHD %P_JETTY_HOME%
ECHO 
SET PRUNSRV=prunsrv64.exe

ECHO %PRUNSRV%

REM SET P_JAVA_HOME=C:\Program Files\Java\jdk1.7.0_01

%PRUNSRV% //IS//%P_SERVICE_NAME% ^
--DisplayName="%P_SERVICE_NAME%" ^
--Description="%P_SERVICE_NAME%" ^
--Install=%P_JETTY_HOME%\%PRUNSRV% ^
--Startup=manual ^
--LogPath=%P_JETTY_HOME%\jettylogs ^
--LogLevel=Warn ^
--StdOutput=auto ^
--StdError=auto ^
--StartMode=Java ^
--StopMode=Java ^
--Jvm=auto ^
--JvmMx=512 ^
++JvmOptions=-Dwakaba.home=%P_APP_DATA% ^
++JvmOptions=-Djetty.home=%P_JETTY_HOME% ^
++JvmOptions=-Djetty.port=%P_PORT% ^
++JvmOptions=-Dorg.eclipse.jetty.util.log.SOURCE=true  ^
++JvmOptions=-XX:MaxPermSize=128m  ^
++JvmOptions=-XX:+CMSClassUnloadingEnabled  ^
++JvmOptions=-XX:+CMSPermGenSweepingEnabled ^
--Classpath=%P_JETTY_HOME%\start.jar  ^
--StartClass=org.eclipse.jetty.start.Main ^
--StopTimeout=30 ^
--StopClass=org.eclipse.jetty.start.Main ^
++StopParams=--stop

SC CONFIG %P_SERVICE_NAME% START= AUTO


PAUSE