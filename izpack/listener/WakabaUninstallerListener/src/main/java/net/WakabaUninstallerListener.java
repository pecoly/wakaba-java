package net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.izforge.izpack.util.AbstractUIProgressHandler;
import com.izforge.izpack.event.SimpleUninstallerListener;

public class WakabaUninstallerListener extends SimpleUninstallerListener {
	public void beforeDeletion(List files, AbstractUIProgressHandler handler) {
		System.out.println("beforeDeletion");
		if (containsService()) {
			removeService();
		}
	}

	@Override
	public void afterDeletion(List files, AbstractUIProgressHandler handler) {
		System.out.println("afterDeletion");
	}

	boolean containsService() {
		Process p;
		try {
			p = Runtime.getRuntime().exec("SC QUERY WakabaServer");
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		boolean contains = false;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			while (true) {
				String text = reader.readLine();
				if (text == null) {
					break;
				}

				System.out.println("<" + text + ">");

				if (text.indexOf("WakabaServer") >= 0) {
					contains = true;
					break;
				}
			}
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		return contains;
	}

	void removeService() {
		System.out.println("removeService");

		Process p;
		try {
			p = Runtime.getRuntime().exec("SC DELETE WakabaServer");
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			while (true) {
				String text = reader.readLine();
				if (text == null) {
					break;
				}

				System.out.println("<" + text + ">");
			}
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
