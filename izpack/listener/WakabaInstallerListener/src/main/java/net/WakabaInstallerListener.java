package net;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import com.izforge.izpack.Pack;
import com.izforge.izpack.PackFile;
import com.izforge.izpack.util.AbstractUIProgressHandler;
import com.izforge.izpack.event.SimpleInstallerListener;
import com.izforge.izpack.installer.AutomatedInstallData;

public class WakabaInstallerListener extends SimpleInstallerListener {
	@Override
	public boolean isFileListener() {
		System.out.println("isFileListener");
		return true;
	}

	@Override
	public void afterPack(Pack pack, Integer i, AbstractUIProgressHandler handler) {
		System.out.println("afterPack");
	}

	@Override
	public void afterPacks(AutomatedInstallData idata, AbstractUIProgressHandler handler) {
		System.out.println("afterPacks");
	}

	@Override
	public void beforePacks(AutomatedInstallData idata, Integer npacks, AbstractUIProgressHandler handler) {
		System.out.println("beforePacks");
		if (containsService()) {
			removeService();
		}
	}

	@Override
	public void afterFile(File filePath, PackFile pf) throws Exception {
		System.out.println("afterFile");
	}

	@Override
	public void afterDir(File dirPath, PackFile pf) throws Exception {
		System.out.println("afterDir");
	}

	boolean containsService() {
		Process p;
		try {
			p = Runtime.getRuntime().exec("SC QUERY WakabaServer");
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		boolean contains = false;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			while (true) {
				String text = reader.readLine();
				if (text == null) {
					break;
				}

				System.out.println("<" + text + ">");

				if (text.indexOf("WakabaServer") >= 0) {
					contains = true;
					break;
				}
			}
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		return contains;
	}

	void removeService() {
		System.out.println("removeService");

		Process p;
		try {
			p = Runtime.getRuntime().exec("SC DELETE WakabaServer");
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			while (true) {
				String text = reader.readLine();
				if (text == null) {
					break;
				}

				System.out.println("<" + text + ">");
			}
		}
		catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
