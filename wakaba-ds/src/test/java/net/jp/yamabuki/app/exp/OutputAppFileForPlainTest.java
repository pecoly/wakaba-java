package net.jp.yamabuki.app.exp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.pkg.app.exp.AppFileConstants;
import net.jp.yamabuki.pkg.app.exp.OutputAppFileForPlain;
import net.jp.yamabuki.pkg.app.imp.InputAppFileForPlain;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class OutputAppFileForPlainTest {
	String DIR_NAME= "testdata";
	String FILE_PATH = DIR_NAME + "/" + AppFileConstants.FILE_NAME;

	@Before
	public void before() {
		File dir = new File(DIR_NAME);
		if (!dir.exists()) {
			dir.mkdir();
		}

		removeFile();
	}

	@After
	public void after() {
		removeFile();

		File dir = new File(DIR_NAME);
		if (dir.exists()) {
			dir.delete();
		}
	}

	public void removeFile() {
		File f = new File(FILE_PATH);
		if (f.exists()) {
			f.delete();
		}
	}

	@Test
	public void test() throws FileNotFoundException, IOException {

		AppFile outAppFile = new AppFile();
		outAppFile.setUserId(new UserIdImpl("suzuki"));
		outAppFile.setAppId(new AppIdImpl("jiro"));
		outAppFile.setAppName("hoge");

		// AppFile -> File
		OutputAppFileForPlain output = new OutputAppFileForPlain(DIR_NAME);

		// AppFileの内容を出力
		output.execute(outAppFile);

		AppFile inAppFile = null;

		// File -> AppFile
		InputAppFileForPlain input = new InputAppFileForPlain(DIR_NAME);
		inAppFile = input.execute();

		assertThat(inAppFile, is(not(nullValue())));
		assertThat(inAppFile.getUserId().getValue(), is("suzuki"));
		assertThat(inAppFile.getAppId().getValue(), is("jiro"));
		assertThat(inAppFile.getAppName(), is("hoge"));
	}
}
