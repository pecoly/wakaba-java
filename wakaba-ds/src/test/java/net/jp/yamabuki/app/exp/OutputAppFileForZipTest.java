package net.jp.yamabuki.app.exp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.pkg.app.exp.OutputAppFileForZip;
import net.jp.yamabuki.pkg.app.imp.InputAppFileForZip;

import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class OutputAppFileForZipTest {
	@Test
	public void test() throws FileNotFoundException, IOException {

		AppFile outAppFile = new AppFile();
		outAppFile.setUserId(new UserIdImpl("suzuki"));
		outAppFile.setAppId(new AppIdImpl("jiro"));
		outAppFile.setAppName("hoge");

		byte[] data = null;

		// AppFile -> Binary
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			try (ZipArchiveOutputStream outZip
					= new ZipArchiveOutputStream(out)) {
				OutputAppFileForZip output = new OutputAppFileForZip(outZip);

				// AppFileの内容を出力
				output.execute(outAppFile);
			}

			// 出力されたデータをバイナリで取得
			data = out.toByteArray();
		}

		AppFile inAppFile = null;

		// Binary -> AppFile
		try (ByteArrayInputStream in = new ByteArrayInputStream(data)) {
			try (ZipArchiveInputStream inZip
					= new ZipArchiveInputStream(in)) {

				InputAppFileForZip input = new InputAppFileForZip(inZip);
				inAppFile = input.execute();
			}
		}

		assertThat(inAppFile, is(not(nullValue())));
		assertThat(inAppFile.getUserId().getValue(), is("suzuki"));
		assertThat(inAppFile.getAppId().getValue(), is("jiro"));
		assertThat(inAppFile.getAppName(), is("hoge"));
	}
}
