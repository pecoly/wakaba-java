package net.jp.yamabuki.model.resource;

import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.util.DateTimeUtils;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MessageTest {

	public static class Constructor_WithModelId_WithLastModified_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");

		@Test
		public void codeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(id, userId, appId, null, localeId, message, lastModified);
		}

		@Test
		public void codeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(id, userId, appId, "", localeId, message, lastModified);

		}

		@Test
		public void codeIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' is not valid name.");
			new Message(id, userId, appId, "a.b", localeId, message, lastModified);
		}

		@Test
		public void localeIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'localeId' must not be null.");
			new Message(id, userId, appId, code, null, message, lastModified);
		}

		@Test
		public void messageIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(id, userId, appId, code, localeId, null, lastModified);
		}

		@Test
		public void messageIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(id, userId, appId, code, localeId, "", lastModified);
		}
	}
	public static class Constructor_WithModelId_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";

		@Test
		public void codeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(id, userId, appId, null, localeId, message);
		}

		@Test
		public void codeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(id, userId, appId, "", localeId, message);
		}

		@Test
		public void codeIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' is not valid name.");
			new Message(id, userId, appId, "a.b", localeId, message);
		}

		@Test
		public void localeIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'localeId' must not be null.");
			new Message(id, userId, appId, code, null, message);
		}

		@Test
		public void messageIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(id, userId, appId, code, localeId, null);
		}

		@Test
		public void messageIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(id, userId, appId, code, localeId, "");
		}
	}
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";

		@Test
		public void codeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(userId, appId, null, localeId, message);
		}

		@Test
		public void codeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' must not be blank.");
			new Message(userId, appId, "", localeId, message);
		}

		@Test
		public void codeIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'code' is not valid name.");
			new Message(userId, appId, "a.b", localeId, message);
		}

		@Test
		public void localeIdIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'localeId' must not be null.");
			new Message(userId, appId, code, null, message);
		}

		@Test
		public void messageIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(userId, appId, code, localeId, null);
		}

		@Test
		public void messageIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'message' must not be blank.");
			new Message(userId, appId, code, localeId, "");
		}
	}

	public static class Constructor_WithModelId_WithLastModified_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Message model = new Message(id, userId, appId,
				code, localeId, message, lastModified);
		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getCode(), is(code));
			assertThat(model.getLocaleId(), is(localeId));
			assertThat(model.getMessage(), is(message));
			assertThat(model.getLastModified(), is(new DateTime(2001, 2, 3, 4, 5, 6)));
		}
	}

	public static class Constructor_WithModelId_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		Message model = new Message(id, userId, appId,
				code, localeId, message);

		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getCode(), is(code));
			assertThat(model.getLocaleId(), is(localeId));
			assertThat(model.getMessage(), is(message));
		}
	}

	public static class Constructor_Ok {
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		Message model = new Message(userId, appId,
				code, localeId, message);

		@Test
		public void value() {
			assertThat(model.getId().getValue(), is(nullValue()));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getCode(), is(code));
			assertThat(model.getLocaleId(), is(localeId));
			assertThat(model.getMessage(), is(message));
		}
	}

	public static class ToStringMap_ToString {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Message model = new Message(id, userId, appId,
				code, localeId, message, lastModified);

		Map<String, String> map = model.toStringMap();
		String string = model.toString();

		@Test
		public void toStringMap() {
			assertThat(map.size(), is(7));
			assertThat(map.containsKey("id"), is(true));
			assertThat(map.containsKey("userId"), is(true));
			assertThat(map.containsKey("appId"), is(true));
			assertThat(map.containsKey("lastModified"), is(true));
			assertThat(map.containsKey("code"), is(true));
			assertThat(map.containsKey("localeId"), is(true));
			assertThat(map.containsKey("message"), is(true));
			assertThat(map.get("id"), is("9876"));
			assertThat(map.get("userId"), is("suzuki"));
			assertThat(map.get("appId"), is("jiro"));
			assertThat(map.get("lastModified"), is("20010203040506"));
			assertThat(map.get("code"), is("code"));
			assertThat(map.get("localeId"), is("ja"));
			assertThat(map.get("message"), is("message"));
		}

		@Test
		public void toStringTest() {
			assertThat(string, is("{ id : 9876, userId : suzuki, appId : jiro, "
					+ "code : code, localeId : ja, message : message }"));
		}
	}
	public static class Equals {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String code = "code";
		LocaleId localeId = new LocaleIdImpl("ja");
		String message = "message";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		MessageBuilder builder = new MessageBuilder("9876", "suzuki", "jiro",
				"code", "ja", "message", "20010203040506");
		Message model = builder.build();

		@Test
		public void equals_null() {
			assertThat(model.equals(null), is(false));
		}

		@Test
		public void equals_string() {
			assertThat(model.equals("9876"), is(false));
		}

		@Test
		public void equals_modelId() {
			Message tmp = new MessageBuilder(builder).withId("5678").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_userId() {
			Message tmp = new MessageBuilder(builder).withUserId("tanaka").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_appId() {
			Message tmp = new MessageBuilder(builder).withAppId("taro").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_code() {
			Message tmp = new MessageBuilder(builder).withCode("hoge").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_localeId() {
			Message tmp = new MessageBuilder(builder).withLocaleId("en").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_message() {
			Message tmp = new MessageBuilder(builder).withMessage("fuga").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals() {
			Message tmp = new MessageBuilder(builder).build();
			assertThat(model.equals(tmp), is(true));
		}

		@Test
		public void ref() {
			assertThat(model.equals(model), is(true));
		}
	}
}
