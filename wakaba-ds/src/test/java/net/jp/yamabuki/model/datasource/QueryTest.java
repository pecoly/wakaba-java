package net.jp.yamabuki.model.datasource;

import static org.junit.Test.*;

import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class QueryTest {
	public static class Constructor_WithModelId_WithLastModified_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");

		@Test
		public void typeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(id, userId, appId, null, name, xml, lastModified);
		}

		@Test
		public void typeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(id, userId, appId, "", name, xml, lastModified);
		}

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(id, userId, appId, type, null, xml, lastModified);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(id, userId, appId, type, "", xml, lastModified);
		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Query(id, userId, appId, type, "a.b", xml, lastModified);
		}

		@Test
		public void xmlIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(id, userId, appId, type, name, null, lastModified);
		}

		@Test
		public void xmlIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(id, userId, appId, type, name, null, lastModified);
		}
	}
	public static class Constructor_WithModelId_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";

		@Test
		public void typeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(id, userId, appId, null, name, xml);
		}

		@Test
		public void typeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(id, userId, appId, "", name, xml);
		}

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(id, userId, appId, type, null, xml);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(id, userId, appId, type, "", xml);
		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Query(id, userId, appId, type, "a.b", xml);
		}

		@Test
		public void xmlIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(id, userId, appId, type, name, null);
		}

		@Test
		public void xmlIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(id, userId, appId, type, name, null);
		}
	}
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";

		@Test
		public void typeIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(userId, appId, null, name, xml);
		}

		@Test
		public void typeIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'type' must not be blank.");
			new Query(userId, appId, "", name, xml);
		}

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(userId, appId, type, null, xml);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Query(userId, appId, type, "", xml);
		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Query(userId, appId, type, "a.b", xml);

		}

		@Test
		public void xmlIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(userId, appId, type, name, null);
		}

		@Test
		public void xmlIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'xml' must not be blank.");
			new Query(userId, appId, type, name, null);
		}
	}

	public static class Constructor_WithModelId_WithLastModified_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Query model = new Query(id, userId, appId,
				type, name, xml, lastModified);
		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getType(), is(type));
			assertThat(model.getName(), is(name));
			assertThat(model.getXml(), is(xml));
			assertThat(model.getLastModified(), is(new DateTime(2001, 2, 3, 4, 5, 6)));
		}
	}

	public static class Constructor_WithModelId_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		Query model = new Query(id, userId, appId,
				type, name, xml);

		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getName(), is(name));
			assertThat(model.getType(), is(type));
			assertThat(model.getName(), is(name));
			assertThat(model.getXml(), is(xml));
		}
	}

	public static class Constructor_Ok {
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		Query model = new Query(userId, appId,
				type, name, xml);

		@Test
		public void value() {
			assertThat(model.getId().getValue(), is(nullValue()));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getName(), is(name));
			assertThat(model.getType(), is(type));
			assertThat(model.getName(), is(name));
			assertThat(model.getXml(), is(xml));
		}
	}

	public static class ToStringMap_ToString {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Query model = new Query(id, userId, appId,
				type, name, xml, lastModified);

		Map<String, String> map = model.toStringMap();
		String string = model.toString();

		@Test
		public void toStringMap() {
			assertThat(map.size(), is(7));
			assertThat(map.containsKey("id"), is(true));
			assertThat(map.containsKey("userId"), is(true));
			assertThat(map.containsKey("appId"), is(true));
			assertThat(map.containsKey("lastModified"), is(true));
			assertThat(map.containsKey("type"), is(true));
			assertThat(map.containsKey("name"), is(true));
			assertThat(map.containsKey("xml"), is(true));
			assertThat(map.get("id"), is("9876"));
			assertThat(map.get("userId"), is("suzuki"));
			assertThat(map.get("appId"), is("jiro"));
			assertThat(map.get("lastModified"), is("20010203040506"));
			assertThat(map.get("type"), is("query"));
			assertThat(map.get("name"), is("hoge"));
			assertThat(map.get("xml"), is("fuga"));
		}

		@Test
		public void toStringTest() {
			assertThat(string, is("{ id : 9876, userId : suzuki, appId : jiro, "
					+ "type : query, name : hoge, xml : fuga }"));
		}
	}
	public static class Equals {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String type = "query";
		String name = "hoge";
		String xml = "fuga";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		QueryBuilder builder = new QueryBuilder("9876", "suzuki", "jiro",
				"query", "hoge", "fuga", "20010203040506");
		Query model = builder.build();

		@Test
		public void equals_null() {
			assertThat(model.equals(null), is(false));
		}

		@Test
		public void equals_string() {
			assertThat(model.equals("9876"), is(false));
		}

		@Test
		public void equals_modelId() {
			Query tmp = new QueryBuilder(builder).withId("5678").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_userId() {
			Query tmp = new QueryBuilder(builder).withUserId("tanaka").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_appId() {
			Query tmp = new QueryBuilder(builder).withAppId("taro").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_type() {
			Query tmp = new QueryBuilder(builder).withType("edc").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_name() {
			Query tmp = new QueryBuilder(builder).withName("foo").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_xml() {
			Query tmp = new QueryBuilder(builder).withXml("bar").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals() {
			Query tmp = new QueryBuilder(builder).build();
			assertThat(model.equals(tmp), is(true));
		}

		@Test
		public void ref() {
			assertThat(model.equals(model), is(true));
		}
	}
}
