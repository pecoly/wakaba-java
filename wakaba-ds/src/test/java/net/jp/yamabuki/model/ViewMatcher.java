package net.jp.yamabuki.model;

import net.jp.yamabuki.model.View;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class ViewMatcher extends TypeSafeMatcher<View> {
	private final View expected;
	
	private String description;
	
	public ViewMatcher(View expected) {
		this.expected = expected;
	}
	
	public static ViewMatcher matches(View view) {
		return new ViewMatcher(view);
	}

	@Override
	protected boolean matchesSafely(View actual) {
		if (!(actual instanceof View)) {
			this.description = "Parameter 'actual' is not View.";
			return false;
		}

		View obj = (View)actual;
		
		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;			
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;			
		}		
		
		
		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			this.description = "'Name' are not equal.";
			return false;
		}
		
		// Content
		if (!this.expected.getContent().equals(obj.getContent())) {
			this.description = "'Content' are not equal.";
			return false;
		}
		
		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
