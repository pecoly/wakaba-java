package net.jp.yamabuki.model;

import net.jp.yamabuki.model.JdbcConnection;

import org.joda.time.DateTime;
import org.junit.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class JdbcConnectionTest {
	public static class Matcher {
		@Test
		public void notEqualsUserId() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withUserId("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withUserId("def")
					.build();

			assertThat(obj1, is(not(JdbcConnectionMatcher.matches(obj2))));
		}
	}

	public static class Equals {
		@Test
		public void notEqualsId() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withId("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withId("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsUserId() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withUserId("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withUserId("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsAppId() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withAppId("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withAppId("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsName() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withName("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withName("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsDriver() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withDriver("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withDriver("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsUrl() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withUrl("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withUrl("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsUsername() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withUsername("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withUsername("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void notEqualsPassword() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.withPassword("abc")
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.withPassword("def")
					.build();

			assertThat(obj1.equals(obj2), is(false));
		}

		@Test
		public void equals() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.build();

			JdbcConnection obj2 = new JdbcConnectionBuilder(builder)
					.build();

			assertThat(obj1.equals(obj2), is(true));
		}

		@Test
		public void refEquals() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.build();

			assertThat(obj1.equals(obj1), is(true));
		}

		@Test
		public void notEqualsNull() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.build();

			assertThat(obj1.equals(null), is(false));
		}

		@Test
		public void notEqualsClass() {
			JdbcConnectionBuilder builder
					= new JdbcConnectionBuilder(null,
							"a", "b", "c",
							"s", "t", "u", "v", null);

			JdbcConnection obj1 = new JdbcConnectionBuilder(builder)
					.build();

			assertThat(obj1.equals(""), is(false));
		}
	}

	public static class Constructor {
		JdbcConnection obj1
				= new JdbcConnectionBuilder(null,
					"a", "b", "c",
					"s", "t", "u", "v", null)
				.build();

		@Test
		public void id() {
			assertThat(obj1.getId().getValue(), is(nullValue()));
		}
		@Test
		public void userId() {
			assertThat(obj1.getUserId().getValue(), is("a"));
		}
		@Test
		public void appId() {
			assertThat(obj1.getAppId().getValue(), is("b"));
		}
		@Test
		public void name() {
			assertThat(obj1.getName(), is("c"));
		}
		@Test
		public void driver() {
			assertThat(obj1.getDriver(), is("s"));
		}
		@Test
		public void url() {
			assertThat(obj1.getUrl(), is("t"));
		}
		@Test
		public void username() {
			assertThat(obj1.getUsername(), is("u"));
		}
		@Test
		public void password() {
			assertThat(obj1.getPassword(), is("v"));
		}
	}

	public static class ConstructorWithId {
		JdbcConnection obj1
				= new JdbcConnectionBuilder("123",
					"a", "b", "c",
					"s", "t", "u", "v", null)
				.build();

		@Test
		public void id() {
			assertThat(obj1.getId().getValue(), is("123"));
		}
		@Test
		public void userId() {
			assertThat(obj1.getUserId().getValue(), is("a"));
		}
		@Test
		public void appId() {
			assertThat(obj1.getAppId().getValue(), is("b"));
		}
		@Test
		public void name() {
			assertThat(obj1.getName(), is("c"));
		}
		@Test
		public void driver() {
			assertThat(obj1.getDriver(), is("s"));
		}
		@Test
		public void url() {
			assertThat(obj1.getUrl(), is("t"));
		}
		@Test
		public void username() {
			assertThat(obj1.getUsername(), is("u"));
		}
		@Test
		public void password() {
			assertThat(obj1.getPassword(), is("v"));
		}
	}

	public static class ConstructorWithIdWithLastModified {
		JdbcConnection obj1
				= new JdbcConnectionBuilder("123",
					"a", "b", "c",
					"s", "t", "u", "v", "20120304112233")
				.build();

		@Test
		public void id() {
			assertThat(obj1.getId().getValue(), is("123"));
		}
		@Test
		public void userId() {
			assertThat(obj1.getUserId().getValue(), is("a"));
		}
		@Test
		public void appId() {
			assertThat(obj1.getAppId().getValue(), is("b"));
		}
		@Test
		public void name() {
			assertThat(obj1.getName(), is("c"));
		}
		@Test
		public void driver() {
			assertThat(obj1.getDriver(), is("s"));
		}
		@Test
		public void url() {
			assertThat(obj1.getUrl(), is("t"));
		}
		@Test
		public void username() {
			assertThat(obj1.getUsername(), is("u"));
		}
		@Test
		public void password() {
			assertThat(obj1.getPassword(), is("v"));
		}
		@Test
		public void lastModified() {
			assertThat(obj1.getLastModified(), is(new DateTime(2012, 3, 4, 11, 22, 33)));
		}
	}
}
