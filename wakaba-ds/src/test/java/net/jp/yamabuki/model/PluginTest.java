package net.jp.yamabuki.model;

import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PluginTest {
	public static class Constructor_WithModelId_WithLastModified_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String content = "content";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(id, userId, appId, null, content, lastModified);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(id, userId, appId, "", content, lastModified);

		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Plugin(id, userId, appId, "a.b", content, lastModified);

		}

		@Test
		public void descriptionIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(id, userId, appId, name, null, lastModified);
		}

		@Test
		public void descriptionIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(id, userId, appId, name, "", lastModified);
		}
	}
	public static class Constructor_WithModelId_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String content = "content";

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(id, userId, appId, null, content);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(id, userId, appId, "", content);

		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Plugin(id, userId, appId, "a.b", content);

		}

		@Test
		public void descriptionIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(id, userId, appId, name, null);
		}

		@Test
		public void descriptionIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(id, userId, appId, name, "");
		}
	}
	public static class Constructor_Ng {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String content = "content";

		@Test
		public void nameIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(userId, appId, null, content);
		}

		@Test
		public void nameIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' must not be blank.");
			new Plugin(userId, appId, "", content);

		}

		@Test
		public void nameIsInvalid() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'name' is not valid name.");
			new Plugin(userId, appId, "a.b", content);

		}

		@Test
		public void descriptionIsNull() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(userId, appId, name, null);
		}

		@Test
		public void descriptionIsEmpty() {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("'description' must not be blank.");
			new Plugin(userId, appId, name, "");
		}
	}

	public static class Constructor_WithModelId_WithLastModified_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String description = "description";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Plugin model = new Plugin(id, userId, appId,
				name, description, lastModified);
		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getName(), is(name));
			assertThat(model.getDescription(), is(description));
			assertThat(model.getLastModified(), is(new DateTime(2001, 2, 3, 4, 5, 6)));
		}
	}

	public static class Constructor_WithModelId_Ok {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String description = "description";
		Plugin model = new Plugin(id, userId, appId,
				name, description);

		@Test
		public void value() {
			assertThat(model.getId(), is(id));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getName(), is(name));
			assertThat(model.getDescription(), is(description));
		}
	}

	public static class Constructor_Ok {
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String description = "description";
		Plugin model = new Plugin(userId, appId, name, description);

		@Test
		public void value() {
			assertThat(model.getId().getValue(), is(nullValue()));
			assertThat(model.getUserId(), is(userId));
			assertThat(model.getAppId(), is(appId));
			assertThat(model.getName(), is(name));
			assertThat(model.getDescription(), is(description));
		}
	}

	public static class ToStringMap_ToString {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String description = "description";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		Plugin model = new Plugin(id, userId, appId,
				name, description, lastModified);

		Map<String, String> map = model.toStringMap();
		String string = model.toString();

		@Test
		public void toStringMap() {
			assertThat(map.size(), is(6));
			assertThat(map.containsKey("id"), is(true));
			assertThat(map.containsKey("userId"), is(true));
			assertThat(map.containsKey("appId"), is(true));
			assertThat(map.containsKey("lastModified"), is(true));
			assertThat(map.containsKey("name"), is(true));
			assertThat(map.containsKey("description"), is(true));
			assertThat(map.get("id"), is("9876"));
			assertThat(map.get("userId"), is("suzuki"));
			assertThat(map.get("appId"), is("jiro"));
			assertThat(map.get("lastModified"), is("20010203040506"));
			assertThat(map.get("name"), is("name"));
			assertThat(map.get("description"), is("description"));
		}

		@Test
		public void toStringTest() {
			assertThat(string, is("{ id : 9876, userId : suzuki, appId : jiro, "
					+ "name : name, description : description }"));
		}
	}
	public static class Equals {
		ModelId id = new ModelIdImpl("9876");
		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "name";
		String description = "description";
		DateTime lastModified = DateTimeUtils
				.toDateTimeFromStringYYYYMMDDHHMISS("20010203040506");
		PluginBuilder builder = new PluginBuilder("9876", "suzuki", "jiro",
				"name", "description", "20010203040506");
		Plugin model = builder.build();

		@Test
		public void equals_null() {
			assertThat(model.equals(null), is(false));
		}

		@Test
		public void equals_string() {
			assertThat(model.equals("9876"), is(false));
		}

		@Test
		public void equals_modelId() {
			Plugin tmp = new PluginBuilder(builder).withId("5678").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_userId() {
			Plugin tmp = new PluginBuilder(builder).withUserId("tanaka").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_appId() {
			Plugin tmp = new PluginBuilder(builder).withAppId("taro").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_name() {
			Plugin tmp = new PluginBuilder(builder).withName("hoge").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals_description() {
			Plugin tmp = new PluginBuilder(builder).withDescription("fuga").build();
			assertThat(model.equals(tmp), is(false));
		}

		@Test
		public void equals() {
			Plugin tmp = new PluginBuilder(builder).build();
			assertThat(model.equals(tmp), is(true));
		}

		@Test
		public void ref() {
			assertThat(model.equals(model), is(true));
		}
	}
}
