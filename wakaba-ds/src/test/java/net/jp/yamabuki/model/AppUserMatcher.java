package net.jp.yamabuki.model;

import net.jp.yamabuki.model.AppUser;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class AppUserMatcher extends TypeSafeMatcher<AppUser> {
	private final AppUser expected;

	private String description;

	public AppUserMatcher(AppUser expected) {
		this.expected = expected;
	}

	public static AppUserMatcher matches(AppUser appUser) {
		return new AppUserMatcher(appUser);
	}

	@Override
	protected boolean matchesSafely(AppUser actual) {
		if (!(actual instanceof AppUser)) {
			this.description = "Parameter 'actual' is not AppUser.";
			return false;
		}

		AppUser obj = (AppUser)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// LoginId
		if (!this.expected.getLoginId().equals(obj.getLoginId())) {
			this.description = "'LoginId' are not equal.";
			return false;
		}

		// LoginPassword
		if (!this.expected.getLoginPassword().equals(obj.getLoginPassword())) {
			this.description = "'LoginPassword' are not equal.";
			return false;
		}

		// MainAuthority
		if (!this.expected.getMainAuthority().equals(obj.getMainAuthority())) {
			this.description = "'MainAuthority' are not equal.";
			return false;
		}

		// SubAuthority
		if (!this.expected.getSubAuthority().equals(obj.getSubAuthority())) {
			this.description = "'SubAuthority' are not equal.";
			return false;
		}

		// UserIdAuthority
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppIdAuthority
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
