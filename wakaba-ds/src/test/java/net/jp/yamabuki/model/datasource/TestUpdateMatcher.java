package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.datasource.TestUpdate;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class TestUpdateMatcher extends TypeSafeMatcher<TestUpdate> {
	private final TestUpdate expected;

	private String description;

	public TestUpdateMatcher(TestUpdate expected) {
		Argument.isNotNull(expected, "expected");
		this.expected = expected;
	}

	public static TestUpdateMatcher matches(TestUpdate query) {
		return new TestUpdateMatcher(query);
	}

	@Override
	protected boolean matchesSafely(TestUpdate actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof TestUpdate)) {
			this.description = "Parameter 'actual' is not Query.";
			return false;
		}

		TestUpdate obj = (TestUpdate)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Xml
		if (!this.expected.getXml().equals(obj.getXml())) {
			this.description = "'Xml' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
