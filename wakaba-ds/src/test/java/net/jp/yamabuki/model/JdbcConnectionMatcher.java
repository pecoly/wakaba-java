package net.jp.yamabuki.model;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.JdbcConnection;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class JdbcConnectionMatcher extends TypeSafeMatcher<JdbcConnection> {
	private final JdbcConnection expected;

	private String description;

	public JdbcConnectionMatcher(JdbcConnection expected) {
		this.expected = expected;
		Argument.isNotNull(expected, "expected");
	}

	public static JdbcConnectionMatcher matches(JdbcConnection message) {
		return new JdbcConnectionMatcher(message);
	}

	@Override
	protected boolean matchesSafely(JdbcConnection actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof JdbcConnection)) {
			this.description = "Parameter 'actual' is not JdbcConnection.";
			return false;
		}

		JdbcConnection obj = (JdbcConnection)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			this.description = "'Name' are not equal.";
			return false;
		}

		// Driver
		if (!this.expected.getDriver().equals(obj.getDriver())) {
			this.description = "'Driver' are not equal.";
			return false;
		}

		// Url
		if (!this.expected.getUrl().equals(obj.getUrl())) {
			this.description = "'Url' are not equal.";
			return false;
		}

		// Username
		if (!this.expected.getUsername().equals(obj.getUsername())) {
			this.description = "'Username' are not equal.";
			return false;
		}

		// Password
		if (!this.expected.getPassword().equals(obj.getPassword())) {
			this.description = "'Password' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}


}
