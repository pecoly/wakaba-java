package net.jp.yamabuki.model.validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.converter.IntegerConverter;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.*;

public class UpdateParameterListValidatorTest {
	UpdateParameterListValidator validator;

	@Before
	public void before() {
		UpdateParameter p1 = new UpdateParameter("name", new StringConverter(),
				Arrays.asList(new Validator[] {
						new NotEmptyValidator(),
				}));
		UpdateParameter p2 = new UpdateParameter("age", new IntegerConverter(),
				Arrays.asList(new Validator[] {
						new MinValidator(0),
				}));

		validator = new UpdateParameterListValidator(
				Arrays.asList(new UpdateParameter[] {
						p1, p2,
				}));
	}

	@Test
	public void ok() {
		Map<String, String> map = new HashMap<>();
		map.put("name", "hoge");
		map.put("age", "20");

		String error = validator.validate(map);
		assertThat(error, is(nullValue()));

		Map<String, Object> result = validator.getObjectParameterList();
		assertThat(result.size(), is(2));
		assertThat((String)result.get("name"), is("hoge"));
		assertThat((Integer)result.get("age"), is(20));
	}

	@Test
	public void moreParameter() {
		Map<String, String> map = new HashMap<>();
		map.put("name", "hoge");
		map.put("age", "20");
		map.put("address", "fuga");

		String error = validator.validate(map);
		assertThat(error, is(nullValue()));

		Map<String, Object> result = validator.getObjectParameterList();
		assertThat(result.size(), is(3));
		assertThat((String)result.get("name"), is("hoge"));
		assertThat((Integer)result.get("age"), is(20));
		assertThat((String)result.get("address"), is("fuga"));
	}

	@Test
	public void nameIsEmpty() {
		Map<String, String> map = new HashMap<>();
		map.put("name", "");
		map.put("age", "20");
		map.put("address", "fuga");

		String error = validator.validate(map);
		assertThat(error, is("must not be empty."));
	}
}
