package net.jp.yamabuki.model;

import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

public class ViewTemplateBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String content;

	private String lastModified;

	public ViewTemplateBuilder(String id, String userId, String appId,
			String name, String content, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.content = content;
		this.lastModified = lastModified;
	}

	public ViewTemplateBuilder(ViewTemplateBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.content = builder.content;
		this.lastModified = builder.lastModified;
	}

	public ViewTemplateBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public ViewTemplateBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public ViewTemplateBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public ViewTemplateBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public ViewTemplateBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public ViewTemplateBuilder withContent(String content) {
		this.content = content;
		return this;
	}

	public ViewTemplateBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public ViewTemplate build() {
		if (this.id == null) {
			return new ViewTemplate(
					UserIdUtils.create(this.userId),
					AppIdUtils.create(this.appId),
					this.name, this.content);
		}
		else {
			if (this.lastModified == null) {
				return new ViewTemplate(new ModelIdImpl(this.id),
						UserIdUtils.create(this.userId),
						AppIdUtils.create(this.appId),
						this.name, this.content);
			}
			else {
				return new ViewTemplate(new ModelIdImpl(this.id),
						UserIdUtils.create(this.userId),
						AppIdUtils.create(this.appId),
						this.name, this.content,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
