package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.datasource.TestQuery;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class TestQueryMatcher extends TypeSafeMatcher<TestQuery> {
	private final TestQuery expected;

	private String description;

	public TestQueryMatcher(TestQuery expected) {
		Argument.isNotNull(expected, "expected");
		this.expected = expected;
	}

	public static TestQueryMatcher matches(TestQuery query) {
		return new TestQueryMatcher(query);
	}

	@Override
	protected boolean matchesSafely(TestQuery actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof TestQuery)) {
			this.description = "Parameter 'actual' is not Query.";
			return false;
		}

		TestQuery obj = (TestQuery)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Xml
		if (!this.expected.getXml().equals(obj.getXml())) {
			this.description = "'Xml' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
