package net.jp.yamabuki.model;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.util.DateTimeUtils;

public class JdbcConnectionBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String driver;

	private String url;

	private String username;

	private String password;

	private String lastModified;

	public JdbcConnectionBuilder(String id, String userId, String appId,
			String name, String driver, String url,
			String username, String password, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
		this.lastModified = lastModified;
	}

	public JdbcConnectionBuilder(JdbcConnectionBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.driver = builder.driver;
		this.url = builder.url;
		this.username = builder.username;
		this.password = builder.password;
		this.lastModified = builder.lastModified;
	}

	public JdbcConnectionBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public JdbcConnectionBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public JdbcConnectionBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public JdbcConnectionBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public JdbcConnectionBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public JdbcConnectionBuilder withDriver(String driver) {
		this.driver = driver;
		return this;
	}

	public JdbcConnectionBuilder withUrl(String url) {
		this.url = url;
		return this;
	}

	public JdbcConnectionBuilder withUsername(String username) {
		this.username = username;
		return this;
	}

	public JdbcConnectionBuilder withPassword(String password) {
		this.password = password;
		return this;
	}

	public JdbcConnectionBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public JdbcConnection build() {
		if (this.id == null) {
			return new JdbcConnection(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.name,
					this.driver, this.url,
					this.username, this.password);
		}
		else {
			if (this.lastModified == null) {
				return new JdbcConnection(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name,
						this.driver, this.url,
						this.username, this.password);
			}
			else {
				return new JdbcConnection(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name,
						this.driver, this.url,
						this.username, this.password,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
