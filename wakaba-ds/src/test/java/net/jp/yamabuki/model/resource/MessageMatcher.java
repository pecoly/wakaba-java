package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.resource.Message;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class MessageMatcher extends TypeSafeMatcher<Message> {
	private final Message expected;

	private String description;

	public MessageMatcher(Message expected) {
		this.expected = expected;
		Argument.isNotNull(expected, "expected");
	}

	public static MessageMatcher matches(Message message) {
		return new MessageMatcher(message);
	}

	@Override
	protected boolean matchesSafely(Message actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof Message)) {
			this.description = "Parameter 'actual' is not Message.";
			return false;
		}

		Message obj = (Message)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Code
		if (!this.expected.getCode().equals(obj.getCode())) {
			this.description = "'Code' are not equal.";
			return false;
		}

		// Locale
		if (!this.expected.getLocaleId().equals(obj.getLocaleId())) {
			this.description = "'LocaleId' are not equal.";
			return false;
		}

		// Message
		if (!this.expected.getMessage().equals(obj.getMessage())) {
			this.description = "'Message' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
