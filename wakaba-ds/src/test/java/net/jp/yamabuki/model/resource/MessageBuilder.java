package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.util.DateTimeUtils;

public class MessageBuilder {
	private String id;

	private String userId;

	private String appId;

	private String code;

	private String localeId;

	private String message;

	private String lastModified;

	public MessageBuilder(String id, String userId, String appId,
			String code, String localeId, String message, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.code = code;
		this.localeId = localeId;
		this.message = message;
		this.lastModified = lastModified;
	}

	public MessageBuilder(MessageBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.code = builder.code;
		this.localeId = builder.localeId;
		this.message = builder.message;
	}

	public MessageBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public MessageBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public MessageBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public MessageBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public MessageBuilder withCode(String code) {
		this.code = code;
		return this;
	}

	public MessageBuilder withLocaleId(String localeId) {
		this.localeId = localeId;
		return this;
	}

	public MessageBuilder withMessage(String message) {
		this.message = message;
		return this;
	}

	public MessageBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Message build() {
		if (this.id == null) {
			return new Message(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.code,
					new LocaleIdImpl(this.localeId), this.message);
		}
		else {
			if (this.lastModified == null) {
				return new Message(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.code,
						new LocaleIdImpl(this.localeId), this.message);
			}
			else {
				return new Message(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.code,
						new LocaleIdImpl(this.localeId), this.message,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
