package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.model.resource.Css;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class CssMatcher extends TypeSafeMatcher<Css> {
	private final Css expected;
	
	public CssMatcher(Css expected) {
		this.expected = expected;
	}
	
	public static CssMatcher matches(Css view) {
		return new CssMatcher(view);
	}

	@Override
	protected boolean matchesSafely(Css actual) {
		if (!(actual instanceof Css)) {
			return false;
		}

		Css obj = (Css)actual;
		
		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			return false;			
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			return false;			
		}		
		
		
		if (!this.expected.getId().equals(obj.getId())) {
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			return false;
		}
		
		// Content
		if (!this.expected.getContent().equals(obj.getContent())) {
			return false;
		}
		
		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
