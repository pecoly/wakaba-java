package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.TestQuery;

public class TestQueryBuilder {
	private String id;

	private String userId;

	private String appId;

	private String xml;

	public TestQueryBuilder(String id, String userId, String appId,
			String type, String name, String xml) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.xml = xml;
	}

	public TestQueryBuilder(TestQueryBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.xml = builder.xml;
	}

	public TestQueryBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public TestQueryBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public TestQueryBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public TestQueryBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public TestQueryBuilder withXml(String xml) {
		this.xml = xml;
		return this;
	}

	public TestQuery build() {
		if (this.id != null) {
			return new TestQuery(new ModelIdImpl(this.id),
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.xml);
		}
		else {
			return new TestQuery(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.xml);
		}
	}
}
