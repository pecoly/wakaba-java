package net.jp.yamabuki.model;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.util.DateTimeUtils;

public class PluginBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String description;

	private String lastModified;

	public PluginBuilder(String id, String userId, String appId,
			String name, String description, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.description = description;
		this.lastModified = lastModified;
	}

	public PluginBuilder(PluginBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.description = builder.description;
		this.lastModified = builder.lastModified;
	}

	public PluginBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public PluginBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public PluginBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public PluginBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public PluginBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public PluginBuilder withDescription(String description) {
		this.description = description;
		return this;
	}

	public PluginBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Plugin build() {
		if (this.id == null) {
			return new Plugin(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.name, this.description);
		}
		else {
			if (this.lastModified == null) {
				return new Plugin(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.description);
			}
			else {
				return new Plugin(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.description,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
