package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.TestUpdate;

public class TestUpdateBuilder {
	private String id;

	private String userId;

	private String appId;

	private String xml;

	public TestUpdateBuilder(String id, String userId, String appId,
			String type, String name, String xml) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.xml = xml;
	}

	public TestUpdateBuilder(TestUpdateBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.xml = builder.xml;
	}

	public TestUpdateBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public TestUpdateBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public TestUpdateBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public TestUpdateBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public TestUpdateBuilder withXml(String xml) {
		this.xml = xml;
		return this;
	}

	public TestUpdate build() {
		if (this.id != null) {
			return new TestUpdate(new ModelIdImpl(this.id),
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.xml);
		}
		else {
			return new TestUpdate(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.xml);
		}
	}
}
