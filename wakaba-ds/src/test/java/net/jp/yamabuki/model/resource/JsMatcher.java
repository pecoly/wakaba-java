package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.model.resource.Js;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class JsMatcher extends TypeSafeMatcher<Js> {
	private final Js expected;

	public JsMatcher(Js expected) {
		this.expected = expected;
	}

	public static JsMatcher matches(Js model) {
		return new JsMatcher(model);
	}

	@Override
	protected boolean matchesSafely(Js actual) {
		if (!(actual instanceof Js)) {
			return false;
		}

		Js obj = (Js)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			return false;
		}


		if (!this.expected.getId().equals(obj.getId())) {
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			return false;
		}

		// Content
		if (!this.expected.getContent().equals(obj.getContent())) {
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
