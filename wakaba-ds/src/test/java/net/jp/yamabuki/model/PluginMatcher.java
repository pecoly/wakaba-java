package net.jp.yamabuki.model;

import net.jp.yamabuki.model.Plugin;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class PluginMatcher extends TypeSafeMatcher<Plugin> {
	private final Plugin expected;

	public PluginMatcher(Plugin expected) {
		this.expected = expected;
	}

	public static PluginMatcher matches(Plugin view) {
		return new PluginMatcher(view);
	}

	@Override
	protected boolean matchesSafely(Plugin actual) {
		if (!(actual instanceof Plugin)) {
			return false;
		}

		Plugin obj = (Plugin)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			return false;
		}


		if (!this.expected.getId().equals(obj.getId())) {
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			return false;
		}

		// Description
		if (!this.expected.getDescription().equals(obj.getDescription())) {
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
