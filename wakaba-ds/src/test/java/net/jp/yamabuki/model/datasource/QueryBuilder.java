package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.util.DateTimeUtils;

public class QueryBuilder {
	private String id;

	private String userId;

	private String appId;

	private String type;

	private String name;

	private String xml;

	private String lastModified;

	public QueryBuilder(String id, String userId, String appId,
			String type, String name, String xml, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.type = type;
		this.name = name;
		this.xml = xml;
		this.lastModified = lastModified;
	}

	public QueryBuilder(QueryBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.type = builder.type;
		this.name = builder.name;
		this.xml = builder.xml;
		this.lastModified = builder.lastModified;
	}

	public QueryBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public QueryBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public QueryBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public QueryBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public QueryBuilder withType(String type) {
		this.type = type;
		return this;
	}

	public QueryBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public QueryBuilder withXml(String xml) {
		this.xml = xml;
		return this;
	}

	public QueryBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Query build() {
		if (this.id == null) {
			return new Query(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.type, this.name, this.xml);
		}
		else {

			if (this.lastModified == null) {
				return new Query(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.type, this.name, this.xml);
			}
			else {
				return new Query(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.type, this.name, this.xml,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
