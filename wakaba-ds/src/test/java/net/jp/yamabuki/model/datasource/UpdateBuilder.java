package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.util.DateTimeUtils;

public class UpdateBuilder {
	private String id;

	private String userId;

	private String appId;

	private String type;

	private String name;

	private String xml;

	private String lastModified;

	public UpdateBuilder(String id, String userId, String appId,
			String type, String name, String xml, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.type = type;
		this.name = name;
		this.xml = xml;
		this.lastModified = lastModified;
	}

	public UpdateBuilder(UpdateBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.type = builder.type;
		this.name = builder.name;
		this.xml = builder.xml;
		this.lastModified = builder.lastModified;
	}

	public UpdateBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public UpdateBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public UpdateBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public UpdateBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public UpdateBuilder withType(String type) {
		this.type = type;
		return this;
	}

	public UpdateBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public UpdateBuilder withXml(String xml) {
		this.xml = xml;
		return this;
	}

	public UpdateBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Update build() {
		if (this.id == null) {
			return new Update(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.type, this.name, this.xml);
		}
		else {
			if (this.lastModified == null) {
				return new Update(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.type, this.name, this.xml);
			}
			else {
				return new Update(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.type, this.name, this.xml,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
