package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.util.DateTimeUtils;

public class CssBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String content;

	private String lastModified;

	public CssBuilder(String id, String userId, String appId,
			String name, String content, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.content = content;
		this.lastModified = lastModified;
	}

	public CssBuilder(CssBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.content = builder.content;
		this.lastModified = builder.lastModified;
	}

	public CssBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public CssBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public CssBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public CssBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public CssBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public CssBuilder withContent(String content) {
		this.content = content;
		return this;
	}

	public CssBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Css build() {
		if (this.id == null) {
			return new Css(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.name, this.content);
		}
		else {
			if (this.lastModified == null) {
				return new Css(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.content);
			}
			else {
				return new Css(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.content,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
