package net.jp.yamabuki.model;

import net.jp.yamabuki.model.ViewTemplate;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class ViewTemplateMatcher extends TypeSafeMatcher<ViewTemplate> {
	private final ViewTemplate expected;
	
	private String description;
	
	public ViewTemplateMatcher(ViewTemplate expected) {
		this.expected = expected;
	}
	
	public static ViewTemplateMatcher matches(ViewTemplate viewTemplate) {
		return new ViewTemplateMatcher(viewTemplate);
	}

	@Override
	protected boolean matchesSafely(ViewTemplate actual) {
		if (!(actual instanceof ViewTemplate)) {
			this.description = "Parameter 'actual' is not ViewTemplate.";
			return false;
		}

		ViewTemplate obj = (ViewTemplate)actual;
		
		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			this.description = "'Name' are not equal.";
			return false;
		}

		// Content
		if (!this.expected.getContent().equals(obj.getContent())) {
			this.description = "'Content' are not equal.";
			return false;
		}
		
		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
