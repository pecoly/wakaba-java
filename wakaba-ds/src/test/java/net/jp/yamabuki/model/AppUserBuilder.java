package net.jp.yamabuki.model;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.util.DateTimeUtils;

public class AppUserBuilder {
	private String id;
	private String loginId;
	private String loginPassword;
	private String mainAuthority;
	private String subAuthority;
	private String userId;
	private String appId;
	private String lastModified;

	public AppUserBuilder(String id, String loginId, String loginPassword,
			String mainAuthority, String subAuthority,
			String userId, String appId, String lastModified) {
		this.id = id;
		this.loginId = loginId;
		this.loginPassword = loginPassword;
		this.mainAuthority = mainAuthority;
		this.subAuthority = subAuthority;
		this.userId = userId;
		this.appId = appId;
		this.lastModified = lastModified;
	}

	public AppUserBuilder(AppUserBuilder builder) {
		this.id = builder.id;
		this.loginId = builder.loginId;
		this.loginPassword = builder.loginPassword;
		this.mainAuthority = builder.mainAuthority;
		this.subAuthority = builder.subAuthority;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.lastModified = builder.lastModified;
	}

	public AppUserBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public AppUserBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public AppUserBuilder withLoginId(String loginId) {
		this.loginId = loginId;
		return this;
	}

	public AppUserBuilder withLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
		return this;
	}

	public AppUserBuilder withMainAuthority(String mainAuthority) {
		this.mainAuthority = mainAuthority;
		return this;
	}

	public AppUserBuilder withSubAuthority(String subAuthority) {
		this.subAuthority = subAuthority;
		return this;
	}

	public AppUserBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public AppUserBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public AppUserBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public AppUser build() {
		if (this.id == null) {
			return new AppUser(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.loginId, this.loginPassword,
					this.mainAuthority, this.subAuthority);
		}
		else {
			if (this.lastModified == null) {
				return new AppUser(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.loginId, this.loginPassword,
						this.mainAuthority, this.subAuthority);
			}
			else {
				return new AppUser(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.loginId, this.loginPassword,
						this.mainAuthority, this.subAuthority,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
