package net.jp.yamabuki.model.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.util.DateTimeUtils;

public class JsBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String content;

	private String lastModified;

	public JsBuilder(String id, String userId, String appId,
			String name, String content, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.content = content;
		this.lastModified = lastModified;
	}

	public JsBuilder(JsBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.content = builder.content;
		this.lastModified = builder.lastModified;
	}

	public JsBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public JsBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public JsBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public JsBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public JsBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public JsBuilder withContent(String content) {
		this.content = content;
		return this;
	}

	public JsBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public Js build() {
		if (this.id == null) {
			return new Js(
					new UserIdImpl(this.userId),
					new AppIdImpl(this.appId),
					this.name, this.content);
		}
		else {
			if (this.lastModified == null) {
				return new Js(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.content);
			}
			else {
				return new Js(new ModelIdImpl(this.id),
						new UserIdImpl(this.userId),
						new AppIdImpl(this.appId),
						this.name, this.content,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
