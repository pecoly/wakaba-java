package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.datasource.Update;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class UpdateMatcher extends TypeSafeMatcher<Update> {
	private final Update expected;

	private String description;

	public UpdateMatcher(Update expected) {
		Argument.isNotNull(expected, "expected");
		this.expected = expected;
	}

	public static UpdateMatcher matches(Update query) {
		return new UpdateMatcher(query);
	}

	@Override
	protected boolean matchesSafely(Update actual) {
		Argument.isNotNull(actual, "actual");

		if (!(actual instanceof Update)) {
			this.description = "Parameter 'actual' is not Update.";
			return false;
		}

		Update obj = (Update)actual;

		// Id
		if (this.expected.getId() == null && obj.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}
		else if (obj.getId() == null && this.expected.getId() != null) {
			this.description = "'Id' are not equal.";
			return false;
		}

		if (!this.expected.getId().equals(obj.getId())) {
			this.description = "'Id' are not equal.";
			return false;
		}

		// UserId
		if (!this.expected.getUserId().equals(obj.getUserId())) {
			this.description = "'UserId' are not equal.";
			return false;
		}

		// AppId
		if (!this.expected.getAppId().equals(obj.getAppId())) {
			this.description = "'AppId' are not equal.";
			return false;
		}

		// Name
		if (!this.expected.getName().equals(obj.getName())) {
			this.description = "'Name' are not equal.";
			return false;
		}

		// Type
		if (!this.expected.getType().equals(obj.getType())) {
			this.description = "'Type' are not equal.";
			return false;
		}

		// Xml
		if (!this.expected.getXml().equals(obj.getXml())) {
			this.description = "'Xml' are not equal.";
			return false;
		}

		return true;
	}

	@Override
	public void describeTo(Description arg) {
		arg.appendValue(this.expected);
	}
}
