package net.jp.yamabuki.model;

import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

public class ViewBuilder {
	private String id;

	private String userId;

	private String appId;

	private String name;

	private String content;

	private String lastModified;

	public ViewBuilder(String id, String userId, String appId,
			String name, String content, String lastModified) {
		this.id = id;
		this.userId = userId;
		this.appId = appId;
		this.name = name;
		this.content = content;
		this.lastModified = lastModified;
	}

	public ViewBuilder(ViewBuilder builder) {
		this.id = builder.id;
		this.userId = builder.userId;
		this.appId = builder.appId;
		this.name = builder.name;
		this.content = builder.content;
		this.lastModified = builder.lastModified;
	}

	public ViewBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public ViewBuilder withId(ModelId id) {
		this.id = id.getValue();
		return this;
	}

	public ViewBuilder withUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public ViewBuilder withAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public ViewBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public ViewBuilder withContent(String content) {
		this.content = content;
		return this;
	}

	public ViewBuilder withLastModified(String lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public View build() {
		if (this.id == null) {
			return new View(
					UserIdUtils.create(this.userId),
					AppIdUtils.create(this.appId),
					this.name, this.content);
		}
		else {
			if (this.lastModified == null) {
				return new View(new ModelIdImpl(this.id),
						UserIdUtils.create(this.userId),
						AppIdUtils.create(this.appId),
						this.name, this.content);
			}
			else {
				return new View(new ModelIdImpl(this.id),
						UserIdUtils.create(this.userId),
						AppIdUtils.create(this.appId),
						this.name, this.content,
						DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS(this.lastModified));
			}
		}
	}
}
