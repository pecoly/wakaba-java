package net.jp.yamabuki.util;

import net.jp.yamabuki.util.PostgreSqlUtils;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PostgresqlUtilsTest {
	public static class ToLikeKeyword {
		@Test
		public void nullValue() {
			assertThat(PostgreSqlUtils.toLikeKeyword(null),
					is(""));
		}
		
		@Test
		public void emptyValue() {
			assertThat(PostgreSqlUtils.toLikeKeyword(""),
					is(""));			
		}
		
		@Test
		public void toLikeKeyword() {
			assertThat(PostgreSqlUtils.toLikeKeyword("abc"),
					is("%abc%"));
		}
	}
}
