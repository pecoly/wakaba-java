package net.jp.yamabuki.service.manager.query.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class AddQueryImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			AddQueryImpl x = new AddQueryImpl();
			AddQueryImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", stringConverter);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ThrowEx {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		Query model;

		@Before
		public void before() {
			QueryBuilder builder = new QueryBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void throwEx() {
			thrown.expect(ModelAlreadyExistsException.class);
			thrown.expectMessage(WakabaErrorCode.QUERY_ALREADY_EXISTS.getNumber()
					+ " Query already exists. name : hoge");

			AddQueryImpl x = new AddQueryImpl();
			x.throwEx(model);
		}
	}
}
