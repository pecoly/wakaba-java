package net.jp.yamabuki.service.manager.sql;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.query.GetQueryByKey;
import net.jp.yamabuki.service.manager.query.sql.AddQueryImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseAddAppModelTest {
	public static class Testing1 extends BaseAddAppModel<Query> {

		public boolean getMangerSqlCalled;

		public boolean getGetModelByKeyCalled;

		public boolean getUpdateParameterListCalled;

		public boolean containsCalled;

		public boolean toObjectParameterListCalled;

		public boolean updateCalled;

		public boolean getAddSqlCalled;

		public boolean contains;

		public Map<String, Object> objList;

		public String getSql;

		public int update;

		public boolean updateError;

		@Override
		public void throwEx(Query t) {
			throw new RuntimeException("testing");
		}

		@Override
		public ManagerSql getManagerSql() {
			this.getMangerSqlCalled = true;
			return null;
		}

		@Override
		public GetModelByKey<Query> getGetModelByKey() {
			this.getGetModelByKeyCalled = true;
			return null;
		}

		@Override
		boolean contains(Query t) {
			this.containsCalled = true;
			return this.contains;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return this.objList;
		}

		@Override
		String getAddSql(Map<String, Object> parameterList) {
			this.getAddSqlCalled = true;
			return this.getSql;
		}

		@Override
		public int update(String sql, Map<String, Object> objList) {
			this.updateCalled = true;
			if (this.updateError) {
				throw new DataIntegrityViolationException("update error");
			}
			else {
				return this.update;
			}
		}
	}

	public static class Execute {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		Query model;

		Map<String, Object> objList = new HashMap<>();

		String getSql;

		@Before
		public void before() {
			QueryBuilder builder = new QueryBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void execute() {
			Testing1 x = new Testing1();
			x.contains = false;
			x.objList = objList;
			x.update = 1;

			x.execute(model);

			assertThat(x.containsCalled, is(true));
			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.updateCalled, is(true));
			assertThat(x.getAddSqlCalled, is(true));
		}

		@Test
		public void thrownExceptionWhenModelExists() {
			thrown.expect(RuntimeException.class);
			thrown.expectMessage("testing");
			Testing1 x = new Testing1();
			x.contains = true;

			x.execute(model);
		}

		@Test
		public void thrownExceptionWhenExceptionThrown() {
			thrown.expect(RuntimeException.class);
			thrown.expectMessage("testing");
			Testing1 x = new Testing1();
			x.contains = false;
			x.objList = objList;
			x.getSql = getSql;
			x.update = 1;
			x.updateError = true;

			x.execute(model);
		}
	}

	public static class Contains {
		GetQueryByKey getByKey = mock(GetQueryByKey.class);
		QueryBuilder builder = new QueryBuilder(
				null, "suzuki", "jiro",
				"query", "hoge", "fuga", "2000010203112233");
		Query model = builder.build();

		BaseAddAppModel<Query> createAddSpy() {
			BaseAddAppModel<Query> x = new AddQueryImpl();
			ReflectionTestUtils.setField(x, "getQueryByKey", getByKey);
			return spy(x);
		}

		@Test
		public void contains() {
			BaseAddAppModel<Query> spy = createAddSpy();
			when(getByKey.execute((Query)any())).thenReturn(model);

			boolean result = spy.contains(model);
			assertThat(result, is(true));
		}

		@Test
		public void notContains() {
			BaseAddAppModel<Query> spy = createAddSpy();
			when(getByKey.execute((Query)any())).thenReturn(null);

			boolean result = spy.contains(model);
			assertThat(result, is(false));
		}
	}

	public static class GetAddSql {
		BaseAddAppModel<Query> x = new AddQueryImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getAddSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getAddSql(objList);

			verify(sql).getAddSql(objList);
		}
	}
}
