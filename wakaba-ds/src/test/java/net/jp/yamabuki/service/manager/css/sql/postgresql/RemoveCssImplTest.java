package net.jp.yamabuki.service.manager.css.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.CssBuilder;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class RemoveCssImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static String modelId;
		protected static AddAppModel<Css> addModel;
		protected static RemoveAppModel<Css> removeModel;
		protected static GetAppModelList<Css> getModelList;
		protected static CssBuilder modelBuilder;
		protected static Css model1;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "hoge";
			String content = "あいうえお";
			modelBuilder = new CssBuilder(id, userId, appId,
					name, content, "20001011121314");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/css/sql/postgresql/RemoveCssImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);
			removeModel = context.getBean(RemoveAppModel.class);

			removeAllModels();

			addModel.execute(modelBuilder.build());

			Map<String, String> map = new HashMap<>();
			List<Css> list = getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));

			model1 = list.get(0);
			modelId = list.get(0).getId().getValue();
			assertThat(modelId, is(not(nullValue())));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM CSS_LIST");
		}

		public static class Remove extends Base {
			static List<Css> list;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				removeModel.execute(new CssBuilder(modelBuilder)
					.withId(modelId)
					.withLastModified(DateTimeUtils.toStringYYYYMMDDHHMISS(
							model1.getLastModified()))
					.build());
				Map<String, String> map = new HashMap<>();
				list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
			}

			@Test
			public void size() {
				assertThat(list.size(), is(0));
			}
		}

		public static class Remove_NotExists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelNotFoundException.class);
				thrown.expectMessage(WakabaErrorCode.CSS_NOT_FOUND.getNumber()
						+ " CSS not found. id : 9999");
				removeModel.execute(new CssBuilder(modelBuilder)
					.withId("9999")
					.build());
			}
		}
	}
}
