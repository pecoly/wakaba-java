package net.jp.yamabuki.service.manager.update.sql.postgresql;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.datasource.UpdateBuilder;
import net.jp.yamabuki.model.datasource.UpdateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetUpdateByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static Update model;
		protected static GetAppModelList<Update> getModelList;
		protected static GetAppModelById<Update> getModelById;
		protected static AddAppModel<Update> addModel;
		protected static UpdateBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String userId = "user";
			String appId = "app";
			String type = "update";
			String name = "abc";
			String xml = "def";

			modelBuilder = new UpdateBuilder(null, userId, appId,
					type, name, xml, "20101112030405");
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/update/sql/postgresql/GetUpdateByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Update x : getModelList.execute(
					new UserIdImpl("user"), new AppIdImpl("app"),
					new PageRequest(0, 10), map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(not(nullValue())));

		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM UPDATE_LIST");
		}
	}

	public static class ExecuteByUpdate extends Base {
		@Test
		public void execute() {
			String userId = "foo";
			String appId = "bar";
			String type = "update";
			String name = "x";
			String xml = "y";

			GetAppModelById<Update> getUpdateByIdSpy = spy(getModelById);
			getUpdateByIdSpy.execute(new Update(
					new ModelIdImpl(modelId), new UserIdImpl(userId), new AppIdImpl(appId),
					type, name, xml));

			verify(getUpdateByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(modelId));
		}
	}

	public static class Exists extends Base {
		protected static Update update2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			update2 = getModelById.execute(
					new UserIdImpl("user"),
					new AppIdImpl("app"),
					new ModelIdImpl(modelId));
			assertThat(update2, is(not(nullValue())));
		}

		@Test
		public void equals() {
			Update obj = new UpdateBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(update2, UpdateMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Update update = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(update, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Update update = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(update, nullValue());
		}

		@Test
		public void notEqualId() {
			Update update = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(update, nullValue());
		}
	}
}
