package net.jp.yamabuki.service.manager.jdbcconnection.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.JdbcConnectionMatcher;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddJdbcConnectionImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<JdbcConnection> addModel;
		protected static GetAppModelList<JdbcConnection> getModelList;
		protected static JdbcConnectionBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "yamada";
			String driver = "driver";
			String url = "url";
			String username = "username";
			String password = "password";
			modelBuilder = new JdbcConnectionBuilder(
					id, userId, appId, name,
					driver, url, username, password, null);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/jdbcconnection/sql/postgresql/JdbcConnectionTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllAppUsers();
		}

		@AfterClass
		public static void afterClass() {
			removeAllAppUsers();
			context = null;
			dataSource = null;
			addModel = null;
			getModelList = null;
			modelBuilder = null;
		}

		static void removeAllAppUsers() {
			new JdbcTemplate(dataSource).update("DELETE FROM JDBC_CONNECTION_LIST");
		}

		public static class Add extends Base {
			static JdbcConnection model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<JdbcConnection> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				JdbcConnection obj = new JdbcConnectionBuilder(modelBuilder)
						.withId(model2.getId())
						.build();
				assertThat(model2, JdbcConnectionMatcher.matches(obj));
			}
		}

		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.JDBC_CONNECTION_ALREADY_EXISTS.getNumber()
						+ " JDBC connection already exists. name : yamada");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
