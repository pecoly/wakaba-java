package net.jp.yamabuki.service.manager.query.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetQueryListImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetQueryListImpl x = new GetQueryListImpl();
			GetQueryListImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ToStringParameterList {
		Map<String, String> map = new HashMap<>();

		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		GetQueryListImpl createGetList(ManagerSql sql, SqlUtils utils) {
			GetQueryListImpl x = new GetQueryListImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x;
		}

		@Test
		public void none() {
			GetQueryListImpl x = new GetQueryListImpl();
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}

		@Test
		public void hasName() {
			map.put("name", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetQueryListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("fuga"));
		}

		@Test
		public void empty() {
			map.put("name", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetQueryListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}
	}
}
