package net.jp.yamabuki.service.manager.query.sql;

import java.util.List;

import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetQueryByIdImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetQueryByIdImpl x = new GetQueryByIdImpl();
			GetQueryByIdImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}
}
