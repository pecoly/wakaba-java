package net.jp.yamabuki.service.manager.message.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.query.sql.GetQueryListImpl;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetMessageListImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetMessageListImpl x = new GetMessageListImpl();
			GetMessageListImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map = new HashMap<>();

		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		GetMessageListImpl createGetList(ManagerSql sql, SqlUtils utils) {
			GetMessageListImpl x = new GetMessageListImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x;
		}

		@Test
		public void none() {
			GetMessageListImpl x = new GetMessageListImpl();
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}


		@Test
		public void hasCode() {
			map.put("code", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetMessageListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("code"), is(true));
			assertThat(result.get("code"), is("fuga"));
		}

		@Test
		public void hasLocaleId() {
			map.put("localeId", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetMessageListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("localeId"), is(true));
			assertThat(result.get("localeId"), is("fuga"));
		}

		@Test
		public void hasMessage() {
			map.put("message", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetMessageListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("message"), is(true));
			assertThat(result.get("message"), is("fuga"));
		}

		@Test
		public void emptyCode() {
			map.put("code", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetMessageListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
			assertThat(result.containsKey("code"), is(false));
		}
	}
}
