package net.jp.yamabuki.service.manager.query.sql.postgresql;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.QueryMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetQueryByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static Query model;
		protected static GetAppModelList<Query> getModelList;
		protected static GetAppModelById<Query> getModelById;
		protected static AddAppModel<Query> addModel;
		protected static QueryBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String userId = "user";
			String appId = "app";
			String type = "query";
			String name = "abc";
			String xml = "def";

			modelBuilder = new QueryBuilder(null, userId, appId,
					type, name, xml, "20101112030405");
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/query/sql/postgresql/GetQueryByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Query x : getModelList.execute(
					new UserIdImpl("user"), new AppIdImpl("app"),
					new PageRequest(0, 10), map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(not(nullValue())));

		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM QUERY_LIST");
		}
	}

	public static class ExecuteByQuery extends Base {
		@Test
		public void execute() {
			String userId = "foo";
			String appId = "bar";
			String type = "query";
			String name = "x";
			String xml = "y";

			GetAppModelById<Query> getQueryByIdSpy = spy(getModelById);
			getQueryByIdSpy.execute(new Query(
					new ModelIdImpl(modelId), new UserIdImpl(userId), new AppIdImpl(appId),
					type, name, xml));

			verify(getQueryByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(modelId));
		}
	}

	public static class Exists extends Base {
		protected static Query query2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			query2 = getModelById.execute(
					new UserIdImpl("user"),
					new AppIdImpl("app"),
					new ModelIdImpl(modelId));
			assertThat(query2, is(not(nullValue())));
		}

		@Test
		public void equals() {
			Query obj = new QueryBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(query2, QueryMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Query query = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(query, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Query query = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(query, nullValue());
		}

		@Test
		public void notEqualId() {
			Query query = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(query, nullValue());
		}
	}
}
