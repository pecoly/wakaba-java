package net.jp.yamabuki.service.manager.css.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.CssBuilder;
import net.jp.yamabuki.model.resource.CssMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetCssByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static Css model;
		protected static GetAppModelList<Css> getModelList;
		protected static GetAppModelById<Css> getModelById;
		protected static AddAppModel<Css> addModel;
		protected static CssBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "content";
			modelBuilder = new CssBuilder(id, userId, appId,
					name, content, "20001011121314");
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/css/sql/postgresql/GetCssByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (Css x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM CSS_LIST");
		}
	}

	public static class ExecuteByCss extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String userId = "kimura";
			String appId = "honda";
			String name = "B";
			String content = "b";

			GetAppModelById<Css> getModelByIdSpy = spy(getModelById);
			getModelByIdSpy.execute(new Css(
					new ModelIdImpl(id),
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					name, content));

			verify(getModelByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するメッセージを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static Css model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Css obj = new CssBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, CssMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないメッセージを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Css model = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Css model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualId() {
			Css	model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(model, nullValue());
		}
	}
}
