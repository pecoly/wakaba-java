package net.jp.yamabuki.service.manager.update.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.datasource.UpdateBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class AddUpdateImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			AddUpdateImpl x = new AddUpdateImpl();
			AddUpdateImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", stringConverter);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ThrowEx {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		Update model;

		@Before
		public void before() {
			UpdateBuilder builder = new UpdateBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void throwEx() {
			thrown.expect(ModelAlreadyExistsException.class);
			thrown.expectMessage(WakabaErrorCode.UPDATE_ALREADY_EXISTS.getNumber()
					+ " Update already exists. name : hoge");

			AddUpdateImpl x = new AddUpdateImpl();
			x.throwEx(model);
		}
	}
}
