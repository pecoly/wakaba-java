package net.jp.yamabuki.service.manager.query.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetQueryListImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelList<Query> getModelList;
		protected static AddAppModel<Query> addModel;
		protected static DataSource dataSource;
		protected static QueryBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "getAddressList";
			String type = "query";
			String xml = "ja";
			modelBuilder = new QueryBuilder(id, userId, appId,
					type, name, xml, "20101112030405");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/query/sql/postgresql/GetQueryListImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM QUERY_LIST");
		}
	}

	public static class AllQueries extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void list() {
			List<Query> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
			assertThat(list.get(2).getName(), is("bbc"));
		}
	}

	public static class Limit extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void limitIs2() {
			List<Query> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 2), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();


		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void userId_appId() {

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			List<Query> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			for (Query x : list) {
				assertThat(x.getUserId().getValue(), is("u1"));
				assertThat(x.getAppId().getValue(), is("a1"));
			}
		}

		@Test
		public void name() {

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			map.put("name", "bc");
			List<Query> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("abc"));
			assertThat(list.get(1).getName(), is("bbc"));
		}
	}
}
