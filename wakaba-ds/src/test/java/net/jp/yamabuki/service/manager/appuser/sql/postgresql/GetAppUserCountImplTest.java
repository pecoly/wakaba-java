package net.jp.yamabuki.service.manager.appuser.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelCount;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetAppUserCountImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelCount getModelCount;
		protected static AddAppModel<AppUser> addModel;
		protected static DataSource dataSource;
		protected static AppUserBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String loginId = "yamada";
			String password = "taro";
			String main = "main";
			String sub = "sub";
			String userId = "suzuki";
			String appId = "jiro";
			String lastModified = "20001011121314";
			modelBuilder = new AppUserBuilder( id, loginId, password,
					main, sub, userId, appId, lastModified);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/appuser/sql/postgresql/appUser.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelCount = context.getBean(GetAppModelCount.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM USER_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("bbc").build());
		}

		@Test
		public void list() {
			long count = getModelCount.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					map);
			assertThat(count, is(3L));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@Before
		public void setUp() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void userId_appId() {
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("bbc").build());

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withLoginId("aaa").build());

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withLoginId("aaa").build());

			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(3L));
		}

		@Test
		public void loginId() {
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("bbc").build());

			map.put("loginId", "bc");
			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(2L));
		}
	}
}
