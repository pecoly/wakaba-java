package net.jp.yamabuki.service.manager.appuser.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetAppUserByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static AppUser model;
		protected static GetAppModelList<AppUser> getModelList;
		protected static GetAppModelById<AppUser> getModelById;
		protected static AddAppModel<AppUser> addModel;
		protected static AppUserBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String loginId = "yamada";
			String loginPassword = "taro";
			String main = "main";
			String sub = "sub";
			String userId = "suzuki";
			String appId = "jiro";
			String lastModified = "20001011121314";

			modelBuilder = new AppUserBuilder(null, loginId, loginPassword,
					main, sub, userId, appId, lastModified);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/appuser/sql/postgresql/appUser.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllAppUsers();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (AppUser x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllAppUsers();
			context = null;
			dataSource = null;
		}

		static void removeAllAppUsers() {
			new JdbcTemplate(dataSource).update("DELETE FROM USER_LIST");
		}
	}

	public static class ExecuteByModel extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String loginId = "A";
			String loginPassword = "B";
			String main = "a";
			String sub = "b";
			String userId = "kimura";
			String appId = "honda";

			GetAppModelById<AppUser> getAppUserByIdSpy = spy(getModelById);
			getAppUserByIdSpy.execute(new AppUser(
					new ModelIdImpl(id),
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					loginId, loginPassword,
					main, sub));

			verify(getAppUserByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するユーザーを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static AppUser model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			AppUser obj = new AppUserBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, AppUserMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないユーザーを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			AppUser appUser = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			AppUser appUser = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualId() {
			AppUser appUser = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(appUser, nullValue());
		}
	}
}
