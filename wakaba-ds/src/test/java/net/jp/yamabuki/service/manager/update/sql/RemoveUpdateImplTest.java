package net.jp.yamabuki.service.manager.update.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.model.converter.StringConverter;

import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RemoveUpdateImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			RemoveUpdateImpl x = new RemoveUpdateImpl();
			RemoveUpdateImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}
}
