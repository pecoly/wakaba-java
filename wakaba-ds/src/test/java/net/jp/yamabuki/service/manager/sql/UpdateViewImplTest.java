package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.query.sql.UpdateQueryImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UpdateViewImplTest {

	public static class Testing1 extends BaseUpdateAppModel<Query> {

		public boolean getMangerSqlCalled;

		public boolean getGetModelByKeyCalled;

		public boolean getUpdateParameterListCalled;

		public boolean toObjectParameterListCalled;

		public boolean updateCalled;

		public boolean getUpdateSqlCalled;

		public boolean contains;

		public Map<String, Object> objList;

		public String getSql;

		public int update;


		@Override
		public void throwEx(ModelId id) {
			throw new RuntimeException("testing");
		}

		@Override
		public ManagerSql getManagerSql() {
			this.getMangerSqlCalled = true;
			return null;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return this.objList;
		}

		@Override
		String getUpdateSql(Map<String, Object> parameterList) {
			this.getUpdateSqlCalled = true;
			return this.getSql;
		}

		@Override
		public int update(String sql, Map<String, Object> objList) {
			this.updateCalled = true;
			return this.update;
		}
	}

	public static class Execute {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		Query model;

		Map<String, Object> objList = new HashMap<>();

		@Before
		public void before() {
			QueryBuilder builder = new QueryBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void execute() {
			Testing1 x = new Testing1();
			x.objList = objList;
			x.update = 1;

			x.execute(model);

			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.updateCalled, is(true));
			assertThat(x.getUpdateSqlCalled, is(true));
		}

		@Test
		public void exceptionThrownWhenModelNotExists() {
			thrown.expect(RuntimeException.class);
			thrown.expectMessage("testing");
			Testing1 x = new Testing1();
			x.objList = objList;
			x.update = 0;

			x.execute(model);
		}
	}

	public static class GetUpdateSql {
		BaseUpdateAppModel<Query> x = new UpdateQueryImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getUpdateSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getUpdateSql(objList);

			verify(sql).getUpdateSql(objList);
		}
	}
}
