package net.jp.yamabuki.service.manager.viewtemplate.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.ViewTemplateBuilder;
import net.jp.yamabuki.model.ViewTemplateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetViewTemplateByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static ViewTemplate model;
		protected static GetAppModelList<ViewTemplate> getModelList;
		protected static GetAppModelById<ViewTemplate> getModelById;
		protected static AddAppModel<ViewTemplate> addModel;
		protected static ViewTemplateBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "abc";
			String lastModified = "20010203040506";
			modelBuilder = new ViewTemplateBuilder(
					id, userId, appId,
					name, content, lastModified);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/viewtemplate/sql/postgresql/GetViewTemplateByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (ViewTemplate x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_TEMPLATE_LIST");
		}
	}

	public static class ExecuteByViewTemplate extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String userId = "kimura";
			String appId = "honda";
			String name = "B";
			String content = "b";

			GetAppModelById<ViewTemplate> getModelByIdSpy = spy(getModelById);
			getModelByIdSpy.execute(new ViewTemplate(
					new ModelIdImpl(id),
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					name,
					content));

			verify(getModelByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するビューを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static ViewTemplate model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			ViewTemplate obj = new ViewTemplateBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, ViewTemplateMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないメッセージを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			ViewTemplate model = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualAppId() {
			ViewTemplate model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualId() {
			ViewTemplate	model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(model, nullValue());
		}
	}
}
