package net.jp.yamabuki.service.manager.view.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.ViewBuilder;
import net.jp.yamabuki.model.ViewMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetViewByKeyImplTest {
	public static class Base {

		protected static View model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<View> getModelList;
		protected static GetModelByKey<View> getModelByKey;
		protected static AddAppModel<View> addModel;
		protected static DataSource dataSource;
		protected static ViewBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "abc";
			modelBuilder = new ViewBuilder(id, userId, appId,
					name, content, "20111213010203");

			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/view/sql/postgresql/GetViewByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (View x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_LIST");
		}
	}

	public static class Exists extends Base {
		protected static View model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			View key = new ViewBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			View obj = new ViewBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, ViewMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			View key = new ViewBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			View view = getModelByKey.execute(key);
			assertThat(view, nullValue());
		}

		@Test
		public void notEqualAppId() {
			View key = new ViewBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			View view = getModelByKey.execute(key);
			assertThat(view, nullValue());
		}

		@Test
		public void notEqualName() {
			View key = new ViewBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("hoge")
					.build();

			View view = getModelByKey.execute(key);
			assertThat(view, nullValue());
		}
	}
}
