package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.message.sql.GetMessageListImpl;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetMessageListImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelList<Message> getModelList;
		protected static AddAppModel<Message> addModel;
		protected static DataSource dataSource;
		protected static MessageBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "main";
			String locale = "ja";
			String msg = "suzuki";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, locale, msg, "20010203102030");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/GetMessageListImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("bbc").build());
		}

		@Test
		public void list() {
			List<Message> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getCode(), is("aaa"));
			assertThat(list.get(1).getCode(), is("abc"));
			assertThat(list.get(2).getCode(), is("bbc"));
		}
	}

	public static class Limit extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("bbc").build());
		}

		@Test
		public void limitIs2() {
			List<Message> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 2), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getCode(), is("aaa"));
			assertThat(list.get(1).getCode(), is("abc"));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
		}

		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void by_userId_appId() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").build());

			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withCode("aaa").build());

			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withCode("aaa").build());

			List<Message> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			for (Message x : list) {
				assertThat(x.getUserId().getValue(), is("u1"));
				assertThat(x.getAppId().getValue(), is("a1"));
			}
		}

		@Test
		public void by_code() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").build());

			map.put("code", "bc");
			List<Message> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getCode(), is("abc"));
			assertThat(list.get(1).getCode(), is("bbc"));
		}

		@Test
		public void by_locale() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("cde").withLocaleId("en").build());

			map.put("localeId", "ja");
			List<Message> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getCode(), is("aaa"));
			assertThat(list.get(1).getCode(), is("abc"));
			assertThat(list.get(2).getCode(), is("bbc"));
		}

		@Test
		public void by_message() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").withMessage("xxx").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").withMessage("xyz").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").withMessage("yyz").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("cde").withMessage("zyx").build());

			map.put("message", "yz");
			List<Message> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getCode(), is("abc"));
			assertThat(list.get(1).getCode(), is("bbc"));
		}
	}
}
