package net.jp.yamabuki.service.manager.message.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.*;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetMessageByKeyImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetMessageByKeyImpl x = new GetMessageByKeyImpl();
			GetMessageByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", new StringConverter());
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class Execute {

		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		String code = "hoge";
		LocaleIdImpl localeId = new LocaleIdImpl("ja");
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		String getSql = "get";
		RowMapper<Message> mapper = new MessageMapper();
		MessageBuilder builder = new MessageBuilder("9876", "suzuki", "jiro",
				"hoge", "ja", "fuga", "20121211100908");
		Message model = builder.build();

		public GetMessageByKeyImpl createGetByKeySpy() {
			GetMessageByKeyImpl x = new GetMessageByKeyImpl();
			GetMessageByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "mapper", mapper);
			return spy;
		}

		@Test
		public void execute() {
			GetMessageByKeyImpl spy = createGetByKeySpy();

			doReturn(strList).when(spy)
					.toStringParameterList(userId, appId, code, localeId);
			doReturn(objList).when(spy)
					.toObjectParameterList(strList);
			doReturn(getSql).when(spy)
					.getGetByKeySql(objList);
			doReturn(model).when(spy)
					.queryForObject(getSql, objList, mapper);

			Message result = spy.execute(userId, appId, code, localeId);

			assertThat(model, MessageMatcher.matches(result));

			verify(spy).toStringParameterList(userId, appId, code, localeId);
			verify(spy).toObjectParameterList(strList);
			verify(spy).getGetByKeySql(objList);
			verify(spy).queryForObject(getSql, objList, mapper);
		}

		@Test
		public void executeByModel() {
			GetMessageByKeyImpl spy = createGetByKeySpy();

			doReturn(null).when(spy).execute(userId, appId, code, localeId);

			spy.execute(model);

			verify(spy).execute(userId, appId, code, localeId);
		}
	}

	public static class ToStringParameterList {
		@Test
		public void toStringParameterList() {
			GetMessageByKeyImpl x = new GetMessageByKeyImpl();

			UserIdImpl userId = new UserIdImpl("suzuki");
			AppIdImpl appId = new AppIdImpl("jiro");
			ModelIdImpl id = new ModelIdImpl("9876");
			String code = "hoge";
			LocaleIdImpl localeId = new LocaleIdImpl("ja");
			Map<String, String> result = x.toStringParameterList(userId, appId, code, localeId);
			assertThat(result.size(), is(4));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("code"), is(true));
			assertThat(result.containsKey("localeId"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("code"), is("hoge"));
			assertThat(result.get("localeId"), is("ja"));
		}
	}


	public static class GetGetCountSql {
		@Test
		public void getGetCountSqlCalled() {
			GetMessageByKeyImpl x = new GetMessageByKeyImpl();
			ManagerSql sql = mock(ManagerSql.class);
			ReflectionTestUtils.setField(x, "managerSql", sql);
			Map<String, Object> objList = new HashMap<>();

			x.getGetByKeySql(objList);

			verify(sql).getGetByKeySql(objList);
		}
	}
}
