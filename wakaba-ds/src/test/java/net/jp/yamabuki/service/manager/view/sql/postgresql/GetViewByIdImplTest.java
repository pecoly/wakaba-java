package net.jp.yamabuki.service.manager.view.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.ViewBuilder;
import net.jp.yamabuki.model.ViewMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetViewByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static View model;
		protected static GetAppModelList<View> getModelList;
		protected static GetAppModelById<View> getModelById;
		protected static AddAppModel<View> addModel;
		protected static ViewBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "abc";
			modelBuilder = new ViewBuilder(id, userId, appId,
					name, content, "20111213010203");
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/view/sql/postgresql/GetViewByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (View x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_LIST");
		}
	}

	public static class ExecuteByView extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String userId = "kimura";
			String appId = "honda";
			String name = "B";
			String content = "b";

			GetAppModelById<View> getModelByIdSpy = spy(getModelById);
			getModelByIdSpy.execute(new View(
					new ModelIdImpl(id),
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					name,
					content));

			verify(getModelByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するビューを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static View model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			View obj = new ViewBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, ViewMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないメッセージを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			View model = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualAppId() {
			View model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualId() {
			View	model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(model, nullValue());
		}
	}
}
