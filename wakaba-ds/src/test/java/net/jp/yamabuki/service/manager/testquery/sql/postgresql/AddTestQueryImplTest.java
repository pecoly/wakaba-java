package net.jp.yamabuki.service.manager.testquery.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.model.datasource.TestQueryBuilder;
import net.jp.yamabuki.model.datasource.TestQueryMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.testquery.GetTestQueryByKey;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

public class AddTestQueryImplTest {
	public static class Base {
		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<TestQuery> addTestQuery;
		protected static GetTestQueryByKey getTestQueryByKey;
		protected static TestQueryBuilder testQueryBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String type = "query";
			String name = "abc";
			String xml = "def";
			testQueryBuilder = new TestQueryBuilder(id, userId, appId, type, name, xml);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/testquery/sql/postgresql/AddTestQueryImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addTestQuery = context.getBean(AddAppModel.class);
			getTestQueryByKey = context.getBean(GetTestQueryByKey.class);

			removeAllQueries();

			assertThat(countQueries(), is(0));
		}

		static void removeAllQueries() {
			new JdbcTemplate(dataSource).update("DELETE FROM TEST_QUERY_LIST");
		}

		static int countQueries() {
			return new JdbcTemplate(dataSource).queryForObject(
					"SELECT COUNT(*) FROM TEST_QUERY_LIST",
					Integer.class);
		}

		@AfterClass
		public static void afterClass() {
			removeAllQueries();
			context = null;
			dataSource = null;
			addTestQuery = null;
		}
	}

	public static class Add_Ok_WhenTestQueryNotExists extends Base {

		static TestQuery query2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addTestQuery.execute(testQueryBuilder.build());
			Map<String, String> map = new HashMap<>();
			assertThat(countQueries(), is(1));
			query2 = getTestQueryByKey.execute(testQueryBuilder.build());
			assertThat(query2, is(notNullValue()));
		}

		@Test
		public void equals() {
			TestQuery obj = new TestQueryBuilder(testQueryBuilder)
					.withId(query2.getId())
					.build();
			assertThat(query2, TestQueryMatcher.matches(obj));
		}
	}

	public static class Add_Ok_WhenTestQueryExists extends Base {

		static TestQuery query2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			// 1件追加
			addTestQuery.execute(testQueryBuilder.withXml("first").build());
			assertThat(countQueries(), is(1));

			// さらに追加したとき、問題なく追加できること
			addTestQuery.execute(testQueryBuilder.withXml("second").build());
			assertThat(countQueries(), is(1));

			query2 = getTestQueryByKey.execute(testQueryBuilder.build());
			assertThat(query2, is(notNullValue()));
		}

		@Test
		public void equals() {
			TestQuery obj = new TestQueryBuilder(testQueryBuilder)
					.withId(query2.getId())
					.withXml("second")
					.build();
			assertThat(query2, TestQueryMatcher.matches(obj));
		}
	}



	public static class Add_Rollback_WhenTestQueryExistsAndErrorOccurred extends Base {
		@Test
		public void thrown() {
			removeAllQueries();
			assertThat(countQueries(), is(0));

			// 1件追加
			addTestQuery.execute(testQueryBuilder.build());
			assertThat(countQueries(), is(1));

			try {
				TestQuery invalid = testQueryBuilder.build();
				ReflectionTestUtils.setField(invalid, "xml", null);

				// さらに追加
				addTestQuery.execute(invalid);
			}
			catch (ModelAlreadyExistsException ex) {
			}

			// 削除されたのがロールバックされていること
			assertThat(countQueries(), is(1));
		}
	}
}
