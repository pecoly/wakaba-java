package net.jp.yamabuki.service.manager.sql;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.query.sql.RemoveQueryImpl;
import net.jp.yamabuki.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseRemoveAppModelTest {
	public static class Testing1 extends BaseRemoveAppModel<Query> {

		public boolean toStringParameterListCalled;

		public boolean toObjectParameterListCalled;

		public boolean updateCalled;

		public boolean getRemoveSqlCalled;

		public boolean executeCalled;

		public Map<String, String> strList;

		public Map<String, Object> objList;

		public String getSql;

		public int update;

		@Override
		public ManagerSql getManagerSql() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void throwEx(ModelId id) {
			throw new RuntimeException("testing");
		}

		@Override
		public Map<String, String> toStringParameterList(UserId userId,
				AppId appId, ModelId id, DateTime lastModified) {
			this.toStringParameterListCalled = true;
			return this.strList;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return this.objList;
		}

		@Override
		String getRemoveSql(Map<String, Object> parameterList) {
			this.getRemoveSqlCalled = true;
			return this.getSql;
		}

		@Override
		public int update(String sql, Map<String, Object> objList) {
			this.updateCalled = true;
			return this.update;
		}

		@Override
		public void execute(Query query) {
			this.executeCalled = true;
		}
	}

	public static class Execute {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		ModelIdImpl id = new ModelIdImpl("9999");
		DateTime lastModified = DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS("20121122101112");


		String removeSql = "remove";

		Map<String, String> strList = new HashMap<>();

		Map<String, Object> objList = new HashMap<>();

		Query model;

		@Before
		public void before() {
			QueryBuilder builder = new QueryBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void execute() {
			Testing1 x = new Testing1();
			x.strList = strList;
			x.objList = objList;
			x.update = 1;

			x.execute(userId, appId, id, lastModified);

			assertThat(x.toStringParameterListCalled, is(true));
			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.updateCalled, is(true));
			assertThat(x.getRemoveSqlCalled, is(true));
		}

		@Test
		public void executeByModel() {
			Testing1 x = new Testing1();

			x.execute(model);

			assertThat(x.executeCalled, is(true));
		}

		@Test
		public void thrownExceptionWhenModelNotExists() {
			thrown.expect(RuntimeException.class);
			thrown.expectMessage("testing");

			Testing1 x = new Testing1();
			x.strList = strList;
			x.objList = objList;
			x.update = 0;

			x.execute(userId, appId, id, lastModified);
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		ModelIdImpl id = new ModelIdImpl("9999");
		DateTime lastModified = DateTimeUtils.toDateTimeFromStringYYYYMMDDHHMISS("20121122101112");

		@Test
		public void toStringParameterList() {
			BaseRemoveAppModel<Query> x = mock(BaseRemoveAppModel.class,
					Mockito.CALLS_REAL_METHODS);

			Map<String, String> result = x.toStringParameterList(
					userId, appId, id, lastModified);

			assertThat(result.size(), is(4));

			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("id"), is(true));
			assertThat(result.containsKey("lastModified"), is(true));

			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("id"), is("9999"));
			assertThat(result.get("lastModified"), is("20121122101112"));
		}
	}

	public static class GetRemoveSql {
		BaseRemoveAppModel<Query> x = new RemoveQueryImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getRemoveSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getRemoveSql(objList);

			verify(sql).getRemoveSql(objList);
		}
	}
}
