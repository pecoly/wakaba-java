package net.jp.yamabuki.service.manager.css.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.CssBuilder;
import net.jp.yamabuki.model.resource.CssMatcher;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddCssImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<Css> addModel;
		protected static GetAppModelList<Css> getModelList;
		protected static CssBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "hoge";
			String content = "あいうえお";
			modelBuilder = new CssBuilder(id, userId, appId, name, content, "20001011121314");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/css/sql/postgresql/AddCssImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllModels();

			assertThat(countMessages(), is(0));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
			addModel = null;
			getModelList = null;
			modelBuilder = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM CSS_LIST");
		}

		static int countMessages() {
			return new JdbcTemplate(dataSource).queryForObject(
					"SELECT COUNT(*) FROM CSS_LIST",
					Integer.class);
		}

		/**
		 * Cssを追加できること
		 *
		 */
		public static class Add extends Base {
			static Css model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<Css> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				Css obj = new CssBuilder(modelBuilder)
						.withId(model2.getId())
						.build();
				assertThat(model2, CssMatcher.matches(obj));
			}
		}

		/**
		 * 存在しているCssをさらに追加した場合、例外が発生すること
		 *
		 */
		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.CSS_ALREADY_EXISTS.getNumber()
						+ " CSS already exists. name : hoge");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
