package net.jp.yamabuki.service.manager.jdbcconnection.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.JdbcConnectionMatcher;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetJdbcConnectionByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static JdbcConnection model;
		protected static GetAppModelList<JdbcConnection> getModelList;
		protected static GetAppModelById<JdbcConnection> getModelById;
		protected static AddAppModel<JdbcConnection> addModel;
		protected static JdbcConnectionBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "yamada";
			String driver = "driver";
			String url = "url";
			String username = "username";
			String password = "password";
			modelBuilder = new JdbcConnectionBuilder(
					id, userId, appId, name,
					driver, url, username, password, null);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/jdbcconnection/sql/postgresql/JdbcConnectionTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllAppUsers();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (JdbcConnection x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllAppUsers();
			context = null;
			dataSource = null;
		}

		static void removeAllAppUsers() {
			new JdbcTemplate(dataSource).update("DELETE FROM JDBC_CONNECTION_LIST");
		}
	}

	public static class ExecuteByModel extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String userId = "kimura";
			String appId = "honda";

			GetAppModelById<JdbcConnection> getModelByIdSpy = spy(getModelById);
			getModelByIdSpy.execute(new JdbcConnection(
					new ModelIdImpl(id),
					new UserIdImpl(userId), new AppIdImpl(appId),
					"a", "s", "t", "u", "v"));

			verify(getModelByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するJdbcコネクションを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static JdbcConnection model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			JdbcConnection obj = new JdbcConnectionBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, JdbcConnectionMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないJdbcコネクションを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			JdbcConnection appUser = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			JdbcConnection appUser = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualId() {
			JdbcConnection appUser = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(appUser, nullValue());
		}
	}
}
