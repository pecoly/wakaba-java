package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.service.manager.query.sql.GetQueryByIdImpl;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseGetAppModelByIdTest {

	public static class Testing1 extends BaseGetAppModelById<Query> {

		public boolean getMangerSqlCalled;

		public boolean getMapperCalled;

		public boolean toStringParameterListCalled;

		public boolean toObjectParameterListCalled;

		public boolean getGetByIdSqlCalled;

		public boolean queryForObjectCalled;

		public boolean executeCalled;

		@Override
		public ManagerSql getManagerSql() {
			this.getMangerSqlCalled = true;
			return null;
		}

		@Override
		public RowMapper<Query> getMapper() {
			this.getMapperCalled = true;
			return null;
		}

		@Override
		Map<String, String> toStringParameterList(UserId userId, AppId appId, ModelId id) {
			this.toStringParameterListCalled = true;
			return null;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return null;
		}

		@Override
		public String getGetByIdSql(Map<String, Object> parameterList) {
			this.getGetByIdSqlCalled = true;
			return null;
		}

		@Override
		public Query execute(Query query) {
			this.executeCalled = true;
			return null;
		}

		@Override
		public <T> T queryForObject(String sql, Map<String, Object> map,
				RowMapper<T> rowMapper) {
			this.queryForObjectCalled = true;
			return null;
		}
	}

	public static class Execute {

		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		ModelIdImpl id = new ModelIdImpl("9876");
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		String getSql = "get";

		Query model;

		@Before
		public void before() {
			QueryBuilder builder = new QueryBuilder(
					null, "suzuki", "jiro",
					"query", "hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void execute() {
			Testing1 x = new Testing1();

			x.execute(userId, appId, id);

			assertThat(x.toStringParameterListCalled, is(true));
			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.getGetByIdSqlCalled, is(true));
			assertThat(x.getMapperCalled, is(true));
			assertThat(x.queryForObjectCalled, is(true));
		}

		@Test
		public void executeByModel() {
			Testing1 x = new Testing1();

			x.execute(model);

			assertThat(x.executeCalled, is(true));
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		ModelIdImpl id = new ModelIdImpl("9876");

		@Test
		public void toStringParameterList() {
			BaseGetAppModelById<Query> x = mock(BaseGetAppModelById.class,
					Mockito.CALLS_REAL_METHODS);

			Map<String, String> result = x.toStringParameterList(userId, appId, id);

			assertThat(result.size(), is(3));

			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("id"), is(true));

			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("id"), is("9876"));
		}
	}


	public static class GetGetByIdSql {
		BaseGetAppModelById<Query> x = new GetQueryByIdImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getGetCountSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getGetByIdSql(objList);

			verify(sql).getGetByIdSql(objList);
		}
	}
}
