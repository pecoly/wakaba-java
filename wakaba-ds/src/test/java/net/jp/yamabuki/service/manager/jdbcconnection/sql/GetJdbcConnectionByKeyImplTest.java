package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.JdbcConnectionMatcher;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.*;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetJdbcConnectionByKeyImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetJdbcConnectionByKeyImpl x = new GetJdbcConnectionByKeyImpl();
			GetJdbcConnectionByKeyImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class Execute {

		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		String name = "hoge";
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		String getSql = "get";
		RowMapper<JdbcConnection> mapper = new JdbcConnectionMapper();
		JdbcConnectionBuilder builder = new JdbcConnectionBuilder(
				"9876", "suzuki", "jiro",
				"hoge", "fuga", "baru", "foo", "bar", "20121211100908");
		JdbcConnection model = builder.build();

		public GetJdbcConnectionByKeyImpl createGetByKeySpy() {
			GetJdbcConnectionByKeyImpl x = new GetJdbcConnectionByKeyImpl();
			GetJdbcConnectionByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "mapper", mapper);
			return spy;
		}

		@Test
		public void execute() {
			GetJdbcConnectionByKeyImpl spy = createGetByKeySpy();

			doReturn(strList).when(spy)
					.toStringParameterList(userId, appId, name);
			doReturn(objList).when(spy)
					.toObjectParameterList(strList);
			doReturn(getSql).when(spy)
					.getGetByKeySql(objList);
			doReturn(model).when(spy)
					.queryForObject(getSql, objList, mapper);

			JdbcConnection result = spy.execute(userId, appId, name);

			assertThat(model, JdbcConnectionMatcher.matches(result));

			verify(spy).toStringParameterList(userId, appId, name);
			verify(spy).toObjectParameterList(strList);
			verify(spy).getGetByKeySql(objList);
			verify(spy).queryForObject(getSql, objList, mapper);
		}

		@Test
		public void executeByModel() {
			GetJdbcConnectionByKeyImpl spy = createGetByKeySpy();

			doReturn(null).when(spy).execute(userId, appId, name);

			spy.execute(model);

			verify(spy).execute(userId, appId, name);
		}
	}

	public static class ToStringParameterList {
		@Test
		public void toStringParameterList() {
			GetJdbcConnectionByKeyImpl x = new GetJdbcConnectionByKeyImpl();

			UserIdImpl userId = new UserIdImpl("suzuki");
			AppIdImpl appId = new AppIdImpl("jiro");
			String name = "hoge";
			Map<String, String> result = x.toStringParameterList(userId, appId, name);
			assertThat(result.size(), is(3));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("name"), is("hoge"));
		}
	}


	public static class GetGetByKeySql {
		@Test
		public void getGetCountSqlCalled() {
			GetJdbcConnectionByKeyImpl x = new GetJdbcConnectionByKeyImpl();
			ManagerSql sql = mock(ManagerSql.class);
			ReflectionTestUtils.setField(x, "managerSql", sql);
			Map<String, Object> objList = new HashMap<>();

			x.getGetByKeySql(objList);

			verify(sql).getGetByKeySql(objList);
		}
	}
}
