package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetMessageByKeyImplTest {
	public static class Base {

		protected static Message model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<Message> getModelList;
		protected static GetModelByKey<Message> getModelByKey;
		protected static AddAppModel<Message> addModel;
		protected static DataSource dataSource;
		protected static MessageBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "main";
			String localeId = "ja";
			String msg = "suzuki";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, localeId, msg, "20010203102030");

			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/GetMessageByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Message x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}
	}

	public static class Exists extends Base {
		protected static Message model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			Message key = new MessageBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Message obj = new MessageBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, MessageMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Message key = new MessageBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			Message appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Message key = new MessageBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			Message appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualCode() {
			Message key = new MessageBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withCode("hoge")
					.build();

			Message appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualLocaleId() {
			Message key = new MessageBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withLocaleId("en")
					.build();

			Message appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
