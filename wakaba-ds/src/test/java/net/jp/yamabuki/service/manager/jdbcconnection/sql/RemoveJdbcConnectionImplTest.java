package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RemoveJdbcConnectionImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			RemoveJdbcConnectionImpl x = new RemoveJdbcConnectionImpl();
			RemoveJdbcConnectionImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}
}
