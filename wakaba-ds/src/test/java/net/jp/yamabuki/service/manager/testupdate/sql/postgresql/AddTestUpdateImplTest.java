package net.jp.yamabuki.service.manager.testupdate.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.datasource.TestUpdate;
import net.jp.yamabuki.model.datasource.TestUpdateBuilder;
import net.jp.yamabuki.model.datasource.TestUpdateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.testupdate.GetTestUpdateByKey;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

public class AddTestUpdateImplTest {
	public static class Base {
		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<TestUpdate> addTestUpdate;
		protected static GetTestUpdateByKey getTestUpdateByKey;
		protected static TestUpdateBuilder testUpdateBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String type = "query";
			String name = "abc";
			String xml = "def";
			testUpdateBuilder = new TestUpdateBuilder(id, userId, appId, type, name, xml);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/testupdate/sql/postgresql/AddTestUpdateImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addTestUpdate = context.getBean(AddAppModel.class);
			getTestUpdateByKey = context.getBean(GetTestUpdateByKey.class);

			removeAllQueries();

			assertThat(countQueries(), is(0));
		}

		static void removeAllQueries() {
			new JdbcTemplate(dataSource).update("DELETE FROM TEST_UPDATE_LIST");
		}

		static int countQueries() {
			return new JdbcTemplate(dataSource).queryForObject(
					"SELECT COUNT(*) FROM TEST_UPDATE_LIST",
					Integer.class);
		}

		@AfterClass
		public static void afterClass() {
			removeAllQueries();
			context = null;
			dataSource = null;
			addTestUpdate = null;
		}
	}

	public static class Add_Ok_WhenTestUpdateNotExists extends Base {

		static TestUpdate query2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addTestUpdate.execute(testUpdateBuilder.build());
			Map<String, String> map = new HashMap<>();
			assertThat(countQueries(), is(1));
			query2 = getTestUpdateByKey.execute(testUpdateBuilder.build());
			assertThat(query2, is(notNullValue()));
		}

		@Test
		public void equals() {
			TestUpdate obj = new TestUpdateBuilder(testUpdateBuilder)
					.withId(query2.getId())
					.build();
			assertThat(query2, TestUpdateMatcher.matches(obj));
		}
	}

	public static class Add_Ok_WhenTestUpdateExists extends Base {

		static TestUpdate query2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			// 1件追加
			addTestUpdate.execute(testUpdateBuilder.withXml("first").build());
			assertThat(countQueries(), is(1));

			// さらに追加したとき、問題なく追加できること
			addTestUpdate.execute(testUpdateBuilder.withXml("second").build());
			assertThat(countQueries(), is(1));

			query2 = getTestUpdateByKey.execute(testUpdateBuilder.build());
			assertThat(query2, is(notNullValue()));
		}

		@Test
		public void equals() {
			TestUpdate obj = new TestUpdateBuilder(testUpdateBuilder)
					.withId(query2.getId())
					.withXml("second")
					.build();
			assertThat(query2, TestUpdateMatcher.matches(obj));
		}
	}



	public static class Add_Rollback_WhenTestUpdateExistsAndErrorOccurred extends Base {
		@Test
		public void thrown() {
			removeAllQueries();
			assertThat(countQueries(), is(0));

			// 1件追加
			addTestUpdate.execute(testUpdateBuilder.build());
			assertThat(countQueries(), is(1));

			try {
				TestUpdate invalid = testUpdateBuilder.build();
				ReflectionTestUtils.setField(invalid, "xml", null);

				// さらに追加
				addTestUpdate.execute(invalid);
			}
			catch (ModelAlreadyExistsException ex) {
			}

			// 削除されたのがロールバックされていること
			assertThat(countQueries(), is(1));
		}
	}
}
