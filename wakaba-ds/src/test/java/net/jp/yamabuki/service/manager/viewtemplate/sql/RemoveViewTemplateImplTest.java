package net.jp.yamabuki.service.manager.viewtemplate.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.model.converter.StringConverter;

import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RemoveViewTemplateImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			RemoveViewTemplateImpl x = new RemoveViewTemplateImpl();
			RemoveViewTemplateImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}
}
