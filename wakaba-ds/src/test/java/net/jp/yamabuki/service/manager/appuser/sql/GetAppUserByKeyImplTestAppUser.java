package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.*;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetAppUserByKeyImplTestAppUser {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetAppUserByKeyImpl x = new GetAppUserByKeyImpl();
			GetAppUserByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", new StringConverter());
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class Execute {

		UserId userId = new UserIdImpl("suzuki");
		AppId appId = new AppIdImpl("jiro");
		String name = "hoge";
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		String getSql = "get";
		RowMapper<AppUser> mapper = new AppUserMapper();
		AppUserBuilder builder = new AppUserBuilder(
				"9876", "hoge", "fuga",
				"main", "sub", "suzuki", "jiro", "20121211100908");
		AppUser model = builder.build();

		public GetAppUserByKeyImpl createGetByKeySpy() {
			GetAppUserByKeyImpl x = new GetAppUserByKeyImpl();
			GetAppUserByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "mapper", mapper);
			return spy;
		}

		@Test
		public void execute() {
			GetAppUserByKeyImpl spy = createGetByKeySpy();

			doReturn(strList).when(spy)
					.toStringParameterList(userId, appId, name);
			doReturn(objList).when(spy)
					.toObjectParameterList(strList);
			doReturn(getSql).when(spy)
					.getGetByKeySql(objList);
			doReturn(model).when(spy)
					.queryForObject(getSql, objList, mapper);

			AppUser result = spy.execute(userId, appId, name);

			assertThat(model, AppUserMatcher.matches(result));

			verify(spy).toStringParameterList(userId, appId, name);
			verify(spy).toObjectParameterList(strList);
			verify(spy).getGetByKeySql(objList);
			verify(spy).queryForObject(getSql, objList, mapper);
		}

		@Test
		public void executeByModel() {
			GetAppUserByKeyImpl spy = createGetByKeySpy();

			doReturn(null).when(spy).execute(userId, appId, name);

			spy.execute(model);

			verify(spy).execute(userId, appId, name);
		}
	}

	public static class ToStringParameterList {
		@Test
		public void toStringParameterList() {
			GetAppUserByKeyImpl x = new GetAppUserByKeyImpl();

			UserId userId = new UserIdImpl("suzuki");
			AppId appId = new AppIdImpl("jiro");
			ModelId id = new ModelIdImpl("9876");
			String loginId = "hoge";
			Map<String, String> result = x.toStringParameterList(userId, appId, loginId);
			assertThat(result.size(), is(3));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("loginId"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("loginId"), is("hoge"));
		}
	}


	public static class GetGetCountSql {
		@Test
		public void getGetCountSqlCalled() {
			GetAppUserByKeyImpl x = new GetAppUserByKeyImpl();
			ManagerSql sql = mock(ManagerSql.class);
			ReflectionTestUtils.setField(x, "managerSql", sql);
			Map<String, Object> objList = new HashMap<>();

			x.getGetByKeySql(objList);

			verify(sql).getGetByKeySql(objList);
		}
	}
}
