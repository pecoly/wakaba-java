package net.jp.yamabuki.service.manager.update.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.datasource.UpdateBuilder;
import net.jp.yamabuki.model.datasource.UpdateMatcher;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetUpdateByKeyImplTest {
	public static class Base {

		protected static Update model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<Update> getModelList;
		protected static GetModelByKey<Update> getModelByKey;
		protected static AddAppModel<Update> addModel;
		protected static DataSource dataSource;
		protected static UpdateBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String type = "query";
			String name = "abc";
			String xml = "def";
			modelBuilder = new UpdateBuilder(id, userId, appId,
					type, name, xml, "20101112030405");

			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/update/sql/postgresql/GetUpdateByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Update x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM UPDATE_LIST");
		}
	}

	public static class Exists extends Base {
		protected static Update model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			Update key = new UpdateBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Update obj = new UpdateBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, UpdateMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Update key = new UpdateBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			Update appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Update key = new UpdateBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			Update appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualName() {
			Update key = new UpdateBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("hoge")
					.build();

			Update appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
