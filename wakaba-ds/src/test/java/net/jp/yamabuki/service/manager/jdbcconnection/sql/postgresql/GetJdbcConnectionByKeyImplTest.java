package net.jp.yamabuki.service.manager.jdbcconnection.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.JdbcConnectionMatcher;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetJdbcConnectionByKeyImplTest {
	public static class Base {

		protected static JdbcConnection model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<JdbcConnection> getModelList;
		protected static GetModelByKey<JdbcConnection> getModelByKey;
		protected static AddAppModel<JdbcConnection> addModel;
		protected static DataSource dataSource;
		protected static JdbcConnectionBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "yamada";
			String driver = "driver";
			String url = "url";
			String username = "username";
			String password = "password";
			modelBuilder = new JdbcConnectionBuilder(
					id, userId, appId, name,
					driver, url, username, password, null);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/jdbcconnection/sql/postgresql/JdbcConnectionTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (JdbcConnection x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM JDBC_CONNECTION_LIST");
		}
	}

	public static class Exists extends Base {
		protected static JdbcConnection model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			JdbcConnection key = new JdbcConnectionBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("yamada")
					.build();
			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			JdbcConnection obj = new JdbcConnectionBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, JdbcConnectionMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			JdbcConnection key = new JdbcConnectionBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			// キーのIdはnullとなっていること
			assertThat(key.getId().getValue(), is(nullValue()));

			JdbcConnection appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			JdbcConnection key = new JdbcConnectionBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			JdbcConnection appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualName() {
			JdbcConnection key = new JdbcConnectionBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("hoge")
					.build();

			JdbcConnection appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
