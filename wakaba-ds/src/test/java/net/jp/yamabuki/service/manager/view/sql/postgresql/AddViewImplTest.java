package net.jp.yamabuki.service.manager.view.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.ViewBuilder;
import net.jp.yamabuki.model.ViewMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddViewImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<View> addModel;
		protected static GetAppModelList<View> getModelList;
		protected static ViewBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "a/b";
			String content = "あいうえお";
			modelBuilder = new ViewBuilder(id, userId, appId,
					name, content, "20111213010203");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/view/sql/postgresql/AddViewImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_LIST");
		}

		public static class Add extends Base {
			static View model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<View> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				View obj = new ViewBuilder(modelBuilder)
						.withId(model2.getId())
						.build();
				assertThat(model2, ViewMatcher.matches(obj));
			}
		}

		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.VIEW_ALREADY_EXISTS.getNumber()
						+ " View already exists. name : a/b");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
