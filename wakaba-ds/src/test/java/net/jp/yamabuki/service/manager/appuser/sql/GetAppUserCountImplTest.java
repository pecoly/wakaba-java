package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetAppUserCountImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetAppUserCountImpl x = new GetAppUserCountImpl();
			GetAppUserCountImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}


	public static class ToStringParameterList {
		Map<String, String> map = new HashMap<>();
		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		Map<String, String> toStringParameterList(Map<String, String> map, ManagerSql sql, SqlUtils utils) {
			GetAppUserCountImpl x = new GetAppUserCountImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x.toStringParameterList(map);
		}

		/**
		 * パラメータなしの場合。
		 */
		@Test
		public void none() {
			Map<String, String> result = toStringParameterList(map, null, null);

			assertThat(result.size(), is(0));
		}

		/**
		 * パラメータありの場合。
		 * loginId
		 */
		@Test
		public void hasLoginId() {
			map.put("loginId", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			Map<String, String> result = toStringParameterList(map, sql, utils);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("loginId"), is(true));
			assertThat(result.get("loginId"), is("fuga"));
		}

		/**
		 * パラメータが空文字の場合。
		 */
		@Test
		public void emptyLoginId() {
			map.put("loginId", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			Map<String, String> result = toStringParameterList(map, sql, utils);

			assertThat(result.size(), is(0));
		}
	}
}
