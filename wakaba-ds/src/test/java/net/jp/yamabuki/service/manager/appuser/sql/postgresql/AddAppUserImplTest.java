package net.jp.yamabuki.service.manager.appuser.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddAppUserImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<AppUser> addModel;
		protected static GetAppModelList<AppUser> getModelList;
		protected static AppUserBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String loginId = "yamada";
			String password = "taro";
			String main = "main";
			String sub = "sub";
			String userId = "suzuki";
			String appId = "jiro";
			String lastModified = "20001011121314";
			modelBuilder = new AppUserBuilder(id, loginId, password,
					main, sub, userId, appId, lastModified);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/appuser/sql/postgresql/appUser.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
			addModel = null;
			getModelList = null;
			modelBuilder = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM USER_LIST");
		}

		public static class Add extends Base {
			static AppUser model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<AppUser> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				AppUser obj = new AppUserBuilder(modelBuilder)
						.withId(model2.getId())
						.build();
				assertThat(model2, AppUserMatcher.matches(obj));
			}
		}

		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.APP_USER_ALREADY_EXISTS.getNumber()
						+ " App user already exists. loginId : yamada");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
