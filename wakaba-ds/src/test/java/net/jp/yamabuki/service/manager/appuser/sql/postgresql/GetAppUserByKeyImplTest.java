package net.jp.yamabuki.service.manager.appuser.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.AppUserMatcher;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetAppUserByKeyImplTest {
	public static class Base {

		protected static AppUser model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<AppUser> getModelList;
		protected static GetModelByKey<AppUser> getModelByKey;
		protected static AddAppModel<AppUser> addModel;
		protected static DataSource dataSource;
		protected static AppUserBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String loginId = "yamada";
			String loginPassword = "taro";
			String main = "main";
			String sub = "sub";
			String userId = "suzuki";
			String appId = "jiro";
			String lastModified = "20001011121314";

			modelBuilder = new AppUserBuilder(null, loginId, loginPassword,
					main, sub, userId, appId, lastModified);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/appuser/sql/postgresql/appUser.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (AppUser x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM USER_LIST");
		}
	}

	public static class Exists extends Base {
		protected static AppUser model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			AppUser key = new AppUserBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withLoginId("yamada")
					.build();
			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			AppUser obj = new AppUserBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, AppUserMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			AppUser key = new AppUserBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			// キーのIdはnullとなっていること
			assertThat(key.getId().getValue(), is(nullValue()));

			AppUser appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			AppUser key = new AppUserBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			AppUser appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualLoginId() {
			AppUser key = new AppUserBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withLoginId("hoge")
					.build();

			AppUser appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
