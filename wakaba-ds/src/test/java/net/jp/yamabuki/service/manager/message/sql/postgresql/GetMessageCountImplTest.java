package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelCount;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetMessageCountImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelCount getModelCount;
		protected static AddAppModel<Message> addModel;
		protected static DataSource dataSource;
		protected static MessageBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "main";
			String localeId = "ja";
			String msg = "suzuki";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, localeId, msg, "20010203102030");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/GetMessageCountImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelCount = context.getBean(GetAppModelCount.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withCode("bbc").build());
		}

		@Test
		public void list() {
			long count = getModelCount.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					map);
			assertThat(count, is(3L));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void by_userId_appId() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").build());

			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withCode("aaa").build());

			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withCode("aaa").build());

			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(3L));
		}

		@Test
		public void code() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").build());

			map.put("code", "bc");
			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(2L));
		}

		@Test
		public void by_locale() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").withLocaleId("ja").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("cde").withLocaleId("en").build());

			map.put("localeId", "ja");
			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(3L));
		}

		@Test
		public void by_message() {
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("aaa").withMessage("xxx").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("abc").withMessage("xyz").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("bbc").withMessage("yyz").build());
			addModel.execute(new MessageBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withCode("cde").withMessage("zyx").build());

			map.put("message", "yz");
			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(2L));
		}
	}
}
