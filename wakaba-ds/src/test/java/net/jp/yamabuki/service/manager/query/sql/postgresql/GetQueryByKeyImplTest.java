package net.jp.yamabuki.service.manager.query.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.QueryMatcher;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetQueryByKeyImplTest {
	public static class Base {

		protected static Query model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<Query> getModelList;
		protected static GetModelByKey<Query> getModelByKey;
		protected static AddAppModel<Query> addModel;
		protected static DataSource dataSource;
		protected static QueryBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "hoge.fuga";
			String locale = "ja";
			String query = "あいうえお";
			modelBuilder = new QueryBuilder(id, userId, appId,
					code, locale, query, "20101112030405");

			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/query/sql/postgresql/GetQueryByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Query x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM QUERY_LIST");
		}
	}

	public static class Exists extends Base {
		protected static Query model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			Query key = new QueryBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Query obj = new QueryBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, QueryMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Query key = new QueryBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			Query appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Query key = new QueryBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			Query appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualName() {
			Query key = new QueryBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("hoge")
					.build();

			Query appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
