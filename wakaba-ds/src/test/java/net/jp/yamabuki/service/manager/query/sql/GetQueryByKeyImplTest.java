package net.jp.yamabuki.service.manager.query.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.datasource.QueryMatcher;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.*;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetQueryByKeyImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetQueryByKeyImpl x = new GetQueryByKeyImpl();
			GetQueryByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", new StringConverter());
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class Execute {

		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		String name = "hoge";
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		String getSql = "get";
		RowMapper<Query> mapper = new QueryMapper();
		QueryBuilder builder = new QueryBuilder("9876", "suzuki", "jiro",
				"query", "hoge", "fuga", "20121211100908");
		Query model = builder.build();

		public GetQueryByKeyImpl createGetByKeySpy() {
			GetQueryByKeyImpl x = new GetQueryByKeyImpl();
			GetQueryByKeyImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "mapper", mapper);
			return spy;
		}

		@Test
		public void execute() {
			GetQueryByKeyImpl spy = createGetByKeySpy();

			doReturn(strList).when(spy)
					.toStringParameterList(userId, appId, name);
			doReturn(objList).when(spy)
					.toObjectParameterList(strList);
			doReturn(getSql).when(spy)
					.getGetByKeySql(objList);
			doReturn(model).when(spy)
					.queryForObject(getSql, objList, mapper);

			Query result = spy.execute(userId, appId, name);

			assertThat(model, QueryMatcher.matches(result));

			verify(spy).toStringParameterList(userId, appId, name);
			verify(spy).toObjectParameterList(strList);
			verify(spy).getGetByKeySql(objList);
			verify(spy).queryForObject(getSql, objList, mapper);
		}

		@Test
		public void executeByModel() {
			GetQueryByKeyImpl spy = createGetByKeySpy();

			doReturn(null).when(spy).execute(userId, appId, name);

			spy.execute(model);

			verify(spy).execute(userId, appId, name);
		}
	}

	public static class ToStringParameterList {
		@Test
		public void toStringParameterList() {
			GetQueryByKeyImpl x = new GetQueryByKeyImpl();

			UserIdImpl userId = new UserIdImpl("suzuki");
			AppIdImpl appId = new AppIdImpl("jiro");
			ModelIdImpl id = new ModelIdImpl("9876");
			String name = "hoge";
			Map<String, String> result = x.toStringParameterList(userId, appId, name);
			assertThat(result.size(), is(3));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("name"), is("hoge"));
		}
	}


	public static class GetGetCountSql {
		@Test
		public void getGetCountSqlCalled() {
			GetQueryByKeyImpl x = new GetQueryByKeyImpl();
			ManagerSql sql = mock(ManagerSql.class);
			ReflectionTestUtils.setField(x, "managerSql", sql);
			Map<String, Object> objList = new HashMap<>();

			x.getGetByKeySql(objList);

			verify(sql).getGetByKeySql(objList);
		}
	}
}
