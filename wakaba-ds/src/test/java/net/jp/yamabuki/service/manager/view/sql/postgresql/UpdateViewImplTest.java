package net.jp.yamabuki.service.manager.view.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.ViewBuilder;
import net.jp.yamabuki.model.ViewMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class UpdateViewImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static String modelId;
		protected static AddAppModel<View> addModel;
		protected static UpdateAppModel<View> updateModel;
		protected static GetAppModelList<View> getModelList;
		protected static ViewBuilder modelBuilder;
		protected static View model1;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "ABC";
			String content = "あいうえお";
			modelBuilder = new ViewBuilder(id, userId, appId,
					name, content, "20111213010203");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/view/sql/postgresql/UpdateViewImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);
			updateModel = context.getBean(UpdateAppModel.class);

			removeAllModels();

			addModel.execute(modelBuilder.build());

			Map<String, String> map = new HashMap<>();
			List<View> list = getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));

			model1 = list.get(0);
			modelId = list.get(0).getId().getValue();
			assertThat(modelId, is(not(nullValue())));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_LIST");
		}

		public static class Update extends Base {
			static View model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				updateModel.execute(new ViewBuilder(modelBuilder)
					.withId(modelId)
					.withName("abc")
					.withContent("かきくけこ")
					.withLastModified(DateTimeUtils.toStringYYYYMMDDHHMISS(
							model1.getLastModified()))
					.build());
				Map<String, String> map = new HashMap<>();
				List<View> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				View obj = new ViewBuilder(modelBuilder)
						.withId(model2.getId())
						.withName("abc")
						.withContent("かきくけこ")
						.build();
				assertThat(model2, ViewMatcher.matches(obj));
			}
		}

		public static class Update_NotExists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelNotFoundException.class);
				thrown.expectMessage(WakabaErrorCode.VIEW_NOT_FOUND.getNumber()
						+ " View not found. id : 9999");
				updateModel.execute(new ViewBuilder(modelBuilder)
					.withId("9999")
					.build());
			}
		}
	}
}
