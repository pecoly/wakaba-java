package net.jp.yamabuki.service.manager.plugin.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetPluginCountImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetPluginCountImpl x = new GetPluginCountImpl();
			GetPluginCountImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}


	public static class ToStringParameterList {
		Map<String, String> map = new HashMap<>();

		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		Map<String, String> toStringParameterList(Map<String, String> map,
				ManagerSql sql, SqlUtils utils) {
			GetPluginCountImpl x = new GetPluginCountImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x.toStringParameterList(map);
		}

		/**
		 * パラメータなしの場合。
		 */
		@Test
		public void none() {
			Map<String, String> result = toStringParameterList(map, null, null);

			assertThat(result.size(), is(0));
		}

		/**
		 * パラメータありの場合。
		 * name
		 */
		@Test
		public void hasName() {
			map.put("name", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			Map<String, String> result = toStringParameterList(map, sql, utils);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("fuga"));
		}

		/**
		 * パラメータが空文字の場合。
		 */
		@Test
		public void emptyName() {
			map.put("name", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			Map<String, String> result = toStringParameterList(map, sql, utils);

			assertThat(result.size(), is(0));
		}
	}
}
