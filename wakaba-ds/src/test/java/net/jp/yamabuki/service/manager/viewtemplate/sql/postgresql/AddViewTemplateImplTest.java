package net.jp.yamabuki.service.manager.viewtemplate.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.ViewTemplateBuilder;
import net.jp.yamabuki.model.ViewTemplateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddViewTemplateImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<ViewTemplate> addModel;
		protected static GetAppModelList<ViewTemplate> getModelList;
		protected static ViewTemplateBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "a/b";
			String content = "あいうえお";
			String lastModified = "20010203040506";
			modelBuilder = new ViewTemplateBuilder(id, userId, appId,
					name, content, lastModified);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/viewtemplate/sql/postgresql/AddViewTemplateImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_TEMPLATE_LIST");
		}

		public static class Add extends Base {
			static ViewTemplate model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<ViewTemplate> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				ViewTemplate obj = new ViewTemplateBuilder(modelBuilder)
						.withId(model2.getId())
						.build();
				assertThat(model2, ViewTemplateMatcher.matches(obj));
			}
		}

		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();
			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.VIEW_TEMPLATE_ALREADY_EXISTS.getNumber()
						+ " View template already exists. name : a/b");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
