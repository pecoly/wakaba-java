package net.jp.yamabuki.service.manager.viewtemplate.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ManagerAppId;
import net.jp.yamabuki.model.ManagerUserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.ViewTemplateBuilder;
import net.jp.yamabuki.model.ViewTemplateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.viewtemplate.GetViewTemplateByKey;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetViewTemplateByKeyImplTest {
	public static class Base {

		protected static ViewTemplate model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<ViewTemplate> getModelList;
		protected static GetViewTemplateByKey getModelByKey;
		protected static AddAppModel<ViewTemplate> addModel;
		protected static DataSource dataSource;
		protected static ViewTemplateBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = "123";
			String userId = "*";
			String appId = "*";
			String name = "yamada";
			String content = "taro";
			String lastModified = "20010203040506";

			modelBuilder = new ViewTemplateBuilder(id, userId, appId,
					name, content, lastModified);
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/viewtemplate/sql/postgresql/GetViewTemplateByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetViewTemplateByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			List<ViewTemplate> list = getModelList.execute(
					ManagerUserId.getInstance(),
					ManagerAppId.getInstance(),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_TEMPLATE_LIST");
		}
	}

	public static class Exists extends Base {
		protected static ViewTemplate model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelByKey.execute(
					ManagerUserId.getInstance(),
					ManagerAppId.getInstance(),
					"yamada");
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			ViewTemplate obj = new ViewTemplateBuilder(modelBuilder)
					.build();

			assertThat(model2, ViewTemplateMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			ViewTemplate viewTemplate = getModelByKey.execute(
					new UserIdImpl("hoge"),
					ManagerAppId.getInstance(),
					"yamada");
			assertThat(viewTemplate, nullValue());
		}

		@Test
		public void notEqualAppId() {
			ViewTemplate viewTemplate = getModelByKey.execute(
					ManagerUserId.getInstance(),
					new AppIdImpl("hoge"),
					"yamada");
			assertThat(viewTemplate, nullValue());
		}

		@Test
		public void notEqualName() {
			ViewTemplate viewTemplate = getModelByKey.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					"tanaka");
			assertThat(viewTemplate, nullValue());
		}
	}
}
