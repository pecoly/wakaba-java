package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class UpdateMessageImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static String modelId;
		protected static AddAppModel<Message> addModel;
		protected static UpdateAppModel<Message> updateModel;
		protected static GetAppModelList<Message> getModelList;
		protected static MessageBuilder modelBuilder;
		protected static Message model1;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "hoge_fuga";
			String locale = "ja";
			String message = "あいうえお";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, locale, message, "20010203102030");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/UpdateMessageImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);
			updateModel = context.getBean(UpdateAppModel.class);

			removeAllModels();

			addModel.execute(modelBuilder.build());

			Map<String, String> map = new HashMap<>();
			List<Message> list = getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));

			model1 = list.get(0);
			modelId = list.get(0).getId().getValue();
			assertThat(modelId, is(not(nullValue())));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
			modelId = null;
			addModel = null;
			updateModel = null;
			getModelList = null;
			modelBuilder = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}

		public static class Update extends Base {
			static Message message2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				updateModel.execute(new MessageBuilder(modelBuilder)
					.withId(modelId)
					.withLocaleId("en")
					.withMessage("かきくけこ")
					.withLastModified(DateTimeUtils.toStringYYYYMMDDHHMISS(
							model1.getLastModified()))
					.build());
				Map<String, String> map = new HashMap<>();
				List<Message> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				message2 = list.get(0);
			}

			@Test
			public void equals() {
				Message obj = new MessageBuilder(modelBuilder)
						.withId(message2.getId())
						.withLocaleId("en")
						.withMessage("かきくけこ")
						.build();
				assertThat(message2, MessageMatcher.matches(obj));
			}
		}

		public static class Update_NotExists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelNotFoundException.class);
				thrown.expectMessage(WakabaErrorCode.MESSAGE_NOT_FOUND.getNumber()
						+ " Message not found. id : 9999");
				updateModel.execute(new MessageBuilder(modelBuilder)
					.withId("9999")
					.build());
			}
		}
	}
}
