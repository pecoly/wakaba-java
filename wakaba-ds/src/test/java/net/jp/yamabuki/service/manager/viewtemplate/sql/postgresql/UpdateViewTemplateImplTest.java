package net.jp.yamabuki.service.manager.viewtemplate.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.exception.UpdateException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.ViewTemplateBuilder;
import net.jp.yamabuki.model.ViewTemplateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class UpdateViewTemplateImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static String modelId;
		protected static AddAppModel<ViewTemplate> addModel;
		protected static UpdateAppModel<ViewTemplate> updateModel;
		protected static GetAppModelList<ViewTemplate> getModelList;
		protected static ViewTemplateBuilder modelBuilder;
		protected static ViewTemplate model1;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "abc";
			String content = "あいうえお";
			String lastModified = "20010203040506";
			modelBuilder = new ViewTemplateBuilder(id, userId, appId,
					name, content, lastModified);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/viewtemplate/sql/postgresql/UpdateViewTemplateImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);
			updateModel = context.getBean(UpdateAppModel.class);

			removeAllModels();

			addModel.execute(modelBuilder.build());

			Map<String, String> map = new HashMap<>();
			List<ViewTemplate> list = getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));

			model1 = list.get(0);
			modelId = list.get(0).getId().getValue();
			assertThat(modelId, is(not(nullValue())));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_TEMPLATE_LIST");
		}

		public static class Update extends Base {
			static ViewTemplate model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				updateModel.execute(new ViewTemplateBuilder(modelBuilder)
					.withId(modelId)
					.withName("abc")
					.withContent("かきくけこ")
					.withLastModified(DateTimeUtils.toStringYYYYMMDDHHMISS(
							model1.getLastModified()))
					.build());
				Map<String, String> map = new HashMap<>();
				List<ViewTemplate> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				ViewTemplate obj = new ViewTemplateBuilder(modelBuilder)
						.withId(model2.getId())
						.withName("abc")
						.withContent("かきくけこ")
						.build();
				assertThat(model2, ViewTemplateMatcher.matches(obj));
			}
		}

		public static class Update_NotExists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelNotFoundException.class);
				thrown.expectMessage(WakabaErrorCode.VIEW_TEMPLATE_NOT_FOUND.getNumber()
						+ " View template not found. id : 9999");
				updateModel.execute(new ViewTemplateBuilder(modelBuilder)
					.withId("9999")
					.build());
			}
		}
	}
}
