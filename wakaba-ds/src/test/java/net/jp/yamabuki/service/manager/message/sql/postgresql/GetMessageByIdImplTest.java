package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.LocaleIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelById;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.util.LocaleIdUtils;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class GetMessageByIdImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static String modelId;
		protected static DataSource dataSource;
		protected static Message model;
		protected static GetAppModelList<Message> getModelList;
		protected static GetAppModelById<Message> getModelById;
		protected static AddAppModel<Message> addModel;
		protected static MessageBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "main";
			String locale = "ja";
			String msg = "suzuki";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, locale, msg, "20010203102030");
			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/GetMessageByIdImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelById = context.getBean(GetAppModelById.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();

			for (Message x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, is(notNullValue()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}
	}

	public static class ExecuteByMessage extends Base {
		@Test
		public void execute() {
			String id = "1234";
			String userId = "kimura";
			String appId = "honda";
			String code = "B";
			String localeId = "*";
			String msg = "b";

			GetAppModelById<Message> getModelByIdSpy = spy(getModelById);
			getModelByIdSpy.execute(new Message(
					new ModelIdImpl(id),
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					code,
					LocaleIdUtils.create(localeId),
					msg));

			verify(getModelByIdSpy).execute(
					new UserIdImpl(userId),
					new AppIdImpl(appId),
					new ModelIdImpl(id));
		}
	}

	/**
	 * 存在するメッセージを取得できること。
	 *
	 */
	public static class Exists extends Base {
		protected static Message model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			model2 = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Message obj = new MessageBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, MessageMatcher.matches(obj));
		}
	}

	/**
	 * 存在しないメッセージを取得しようとした場合、 nullが返却されること。
	 *
	 */
	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Message model = getModelById.execute(
					new UserIdImpl("hoge"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Message model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("hoge"),
					new ModelIdImpl(modelId));
			assertThat(model, nullValue());
		}

		@Test
		public void notEqualId() {
			Message	model = getModelById.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new ModelIdImpl(modelId + "0"));
			assertThat(model, nullValue());
		}
	}
}
