package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.service.manager.query.sql.GetQueryListImpl;

import org.junit.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseGetAppModelListImplTest {
	public static class Testing1 extends BaseGetAppModelList<Query> {

		public boolean getMangerSqlCalled;

		public boolean toStringParameterListCalled;

		public boolean toObjectParameterListCalled;

		public boolean getGetListSqlCalled;

		public boolean queryCalled;

		@Override
		public ManagerSql getManagerSql() {
			this.getMangerSqlCalled = true;
			return null;
		}

		@Override
		public Map<String, String> toStringParameterList(
				UserId userId, AppId appId, Pageable page, Map<String, String> map) {
			this.toStringParameterListCalled = true;
			return null;
		}

		@Override
		public Map<String, String> toStringParameterList(Map<String, String> map) {
			return null;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return null;
		}

		@Override
		public <T> List<T> query(String sql, Map<String, Object> map,
				RowMapper<T> mapper) {
			this.queryCalled = true;
			return null;
		}

		@Override
		String getGetListSql(Map<String, Object> parameterList) {
			this.getGetListSqlCalled = true;
			return null;
		}

		@Override
		public RowMapper<Query> getMapper() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public static class Testing2 extends BaseGetAppModelList<Query> {

		public Map<String, String> toStringParameterList;

		@Override
		public Map<String, String> toStringParameterList(Map<String, String> map) {
			return this.toStringParameterList;
		}

		@Override
		public ManagerSql getManagerSql() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public RowMapper<Query> getMapper() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public static class Execute {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map = new HashMap<>();
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();
		Pageable page = new PageRequest(2, 30);

		@Test
		public void execute() {
			Testing1 x = new Testing1();

			x.execute(userId, appId, page, map);

			assertThat(x.toStringParameterListCalled, is(true));
			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.getGetListSqlCalled, is(true));
			assertThat(x.queryCalled, is(true));
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map;

		@Before
		public void before() {
			map = new HashMap<>();
		}

		/**
		 * パラメータなしの場合。
		 */
		@Test
		public void none() {
			Testing2 x = new Testing2();
			x.toStringParameterList = map;
			Map<String, String> result = x.toStringParameterList(
					userId, appId, new PageRequest(2, 10), map);

			assertThat(result.size(), is(4));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.containsKey("limit"), is(true));
			assertThat(result.containsKey("offset"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
			assertThat(result.get("limit"), is("10"));
			assertThat(result.get("offset"), is("20"));
		}

		/**
		 * パラメータありの場合。
		 * name
		 */
		@Test
		public void hasName() {
			map.put("name", "hoge");
			Testing2 x = new Testing2();
			x.toStringParameterList = map;
			Map<String, String> result = x.toStringParameterList(
					userId, appId, new PageRequest(2, 10), map);

			assertThat(result.size(), is(5));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("hoge"));
		}
	}

	public static class GetGetListSql {
		BaseGetAppModelList<Query> x = new GetQueryListImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getGetCountSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getGetListSql(objList);

			verify(sql).getGetListSql(objList);
		}
	}
}
