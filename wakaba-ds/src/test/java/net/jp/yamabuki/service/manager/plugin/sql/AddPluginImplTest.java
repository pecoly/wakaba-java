package net.jp.yamabuki.service.manager.plugin.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.model.PluginBuilder;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.service.manager.plugin.GetPluginByKey;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class AddPluginImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			AddPluginImpl x = new AddPluginImpl();
			AddPluginImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", stringConverter);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ThrowEx {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		Plugin model;

		@Before
		public void before() {
			PluginBuilder builder = new PluginBuilder(
					null, "suzuki", "jiro",
					"hoge", "fuga", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void throwEx() {
			thrown.expect(ModelAlreadyExistsException.class);
			thrown.expectMessage(WakabaErrorCode.PLUGIN_ALREADY_EXISTS.getNumber()
					+ " Plugin already exists. name : hoge");

			AddPluginImpl x = new AddPluginImpl();
			x.throwEx(model);
		}
	}
}
