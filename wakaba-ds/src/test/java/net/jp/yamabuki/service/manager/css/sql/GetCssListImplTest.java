package net.jp.yamabuki.service.manager.css.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetCssListImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetCssListImpl x = new GetCssListImpl();
			GetCssListImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map = new HashMap<>();
		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		GetCssListImpl createGetList(ManagerSql sql, SqlUtils utils) {
			GetCssListImpl x = new GetCssListImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x;
		}

		@Test
		public void none() {
			GetCssListImpl x = new GetCssListImpl();
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}

		@Test
		public void hasName() {
			map.put("name", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetCssListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("fuga"));
		}

		@Test
		public void empty() {
			map.put("name", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetCssListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}
	}
}
