package net.jp.yamabuki.service.manager.jdbcconnection.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetJdbcConnectionListImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelList<JdbcConnection> getModelList;
		protected static AddAppModel<JdbcConnection> addModel;
		protected static DataSource dataSource;
		protected static JdbcConnectionBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "yamada";
			String driver = "driver";
			String url = "url";
			String username = "username";
			String password = "password";
			modelBuilder = new JdbcConnectionBuilder(
					id, userId, appId, name,
					driver, url, username, password, null);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/jdbcconnection/sql/postgresql/JdbcConnectionTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM JDBC_CONNECTION_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void list() {
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
			assertThat(list.get(2).getName(), is("bbc"));
		}
	}

	public static class Limit extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void limitIs2() {
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 2), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void userId_appId() {
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			for (JdbcConnection x : list) {
				assertThat(x.getUserId().getValue(), is("u1"));
				assertThat(x.getAppId().getValue(), is("a1"));
			}
		}

		@Test
		public void name() {
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			map.put("name", "bc");
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("abc"));
			assertThat(list.get(1).getName(), is("bbc"));
		}

		@Test
		public void driver() {
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").withDriver("xxy").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").withDriver("xyz").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").withDriver("xxx").build());

			map.put("driver", "xy");
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
		}

		@Test
		public void url() {
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").withUrl("sss").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").withUrl("stu").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").withUrl("sst").build());

			map.put("url", "st");
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("abc"));
			assertThat(list.get(1).getName(), is("bbc"));
		}

		@Test
		public void username() {
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").withUsername("sss").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").withUsername("stu").build());
			addModel.execute(new JdbcConnectionBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").withUsername("sst").build());

			map.put("username", "st");
			List<JdbcConnection> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("abc"));
			assertThat(list.get(1).getName(), is("bbc"));
		}
	}
}
