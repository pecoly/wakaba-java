package net.jp.yamabuki.service.manager.viewtemplate.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.ViewTemplateBuilder;
import net.jp.yamabuki.model.converter.StringConverter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class AddViewTemplateImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			AddViewTemplateImpl x = new AddViewTemplateImpl();
			AddViewTemplateImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", stringConverter);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ThrowEx {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		ViewTemplate model;

		@Before
		public void before() {
			ViewTemplateBuilder builder = new ViewTemplateBuilder(
					null, "suzuki", "jiro",
					"hoge", "fuga", "20120102222324");
			model = builder.build();
		}

		@Test
		public void throwEx() {
			thrown.expect(ModelAlreadyExistsException.class);
			thrown.expectMessage(WakabaErrorCode.VIEW_TEMPLATE_ALREADY_EXISTS.getNumber()
					+ " View template already exists. name : hoge");

			AddViewTemplateImpl x = new AddViewTemplateImpl();
			x.throwEx(model);
		}
	}
}
