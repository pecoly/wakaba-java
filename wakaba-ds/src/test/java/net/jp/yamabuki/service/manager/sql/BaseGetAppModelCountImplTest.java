package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.query.sql.GetQueryCountImpl;

import org.junit.*;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseGetAppModelCountImplTest {
	public static class Testing1 extends BaseGetAppModelCount {

		public boolean getMangerSqlCalled;

		public boolean toStringParameterListCalled;

		public boolean toObjectParameterListCalled;

		public boolean getGetCountSqlCalled;

		public boolean queryForObjectCalled;

		@Override
		public ManagerSql getManagerSql() {
			this.getMangerSqlCalled = true;
			return null;
		}

		@Override
		public Map<String, String> toStringParameterList(
				UserId userId, AppId appId, Map<String, String> map) {
			this.toStringParameterListCalled = true;
			return null;
		}

		@Override
		public Map<String, String> toStringParameterList(Map<String, String> map) {
			return null;
		}

		@Override
		public Map<String, Object> toObjectParameterList(Map<String, String> map) {
			this.toObjectParameterListCalled = true;
			return null;
		}

		@Override
		public <T> T queryForObject(String sql, Map<String, Object> map,
				Class<T> requiredClassType) {
			this.queryForObjectCalled = true;
			return (T)new Long(0);
		}

		@Override
		String getGetCountSql(Map<String, Object> parameterList) {
			this.getGetCountSqlCalled = true;
			return null;
		}
	}

	public static class Testing2 extends BaseGetAppModelCount {
		public Map<String, String> toStringParameterList;

		@Override
		public Map<String, String> toStringParameterList(Map<String, String> map) {
			return this.toStringParameterList;
		}

		@Override
		public ManagerSql getManagerSql() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public static class Execute {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map = new HashMap<>();
		Map<String, String> strList = new HashMap<>();
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void execute() {
			Testing1 x = new Testing1();

			x.execute(userId, appId, map);

			assertThat(x.toStringParameterListCalled, is(true));
			assertThat(x.toObjectParameterListCalled, is(true));
			assertThat(x.getGetCountSqlCalled, is(true));
			assertThat(x.queryForObjectCalled, is(true));
		}
	}

	public static class ToStringParameterList {
		UserIdImpl userId = new UserIdImpl("suzuki");
		AppIdImpl appId = new AppIdImpl("jiro");
		Map<String, String> map;

		@Before
		public void before() {
			map = new HashMap<>();
		}

		/**
		 * パラメータなしの場合。
		 */
		@Test
		public void none() {
			Testing2 x = new Testing2();
			x.toStringParameterList = map;
			Map<String, String> result = x.toStringParameterList(userId, appId, map);

			assertThat(result.size(), is(2));
			assertThat(result.containsKey("userId"), is(true));
			assertThat(result.containsKey("appId"), is(true));
			assertThat(result.get("userId"), is("suzuki"));
			assertThat(result.get("appId"), is("jiro"));
		}

		/**
		 * パラメータありの場合。
		 * name
		 */
		@Test
		public void hasName() {
			map.put("name", "hoge");
			Testing2 x = new Testing2();
			x.toStringParameterList = map;
			Map<String, String> result = x.toStringParameterList(userId, appId, map);

			assertThat(result.size(), is(3));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("hoge"));
		}
	}

	public static class GetGetCountSql {
		BaseGetAppModelCount x = new GetQueryCountImpl();
		ManagerSql sql = mock(ManagerSql.class);
		Map<String, Object> objList = new HashMap<>();

		@Test
		public void getGetCountSqlCalled() {
			ReflectionTestUtils.setField(x, "managerSql", sql);

			x.getGetCountSql(objList);

			verify(sql).getGetCountSql(objList);
		}
	}
}
