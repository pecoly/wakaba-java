package net.jp.yamabuki.service.manager.message.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.model.resource.MessageMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class AddMessageImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static AddAppModel<Message> addModel;
		protected static GetAppModelList<Message> getModelList;
		protected static MessageBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "hoge_fuga";
			String locale = "ja";
			String message = "あいうえお";
			modelBuilder = new MessageBuilder(id, userId, appId,
					code, locale, message, "20010203102030");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/message/sql/postgresql/AddMessageImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);

			removeAllModels();

			assertThat(countMessages(), is(0));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
			addModel = null;
			getModelList = null;
			modelBuilder = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM MESSAGE_LIST");
		}

		static int countMessages() {
			return new JdbcTemplate(dataSource).queryForObject(
					"SELECT COUNT(*) FROM MESSAGE_LIST",
					Integer.class);
		}

		/**
		 * メッセージを追加できること
		 *
		 */
		public static class Add extends Base {
			static Message message2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				addModel.execute(modelBuilder.build());
				Map<String, String> map = new HashMap<>();
				List<Message> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				message2 = list.get(0);
			}

			@Test
			public void equals() {
				Message obj = new MessageBuilder(modelBuilder)
						.withId(message2.getId())
						.build();
				assertThat(message2, MessageMatcher.matches(obj));
			}
		}

		/**
		 * 存在しているメッセージをさらに追加した場合、例外が発生すること
		 *
		 */
		public static class Add_Exists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelAlreadyExistsException.class);
				thrown.expectMessage(WakabaErrorCode.MESSAGE_ALREADY_EXISTS.getNumber()
						+ " Message already exists. { code : hoge_fuga, localeId : ja }");
				addModel.execute(modelBuilder.build());
				addModel.execute(modelBuilder.build());
			}
		}
	}
}
