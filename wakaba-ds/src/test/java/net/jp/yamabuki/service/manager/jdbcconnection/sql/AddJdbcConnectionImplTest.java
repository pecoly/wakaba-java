package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.JdbcConnectionBuilder;
import net.jp.yamabuki.model.converter.StringConverter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class AddJdbcConnectionImplTest {
	public static class PostConstruct {
		StringConverter stringConverter = new StringConverter();

		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			AddJdbcConnectionImpl x = new AddJdbcConnectionImpl();
			AddJdbcConnectionImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", stringConverter);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ThrowEx {
		@Rule
		public ExpectedException thrown = ExpectedException.none();

		JdbcConnection model;

		@Before
		public void before() {
			JdbcConnectionBuilder builder = new JdbcConnectionBuilder(
					null, "suzuki", "jiro",
					"hoge", "fuga", "baru", "foo", "bar", "2000010203112233");
			model = builder.build();
		}

		@Test
		public void throwEx() {
			thrown.expect(ModelAlreadyExistsException.class);
			thrown.expectMessage(WakabaErrorCode.JDBC_CONNECTION_ALREADY_EXISTS.getNumber()
					+ " JDBC connection already exists. name : hoge");

			AddJdbcConnectionImpl x = new AddJdbcConnectionImpl();
			x.throwEx(model);
		}
	}
}
