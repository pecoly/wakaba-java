package net.jp.yamabuki.service.manager.update.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.datasource.UpdateBuilder;
import net.jp.yamabuki.model.datasource.UpdateMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.UpdateAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class UpdateUpdateImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static DataSource dataSource;
		protected static String modelId;
		protected static AddAppModel<Update> addModel;
		protected static UpdateAppModel<Update> updateModel;
		protected static GetAppModelList<Update> getModelList;
		protected static UpdateBuilder modelBuilder;
		protected static Update model1;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String type = "query";
			String name = "abc";
			String xml = "def";
			modelBuilder = new UpdateBuilder(id, userId, appId,
					type, name, xml, "20101112030405");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/update/sql/postgresql/UpdateUpdateImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			getModelList = context.getBean(GetAppModelList.class);
			addModel = context.getBean(AddAppModel.class);
			updateModel = context.getBean(UpdateAppModel.class);

			removeAllModels();

			addModel.execute(modelBuilder.build());

			Map<String, String> map = new HashMap<>();
			List<Update> list = getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map);
			assertThat(list.size(), is(1));

			model1 = list.get(0);
			modelId = list.get(0).getId().getValue();
			assertThat(modelId, is(not(nullValue())));

			System.out.println("last modified : " + DateTimeUtils.toStringYYYYMMDDHHMISS(
					model1.getLastModified()));
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM UPDATE_LIST");
		}

		public static class UpdateModel extends Base {
			static Update model2;

			@BeforeClass
			public static void beforeClass() {
				Base.beforeClass();

				for (Update x : getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						new HashMap<String, String>())) {
					System.out.println(x.getLastModified());
				}

				JdbcTemplate jdbc = context.getBean(JdbcTemplate.class);
				System.out.println("last  = " +
					jdbc.queryForObject("SELECT TO_CHAR(LAST_MODIFIED, 'YYYYMMDDHH24MISSTZ') FROM UPDATE_LIST",
							String.class));

				updateModel.execute(new UpdateBuilder(modelBuilder)
					.withId(modelId)
					.withName("ABC")
					.withXml("DEF")
					.withLastModified(DateTimeUtils.toStringYYYYMMDDHHMISS(
							model1.getLastModified()))
					.build());
				Map<String, String> map = new HashMap<>();
				List<Update> list = getModelList.execute(
						new UserIdImpl("suzuki"),
						new AppIdImpl("jiro"),
						new PageRequest(0, 10),
						map);
				assertThat(list.size(), is(1));

				model2 = list.get(0);
			}

			@Test
			public void equals() {
				Update obj = new UpdateBuilder(modelBuilder)
						.withId(model2.getId())
						.withName("ABC")
						.withXml("DEF")
						.build();
				assertThat(model2, UpdateMatcher.matches(obj));
			}
		}

		public static class Update_NotExists extends Base {
			@Rule
			public ExpectedException thrown = ExpectedException.none();

			@Test
			public void throwsException() {
				thrown.expect(ModelNotFoundException.class);
				thrown.expectMessage(WakabaErrorCode.UPDATE_NOT_FOUND.getNumber()
						+ " Update not found. id : 9999");
				updateModel.execute(new UpdateBuilder(modelBuilder)
					.withId("9999")
					.build());
			}
		}
	}
}
