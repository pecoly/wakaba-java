package net.jp.yamabuki.service.manager.css.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.resource.CssBuilder;
import net.jp.yamabuki.model.resource.CssMatcher;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetCssByKeyImplTest {
	public static class Base {

		protected static Css model;
		protected static String modelId;
		protected static ApplicationContext context;
		protected static GetAppModelList<Css> getModelList;
		protected static GetModelByKey<Css> getModelByKey;
		protected static AddAppModel<Css> addModel;
		protected static DataSource dataSource;
		protected static CssBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "suzuki";
			modelBuilder = new CssBuilder(id, userId, appId,
					name, content, "20001011121314");

			model = modelBuilder.build();

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/css/sql/postgresql/GetCssByKeyImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelByKey = context.getBean(GetModelByKey.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();

			addModel.execute(model);

			Map<String, String> map = new HashMap<>();
			for (Css x : getModelList.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					new PageRequest(0, 10),
					map)) {
				modelId = x.getId().getValue();
			}

			assertThat(modelId, notNullValue());
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM CSS_LIST");
		}
	}

	public static class Exists extends Base {
		protected static Css model2;

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
			Css key = new CssBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			model2 = getModelByKey.execute(key);
			assertThat(model2, is(notNullValue()));
		}

		@Test
		public void equals() {
			Css obj = new CssBuilder(modelBuilder)
					.withId(modelId)
					.build();

			assertThat(model2, CssMatcher.matches(obj));
		}
	}

	public static class NotExists extends Base {
		@Test
		public void notEqualUserId() {
			Css key = new CssBuilder(modelBuilder)
					.withUserId("hoge")
					.withAppId("jiro")
					.build();

			assertThat(key.getId().getValue(), is(nullValue()));

			Css appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualAppId() {
			Css key = new CssBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("hoge")
					.build();

			Css appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}

		@Test
		public void notEqualName() {
			Css key = new CssBuilder(modelBuilder)
					.withUserId("suzuki")
					.withAppId("jiro")
					.withName("hoge")
					.build();

			Css appUser = getModelByKey.execute(key);
			assertThat(appUser, nullValue());
		}
	}
}
