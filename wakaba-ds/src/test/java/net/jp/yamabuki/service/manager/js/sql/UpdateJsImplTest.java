package net.jp.yamabuki.service.manager.js.sql;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.model.resource.JsBuilder;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UpdateJsImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			UpdateJsImpl x = new UpdateJsImpl();
			UpdateJsImpl spy = spy(x);
			ReflectionTestUtils.setField(spy, "stringConverter", new StringConverter());
			spy.postConstruct();

			verify(spy).initialize((List)any());
		}
	}
}
