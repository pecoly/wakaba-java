package net.jp.yamabuki.service.manager.appuser.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetAppUserListImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelList<AppUser> getModelList;
		protected static AddAppModel<AppUser> addModel;
		protected static DataSource dataSource;
		protected static AppUserBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String loginId = "yamada";
			String password = "taro";
			String main = "main";
			String sub = "sub";
			String userId = "suzuki";
			String appId = "jiro";
			String lastModified = "20001011121314";
			modelBuilder = new AppUserBuilder(id, loginId, password,
					main, sub, userId, appId, lastModified);

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/appuser/sql/postgresql/appUser.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM USER_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("bbc").build());
		}

		@Test
		public void list() {
			List<AppUser> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getLoginId(), is("aaa"));
			assertThat(list.get(1).getLoginId(), is("abc"));
			assertThat(list.get(2).getLoginId(), is("bbc"));
		}
	}

	public static class Limit extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withLoginId("bbc").build());
		}

		@Test
		public void limitIs2() {
			List<AppUser> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 2), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getLoginId(), is("aaa"));
			assertThat(list.get(1).getLoginId(), is("abc"));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@Before
		public void setUp() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void userId_appId() {
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("bbc").build());

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withLoginId("aaa").build());

			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withLoginId("aaa").build());

			List<AppUser> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			for (AppUser x : list) {
				assertThat(x.getUserId().getValue(), is("u1"));
				assertThat(x.getAppId().getValue(), is("a1"));
			}
		}

		@Test
		public void loginId() {
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("aaa").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("abc").build());
			addModel.execute(new AppUserBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withLoginId("bbc").build());

			map.put("loginId", "bc");
			List<AppUser> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getLoginId(), is("abc"));
			assertThat(list.get(1).getLoginId(), is("bbc"));
		}
	}
}
