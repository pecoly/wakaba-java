package net.jp.yamabuki.service.manager.js.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.model.resource.JsBuilder;
import net.jp.yamabuki.model.resource.JsMatcher;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.junit.*;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetJsByIdImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetJsByIdImpl x = new GetJsByIdImpl();
			GetJsByIdImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}
}
