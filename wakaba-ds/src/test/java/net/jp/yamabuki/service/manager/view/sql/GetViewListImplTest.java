package net.jp.yamabuki.service.manager.view.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

import org.junit.*;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

public class GetViewListImplTest {
	public static class PostConstruct {
		/**
		 * PostConstructでinitializeが呼び出されること。
		 *
		 */
		@Test
		public void initializeCalled() {
			GetViewListImpl x = new GetViewListImpl();
			GetViewListImpl spy = spy(x);
			spy.postConstruct();
			verify(spy).initialize((List)any());
		}
	}

	public static class ToStringParameterList {
		Map<String, String> map = new HashMap<>();

		SqlUtils utils = mock(SqlUtils.class);
		ManagerSql sql = mock(ManagerSql.class);

		GetViewListImpl createGetList(ManagerSql sql, SqlUtils utils) {
			GetViewListImpl x = new GetViewListImpl();
			ReflectionTestUtils.setField(x, "managerSql", sql);
			ReflectionTestUtils.setField(x, "sqlUtils", utils);
			return x;
		}

		@Test
		public void none() {
			GetViewListImpl x = new GetViewListImpl();
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}

		@Test
		public void hasName() {
			map.put("name", "hoge");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetViewListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(1));
			assertThat(result.containsKey("name"), is(true));
			assertThat(result.get("name"), is("fuga"));
		}

		@Test
		public void empty() {
			map.put("code", "");
			when(utils.toLikeKeyword((String)any())).thenReturn("fuga");
			GetViewListImpl x = createGetList(sql, utils);
			Map<String, String> result = x.toStringParameterList(map);

			assertThat(result.size(), is(0));
		}
	}
}
