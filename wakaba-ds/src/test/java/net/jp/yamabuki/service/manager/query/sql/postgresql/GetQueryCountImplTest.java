package net.jp.yamabuki.service.manager.query.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.AppUserBuilder;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryBuilder;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.resource.MessageBuilder;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelCount;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetQueryCountImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelCount getModelCount;
		protected static AddAppModel<Query> addModel;
		protected static DataSource dataSource;
		protected static QueryBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String code = "hoge.fuga";
			String locale = "ja";
			String query = "あいうえお";
			modelBuilder = new QueryBuilder(id, userId, appId,
					code, locale, query, "20101112030405");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/query/sql/postgresql/GetQueryCountImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelCount = context.getBean(GetAppModelCount.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM QUERY_LIST");
		}
	}

	public static class AllUsers extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void list() {
			long count = getModelCount.execute(
					new UserIdImpl("suzuki"),
					new AppIdImpl("jiro"),
					map);
			assertThat(count, is(3L));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void userId_appId() {

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(3L));
		}

		@Test
		public void name() {

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new QueryBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			map.put("name", "bc");
			long count = getModelCount.execute(
					new UserIdImpl("u1"),
					new AppIdImpl("a1"),
					map);
			assertThat(count, is(2L));
		}
	}
}
