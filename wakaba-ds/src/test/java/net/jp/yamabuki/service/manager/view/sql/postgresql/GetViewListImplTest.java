package net.jp.yamabuki.service.manager.view.sql.postgresql;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.jp.yamabuki.exception.QueryException;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.ViewBuilder;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetAppModelList;
import net.jp.yamabuki.service.manager.message.sql.GetMessageListImpl;

import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;

public class GetViewListImplTest {
	public static class Base {

		protected static ApplicationContext context;
		protected static GetAppModelList<View> getModelList;
		protected static AddAppModel<View> addModel;
		protected static DataSource dataSource;
		protected static ViewBuilder modelBuilder;

		@BeforeClass
		public static void beforeClass() {
			String id = null;
			String userId = "suzuki";
			String appId = "jiro";
			String name = "main";
			String content = "abc";
			modelBuilder = new ViewBuilder(id, userId, appId,
					name, content, "20111213010203");

			if (context == null) {
				context = new ClassPathXmlApplicationContext(
						"service/manager/view/sql/postgresql/GetViewListImplTest.xml");
			}

			dataSource = context.getBean(DataSource.class);
			addModel = context.getBean(AddAppModel.class);
			getModelList = context.getBean(GetAppModelList.class);

			removeAllModels();
		}

		@AfterClass
		public static void afterClass() {
			removeAllModels();
			context = null;
			dataSource = null;
		}

		static void removeAllModels() {
			new JdbcTemplate(dataSource).update("DELETE FROM VIEW_LIST");
		}
	}

	public static class AllModels extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void list() {
			List<View> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
			assertThat(list.get(2).getName(), is("bbc"));
		}
	}

	public static class Limit extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();

			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("aaa").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("abc").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withName("bbc").build());
		}

		@Test
		public void limitIs2() {
			List<View> list = getModelList.execute(
					new UserIdImpl("suzuki"), new AppIdImpl("jiro"),
					new PageRequest(0, 2), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("aaa"));
			assertThat(list.get(1).getName(), is("abc"));
		}
	}

	public static class Find extends Base {
		Map<String, String> map = new HashMap<>();

		@BeforeClass
		public static void beforeClass() {
			Base.beforeClass();
		}

		@Before
		public void before() {
			map.clear();
		}

		@After
		public void after() {
			removeAllModels();
		}

		@Test
		public void by_userId_appId() {
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a2").withName("aaa").build());

			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u2").withAppId("a1").withName("aaa").build());

			List<View> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(3));
			for (View x : list) {
				assertThat(x.getUserId().getValue(), is("u1"));
				assertThat(x.getAppId().getValue(), is("a1"));
			}
		}

		@Test
		public void by_name() {
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("aaa").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("abc").build());
			addModel.execute(new ViewBuilder(modelBuilder)
					.withUserId("u1").withAppId("a1").withName("bbc").build());

			map.put("name", "bc");
			List<View> list = getModelList.execute(
					new UserIdImpl("u1"), new AppIdImpl("a1"),
					new PageRequest(0, 10), map);
			assertThat(list, is(not(nullValue())));
			assertThat(list.size(), is(2));
			assertThat(list.get(0).getName(), is("abc"));
			assertThat(list.get(1).getName(), is("bbc"));
		}
	}
}
