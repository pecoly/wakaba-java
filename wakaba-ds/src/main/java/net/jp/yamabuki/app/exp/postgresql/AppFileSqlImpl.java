package net.jp.yamabuki.app.exp.postgresql;

import org.springframework.stereotype.Component;

import net.jp.yamabuki.pkg.app.exp.AppFileSql;

/**
 * アプリケーションファイルを取り扱うSqlを管理するクラス。
 *
 */
@Component
public class AppFileSqlImpl implements AppFileSql {

	@Override
	public String getGetSql() {
		return "SELECT"
				+ " APP_NAME "
				+ " FROM APP_LIST"
				+ " WHERE USER_ID = :userId"
				+ " AND APP_ID = :appId";
	}

	@Override
	public String getAddSql() {
		return "INSERT INTO APP_LIST( "
				+ "USER_ID, APP_ID, APP_NAME "
				+ ") VALUES ( "
				+ ":userId, :appId, :appName"
				+ ")";
	}

	@Override
	public String getRemoveSql() {
		return "DELETE FROM APP_LIST"
				+ " WHERE USER_ID = :userId"
				+ " AND APP_ID = :appId";
	}
}
