package net.jp.yamabuki.model.sql.statement.select.postgresql;

import net.jp.yamabuki.model.sql.statement.FromStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.select.SelectStatement;

public class SelectSql extends net.jp.yamabuki.model.sql.statement.select.SelectSql {

	public SelectSql(SelectStatement select, FromStatement from, WhereStatement where) {
		super(select, from, where);
	}

	public SelectSql(SelectStatement select, FromStatement from, WhereStatement where,
			OrderByStatement orderBy) {
		super(select, from, where, orderBy);
	}

	public String withLimitOffset() {
		return this.toString()
				+ " LIMIT :limit OFFSET :offset";
	}
}
