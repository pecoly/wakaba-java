package net.jp.yamabuki.model.datasource;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.accessor.Accessor;

public final class QueryColumn {

	private final String name;

	private final Accessor accessor;

	public QueryColumn(String name, Accessor accessor) {
		Argument.isNotBlank(name, "name");
		Argument.isNotNull(accessor, "accessor");

		this.name = name;
		this.accessor = accessor;
	}

	public String getName() {
		return this.name;
	}

	public Accessor getAccessor() {
		return this.accessor;
	}
}
