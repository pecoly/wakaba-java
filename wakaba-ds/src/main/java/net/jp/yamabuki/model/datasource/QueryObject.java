package net.jp.yamabuki.model.datasource;

import java.util.List;
import java.util.Map;

public interface QueryObject {
	List<Map<String, Object>> execute(Map<String, String> map);
}