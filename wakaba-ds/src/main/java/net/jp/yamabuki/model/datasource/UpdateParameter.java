package net.jp.yamabuki.model.datasource;

import java.util.ArrayList;
import java.util.List;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.converter.Converter;
import net.jp.yamabuki.model.validator.Validator;

public class UpdateParameter {

	private final String name;

	private final Converter converter;

	private final boolean isRequired;

	private final Iterable<Validator> validatorList;

	/**
	 * コンストラクタ。
	 * @param name 名前
	 * @param converter 変換処理
	 * @param isRequired 必須有無
	 * @param validatorList チェック処理の一覧
	 */
	public UpdateParameter(String name, Converter converter,
			boolean isRequired, Iterable<Validator> validatorList) {
		Argument.isNotNull(name, "name");
		Argument.isNotNull(converter, "converter");
		Argument.isNotNull(validatorList, "validatorList");

		this.name = name;
		this.converter = converter;
		this.isRequired = isRequired;
		this.validatorList = validatorList;
	}

	/**
	 * コンストラクタ。
	 * @param name 名前
	 * @param converter 変換処理
	 * @param validatorList チェック処理の一覧
	 */
	public UpdateParameter(String name, Converter converter,
			Iterable<Validator> validatorList) {
		Argument.isNotNull(name, "name");
		Argument.isNotNull(converter, "converter");
		Argument.isNotNull(validatorList, "validatorList");

		this.name = name;
		this.converter = converter;
		this.isRequired = true;
		this.validatorList = validatorList;
	}

	public UpdateParameter(String name, Converter converter) {
		Argument.isNotNull(name, "name");
		Argument.isNotNull(converter, "converter");

		this.name = name;
		this.converter = converter;
		this.isRequired = true;
		this.validatorList = new ArrayList<>();
	}

	public String getName() {
		return this.name;
	}

	public Converter getConverter() {
		return this.converter;
	}

	public boolean isRequired() {
		return this.isRequired;
	}

	public String validate(Object value) {
		for (Validator x : this.validatorList) {
			String result = x.validate(value);
			if (result != null) {
				return result;
			}
		}

		return null;
	}
}
