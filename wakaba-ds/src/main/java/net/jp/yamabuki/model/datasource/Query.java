package net.jp.yamabuki.model.datasource;

import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;

/**
 * クエリを表すクラス。
 * ユーザーごと、アプリケーションごとの情報です。
 * このクラスはイミュータブルです。
 *
 */
public final class Query extends AppModel {

	private final String type;

	private final String name;

	private final String xml;

	public static final String KEY_TYPE = "type";

	public static final String KEY_NAME = "name";

	public static final String KEY_XML = "xml";

	/**
	 * コンストラクタ。
	 * @param id クエリId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param type 種別
	 * @param name 名前
	 * @param xml クエリ内容
	 * @param lastModified 最終更新日時
	 */
	public Query(ModelId id, UserId userId, AppId appId,
			String type, String name, String xml,
			DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(type, "type");
		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(xml, "xml");

		this.type = type;
		this.name = name;
		this.xml = xml;
	}

	public Query(ModelId id, UserId userId, AppId appId,
			String type, String name, String xml) {
		super(id, userId, appId);

		Argument.isNotBlank(type, "type");
		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(xml, "xml");

		this.type = type;
		this.name = name;
		this.xml = xml;
	}

	public Query(UserId userId, AppId appId,
			String type, String name, String xml) {
		super(userId, appId);

		Argument.isNotBlank(type, "type");
		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(xml, "xml");

		this.type = type;
		this.name = name;
		this.xml = xml;
	}

	public String getType() {
		return this.type;
	}

	/**
	 * 名前を取得します。
	 * @return 名前
	 */
	public String getName() {
		return this.name;
	}

	public String getXml() {
		return this.xml;
	}

	@Override
	public Map<String, String> toStringMap() {
		Map<String, String> map = super.toStringMap();

		map.put(KEY_TYPE, this.type);
		map.put(KEY_NAME, this.name);
		map.put(KEY_XML, this.xml);

		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "type : %s, name : %s, xml : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.type, this.name, this.xml);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
