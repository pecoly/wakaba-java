package net.jp.yamabuki.model.datasource;

import java.util.Map;


public interface UpdateObject {
	abstract int execute(Map<String, String> map);
}
