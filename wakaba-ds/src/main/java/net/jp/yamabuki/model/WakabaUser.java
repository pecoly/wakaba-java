package net.jp.yamabuki.model;

import org.springframework.security.core.userdetails.User;

public class WakabaUser {

	public WakabaUser(User loginUser, String dbUserName, String dbPassword) {
		this.loginUser = loginUser;
		this.dbUserName = dbUserName;
		this.dbPassword = dbPassword;
	}

	public User getLoginUser() {
		return this.loginUser;
	}

	public String getDbUserName() {
		return this.dbUserName;
	}

	public String getDbPassword() {
		return this.dbPassword;
	}

	private String dbUserName;

	private String dbPassword;

	private User loginUser;
}
