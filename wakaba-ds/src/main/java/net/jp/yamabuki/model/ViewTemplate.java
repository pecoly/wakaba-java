package net.jp.yamabuki.model;

import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;

/**
 * ビューテンプレートクラス。
 * ユーザーごと、アプリケーションごとの情報です。
 * このクラスはイミュターブルです。
 *
 */
public final class ViewTemplate extends AppModel {

	private final String name;

	private final String content;

	public static final String KEY_NAME = "name";

	public static final String KEY_CONTENT = "content";

	/**
	 * コンストラクタ。
	 * @param id ビューテンプレートId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param content 内容
	 * @param lastModified 最終更新日時
	 */
	public ViewTemplate(ModelId id, UserId userId, AppId appId,
			String name, String content, DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(name, "name");
		View.isViewName(name, "name");
		Argument.isNotBlank(content, "content");

		this.name = name;
		this.content = content;
	}

	/**
	 * コンストラクタ。
	 * @param id ビューテンプレートId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param content 内容
	 */
	public ViewTemplate(ModelId id, UserId userId, AppId appId,
			String name, String content) {
		super(id, userId, appId);

		Argument.isNotBlank(name, "name");
		View.isViewName(name, "name");
		Argument.isNotBlank(content, "content");

		this.name = name;
		this.content = content;
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param content 内容
	 */
	public ViewTemplate(UserId userId, AppId appId,
			String name, String content) {
		super(userId, appId);

		Argument.isNotBlank(name, "name");
		View.isViewName(name, "name");
		Argument.isNotBlank(content, "content");

		this.name = name;
		this.content = content;
	}

	/**
	 * 名前を取得します。
	 * @return 名前
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 内容を取得します。
	 * @return 内容
	 */
	public String getContent() {
		return this.content;
	}

	@Override
	public Map<String, String> toStringMap() {
		Map<String, String> map = super.toStringMap();

		map.put(KEY_NAME, this.name);
		map.put(KEY_CONTENT, this.content);

		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "name : %s, content : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.name, this.content);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
