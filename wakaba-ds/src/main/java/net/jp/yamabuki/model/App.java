package net.jp.yamabuki.model;

import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;

/**
 * アプリケーションクラス。
 * このクラスはイミュータブルです。
 *
 */
public final class App extends UserModel {

	private final AppId appId;

	private final String appName;

	public static final String KEY_APP_ID = "appId";

	public static final String KEY_APP_NAME = "appName";

	/**
	 * コンストラクタ。
	 * @param id アプリケーションId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param appName アプリケーション名
	 * @param lastModified 最終更新日時
	 */
	public App(ModelId id,
			UserId userId, AppId appId, String appName,
			DateTime lastModified) {
		super(id, userId, lastModified);

		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(appName, "appName");

		this.appId = appId;
		this.appName = appName;
	}

	/**
	 * コンストラクタ。
	 * @param id アプリケーションId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param appName アプリケーション名
	 */
	public App(ModelId id,
			UserId userId, AppId appId, String appName) {
		super(id, userId);

		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(appName, "appName");

		this.appId = appId;
		this.appName = appName;
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param appName アプリケーション名
	 */
	public App(UserId userId, AppId appId, String appName) {
		super(userId);

		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(appName, "appName");

		this.appId = appId;
		this.appName = appName;
	}

	/**
	 * アプリケーションIdを取得します。
	 * @return アプリケーションId
	 */
	public AppId getAppId() {
		return this.appId;
	}

	/**
	 * アプリケーション名を取得します。
	 * @return アプリケーション名
	 */
	public String getAppName() {
		return this.appName;
	}

	@Override
	public Map<String, String> toStringMap() {
		Map<String, String> map = super.toStringMap();

		map.put("appId", this.getAppId().getValue());
		map.put("appName", this.getAppName());

		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "name : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.appName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
