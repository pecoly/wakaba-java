package net.jp.yamabuki.model;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.util.DateTimeUtils;

/**
 * Jdbcコネクション設定を表すクラス。
 * データベースとの接続ではなく、接続設定を保持します。
 * ユーザーごと、アプリケーションごとの情報です。
 * このクラスはイミュータブルです。
 *
 */
public class JdbcConnection extends AppModel {
	private final String name;

	private final String driver;

	private final String url;

	private final String username;

	private final String password;

	public static final String KEY_NAME = "name";

	public static final String KEY_DRIVER = "driver";

	public static final String KEY_URL = "url";

	public static final String KEY_USERNAME = "username";

	public static final String KEY_PASSWORD = "password";

	/**
	 * コンストラクタ。
	 * @param id JdbcコネクションId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param driver Jdbcドライバー
	 * @param url JdbcUrl
	 * @param username Jdbcユーザー名
	 * @param password Jdbcパスワード
	 * @param lastModified 最終更新日時
	 */
	public JdbcConnection(ModelId id, UserId userId, AppId appId,
			String name, String driver, String url,
			String username, String password,
			DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(driver, "driver");
		Argument.isNotBlank(url, "url");
		Argument.isNotNull(username, "username");
		Argument.isNotNull(password, "password");
		Argument.isNotNull(lastModified, "lastModified");

		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
	}

	/**
	 * コンストラクタ。
	 * @param id JdbcコネクションId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param driver Jdbcドライバー
	 * @param url JdbcUrl
	 * @param username Jdbcユーザー名
	 * @param password Jdbcパスワード
	 */
	public JdbcConnection(ModelId id, UserId userId, AppId appId,
			String name, String driver, String url,
			String username, String password) {
		super(id, userId, appId);

		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(driver, "driver");
		Argument.isNotBlank(url, "url");
		Argument.isNotNull(username, "username");
		Argument.isNotNull(password, "password");

		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param driver Jdbcドライバー
	 * @param url JdbcUrl
	 * @param username Jdbcユーザー名
	 * @param password Jdbcパスワード
	 */
	public JdbcConnection(UserId userId, AppId appId,
			String name, String driver, String url,
			String username, String password) {
		super(userId, appId);

		Argument.isNotBlank(name, "name");
		Argument.isNotBlank(driver, "driver");
		Argument.isNotBlank(url, "url");
		Argument.isNotNull(username, "username");
		Argument.isNotNull(password, "password");

		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
	}

	/**
	 * コネクション名を取得します。
	 * @return コネクション名
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Jdbcドライバを取得します。
	 * @return Jdbcドライバ
	 */
	public String getDriver() {
		return this.driver;
	}

	/**
	 * JdbcUrlを取得します。
	 * @return JdbcUrl
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Jdbcユーザー名を取得します。
	 * @return Jdbcユーザー名
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Jdbcパスワードを取得します。
	 * @return Jdbcパスワード
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * インスタンスの文字列マップ表現を取得します。
	 * @return インスタンスの文字列マップ表現
	 */
	public Map<String, String> toStringMap() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.getId().getValue());
		map.put("userId", this.getUserId().getValue());
		map.put("appId", this.getAppId().getValue());
		map.put("name", this.name);
		map.put("driver", this.driver);
		map.put("url", this.url);
		map.put("username", this.username);
		map.put("password", this.password);
		map.put(KEY_LAST_MODIFIED,
				DateTimeUtils.toStringYYYYMMDDHHMISS(this.getLastModified()));
		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "name : %s, driver : %s, url : %s, username : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.name, this.driver, this.url, this.username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			JdbcConnection that = (JdbcConnection)obj;
			return this.getId().equals(that.getId())
					&& this.getUserId().equals(that.getUserId())
					&& this.getAppId().equals(that.getAppId())
					&& this.name.equals(that.name)
					&& this.driver.equals(that.driver)
					&& this.url.equals(that.url)
					&& this.username.equals(that.username)
					&& this.password.equals(that.password);
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
