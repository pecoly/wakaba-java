package net.jp.yamabuki.model;

import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;

/**
 * プラグインクラス。
 * このクラスはイミュターブルです。
 *
 */
public class Plugin extends AppModel {

	private String name;

	private String description;

	public static final String KEY_NAME = "name";

	public static final String KEY_DESCRIPTION = "description";

	/**
	 * コンストラクタ。
	 * @param id プラグインId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param description 概要
	 * @param lastModified 最終更新日時
	 */
	public Plugin(ModelId id, UserId userId, AppId appId,
			String name, String description, DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(description, "description");

		this.name = name;
		this.description = description;
	}

	/**
	 * コンストラクタ。
	 * @param id プラグインId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param description 概要
	 */
	public Plugin(ModelId id, UserId userId, AppId appId,
			String name, String description) {
		super(id, userId, appId);

		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(description, "description");

		this.name = name;
		this.description = description;
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @param description 概要
	 */
	public Plugin(UserId userId, AppId appId,
			String name, String description) {
		super(userId, appId);

		Argument.isNotBlank(name, "name");
		Argument.isVariableName(name, "name");
		Argument.isNotBlank(description, "description");

		this.name = name;
		this.description = description;
	}

	/**
	 * 名前を取得します。
	 * @return 名前
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 概要を取得します。
	 * @return 概要
	 */
	public String getDescription() {
		return this.description;
	}

	@Override
	public Map<String, String> toStringMap() {
		Map<String, String> map = super.toStringMap();

		map.put(KEY_ID, this.getId().getValue());
		map.put(KEY_USER_ID, this.getUserId().getValue());
		map.put(KEY_APP_ID, this.getAppId().getValue());
		map.put(KEY_NAME, this.name);
		map.put(KEY_DESCRIPTION, this.description);

		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "name : %s, description : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.name, this.description);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
