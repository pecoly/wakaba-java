package net.jp.yamabuki.model.mybatis;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.jp.yamabuki.check.Argument;

import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.DynamicContext;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.apache.ibatis.session.Configuration;

public class MyBatisNode {
	private Configuration configuration;
	private XNode node;
	public MyBatisNode(Configuration configuration, XNode node) {
		this.configuration = configuration;
		this.node = node;
	}

	public String getSql(Map<String, Object> parameterObject) {
		Argument.isNotNull(parameterObject, "parameterObject");

		XMLLanguageDriver langDriver = new XMLLanguageDriver();

		SqlSource sqlSource = langDriver.createSqlSource(
				this.configuration, this.node, Map.class);
		Field nameField = null;
		try {
			nameField = sqlSource.getClass().getDeclaredField("rootSqlNode");
			nameField.setAccessible(true);
			SqlNode rootSqlNode = (SqlNode)nameField.get(sqlSource);
			DynamicContext context = new DynamicContext(configuration, parameterObject);
			rootSqlNode.apply(context);
			System.out.println("sql : " + context.getSql());
			return this.replaceSql(context.getSql());

		} catch (NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	String replaceSql(String sql) {
		Argument.isNotNull(sql, "sql");

		// #{abc} -> :abc に置換
		Pattern p = Pattern.compile("#\\{([A-Za-z0-9]+)\\}");
		Matcher matcher = p.matcher(sql);
		return matcher.replaceAll(":$1");
	}
}
