package net.jp.yamabuki.model.resource;

import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;

/**
 * メッセージを表すクラス。
 * リクエストのロケールIdによって、ビューに表示する文字列を切り替えるために使用します。
 * ユーザーごと、アプリケーションごとの情報です。
 * このクラスはイミュータブルです。
 *
 */
public final class Message extends AppModel {

	private final String code;

	private final LocaleId localeId;

	private final String message;

	public static final String KEY_CODE = "code";

	public static final String KEY_LOCALE_ID = "localeId";

	public static final String KEY_MESSAGE = "message";

	/**
	 * コンストラクタ。
	 * @param id メッセージId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @param localeId ロケールId
	 * @param message メッセージ
	 * @param lastModified 最終更新日時
	 */
	public Message(ModelId id, UserId userId, AppId appId,
			String code, LocaleId localeId, String message,
			DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(code, "code");
		Argument.isVariableName(code, "code");
		Argument.isNotNull(localeId, "localeId");
		Argument.isNotBlank(message, "message");

		this.code = code;
		this.localeId = localeId;
		this.message = message;
	}

	/**
	 * コンストラクタ。
	 * @param id メッセージId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @param localeId ロケールId
	 * @param message メッセージ
	 */
	public Message(ModelId id, UserId userId, AppId appId,
			String code, LocaleId localeId, String message) {
		super(id, userId, appId);

		Argument.isNotBlank(code, "code");
		Argument.isVariableName(code, "code");
		Argument.isNotNull(localeId, "localeId");
		Argument.isNotBlank(message, "message");

		this.code = code;
		this.localeId = localeId;
		this.message = message;
	}

	/**
	 * コンストラクタ。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @param localeId ロケールId
	 * @param message メッセージ
	 */
	public Message(UserId userId, AppId appId,
			String code, LocaleId localeId, String message) {
		super(userId, appId);

		Argument.isNotBlank(code, "code");
		Argument.isVariableName(code, "code");
		Argument.isNotNull(localeId, "localeId");
		Argument.isNotBlank(message, "message");

		this.code = code;
		this.localeId = localeId;
		this.message = message;
	}

	/**
	 * メッセージコードを取得します。
	 * @return メッセージコード
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * ロケールIdを取得します。
	 * @return ロケールId
	 */
	public LocaleId getLocaleId() {
		return this.localeId;
	}

	/**
	 * メッセージを取得します。
	 * @return メッセージ
	 */
	public String getMessage() {
		return this.message;
	}

	@Override
	public Map<String, String> toStringMap() {
		Map<String, String> map = super.toStringMap();

		map.put("code", this.code);
		map.put("localeId", this.localeId.getValue());
		map.put("message", this.message);

		return map;
	}

	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "code : %s, localeId : %s, message : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.code, this.localeId.getValue(), this.message);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
