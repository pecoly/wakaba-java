package net.jp.yamabuki.model.datasource;

import java.util.HashMap;
import java.util.Map;

public class UpdateResult {
	public UpdateResult(int count) {
		this.result = true;
		this.count = count;
	}
	
	public UpdateResult(
			Map<String, String> validationResult) {
		this.validationResult = validationResult;
	}
	
	public UpdateResult(String error) {
		this.error = error;
	}
	
	public boolean getResult() {
		return this.result;
	}
	
	private boolean result;
	
	public int getCount() {
		return this.count;
	}
	
	private int count;
	
	public Map<String, String> getValidationResult() {
		return this.validationResult;
	}
	
	private Map<String, String> validationResult = new HashMap<>();
	
	public String getError() {
		return this.error;
	}
	
	private String error = "";
}
