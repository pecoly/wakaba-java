package net.jp.yamabuki.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.security.authority.AppIdAuthority;
import net.jp.yamabuki.security.authority.MainAuthority;
import net.jp.yamabuki.security.authority.SubAuthority;
import net.jp.yamabuki.security.authority.UserIdAuthority;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.StringUtils;

import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * アプリケーションユーザークラス。
 * このクラスはイミュータブルです。
 *
 */
public final class AppUser extends AppModel {

	private String loginId;

	private String loginPassword;

	private String mainAuthority;

	private String subAuthority;

	public static final String KEY_LOGIN_ID = "loginId";

	public static final String KEY_LOGIN_PASSWORD = "loginPassword";

	public static final String KEY_MAIN_AUTHORITY = "mainAuthority";

	public static final String KEY_SUB_AUTHORITY = "subAuthority";

	/**
	 * コンストラクタ。
	 * @param id アプリケーションユーザーId
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param loginId ログインId
	 * @param loginPassword ログインパスワード
	 * @param mainAuthority
	 * @param subAuthority
	 * @param lastModified 最終更新日時
	 */
	public AppUser(ModelId id, UserId userId, AppId appId,
			String loginId, String loginPassword,
			String mainAuthority, String subAuthority,
			DateTime lastModified) {
		super(id, userId, appId, lastModified);

		Argument.isNotBlank(loginId, "loginId");
		Argument.isNotBlank(loginPassword, "loginPassword");
		Argument.isNotBlank(mainAuthority, "mainAuthority");
		Argument.isNotNull(subAuthority, "subAuthority");
		Argument.isNotNull(lastModified, "lastModified");

		this.loginId = loginId;
		this.loginPassword = loginPassword;
		this.mainAuthority = mainAuthority;
		this.subAuthority = subAuthority;
	}

	public AppUser(ModelId id, UserId userId, AppId appId,
			String loginId, String loginPassword,
			String mainAuthority, String subAuthority) {
		super(id, userId, appId);

		Argument.isNotBlank(loginId, "loginId");
		Argument.isNotBlank(loginPassword, "loginPassword");
		Argument.isNotBlank(mainAuthority, "mainAuthority");
		Argument.isNotNull(subAuthority, "subAuthority");

		this.loginId = loginId;
		this.loginPassword = loginPassword;
		this.mainAuthority = mainAuthority;
		this.subAuthority = subAuthority;
	}

	public AppUser(UserId userId, AppId appId,
			String loginId, String loginPassword,
			String mainAuthority, String subAuthority) {
		super(userId, appId);

		Argument.isNotBlank(loginId, "loginId");
		Argument.isNotBlank(loginPassword, "loginPassword");
		Argument.isNotBlank(mainAuthority, "mainAuthority");
		Argument.isNotNull(subAuthority, "subAuthority");

		this.loginId = loginId;
		this.loginPassword = loginPassword;
		this.mainAuthority = mainAuthority;
		this.subAuthority = subAuthority;
	}

	public User getLoginUser() {
		List<GrantedAuthority> authorityList = new ArrayList<>();

		if (!StringUtils.isBlank(mainAuthority)) {
			authorityList.add(new MainAuthority(mainAuthority));
		}

		if (!StringUtils.isBlank(subAuthority)) {
			authorityList.add(new SubAuthority(subAuthority));
		}

		authorityList.add(new UserIdAuthority(this.getUserId().getValue()));

		authorityList.add(new AppIdAuthority(this.getAppId().getValue()));

		return new User(loginId, loginPassword, true, true, true, true,
				authorityList);
	}

	public String getLoginId() {
		return this.loginId;
	}

	public String getLoginPassword() {
		return this.loginPassword;
	}

	public String getMainAuthority() {
		return this.mainAuthority;
	}

	public String getSubAuthority() {
		return this.subAuthority;
	}

	/**
	 * インスタンスの文字列マップ表現を取得します。
	 * @return インスタンスの文字列マップ表現
	 */
	public Map<String, String> toStringMap() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.getId().getValue());
		map.put("userId", this.getUserId().getValue());
		map.put("appId", this.getAppId().getValue());
		map.put(KEY_LAST_MODIFIED, DateTimeUtils
				.toStringYYYYMMDDHHMISS(this.getLastModified()));
		map.put("loginId", this.loginId);
		map.put("loginPassword", this.loginPassword);
		map.put("mainAuthority", this.mainAuthority);
		map.put("subAuthority", this.subAuthority);
		return map;
	}


	@Override
	public String toString() {
		return String.format(
				"{ id : %s, userId : %s, appId : %s, "
				+ "loginId : %s, main : %s }",
				this.getId().getValue(), this.getUserId().getValue(),
				this.getAppId().getValue(),
				this.loginId, this.mainAuthority);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj == null) {
			return false;
		}
		else if (this.getClass() != obj.getClass()) {
			return false;
		}
		else {
			return this.toString().equals(obj.toString());
		}
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
