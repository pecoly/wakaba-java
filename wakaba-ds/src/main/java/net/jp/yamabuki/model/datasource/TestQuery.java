package net.jp.yamabuki.model.datasource;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;

public class TestQuery extends AppModel {
	private final String xml;

	public TestQuery(ModelId id, UserId userId, AppId appId,
			String xml) {
		super(id, userId, appId);

		Argument.isNotBlank(xml, "xml");

		this.xml = xml;
	}

	public TestQuery(UserId userId, AppId appId,
			String xml) {
		super(userId, appId);

		Argument.isNotBlank(xml, "xml");

		this.xml = xml;
	}

	public String getXml() {
		return this.xml;
	}

	public Map<String, String> toStringMap() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.getId().getValue());
		map.put("userId", this.getUserId().getValue());
		map.put("appId",  this.getAppId().getValue());
		map.put("xml", this.xml);
		return map;
	}
}
