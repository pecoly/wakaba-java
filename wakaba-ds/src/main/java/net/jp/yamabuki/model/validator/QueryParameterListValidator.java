package net.jp.yamabuki.model.validator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.converter.Converter;
import net.jp.yamabuki.model.datasource.QueryParameter;

/**
 * クエリパラメータチェッククラス。
 *
 */
public class QueryParameterListValidator implements MapValidator {

	private final Iterable<QueryParameter> queryParameterList;

	private Map<String, Object> objectParameterList = new HashMap<>();

	/**
	 * コンストラクタ。
	 * @param parameterList チェックするパラメータの一覧
	 */
	public QueryParameterListValidator(Iterable<QueryParameter> parameterList) {
		this.queryParameterList = parameterList;
	}

	@Override
	public String validate(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		this.objectParameterList.clear();

		for (QueryParameter x : this.queryParameterList) {
			String stringParameter = map.get(x.getName());
			Converter converter = x.getConverter();

			Object objectParameter = null;
			if (x.isRequired() || stringParameter != null) {
				objectParameter = converter.convert(stringParameter);

				// パラメータをチェック
				String error = x.validate(objectParameter);
				if (error != null) {
					return String.format("'%s' %s",
							x.getName(), error);
				}
			}

			this.objectParameterList.put(x.getName(), objectParameter);
		}

		// 入力パラメータにあり、出力パラメータにないものを追加
		for (Entry<String, String> kv : map.entrySet()) {
			if (!this.objectParameterList.containsKey(kv.getKey())) {
				this.objectParameterList.put(kv.getKey(), kv.getValue());
			}
		}

		return null;
	}

	/**
	 * チェックしてオブジェクトに変換したパラメータの一覧を取得します。
	 * @return オブジェクトに変換したパラメータの一覧
	 */
	public Map<String, Object> getObjectParameterList() {
		return Collections.unmodifiableMap(this.objectParameterList);
	}
}
