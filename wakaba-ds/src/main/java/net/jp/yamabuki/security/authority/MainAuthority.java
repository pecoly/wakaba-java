package net.jp.yamabuki.security.authority;

import org.springframework.security.core.GrantedAuthority;

@SuppressWarnings("serial")
public class MainAuthority implements GrantedAuthority {

    private final String role;

    public MainAuthority(String role) {
        this.role = role;
    }

    public String getAuthority() {
        return role;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj == null) {
        	return false;
        }
        else if (this.getClass() != obj.getClass()) {
        	return false;
        }
        else {
        	return this.toString().equals(obj.toString());
        }
    }

    @Override
    public int hashCode() {
        return this.role.hashCode();
    }

	@Override
    public String toString() {
        return this.role;
    }
}
