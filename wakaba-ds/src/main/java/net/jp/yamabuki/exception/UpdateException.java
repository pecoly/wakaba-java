package net.jp.yamabuki.exception;

/**
 * 更新例外クラス。
 *
 */
@SuppressWarnings("serial")
public class UpdateException extends WakabaException {
	public UpdateException(WakabaErrorCode errorCode,
			String message, Throwable t) {
		super(errorCode, message, t);
	}

	public UpdateException(WakabaErrorCode errorCode, Throwable t) {
		super(errorCode, t);
	}

	public UpdateException(WakabaErrorCode errorCode, String message) {
		super(errorCode, message);
	}
}
