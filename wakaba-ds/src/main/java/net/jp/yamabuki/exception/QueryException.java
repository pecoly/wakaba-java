package net.jp.yamabuki.exception;

@SuppressWarnings("serial")
public class QueryException extends WakabaException {
	public QueryException(WakabaErrorCode errorCode,
			String message, Throwable t) {
		super(errorCode, message, t);
	}

	public QueryException(WakabaErrorCode errorCode, Throwable t) {
		super(errorCode, t);
	}

	public QueryException(WakabaErrorCode errorCode, String message) {
		super(errorCode, message);
	}
}
