package net.jp.yamabuki.exception;

import net.jp.yamabuki.model.LocaleId;

@SuppressWarnings("serial")
public class NoSuchMessageException extends WakabaException {
	public NoSuchMessageException(String code, LocaleId localeId) {
		super(WakabaErrorCode.NO_SUCH_MESSAGE, String.format("No message found under code '%s' for localeId '%s'.",
				code, localeId.getValue()));
	}
}
