package net.jp.yamabuki.service.manager;

import org.springframework.security.core.userdetails.User;

public interface GetLoginUserByKey {
	User execute(String loginId);
}
