package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.sql.BaseUpdateAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Jdbcコネクションを更新する実装クラス。
 *
 */
@Component("updateJdbcConnection")
public class UpdateJdbcConnectionImpl extends BaseUpdateAppModel<JdbcConnection> {

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("jdbcConnectionSql")
	private ManagerSql managerSql;

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.JDBC_CONNECTION_NOT_FOUND,
				"id : " + id.getValue());
	}

	/**
	 * 初期化処理を行います。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("driver", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("url", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("username", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("password", stringConverter, new ArrayList<Validator>()),
				this.integerIdUpdateParameter,
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
