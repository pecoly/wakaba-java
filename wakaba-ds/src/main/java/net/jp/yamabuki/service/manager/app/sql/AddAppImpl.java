package net.jp.yamabuki.service.manager.app.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.AddUserModel;
import net.jp.yamabuki.service.manager.app.GetAppByKey;
import net.jp.yamabuki.service.manager.sql.BaseManagerUpdate;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アプリケーションを追加する実装クラス。
 *
 */
@Component("addApp")
public final class AddAppImpl extends BaseManagerUpdate
implements AddUserModel<App> {

	@Autowired
	private UpdateParameter loginPasswordUpdateParameter;

	@Autowired
	private GetAppByKey getAppByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public void execute(App app) {
		Argument.isNotNull(app, "app");

		if (this.getAppByKey.execute(app) != null) {
			this.throwEx(app);
		}

		Map<String, Object> parameterList
				= this.toObjectParameterList(app.toStringMap());

		try {
			this.update(
					this.appSql.getAddSql(parameterList),
					parameterList);
		}
		catch (DataIntegrityViolationException ex) {
			this.throwEx(app);
		}
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("userId", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("appId", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("appName", stringConverter, new ArrayList<Validator>()),
		}));
	}

	/**
	 * データ重複時の例外を投げます。
	 * @param app アプリケーション
	 */
	void throwEx(App app) {
		String m = String.format("id : %s", app.getAppId().getValue());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.APP_ALREADY_EXISTS, m);
	}
}
