package net.jp.yamabuki.service.manager.testupdate.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.datasource.TestUpdate;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.testupdate.GetTestUpdateByKey;

/**
 * キーをもとにテストアップデートを取得する実装クラス。
 *
 */
@Component
public class GetTestUpdateByKeyImpl extends BaseManagerQuery
implements GetTestUpdateByKey {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	@Qualifier("testUpdateSql")
	private ManagerSql testUpdateSql;

	@Override
	public TestUpdate execute(UserId userId, AppId appId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId, appId));

		return this.queryForObject(
				this.testUpdateSql.getGetByKeySql(parameterList),
				parameterList,
				new TestUpdateMapper());
	}

	@Override
	public TestUpdate execute(TestUpdate testUpdate) {
		Argument.isNotNull(testUpdate, "testUpdate");

		return this.execute(testUpdate.getUserId(),
				testUpdate.getAppId());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId, AppId appId) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		return map;
	}
}
