package net.jp.yamabuki.service.manager.appuser;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにアプリケーションユーザーを取得するインターフェース。
 *
 */
public interface GetAppUserByKey extends GetModelByKey<AppUser> {
	/**
	 * キーともとにアプリケーションユーザーを取得します。
	 * キーはログインIdです。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param loginId ログインId
	 * @return アプリケーションユーザー
	 */
	AppUser execute(UserId userId, AppId appId, String loginId);
}
