package net.jp.yamabuki.service.manager.sql;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.check.Field;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.validator.QueryParameterListValidator;

/**
 * レコードを取得する基底クラス。
 *
 */
public abstract class BaseManagerQuery {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	private QueryParameterListValidator validator1;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbc;

	/**
	 * 初期化します。
	 * @param queryParameterList パラメータ一覧
	 */
	public void initialize(List<QueryParameter> queryParameterList) {
		Field.isNull(this.validator1, "validator1");

		this.validator1 = new QueryParameterListValidator(
				queryParameterList);
	}

	/**
	 * 文字列パラメータをオブジェクトパラメータに変換します。
	 * @param map 文字列パラメータ
	 * @return オブジェクトパラメータ
	 */
	public Map<String, Object> toObjectParameterList(Map<String, String> map) {
		String error = this.validator1.validate(map);
		if (error != null) {
			throw new ValidationException(error);
		}

		return this.validator1.getObjectParameterList();
	}

	/**
	 * レコードを取得します。
	 * @param <T> レコードのデータ型
	 * @param sql Sql
	 * @param map パラメータ
	 * @param requiredClassType レコードのデータ型
	 * @return レコード
	 */
	public <T> T queryForObject(String sql, Map<String, Object> map,
			Class<T> requiredClassType) {
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(map, "map");
		Argument.isNotNull(requiredClassType, "requiredClassType");

		logger.info(sql);
		logger.info(map.toString());

		return this.namedParameterJdbc.queryForObject(sql, map, requiredClassType);
	}

	/**
	 * レコードを取得します。
	 * レコードが存在しない場合はnullを返却します。
	 * @param <T> レコードのデータ型
	 * @param sql Sql
	 * @param map パラメータ
	 * @param rowMapper マッパークラス。
	 * @return レコード
	 */
	public <T> T queryForObject(String sql, Map<String, Object> map,
			RowMapper<T> rowMapper) {
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(map, "map");
		Argument.isNotNull(rowMapper, "rowMapper");

		logger.info(sql);
		logger.info(map.toString());

		try {
			return this.namedParameterJdbc.queryForObject(sql, map, rowMapper);
		}
		catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/**
	 * レコードを取得します。
	 * @param <T> レコードのデータ型
	 * @param sql Sql
	 * @param map パラメータ
	 * @param rowMapper マッパークラス。
	 * @return レコード
	 */
	public <T> List<T> query(String sql, Map<String, Object> map,
			RowMapper<T> rowMapper) {
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(map, "map");
		Argument.isNotNull(rowMapper, "rowMapper");

		logger.info(sql);
		logger.info(map.toString());

		return this.namedParameterJdbc.query(sql, map, rowMapper);
	}
}
