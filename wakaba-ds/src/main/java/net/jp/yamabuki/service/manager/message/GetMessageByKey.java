package net.jp.yamabuki.service.manager.message;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにメッセージを取得するインターフェース。
 *
 */
public interface GetMessageByKey extends GetModelByKey<Message> {
	/**
	 * キーをもとにメッセージを取得します。
	 * キーはコードとロケールIdです。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code コード
	 * @param localeId ロケールId
	 * @return メッセージ
	 */
	Message execute(UserId userId, AppId appId, String code, LocaleId localeId);
}
