package net.jp.yamabuki.service.manager.view.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * ビューを取得するマッパークラス。
 *
 */
public class ViewMapper implements RowMapper<View> {
	@Override
	public View mapRow(ResultSet rs, int rowNumber) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String name = rs.getString(ViewSqlConstants.COL_NAME);
		String content = rs.getString(ViewSqlConstants.COL_CONTENT);
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(AppModelSqlConstants.COL_LAST_MODIFIED));

		return new View(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				name, content, lastModified);
	}
}
