package net.jp.yamabuki.service.manager.viewtemplate;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにビューテンプレートを取得するインターフェース。
 *
 */
public interface GetViewTemplateByKey extends GetModelByKey<ViewTemplate> {
	/**
	 * キーをもとにビューテンプレートを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return ビュー
	 */
	ViewTemplate execute(UserId userId, AppId appId, String name);
}
