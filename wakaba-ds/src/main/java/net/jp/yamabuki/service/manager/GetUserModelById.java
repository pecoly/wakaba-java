package net.jp.yamabuki.service.manager;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserModel;

/**
 * Idをもとにモデルを取得するインターフェース。
 *
 * @param <T>
 */
public interface GetUserModelById<T extends UserModel> {
	/**
	 * Idをもとにモデルを取得します。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param id Id
	 * @return モデル
	 */
	T execute(UserId userId, ModelId id);

	/**
	 * Idをもとにモデルを取得します。
	 * 見つからない場合はnullを返却します。
	 * @param t もとになるモデル
	 *
	 * @return モデル
	 */
	T execute(T t);
}
