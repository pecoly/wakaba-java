package net.jp.yamabuki.service.manager.sql;

import java.util.Map;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.service.manager.UpdateAppModel;

public abstract class BaseUpdateAppModel<T extends AppModel>
extends BaseManagerUpdate implements UpdateAppModel<T> {

	@Override
	public void execute(T t) {
		Argument.isNotNull(t, "t");

		Map<String, Object> objList
				= this.toObjectParameterList(t.toStringMap());

		int count = this.update(
				this.getUpdateSql(objList), objList);

		if (count == 0) {
			this.throwEx(t.getId());
		}
	}

	String getUpdateSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getUpdateSql(parameterList);
	}

	public abstract void throwEx(ModelId id);

	public abstract ManagerSql getManagerSql();
}
