package net.jp.yamabuki.service.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * Cssを取得するためのインターフェース。
 *
 */
public interface CssQuery {
	/**
	 * 初期化します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 */
	void initialize(UserId userId, AppId appId);

	/**
	 * タイムスタンプ付きの共通Cssの名前を取得します。
	 * @param name Cssの名前
	 * @return タイムスタンプ付きの共通Cssの名前
	 */
	String getCommonCssName(String name);

	/**
	 * タイムスタンプ付きのユーザーCssの名前を取得します。
	 * @param name Cssの名前
	 * @return タイムスタンプ付きのユーザーCssの名前
	 */
	String getUserCssName(String name);
}
