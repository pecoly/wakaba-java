package net.jp.yamabuki.service.manager;

import org.joda.time.DateTime;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserModel;

/**
 * Idをもとにモデルを削除するインターフェース。
 *
 * @param <T>
 */
public interface RemoveUserModel<T extends UserModel> {
	/**
	 * Idをもとにモデルを削除します。
	 * @param t モデル
	 */
	void execute(T t);

	/**
	 * Idをもとにモデルを削除します。
	 * @param userId ユーザーId
	 * @param id モデルId
	 * @param lastModified 最終更新日時
	 */
	void execute(UserId userId, ModelId id, DateTime lastModified);
}
