package net.jp.yamabuki.service.manager.viewtemplate.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.sql.BaseRemoveAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * ビューテンプレートを削除する実装クラス。
 *
 */
@Component("removeViewTemplate")
public class RemoveViewTemplateImpl extends BaseRemoveAppModel<ViewTemplate> {

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("viewTemplateSql")
	private ManagerSql managerSql;

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.VIEW_TEMPLATE_NOT_FOUND,
				"id : " + id.getValue());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.integerIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
