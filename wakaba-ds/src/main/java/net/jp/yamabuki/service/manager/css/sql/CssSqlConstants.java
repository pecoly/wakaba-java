package net.jp.yamabuki.service.manager.css.sql;

/**
 * CssのSqlの定数クラス。
 *
 */
public final class CssSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private CssSqlConstants() {
	}

	public static final String TABLE = "CSS_LIST";

	public static final String COL_NAME = "NAME";

	public static final String COL_CONTENT = "CONTENT";
}
