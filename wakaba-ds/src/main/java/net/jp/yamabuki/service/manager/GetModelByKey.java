package net.jp.yamabuki.service.manager;

/**
 * キーをもとにモデルを取得するインターフェース。
 *
 * @param <T>
 */
public interface GetModelByKey<T> {
	/**
	 * キーをもとにモデルを取得します。
	 * 見つからない場合はnullを返却します。
	 * @param t モデル
	 * @return モデル
	 */
	T execute(T t);
}
