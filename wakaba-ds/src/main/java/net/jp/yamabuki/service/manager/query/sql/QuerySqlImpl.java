package net.jp.yamabuki.service.manager.query.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.LikeExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * クエリを取り扱うSqlを管理するクラス。
 *
 */
@Component("querySql")
public class QuerySqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);
	}

	@Override
	protected String getTableName() {
		return "QUERY_LIST";
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_ID,
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				QuerySqlConstants.COL_TYPE,
				QuerySqlConstants.COL_NAME,
				QuerySqlConstants.COL_XML,
				AppModelSqlConstants.COL_LAST_MODIFIED
		};
	}

	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
						new EqualExpression(QuerySqlConstants.COL_NAME,
								new KeyExpression(Query.KEY_NAME)),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		List<BaseExpression> result = new ArrayList<>();

		if (map.containsKey(Query.KEY_NAME)) {
			result.add(new LikeExpression(QuerySqlConstants.COL_NAME,
					new KeyExpression(Query.KEY_NAME)));
		}

		return result;
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[] {
				new OrderByColumnStatement(QuerySqlConstants.COL_NAME),
		});
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				QuerySqlConstants.COL_TYPE,
				QuerySqlConstants.COL_NAME,
				QuerySqlConstants.COL_XML,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
				new KeyExpression(Query.KEY_TYPE),
				new KeyExpression(Query.KEY_NAME),
				new KeyExpression(Query.KEY_XML),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[] {
				new AssignStatement(QuerySqlConstants.COL_NAME,
						new KeyExpression(Query.KEY_NAME)),
				new AssignStatement(QuerySqlConstants.COL_XML,
						new KeyExpression(Query.KEY_XML)),
		};
	}
}
