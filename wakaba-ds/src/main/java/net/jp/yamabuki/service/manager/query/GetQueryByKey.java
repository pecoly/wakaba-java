package net.jp.yamabuki.service.manager.query;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにクエリを取得するインターフェース。
 *
 */
public interface GetQueryByKey extends GetModelByKey<Query> {
	/**
	 * キーをもとにクエリを取得します。
	 * キーは名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return クエリ
	 */
	Query execute(UserId userId, AppId appId, String name);
}
