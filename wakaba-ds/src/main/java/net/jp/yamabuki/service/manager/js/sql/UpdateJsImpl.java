package net.jp.yamabuki.service.manager.js.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.sql.BaseUpdateAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Jsを更新する実装クラス。
 *
 */
@Component("updateJs")
public class UpdateJsImpl extends BaseUpdateAppModel<Js> {

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("jsSql")
	private ManagerSql managerSql;

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.JS_NOT_FOUND,
				"id : " + id.getValue());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("content", stringConverter, new ArrayList<Validator>()),
				this.integerIdUpdateParameter,
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
