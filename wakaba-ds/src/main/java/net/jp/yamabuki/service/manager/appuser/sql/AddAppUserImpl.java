package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.appuser.GetAppUserByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * ユーザーを追加する実装クラス。
 *
 */
@Component("addAppUser")
public class AddAppUserImpl extends BaseAddAppModel<AppUser> {

	@Autowired
	private GetAppUserByKey getAppUserByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter loginPasswordUpdateParameter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("appUserSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("loginId", stringConverter, new ArrayList<Validator>()),
				this.loginPasswordUpdateParameter,
				new UpdateParameter("mainAuthority", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("subAuthority", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("userId", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("appId", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(AppUser appUser) {
		String m = String.format("loginId : %s", appUser.getLoginId());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.APP_USER_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<AppUser> getGetModelByKey() {
		return this.getAppUserByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
