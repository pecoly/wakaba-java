package net.jp.yamabuki.service.manager.plugin;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにプラグインを取得するインターフェース。
 *
 */
public interface GetPluginByKey extends GetModelByKey<Plugin> {
	/**
	 * キーをもとにプラグインを取得します。
	 * キーは名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return プラグイン
	 */
	Plugin execute(UserId userId, AppId appId, String name);
}
