package net.jp.yamabuki.service.manager.message.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Idをもとにメッセージ取得するクラス。
 *
 */
@Component("getMessageById")
public class GetMessageByIdImpl extends BaseGetAppModelById<Message> {

	private RowMapper<Message> mapper = new MessageMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("messageSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<Message> getMapper() {
		return this.mapper;
	}
}
