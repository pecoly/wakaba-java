package net.jp.yamabuki.service.manager.message.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.LikeExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * メッセージを取り扱うSqlを管理するクラス。
 *
 */
@Component("messageSql")
public class MessageSqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);
	}

	@Override
	protected String getTableName() {
		return "MESSAGE_LIST";
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_ID,
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				MessageSqlConstants.COL_CODE,
				MessageSqlConstants.COL_LOCALE_ID,
				MessageSqlConstants.COL_MESSAGE,
				AppModelSqlConstants.COL_LAST_MODIFIED
		};
	}

	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
						new EqualExpression(MessageSqlConstants.COL_CODE,
								new KeyExpression(Message.KEY_CODE)),
						new EqualExpression(MessageSqlConstants.COL_LOCALE_ID,
								new KeyExpression(Message.KEY_LOCALE_ID)),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		List<BaseExpression> result = new ArrayList<>();

		if (map.containsKey(Message.KEY_CODE)) {
			result.add(new LikeExpression(MessageSqlConstants.COL_CODE,
					new KeyExpression(Message.KEY_CODE)));
		}

		if (map.containsKey(Message.KEY_LOCALE_ID)) {
			result.add(new LikeExpression(MessageSqlConstants.COL_LOCALE_ID,
					new KeyExpression(Message.KEY_LOCALE_ID)));
		}

		if (map.containsKey(Message.KEY_MESSAGE)) {
			result.add(new LikeExpression(MessageSqlConstants.COL_MESSAGE,
					new KeyExpression(Message.KEY_MESSAGE)));
		}

		return result;
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[] {
				new OrderByColumnStatement(MessageSqlConstants.COL_CODE),
				new OrderByColumnStatement(MessageSqlConstants.COL_LOCALE_ID),
		});
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				MessageSqlConstants.COL_CODE,
				MessageSqlConstants.COL_LOCALE_ID,
				MessageSqlConstants.COL_MESSAGE,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
				new KeyExpression(Message.KEY_CODE),
				new KeyExpression(Message.KEY_LOCALE_ID),
				new KeyExpression(Message.KEY_MESSAGE),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[] {
				new AssignStatement(MessageSqlConstants.COL_CODE,
						new KeyExpression(Message.KEY_CODE)),
				new AssignStatement(MessageSqlConstants.COL_LOCALE_ID,
						new KeyExpression(Message.KEY_LOCALE_ID)),
				new AssignStatement(MessageSqlConstants.COL_MESSAGE,
						new KeyExpression(Message.KEY_MESSAGE)),
		};
	}
}
