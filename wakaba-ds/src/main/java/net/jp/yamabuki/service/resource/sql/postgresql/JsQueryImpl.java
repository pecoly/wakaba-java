package net.jp.yamabuki.service.resource.sql.postgresql;

import net.jp.yamabuki.service.resource.sql.BaseJsQuery;

/**
 * Cssを取得する実装クラス。
 *
 */
public class JsQueryImpl extends BaseJsQuery {
	@Override
	protected String getTimestampNameSql() {
		return "SELECT NAME"
				+ " || '?'"
				+ " || TO_CHAR(LAST_MODIFIED, 'YYYYMMDDHHMISS')"
				+ " FROM JS_LIST"
				+ " WHERE USER_ID = ?"
				+ " AND APP_ID = ?"
				+ " AND NAME = ?";
	}
}
