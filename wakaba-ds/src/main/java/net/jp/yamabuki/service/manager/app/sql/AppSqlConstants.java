package net.jp.yamabuki.service.manager.app.sql;


/**
 * アプリケーションのSqlの定数クラス。
 *
 */
public final class AppSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private AppSqlConstants() {
	}


	public static final String KEY_LIMIT = "limit";

	public static final String KEY_OFFSET = "offset";
}
