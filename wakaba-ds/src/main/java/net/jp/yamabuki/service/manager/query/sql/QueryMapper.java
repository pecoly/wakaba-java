package net.jp.yamabuki.service.manager.query.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * クエリを取得するマッパークラス。
 *
 */
public class QueryMapper implements RowMapper<Query> {

	@Override
	public Query mapRow(ResultSet rs, int rowNumber) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String type = rs.getString("QUERY_TYPE");
		String name = rs.getString("NAME");
		String xml = rs.getString("XML");
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(AppModelSqlConstants.COL_LAST_MODIFIED));

		return new Query(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				type, name, xml, lastModified);
	}
}
