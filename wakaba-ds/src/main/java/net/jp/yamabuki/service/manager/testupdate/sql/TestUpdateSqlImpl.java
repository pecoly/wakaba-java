package net.jp.yamabuki.service.manager.testupdate.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.StringValueExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.delete.DeleteSql;
import net.jp.yamabuki.model.sql.statement.delete.DeleteStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * テストアップデートを取り扱うSqlを管理するクラス。
 *
 */
@Component("testUpdateSql")
public class TestUpdateSqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	private String removeSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);

		this.removeSql = new DeleteSql(
				new DeleteStatement(this.getTableName()),
				new WhereStatement(new AndExpression(
						new BaseExpression[] {
								new EqualExpression(AppModelSqlConstants.COL_USER_ID,
										new KeyExpression(AppModel.KEY_USER_ID)),
								new EqualExpression(AppModelSqlConstants.COL_APP_ID,
										new KeyExpression(AppModel.KEY_APP_ID)),
						}))
		).toString();
	}

	@Override
	protected String getTableName() {
		return TestUpdateSqlConstants.TABLE;
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_ID,
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				TestUpdateSqlConstants.COL_XML,
		};
	}
	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		return new ArrayList<>();
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[0]);
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				TestUpdateSqlConstants.COL_TYPE,
				TestUpdateSqlConstants.COL_NAME,
				TestUpdateSqlConstants.COL_XML,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
				new StringValueExpression("'testupdate'"),
				new StringValueExpression("'testupdate'"),
				new KeyExpression("xml"),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[0];
	}

	@Override
	public String getRemoveSql(Map<String, Object> map) {
		return this.removeSql;
	}
}
