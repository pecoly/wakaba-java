package net.jp.yamabuki.service.manager.view.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.view.GetViewByKey;

/**
 * ビューを追加する実装クラス。
 *
 */
@Component("addView")
public class AddViewImpl extends BaseAddAppModel<View> {

	@Autowired
	private GetViewByKey getViewByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("viewSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter),
				new UpdateParameter("content", stringConverter),
		}));
	}

	@Override
	public void throwEx(View view) {
		String m = String.format("name : %s", view.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.VIEW_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<View> getGetModelByKey() {
		return this.getViewByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
