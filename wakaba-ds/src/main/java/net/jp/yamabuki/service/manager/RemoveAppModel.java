package net.jp.yamabuki.service.manager;

import org.joda.time.DateTime;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;

/**
 * Idをもとにモデルを削除するインターフェース。
 *
 * @param <T>
 */
public interface RemoveAppModel<T> {
	/**
	 * Idをもとにモデルを削除します。
	 * @param t モデル
	 */
	void execute(T t);

	/**
	 * Idをもとにモデルを削除します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id モデルId
	 * @param lastModified 最終更新日時
	 */
	void execute(UserId userId, AppId appId, ModelId id, DateTime lastModified);
}
