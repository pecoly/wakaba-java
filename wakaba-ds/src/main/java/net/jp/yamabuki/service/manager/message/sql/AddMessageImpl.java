package net.jp.yamabuki.service.manager.message.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.message.GetMessageByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * メッセージを追加する実装クラス。
 *
 */
@Component("addMessage")
public class AddMessageImpl extends BaseAddAppModel<Message> {

	@Autowired
	private GetMessageByKey getMessageByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("messageSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("code", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("localeId", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("message", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(Message message) {
		String m = String.format("{ code : %s, localeId : %s }",
				message.getCode(), message.getLocaleId().getValue());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.MESSAGE_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<Message> getGetModelByKey() {
		return this.getMessageByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
