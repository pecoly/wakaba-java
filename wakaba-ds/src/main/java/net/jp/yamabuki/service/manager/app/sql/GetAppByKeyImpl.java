package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.app.GetAppByKey;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * キーをもとにアプリケーションを取得する実装クラス。
 *
 */
@Component
public class GetAppByKeyImpl extends BaseManagerQuery
implements GetAppByKey {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public App execute(UserId userId, AppId appId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId, appId));

		return this.queryForObject(
				this.appSql.getGetByKeySql(parameterList),
				parameterList,
				new AppMapper());
	}

	@Override
	public App execute(App app) {
		return this.execute(app.getUserId(),
				app.getAppId());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId, AppId appId) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		return map;
	}
}
