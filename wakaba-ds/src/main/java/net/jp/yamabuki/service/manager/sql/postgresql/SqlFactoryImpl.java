package net.jp.yamabuki.service.manager.sql.postgresql;

import net.jp.yamabuki.model.sql.statement.FromStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.select.SelectStatement;
import net.jp.yamabuki.model.sql.statement.select.postgresql.SelectSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;
import net.jp.yamabuki.service.manager.sql.SqlUtils;

public class SqlFactoryImpl implements SqlFactory {
	public String createGetListSql(SelectStatement selectStatement,
			FromStatement fromStatement, WhereStatement whereStatement,
			OrderByStatement orderByStatement) {
		return new SelectSql(selectStatement, fromStatement,
				whereStatement, orderByStatement).withLimitOffset().toString();
	}

	public String createGetCountSql(SelectStatement selectStatement,
			FromStatement fromStatement, WhereStatement whereStatement) {
		return new SelectSql(selectStatement, fromStatement,
				whereStatement).toString();
	}
}
