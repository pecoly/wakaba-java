package net.jp.yamabuki.service.manager.plugin.sql;

/**
 * プラグインのSqlの定数クラス。
 *
 */
public final class PluginSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private PluginSqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_DESCRIPTION = "DESCRIPTION";
}
