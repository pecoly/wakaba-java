package net.jp.yamabuki.service.manager.message.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.message.GetMessageByKey;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * キーをもとにメッセージを取得するクラス。
 *
 */
@Component
public class GetMessageByKeyImpl extends BaseManagerQuery
implements GetMessageByKey {

	private MessageMapper mapper = new MessageMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	@Qualifier("messageSql")
	private ManagerSql managerSql;

	@Override
	public Message execute(UserId userId, AppId appId, String code, LocaleId localeId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(code, "code");
		Argument.isNotNull(localeId, "localeId");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, code, localeId);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetByKeySql(objList),
				objList, this.mapper);
	}

	@Override
	public Message execute(Message message) {
		return this.execute(message.getUserId(),
				message.getAppId(),
				message.getCode(),
				message.getLocaleId());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				new QueryParameter("code", stringConverter),
				new QueryParameter("localeId", stringConverter),
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @param localeId ロケールId
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId,
			String code, LocaleId localeId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(code, "code");
		Argument.isNotNull(localeId, "localeId");

		Map<String, String> result = new HashMap<>();
		result.put(AppModel.KEY_USER_ID, userId.getValue());
		result.put(AppModel.KEY_APP_ID, appId.getValue());
		result.put(Message.KEY_CODE, code);
		result.put(Message.KEY_LOCALE_ID, localeId.getValue());
		return result;
	}

	/**
	 * キーによるモデル取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return キーによるモデル取得用Sql
	 */
	String getGetByKeySql(Map<String, Object> parameterList) {
		return this.managerSql.getGetByKeySql(parameterList);
	}
}
