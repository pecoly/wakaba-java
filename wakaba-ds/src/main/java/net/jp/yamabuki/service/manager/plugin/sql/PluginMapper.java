package net.jp.yamabuki.service.manager.plugin.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.springframework.jdbc.core.RowMapper;

/**
 * プラグインを取得するマッパークラス。
 *
 */
public class PluginMapper implements RowMapper<Plugin> {

	@Override
	public Plugin mapRow(ResultSet rs, int rowNumber) throws SQLException {
		String userId = rs.getString("USER_ID");
		String appId = rs.getString("APP_ID");
		String id = rs.getString("ID");
		String name = rs.getString("NAME");
		String description = rs.getString("DESCRIPTION");

		return new Plugin(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				name, description);
	}
}
