package net.jp.yamabuki.service.manager.update;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにアップデートを取得するインターフェース。
 *
 */
public interface GetUpdateByKey extends GetModelByKey<Update> {
	/**
	 * キーをもとにアップデートを取得します。
	 * キーは名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return アップデート
	 */
	Update execute(UserId userId, AppId appId, String name);
}
