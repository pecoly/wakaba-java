package net.jp.yamabuki.service.manager.update.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Idをもとにアップデートを取得する実装クラス。
 *
 */
@Component("getUpdateById")
public class GetUpdateByIdImpl extends BaseGetAppModelById<Update> {

	private RowMapper<Update> mapper = new UpdateMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("updateSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<Update> getMapper() {
		return this.mapper;
	}
}
