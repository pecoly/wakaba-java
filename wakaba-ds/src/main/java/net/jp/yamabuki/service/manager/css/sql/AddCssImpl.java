package net.jp.yamabuki.service.manager.css.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.css.GetCssByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Cssを追加する実装クラス。
 *
 */
@Component("addCss")
public class AddCssImpl extends BaseAddAppModel<Css> {

	@Autowired
	private GetCssByKey getCssByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("cssSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("content", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(Css css) {
		String m = String.format("name : %s", css.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.CSS_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<Css> getGetModelByKey() {
		return this.getCssByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
