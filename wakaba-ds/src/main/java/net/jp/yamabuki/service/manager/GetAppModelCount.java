package net.jp.yamabuki.service.manager;

import java.util.Map;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * モデルの総件数を取得するインターフェース。
 *
 */
public interface GetAppModelCount {
	/**
	 * モデルの総件数を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param map 条件パラメータ
	 * @return モデルの総件数
	 */
	long execute(UserId userId, AppId appId, Map<String, String> map);
}
