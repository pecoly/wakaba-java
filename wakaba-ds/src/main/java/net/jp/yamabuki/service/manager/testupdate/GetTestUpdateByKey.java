package net.jp.yamabuki.service.manager.testupdate;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.TestUpdate;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * テストアップデートを取得するインターフェース。
 *
 */
public interface GetTestUpdateByKey extends GetModelByKey<TestUpdate> {
	/**
	 * テストアップデートを取得します。
	 * テストアップデートはアプリケーションに1つなのでキーはありません。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return テストアップデート
	 */
	TestUpdate execute(UserId userId, AppId appId);
}
