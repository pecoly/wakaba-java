package net.jp.yamabuki.service.manager.sql;

import java.util.Map;

/**
 * データ管理用のSqlを取得するインターフェース。
 *
 */
public interface ManagerSql {
	/**
	 * Idをもとにレコードを取得するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getGetByIdSql(Map<String, Object> map);

	/**
	 * キーをもとにレコードを取得するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getGetByKeySql(Map<String, Object> map);

	/**
	 * レコードの一覧を取得するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getGetListSql(Map<String, Object> map);

	/**
	 * レコードの件数を取得するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getGetCountSql(Map<String, Object> map);

	/**
	 * レコードを追加するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getAddSql(Map<String, Object> map);

	/**
	 * レコードを削除するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getRemoveSql(Map<String, Object> map);

	/**
	 * レコードを更新するSqlを取得します。
	 * @param map パラメータ
	 * @return Sql
	 */
	String getUpdateSql(Map<String, Object> map);
}
