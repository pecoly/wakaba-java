package net.jp.yamabuki.service.manager;

import java.util.List;

import net.jp.yamabuki.model.resource.Message;

public interface FindMessageList {
	List<Message> execute(String userId, String appId,
			int pageNumber, String code, String locale, String message);
}
