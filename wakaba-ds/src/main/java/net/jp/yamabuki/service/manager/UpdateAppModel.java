package net.jp.yamabuki.service.manager;

/**
 * Idをもとにモデルを更新するインターフェース。
 *
 * @param <T>
 */
public interface UpdateAppModel<T> {
	/**
	 * Idをもとにモデルを更新します。
	 *
	 * @param t モデル
	 */
	void execute(T t);
}
