package net.jp.yamabuki.service.manager.appuser.sql;

/**
 * アプリケーションユーザーのSqlの定数クラス。
 *
 */
public final class AppUserSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private AppUserSqlConstants() {
	}

	public static final String COL_ID = "ID";

	public static final String COL_USER_ID = "USER_ID";

	public static final String COL_APP_ID = "APP_ID";

	public static final String COL_LOGIN_ID = "LOGIN_ID";

	public static final String COL_LOGIN_PASSWORD = "LOGIN_PASSWORD";

	public static final String COL_MAIN_AUTHORITY = "MAIN_AUTHORITY";

	public static final String COL_SUB_AUTHORITY = "SUB_AUTHORITY";

	public static final String COL_LAST_MODIFIED = "LAST_MODIFIED";
}
