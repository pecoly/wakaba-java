package net.jp.yamabuki.service.manager.jdbcconnection;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにJdbcコネクションを取得するインターフェース。
 *
 */
public interface GetJdbcConnectionByKey extends GetModelByKey<JdbcConnection> {
	/**
	 * キーともとにJdbcコネクションを取得します。
	 * キーは名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return Jdbcコネクション
	 */
	JdbcConnection execute(UserId userId, AppId appId, String name);
}
