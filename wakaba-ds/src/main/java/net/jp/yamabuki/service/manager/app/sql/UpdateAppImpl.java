package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.UpdateUserModel;
import net.jp.yamabuki.service.manager.sql.BaseManagerUpdate;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アプリケーションを更新する実装クラス。
 *
 */
@Component("updateApp")
public class UpdateAppImpl extends BaseManagerUpdate
implements UpdateUserModel<App> {

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public void execute(App app) {
		Argument.isNotNull(app, "app");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						app.toStringMap());

		int count = this.update(
				this.appSql.getUpdateSql(parameterList),
				parameterList);

		if (count == 0) {
			throw new ModelNotFoundException(
					WakabaErrorCode.APP_NOT_FOUND,
					"id : " + app.getId());
		}
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.integerIdUpdateParameter,
				new UpdateParameter("userId", stringConverter),
				new UpdateParameter("appName", stringConverter),
				this.lastModifiedUpdateParameter,
		}));
	}
}
