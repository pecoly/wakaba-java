package net.jp.yamabuki.service.manager.view.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Idをもとにビューを取得する実装クラス。
 *
 */
@Component("getViewById")
public class GetViewByIdImpl extends BaseGetAppModelById<View> {

	private RowMapper<View> mapper = new ViewMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("viewSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<View> getMapper() {
		return this.mapper;
	}
}
