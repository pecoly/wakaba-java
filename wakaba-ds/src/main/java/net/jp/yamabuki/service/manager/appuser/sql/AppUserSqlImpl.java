package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.LikeExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * アプリケーションユーザーを取り扱うSqlを管理するクラス。
 *
 */
@Component("appUserSql")
public class AppUserSqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);
	}

	@Override
	protected String getTableName() {
		return "USER_LIST";
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				AppUserSqlConstants.COL_ID,
				AppUserSqlConstants.COL_USER_ID,
				AppUserSqlConstants.COL_APP_ID,
				AppUserSqlConstants.COL_LOGIN_ID,
				AppUserSqlConstants.COL_LOGIN_PASSWORD,
				AppUserSqlConstants.COL_MAIN_AUTHORITY,
				AppUserSqlConstants.COL_SUB_AUTHORITY,
				AppUserSqlConstants.COL_LAST_MODIFIED,
		};
	}
	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
						new EqualExpression(AppUserSqlConstants.COL_LOGIN_ID,
								new KeyExpression(AppUser.KEY_LOGIN_ID)),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		List<BaseExpression> result = new ArrayList<>();

		if (map.containsKey(AppUser.KEY_LOGIN_ID)) {
			result.add(new LikeExpression(AppUserSqlConstants.COL_LOGIN_ID,
					new KeyExpression(AppUser.KEY_LOGIN_ID)));
		}

		return result;
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[] {
				new OrderByColumnStatement(AppUserSqlConstants.COL_LOGIN_ID),
		});
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				AppUserSqlConstants.COL_LOGIN_ID,
				AppUserSqlConstants.COL_LOGIN_PASSWORD,
				AppUserSqlConstants.COL_MAIN_AUTHORITY,
				AppUserSqlConstants.COL_SUB_AUTHORITY,
				AppUserSqlConstants.COL_USER_ID,
				AppUserSqlConstants.COL_APP_ID,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppUser.KEY_LOGIN_ID),
				new KeyExpression(AppUser.KEY_LOGIN_PASSWORD),
				new KeyExpression(AppUser.KEY_MAIN_AUTHORITY),
				new KeyExpression(AppUser.KEY_SUB_AUTHORITY),
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[] {
				new AssignStatement(AppUserSqlConstants.COL_LOGIN_PASSWORD,
						new KeyExpression(AppUser.KEY_LOGIN_PASSWORD)),
				new AssignStatement(AppUserSqlConstants.COL_MAIN_AUTHORITY,
						new KeyExpression(AppUser.KEY_MAIN_AUTHORITY)),
				new AssignStatement(AppUserSqlConstants.COL_SUB_AUTHORITY,
						new KeyExpression(AppUser.KEY_SUB_AUTHORITY)),
		};
	}
}
