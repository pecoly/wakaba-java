package net.jp.yamabuki.service.manager.update.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Update;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.update.GetUpdateByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アップデートを追加する実装クラス。
 *
 */
@Component("addUpdate")
public class AddUpdateImpl extends BaseAddAppModel<Update> {

	@Autowired
	private GetUpdateByKey getUpdateByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("updateSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("xml", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(Update update) {
		String m = String.format("name : %s", update.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.UPDATE_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<Update> getGetModelByKey() {
		return this.getUpdateByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
