package net.jp.yamabuki.service.initializer;

public interface Initializer {
	boolean isInitialized();

	void setInitialized();

	void initialize();
}
