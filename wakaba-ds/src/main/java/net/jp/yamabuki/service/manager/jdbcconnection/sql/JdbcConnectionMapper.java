package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * Jdbcコネクションを取得するマッパークラス。
 *
 */
public class JdbcConnectionMapper implements RowMapper<JdbcConnection> {
	@Override
	public JdbcConnection mapRow(ResultSet rs, int number) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String name = rs.getString(JdbcConnectionSqlConstants.COL_NAME);
		String driver = rs.getString(JdbcConnectionSqlConstants.COL_DRIVER);
		String url = rs.getString(JdbcConnectionSqlConstants.COL_URL);
		String username = rs.getString(JdbcConnectionSqlConstants.COL_USERNAME);
		String password = rs.getString(JdbcConnectionSqlConstants.COL_PASSWORD);
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(AppModelSqlConstants.COL_LAST_MODIFIED));

		return new JdbcConnection(
				new ModelIdImpl(id), UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				name, driver, url, username, password, lastModified);
	}
}
