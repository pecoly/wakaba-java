package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.LikeExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * Jdbcコネクションを取り扱うSqlを管理するクラス。
 *
 */
@Component("jdbcConnectionSql")
public class JdbcConnectionSqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);
	}

	@Override
	protected String getTableName() {
		return "JDBC_CONNECTION_LIST";
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_ID,
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				JdbcConnectionSqlConstants.COL_NAME,
				JdbcConnectionSqlConstants.COL_DRIVER,
				JdbcConnectionSqlConstants.COL_URL,
				JdbcConnectionSqlConstants.COL_USERNAME,
				JdbcConnectionSqlConstants.COL_PASSWORD,
				AppModelSqlConstants.COL_LAST_MODIFIED,
		};
	}

	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
						new EqualExpression(JdbcConnectionSqlConstants.COL_NAME,
								new KeyExpression(JdbcConnection.KEY_NAME)),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		List<BaseExpression> result = new ArrayList<>();

		if (map.containsKey(JdbcConnection.KEY_NAME)) {
			result.add(new LikeExpression(JdbcConnectionSqlConstants.COL_NAME,
					new KeyExpression(JdbcConnection.KEY_NAME)));
		}

		if (map.containsKey(JdbcConnection.KEY_DRIVER)) {
			result.add(new LikeExpression(JdbcConnectionSqlConstants.COL_DRIVER,
					new KeyExpression(JdbcConnection.KEY_DRIVER)));
		}

		if (map.containsKey(JdbcConnection.KEY_URL)) {
			result.add(new LikeExpression(JdbcConnectionSqlConstants.COL_URL,
					new KeyExpression(JdbcConnection.KEY_URL)));
		}

		if (map.containsKey(JdbcConnection.KEY_USERNAME)) {
			result.add(new LikeExpression(JdbcConnectionSqlConstants.COL_USERNAME,
					new KeyExpression(JdbcConnection.KEY_USERNAME)));
		}

		return result;
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[] {
				new OrderByColumnStatement(JdbcConnectionSqlConstants.COL_NAME),
		});
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				AppModelSqlConstants.COL_USER_ID,
				AppModelSqlConstants.COL_APP_ID,
				JdbcConnectionSqlConstants.COL_NAME,
				JdbcConnectionSqlConstants.COL_DRIVER,
				JdbcConnectionSqlConstants.COL_URL,
				JdbcConnectionSqlConstants.COL_USERNAME,
				JdbcConnectionSqlConstants.COL_PASSWORD,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
				new KeyExpression(JdbcConnection.KEY_NAME),
				new KeyExpression(JdbcConnection.KEY_DRIVER),
				new KeyExpression(JdbcConnection.KEY_URL),
				new KeyExpression(JdbcConnection.KEY_USERNAME),
				new KeyExpression(JdbcConnection.KEY_PASSWORD),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[] {
				new AssignStatement(JdbcConnectionSqlConstants.COL_NAME,
						new KeyExpression(JdbcConnection.KEY_NAME)),
				new AssignStatement(JdbcConnectionSqlConstants.COL_DRIVER,
						new KeyExpression(JdbcConnection.KEY_DRIVER)),
				new AssignStatement(JdbcConnectionSqlConstants.COL_URL,
						new KeyExpression(JdbcConnection.KEY_URL)),
				new AssignStatement(JdbcConnectionSqlConstants.COL_USERNAME,
						new KeyExpression(JdbcConnection.KEY_USERNAME)),
				new AssignStatement(JdbcConnectionSqlConstants.COL_PASSWORD,
						new KeyExpression(JdbcConnection.KEY_PASSWORD)),
		};
	}
}
