package net.jp.yamabuki.service.manager;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * モデルの一覧を取得するインターフェース。
 *
 * @param <T> モデル
 */
public interface GetAppModelList<T> {
	/**
	 * モデルの一覧を取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param page ページ範囲
	 * @param map 条件パラメータ
	 * @return モデルの一覧
	 */
	List<T> execute(UserId userId, AppId appId, Pageable page, Map<String, String> map);
}
