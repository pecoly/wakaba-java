package net.jp.yamabuki.service.manager.js.sql;

/**
 * JsのSqlの定数クラス。
 *
 */
public final class JsSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private JsSqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_CONTENT = "CONTENT";

	public static final String COL_LAST_MODIFIED = "LAST_MODIFIED";
}
