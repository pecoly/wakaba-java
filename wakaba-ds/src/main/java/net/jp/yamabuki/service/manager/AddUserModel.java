package net.jp.yamabuki.service.manager;

import net.jp.yamabuki.model.UserModel;

/**
 * モデルを追加するインターフェース。
 *
 * @param <T>
 */
public interface AddUserModel<T extends UserModel> {
	/**
	 * モデルを追加します。
	 * @param t モデル
	 */
	void execute(T t);
}
