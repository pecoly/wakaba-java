package net.jp.yamabuki.service.manager.app.sql.postgresql;

import java.util.Map;

import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.UserModel;
import net.jp.yamabuki.service.manager.app.sql.AppSqlConstants;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アプリケーションを取り扱うSqlを管理するクラス。
 *
 */
@Component("appSql")
public class AppSqlImpl implements ManagerSql {
	@Override
	public String getGetByIdSql(Map<String, Object> map) {
		return "SELECT ID, USER_ID, APP_ID, APP_NAME"
				+ " FROM APP_LIST"
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID
				+ " AND ID = :" + UserModel.KEY_ID;
	}

	@Override
	public String getGetByKeySql(Map<String, Object> map) {
		return "SELECT ID, USER_ID, APP_ID, APP_NAME"
				+ " FROM APP_LIST"
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID
				+ " AND APP_ID = :" + App.KEY_APP_ID;
	}

	@Override
	public String getGetListSql(Map<String, Object> map) {
		final String sql1
				= "SELECT ID,"
				+ " USER_ID, APP_ID,"
				+ " APP_NAME"
				+ " FROM APP_LIST"
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID;

		final String sql2
				= " ORDER BY APP_NAME"
				+ " LIMIT :" + AppSqlConstants.KEY_LIMIT
				+ " OFFSET :" + AppSqlConstants.KEY_OFFSET;

		StringBuilder sb = new StringBuilder();
		sb.append(sql1);

		sb.append(sql2);
		return sb.toString();
	}

	@Override
	public String getGetCountSql(Map<String, Object> map) {
		return "SELECT COUNT(*) APP_COUNT"
				+ " FROM APP_LIST"
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID;
	}

	@Override
	public String getAddSql(Map<String, Object> map) {
		return "INSERT INTO APP_LIST (USER_ID, APP_ID, APP_NAME)"
				+ " VALUES ("
				+ " :userId"
				+ ", :appId"
				+ ", :appName"
				+ ")";
	}

	@Override
	public String getRemoveSql(Map<String, Object> map) {
		return "DELETE FROM APP_LIST "
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID
				+ " AND ID = :" + UserModel.KEY_ID;
	}

	@Override
	public String getUpdateSql(Map<String, Object> map) {
		return "UPDATE APP_LIST SET"
				+ " APP_NAME = :appName"
				+ " WHERE USER_ID = :" + UserModel.KEY_USER_ID
				+ " AND ID = :" + UserModel.KEY_ID;
	}
}
