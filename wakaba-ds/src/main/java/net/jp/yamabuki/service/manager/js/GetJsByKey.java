package net.jp.yamabuki.service.manager.js;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにJsを取得するインターフェース。
 *
 */
public interface GetJsByKey extends GetModelByKey<Js> {
	/**
	 * キーをもとにJsを取得します。
	 * キーはJsの拡張子なしの名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return Js
	 */
	Js execute(UserId userId, AppId appId, String name);
}
