package net.jp.yamabuki.service.manager.loginuser.sql.postgresql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.GetLoginUserByKey;
import net.jp.yamabuki.service.manager.appuser.sql.AppUserMapper;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.User;

public class GetLoginUserByKeyImpl extends BaseManagerQuery implements GetLoginUserByKey {

	private static final String SQL
			= "SELECT * FROM"
			+ " USER_LIST"
			+ " WHERE LOGIN_ID = :loginId";

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbc;

	@Override
	public User execute(String loginId) {
		AppUser appUser = new AppUser(new ModelIdImpl("1"), new UserIdImpl("sample"),
				new AppIdImpl("sample_1"), "a", "b", "manager", "");
		return appUser.getLoginUser();

		/*
		this.initialize();

		try {
			AppUser appUser = this.execute(this.createParameterList(loginId));
			return appUser.getLoginUser();
		}
		catch (EmptyResultDataAccessException ex) {
			return null;
		}
		*/
	}

	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				new QueryParameter("loginId", stringConverter),
		}));
	}

	AppUser execute(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		return this.namedParameterJdbc.queryForObject(
				SQL,
				this.toObjectParameterList(map),
				new AppUserMapper());
	}

	Map<String, String> createParameterList(String loginId) {
		Map<String, String> map = new HashMap<>();
		map.put("loginId", loginId);
		return map;
	}
}
