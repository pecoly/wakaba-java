package net.jp.yamabuki.service.manager.jdbcconnection.sql;

/**
 * JdbcコネクションのSqlの定数クラス。
 *
 */
public final class JdbcConnectionSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private JdbcConnectionSqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_DRIVER = "DRIVER";

	public static final String COL_URL = "URL";

	public static final String COL_USERNAME = "USERNAME";

	public static final String COL_PASSWORD = "PASSWORD";
}
