package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.jdbcconnection.GetJdbcConnectionByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Jdbcコネクションを追加する実装クラス。
 *
 */
@Component("addJdbcConnection")
public class AddJdbcConnectionImpl extends BaseAddAppModel<JdbcConnection> {

	@Autowired
	private GetJdbcConnectionByKey getJdbcConnectionByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter nameUpdateParameter;

	@Autowired
	@Qualifier("jdbcConnectionSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.nameUpdateParameter,
				new UpdateParameter("driver", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("url", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("username", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("password", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(JdbcConnection jdbcConnection) {
		String m = String.format("name : %s", jdbcConnection.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.JDBC_CONNECTION_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<JdbcConnection> getGetModelByKey() {
		return this.getJdbcConnectionByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
