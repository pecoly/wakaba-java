package net.jp.yamabuki.service.manager.css.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * IdをもとにCssを取得する実装クラス。
 *
 */
@Component("getCssById")
public class GetCssByIdImpl extends BaseGetAppModelById<Css> {

	private RowMapper<Css> mapper = new CssMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("cssSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<Css> getMapper() {
		return this.mapper;
	}
}
