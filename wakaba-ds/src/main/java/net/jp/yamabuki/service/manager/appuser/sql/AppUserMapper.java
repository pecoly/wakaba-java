package net.jp.yamabuki.service.manager.appuser.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * ユーザーを取得するマッパークラス。
 *
 */
public class AppUserMapper implements RowMapper<AppUser> {
	@Override
	public AppUser mapRow(ResultSet rs, int number) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String loginId = rs.getString("LOGIN_ID");
		String loginPassword = rs.getString("LOGIN_PASSWORD");
		String mainAuthority = rs.getString("MAIN_AUTHORITY");
		String subAuthority = rs.getString("SUB_AUTHORITY");
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(AppModelSqlConstants.COL_LAST_MODIFIED));

		return new AppUser(new ModelIdImpl(id),
				new UserIdImpl(userId), new AppIdImpl(appId),
				loginId, loginPassword,
				mainAuthority, subAuthority, lastModified);
	}
}
