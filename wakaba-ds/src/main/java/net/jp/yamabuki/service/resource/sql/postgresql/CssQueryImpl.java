package net.jp.yamabuki.service.resource.sql.postgresql;

import net.jp.yamabuki.service.resource.sql.BaseCssQuery;

/**
 * Cssを取得する実装クラス。
 *
 */
public class CssQueryImpl extends BaseCssQuery {
	@Override
	protected String getTimestampNameSql() {
		return "SELECT NAME"
				+ " || '.css'"
				/*+ " || '?' || TO_CHAR(LAST_MODIFIED, 'YYYYMMDDHHMISS')"*/
				+ " FROM CSS_LIST"
				+ " WHERE USER_ID = ?"
				+ " AND APP_ID = ?"
				+ " AND NAME = ?";
	}
}
