package net.jp.yamabuki.service.manager.testquery.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.testquery.GetTestQueryByKey;

/**
 * キーをもとにテストクエリを取得する実装クラス。
 *
 */
@Component
public class GetTestQueryByKeyImpl extends BaseManagerQuery
implements GetTestQueryByKey {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	@Qualifier("testQuerySql")
	private ManagerSql testQuerySql;

	@Override
	public TestQuery execute(UserId userId, AppId appId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId, appId));

		return this.queryForObject(
				this.testQuerySql.getGetByKeySql(parameterList),
				parameterList,
				new TestQueryMapper());
	}

	@Override
	public TestQuery execute(TestQuery testQuery) {
		Argument.isNotNull(testQuery, "testQuery");

		return this.execute(testQuery.getUserId(),
				testQuery.getAppId());
	}
	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId, AppId appId) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		return map;
	}
}
