package net.jp.yamabuki.service.resource.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import net.jp.yamabuki.exception.NoSuchMessageException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.MessageQuery;
import net.jp.yamabuki.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * メッセージ取得基底クラス。
 *
 */
public abstract class BaseMessageQuery implements MessageQuery {

	private UserId userId;

	private AppId appId;

	private LocaleId localeId;

	private MessageMapper mapper;

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public void initialize(UserId userId, AppId appId, LocaleId localeId) {
		this.userId = userId;
		this.appId = appId;
		this.localeId = localeId;
		this.mapper = new MessageMapper();
	}

	@Override
	public String getUserMessage(String code) {
		return this.getMessage(this.userId.getValue(), "*", code);
	}

	@Override
	public String getAppMessage(String code) {
		return this.getMessage(this.userId.getValue(), this.appId.getValue(), code);
	}

	@Override
	public String getManagerMessage(String code) {
		return this.getMessage("*", "*", code);
	}

	/**
	 * メッセージを取得します。
	 * @param userId ユーザーId(管理アプリケーションメッセージのときは'*')
	 * @param appId アプリケーションId(管理アプリケーションメッセージのときは'*')
	 * @param code メッセージコード
	 * @return メッセージ
	 */
	String getMessage(String userId, String appId, String code) {
		if (StringUtils.isBlank(code)) {
			return "";
		}

		List<String> messageList = this.jdbc.query(
				this.getSql(),
				this.createParameter(userId, appId, code, this.localeId.getValue()),
				this.mapper);
		if (messageList.isEmpty()) {
			messageList = this.jdbc.query(
					this.getSql(),
					this.createParameter(userId, appId, code),
					this.mapper);
			if (messageList.isEmpty()) {
				throw new NoSuchMessageException(code, this.localeId);
			}
		}

		return messageList.get(0);
	}

	/**
	 * 言語コード指定のメッセージを取得するためのパラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @param localeId ロケールId
	 * @return 言語コード指定のメッセージを取得するためのパラメータ
	 */
	Object[] createParameter(String userId, String appId, String code, String localeId) {
		return new Object[] {
				userId,
				appId,
				code,
				localeId,
		};
	}

	/**
	 * デフォルトのメッセージを取得するためのパラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param code メッセージコード
	 * @return デフォルトのメッセージを取得するためのパラメータ
	 */
	Object[] createParameter(String userId, String appId, String code) {
		return new Object[] {
				userId,
				appId,
				code,
				"*",
		};
	}

	/**
	 * メッセージマッパークラス。
	 *
	 */
	private static class MessageMapper implements RowMapper<String> {
		@Override
		public String mapRow(ResultSet rs, int number) throws SQLException {
			return rs.getString("MESSAGE");
		}
	}

	/**
	 * メッセージを取得するためのSqlを取得します。
	 * @return メッセージを取得するためのSql
	 */
	protected abstract String getSql();
}
