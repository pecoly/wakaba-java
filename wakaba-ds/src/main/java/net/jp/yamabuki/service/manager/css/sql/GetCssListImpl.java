package net.jp.yamabuki.service.manager.css.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelList;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;
import net.jp.yamabuki.util.StringUtils;

/**
 * Cssの一覧を取得するクラス。
 *
 */
@Component("getCssList")
public class GetCssListImpl extends BaseGetAppModelList<Css> {

	private RowMapper<Css> mapper = new CssMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter limitQueryParameter;

	@Autowired
	private QueryParameter offsetQueryParameter;

	@Autowired
	@Qualifier("cssSql")
	private ManagerSql managerSql;

	@Autowired
	private SqlUtils sqlUtils;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.limitQueryParameter,
				this.offsetQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param map 条件パラメータ
	 * @return 取得用パラメータ
	 */
	@Override
	public Map<String, String> toStringParameterList(Map<String, String> map) {
		Map<String, String> result = new HashMap<>();

		for (String key : new String[] { Css.KEY_NAME }) {
			String value = map.get(key);
			if (!StringUtils.isBlank(value)) {
				result.put(key, this.sqlUtils.toLikeKeyword(value));
			}
		}

		return result;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<Css> getMapper() {
		return this.mapper;
	}
}
