package net.jp.yamabuki.service.manager.js.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * IdをもとにJsを取得する実装クラス。
 *
 */
@Component("getJsById")
public class GetJsByIdImpl extends BaseGetAppModelById<Js> {

	private RowMapper<Js> mapper = new JsMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("jsSql")
	private ManagerSql managerSql;

	@Override
	public Js execute(Js js) {
		Argument.isNotNull(js, "js");

		return this.execute(js.getUserId(),
				js.getAppId(), js.getId());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<Js> getMapper() {
		return this.mapper;
	}
}
