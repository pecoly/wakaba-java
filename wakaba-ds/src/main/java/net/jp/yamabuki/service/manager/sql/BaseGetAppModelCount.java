package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.service.manager.GetAppModelCount;
import net.jp.yamabuki.util.StringUtils;

public abstract class BaseGetAppModelCount extends BaseManagerQuery
implements GetAppModelCount {

	@Override
	public long execute(UserId userId, AppId appId, Map<String, String> map) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(map, "map");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, map);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetCountSql(objList),
				objList, Long.class);
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param map 条件パラメータ
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId,
			Map<String, String> map) {
		Map<String, String> result = new HashMap<>();

		result.put(AppModel.KEY_USER_ID, userId.getValue());
		result.put(AppModel.KEY_APP_ID, appId.getValue());
		result.putAll(this.toStringParameterList(map));

		return result;
	}

	/**
	 * 件数取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return 件数取得用Sql
	 */
	String getGetCountSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getGetCountSql(parameterList);
	}

	public abstract Map<String, String> toStringParameterList(
			Map<String, String> map);

	public abstract ManagerSql getManagerSql();
}
