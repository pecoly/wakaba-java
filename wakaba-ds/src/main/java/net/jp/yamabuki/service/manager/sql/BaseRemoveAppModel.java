package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.RemoveAppModel;
import net.jp.yamabuki.util.DateTimeUtils;

public abstract class BaseRemoveAppModel<T extends AppModel>
extends BaseManagerUpdate implements RemoveAppModel<T> {

	@Override
	public void execute(UserId userId, AppId appId,
			ModelId id, DateTime lastModified) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(id, "id");
		Argument.isNotNull(lastModified, "lastModified");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, id, lastModified);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		int count = this.update(this.getRemoveSql(objList), objList);

		if (count == 0) {
			this.throwEx(id);
		}
	}

	@Override
	public void execute(T t) {
		this.execute(t.getUserId(), t.getAppId(),
				t.getId(), t.getLastModified());
	}

	/**
	 * 削除用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id クエリId
	 * @param lastModified 最終更新日時
	 * @return 削除用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId,
			AppId appId, ModelId id, DateTime lastModified) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		map.put(AppModel.KEY_ID, id.getValue());
		map.put(AppModel.KEY_LAST_MODIFIED,
				DateTimeUtils.toStringYYYYMMDDHHMISS(lastModified));
		return map;
	}

	/**
	 * 削除用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return 削除用Sql
	 */
	String getRemoveSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getRemoveSql(parameterList);
	}

	public abstract ManagerSql getManagerSql();

	public abstract void throwEx(ModelId id);
}
