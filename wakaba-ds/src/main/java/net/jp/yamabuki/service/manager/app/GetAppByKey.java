package net.jp.yamabuki.service.manager.app;

import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetUserModelByKey;

/**
 * キーをもとにアプリケーションを取得するインターフェース。
 *
 */
public interface GetAppByKey extends GetUserModelByKey<App> {
	/**
	 * キーともとにアプリケーションを取得します。
	 * キーはアプリケーションIdです。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return アプリケーション
	 */
	App execute(UserId userId, AppId appId);
}
