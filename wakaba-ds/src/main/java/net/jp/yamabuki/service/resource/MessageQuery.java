package net.jp.yamabuki.service.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.LocaleId;
import net.jp.yamabuki.model.UserId;

/**
 * ビューに表示するメッセージを取得するためのインターフェース。
 *
 */
public interface MessageQuery {
	/**
	 * 初期化します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param localeId ロケールId
	 */
	void initialize(UserId userId, AppId appId, LocaleId localeId);

	/**
	 * ユーザーメッセージを取得します。
	 * @param code メッセージコード
	 * @return メッセージ
	 */
	String getUserMessage(String code);

	/**
	 * アプリケーションメッセージを取得します。
	 * @param code メッセージコード
	 * @return メッセージ
	 */
	String getAppMessage(String code);

	/**
	 * 管理アプリケーションメッセージを取得します。
	 * @param code メッセージコード
	 * @return メッセージ
	 */
	String getManagerMessage(String code);
}
