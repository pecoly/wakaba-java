package net.jp.yamabuki.service.manager.testupdate.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.datasource.TestUpdate;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.springframework.jdbc.core.RowMapper;

/**
 * テストアップデートを取得するマッパークラス。
 *
 */
public class TestUpdateMapper implements RowMapper<TestUpdate> {

	@Override
	public TestUpdate mapRow(ResultSet rs, int rowNumber) throws SQLException {
		String id = rs.getString("ID");
		String userId = rs.getString("USER_ID");
		String appId = rs.getString("APP_ID");
		String xml = rs.getString("XML");

		return new TestUpdate(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				xml);
	}
}
