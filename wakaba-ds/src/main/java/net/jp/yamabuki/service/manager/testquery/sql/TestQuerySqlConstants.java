package net.jp.yamabuki.service.manager.testquery.sql;

/**
 * テストクエリのSqlの定数クラス。
 *
 */
public final class TestQuerySqlConstants {

	/**
	 * コンストラクタ。
	 */
	private TestQuerySqlConstants() {
	}

	public static final String TABLE = "TEST_QUERY_LIST";

	public static final String COL_TYPE = "QUERY_TYPE";

	public static final String COL_NAME = "NAME";

	public static final String COL_XML = "XML";
}
