package net.jp.yamabuki.service.manager.update.sql;

/**
 * アップデートのSqlの定数クラス。
 *
 */
public final class UpdateSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private UpdateSqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_TYPE = "UPDATE_TYPE";

	public static final String COL_XML = "XML";
}
