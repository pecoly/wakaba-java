package net.jp.yamabuki.service.manager.viewtemplate.sql;

/**
 * ビューテンプレートのSqlの定数クラス。
 *
 */
public final class ViewTemplateSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private ViewTemplateSqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_PATH = "PATH";

	public static final String COL_CONTENT = "CONTENT";
}
