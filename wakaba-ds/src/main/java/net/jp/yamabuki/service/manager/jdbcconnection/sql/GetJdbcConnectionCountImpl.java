package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.JdbcConnection;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelCount;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.sql.SqlUtils;
import net.jp.yamabuki.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Jdbcコネクションの総件数を取得する実装クラス。
 *
 */
@Component("getJdbcConnectionCount")
public class GetJdbcConnectionCountImpl extends BaseGetAppModelCount {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	@Qualifier("jdbcConnectionSql")
	private ManagerSql managerSql;

	@Autowired
	private SqlUtils sqlUtils;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param map 条件パラメータ
	 * @return 取得用パラメータ
	 */
	@Override public Map<String, String> toStringParameterList(Map<String, String> map) {
		Argument.isNotNull(map, "map");

		Map<String, String> result = new HashMap<>();

		for (String key : new String[] {
				JdbcConnection.KEY_NAME,
				JdbcConnection.KEY_DRIVER,
				JdbcConnection.KEY_URL,
				JdbcConnection.KEY_USERNAME }) {
			String value = map.get(key);
			if (!StringUtils.isBlank(value)) {
				result.put(key, this.sqlUtils.toLikeKeyword(value));
			}
		}

		return result;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
