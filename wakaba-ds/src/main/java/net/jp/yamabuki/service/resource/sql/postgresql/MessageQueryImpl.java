package net.jp.yamabuki.service.resource.sql.postgresql;

import net.jp.yamabuki.service.resource.sql.BaseMessageQuery;

/**
 * メッセージを取得する実装クラス。
 *
 */
public class MessageQueryImpl extends BaseMessageQuery {
	@Override
	protected String getSql() {
		return "SELECT MESSAGE FROM MESSAGE_LIST "
				+ "WHERE USER_ID = ? "
				+ "AND APP_ID = ? "
				+ "AND CODE = ? "
				+ "AND LOCALE_ID = ? ";
	}
}
