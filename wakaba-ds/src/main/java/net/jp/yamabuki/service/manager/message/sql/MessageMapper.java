package net.jp.yamabuki.service.manager.message.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.resource.Message;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.LocaleIdUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * メッセージを取得するマッパークラス。
 *
 */
public class MessageMapper implements RowMapper<Message> {
	@Override
	public Message mapRow(ResultSet rs, int number) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String code = rs.getString(MessageSqlConstants.COL_CODE);
		String localeId = rs.getString(MessageSqlConstants.COL_LOCALE_ID);
		String message = rs.getString(MessageSqlConstants.COL_MESSAGE);
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(AppModelSqlConstants.COL_LAST_MODIFIED));

		return new Message(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				code, LocaleIdUtils.create(localeId), message, lastModified);
	}
}
