package net.jp.yamabuki.service.manager;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;

/**
 * Idをもとにモデルを取得するインターフェース。
 *
 * @param <T>
 */
public interface GetAppModelById<T extends AppModel> {
	/**
	 * Idをもとにモデルを取得します。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id Id
	 * @return モデル
	 */
	T execute(UserId userId, AppId appId, ModelId id);

	/**
	 * Idをもとにモデルを取得します。
	 * 見つからない場合はnullを返却します。
	 * @param t もとになるモデル
	 * @return モデル
	 */
	T execute(T t);
}
