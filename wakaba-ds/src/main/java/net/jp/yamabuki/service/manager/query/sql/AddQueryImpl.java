package net.jp.yamabuki.service.manager.query.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.query.GetQueryByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * クエリを追加する実装クラス。
 *
 */
@Component("addQuery")
public class AddQueryImpl extends BaseAddAppModel<Query> {

	@Autowired
	private GetQueryByKey getQueryByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("querySql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("xml", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(Query query) {
		String m = String.format("name : %s", query.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.QUERY_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<Query> getGetModelByKey() {
		return this.getQueryByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
