package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.GetUserModelList;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アプリケーションの一覧を取得する実装クラス。
 *
 */
@Component("getAppList")
public class GetAppListImpl extends BaseManagerQuery
implements GetUserModelList<App> {

	private static final int MAX_LENGTH = 1000;

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter limitQueryParameter;

	@Autowired
	private QueryParameter offsetQueryParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public List<App> execute(UserId userId, Pageable page, Map<String, String> map) {
		Argument.isNotNull(userId, "userId");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId));

		return this.query(
				this.appSql.getGetListSql(parameterList),
				parameterList,
				new AppMapper());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.limitQueryParameter,
				this.offsetQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId) {
		Pageable page = new PageRequest(0, MAX_LENGTH);

		Map<String, String> result = new HashMap<>();
		result.put(AppModel.KEY_USER_ID, userId.getValue());
		result.put(AppSqlConstants.KEY_LIMIT, Integer.toString(page.getPageSize()));
		result.put(AppSqlConstants.KEY_OFFSET,
				Integer.toString((page.getPageNumber()) * page.getPageSize()));

		return result;
	}
}
