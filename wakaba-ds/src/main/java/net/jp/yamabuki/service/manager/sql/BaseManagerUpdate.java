package net.jp.yamabuki.service.manager.sql;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.check.Field;
import net.jp.yamabuki.exception.ValidationException;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.UpdateParameterListValidator;

/**
 * レコードを更新する基底クラス。
 *
 */
public abstract class BaseManagerUpdate {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	private UpdateParameterListValidator validator1;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbc;

	/**
	 * 初期化します。
	 * @param updateParameterList パラメータ一覧
	 */
	public void initialize(List<UpdateParameter> updateParameterList) {
		Field.isNull(this.validator1, "validator1");

		this.validator1 = new UpdateParameterListValidator(
				updateParameterList);
	}

	/**
	 * 文字列パラメータをオブジェクトパラメータに変換します。
	 * @param map 文字列パラメータ
	 * @return オブジェクトパラメータ
	 */
	public Map<String, Object> toObjectParameterList(Map<String, String> map) {
		String result = this.validator1.validate(map);
		if (result != null) {
			throw new ValidationException(result);
		}

		return this.validator1.getObjectParameterList();
	}

	/**
	 * レコードを更新します。
	 * @param sql Sql
	 * @param map パラメータ
	 * @return 更新件数
	 */
	public int update(String sql, Map<String, Object> map) {
		Argument.isNotNull(sql, "sql");
		Argument.isNotNull(map, "map");

		logger.info(sql);
		logger.info(map.toString());

		return this.namedParameterJdbc.update(sql, map);
	}
}
