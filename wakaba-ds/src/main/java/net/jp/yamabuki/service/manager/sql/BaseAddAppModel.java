package net.jp.yamabuki.service.manager.sql;

import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.GetModelByKey;

public abstract class BaseAddAppModel<T extends AppModel>
extends BaseManagerUpdate implements AddAppModel<T> {

	@Override
	public void execute(T t) {
		Argument.isNotNull(t, "t");

		if (this.contains(t)) {
			this.throwEx(t);
		}

		Map<String, Object> objList
				= this.toObjectParameterList(t.toStringMap());

		try {
			this.update(this.getAddSql(objList), objList);
		}
		catch (DataIntegrityViolationException ex) {
			this.throwEx(t);
		}
	}

	/**
	 * モデルが存在するかどうかをチェックします。
	 * @param t モデル
	 * @return モデルの存在有無
	 */
	boolean contains(T t) {
		return this.getGetModelByKey().execute(t) != null;
	}

	/**
	 * 追加用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return 追加用Sql
	 */
	String getAddSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getAddSql(parameterList);
	}

	/**
	 * データ重複時の例外を投げます。
	 * @param t モデル
	 */
	public abstract void throwEx(T t);

	public abstract ManagerSql getManagerSql();

	public abstract GetModelByKey<T> getGetModelByKey();
}
