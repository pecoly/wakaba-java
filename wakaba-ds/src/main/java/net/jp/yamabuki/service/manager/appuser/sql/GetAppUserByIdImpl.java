package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.sql.BaseGetAppModelById;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Idをもとにアプリケーションユーザーを取得するクラス。
 *
 */
@Component("getAppUserById")
public class GetAppUserByIdImpl extends BaseGetAppModelById<AppUser> {

	private RowMapper<AppUser> mapper = new AppUserMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("appUserSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}

	@Override
	public RowMapper<AppUser> getMapper() {
		return this.mapper;
	}
}
