package net.jp.yamabuki.service.manager.sql;

import net.jp.yamabuki.model.sql.statement.FromStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.select.SelectStatement;

public interface SqlFactory {
	String createGetListSql(SelectStatement selectStatement,
			FromStatement fromStatement, WhereStatement whereStatement,
			OrderByStatement orderByStatement);

	String createGetCountSql(SelectStatement selectStatement,
			FromStatement fromStatement, WhereStatement whereStatement);
}
