package net.jp.yamabuki.service.manager;

import net.jp.yamabuki.model.UserModel;
/**
 * Idをもとにモデルを更新するインターフェース。
 *
 * @param <T>
 */
public interface UpdateUserModel<T extends UserModel> {
	/**
	 * Idをもとにモデルを更新します。
	 *
	 * @param t モデル
	 */
	void execute(T t);
}
