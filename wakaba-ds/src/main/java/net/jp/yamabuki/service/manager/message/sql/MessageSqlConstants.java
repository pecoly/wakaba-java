package net.jp.yamabuki.service.manager.message.sql;

/**
 * メッセージのSqlの定数クラス。
 *
 */
public final class MessageSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private MessageSqlConstants() {
	}

	public static final String COL_CODE = "CODE";

	public static final String COL_LOCALE_ID = "LOCALE_ID";

	public static final String COL_MESSAGE = "MESSAGE";
}
