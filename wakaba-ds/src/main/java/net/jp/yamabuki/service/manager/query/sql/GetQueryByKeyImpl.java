package net.jp.yamabuki.service.manager.query.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.query.GetQueryByKey;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * キーをもとにクエリを取得します。
 *
 */
@Component
public class GetQueryByKeyImpl extends BaseManagerQuery
implements GetQueryByKey {

	private RowMapper<Query> mapper = new QueryMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	@Qualifier("querySql")
	private ManagerSql managerSql;

	@Override
	public Query execute(UserId userId, AppId appId, String name) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(name, "name");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, name);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetByKeySql(objList),
				objList, this.mapper);
	}

	@Override
	public Query execute(Query query) {
		Argument.isNotNull(query, "query");

		return this.execute(query.getUserId(),
				query.getAppId(), query.getName());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				new QueryParameter("name", stringConverter),
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId,
			String name) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		map.put(Query.KEY_NAME, name);
		return map;
	}

	/**
	 * キーによるモデル取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return キーによるモデル取得用Sql
	 */
	String getGetByKeySql(Map<String, Object> parameterList) {
		return this.managerSql.getGetByKeySql(parameterList);
	}
}
