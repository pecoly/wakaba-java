package net.jp.yamabuki.service.manager.query.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.sql.BaseUpdateAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アップデートを更新する実装クラス。
 *
 */
@Component("updateQuery")
public class UpdateQueryImpl extends BaseUpdateAppModel<Query> {

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("querySql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("xml", stringConverter, new ArrayList<Validator>()),
				this.integerIdUpdateParameter,
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.QUERY_NOT_FOUND,
				"id : " + id.getValue());
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
