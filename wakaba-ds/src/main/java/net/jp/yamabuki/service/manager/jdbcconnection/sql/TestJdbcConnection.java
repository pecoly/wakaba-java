package net.jp.yamabuki.service.manager.jdbcconnection.sql;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.JdbcConnection;

/**
 * Jdbcの接続をテストするクラス。
 *
 */
@Component
public class TestJdbcConnection {
	/**
	 * 接続をテストします。
	 * 失敗時は例外を投げます。
	 * @param jdbcConnection Jdbcコネクション
	 */
	public void execute(JdbcConnection jdbcConnection) {
		Argument.isNotNull(jdbcConnection, "jdbcConnection");

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(jdbcConnection.getDriver());
		dataSource.setUrl(jdbcConnection.getUrl());
		dataSource.setUsername(jdbcConnection.getUsername());
		dataSource.setPassword(jdbcConnection.getPassword());

		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.queryForObject("SELECT 1", Integer.class);
	}
}
