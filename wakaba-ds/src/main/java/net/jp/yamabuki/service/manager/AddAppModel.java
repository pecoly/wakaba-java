package net.jp.yamabuki.service.manager;

/**
 * モデルを追加するインターフェース。
 *
 * @param <T>
 */
public interface AddAppModel<T> {
	/**
	 * モデルを追加します。
	 * @param t モデル
	 */
	void execute(T t);
}
