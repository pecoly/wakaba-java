package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetAppModelById;

public abstract class BaseGetAppModelById <T extends AppModel>
extends BaseManagerQuery implements GetAppModelById<T> {

	@Override
	public T execute(UserId userId, AppId appId, ModelId id) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(id, "id");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, id);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetByIdSql(objList),
				objList, this.getMapper());
	}

	@Override
	public T execute(T t) {
		Argument.isNotNull(t, "t");

		return this.execute(t.getUserId(),
				t.getAppId(), t.getId());
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param id クエリId
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId, ModelId id) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		map.put(AppModel.KEY_ID, id.getValue());
		return map;
	}

	/**
	 * Idによるモデル取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return Idによるモデル取得用Sql
	 */
	String getGetByIdSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getGetByIdSql(parameterList);
	}

	public abstract ManagerSql getManagerSql();

	public abstract RowMapper<T> getMapper();
}
