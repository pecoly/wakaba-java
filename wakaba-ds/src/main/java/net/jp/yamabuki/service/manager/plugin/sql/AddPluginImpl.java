package net.jp.yamabuki.service.manager.plugin.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.Plugin;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.GetModelByKey;
import net.jp.yamabuki.service.manager.plugin.GetPluginByKey;
import net.jp.yamabuki.service.manager.sql.BaseAddAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * プラグインを追加する実装クラス。
 *
 */
@Component("addPlugin")
public class AddPluginImpl extends BaseAddAppModel<Plugin> {

	@Autowired
	private GetPluginByKey getPluginByKey;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("pluginSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("description", stringConverter, new ArrayList<Validator>()),
		}));
	}

	@Override
	public void throwEx(Plugin plugin) {
		String m = String.format("name : %s", plugin.getName());
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.PLUGIN_ALREADY_EXISTS, m);
	}

	@Override
	public GetModelByKey<Plugin> getGetModelByKey() {
		return this.getPluginByKey;
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
