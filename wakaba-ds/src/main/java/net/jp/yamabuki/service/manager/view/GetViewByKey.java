package net.jp.yamabuki.service.manager.view;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにビューを取得するインターフェース。
 *
 */
public interface GetViewByKey extends GetModelByKey<View> {
	/**
	 * キーをもとにビューを取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return ビュー
	 */
	View execute(UserId userId, AppId appId, String name);
}
