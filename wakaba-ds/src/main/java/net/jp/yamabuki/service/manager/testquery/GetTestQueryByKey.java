package net.jp.yamabuki.service.manager.testquery;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.service.manager.GetModelByKey;
/**
 * テストクエリを取得するインターフェース。
 *
 */
public interface GetTestQueryByKey extends GetModelByKey<TestQuery> {
	/**
	 * テストクエリを取得します。
	 * テストクエリはアプリケーションに1つなのでキーはありません。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return テストクエリ
	 */
	TestQuery execute(UserId userId, AppId appId);
}
