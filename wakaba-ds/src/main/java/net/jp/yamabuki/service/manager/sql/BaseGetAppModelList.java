package net.jp.yamabuki.service.manager.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.manager.GetAppModelList;

public abstract class BaseGetAppModelList<T extends AppModel>
extends BaseManagerQuery implements GetAppModelList<T> {
	@Override
	public List<T> execute(UserId userId, AppId appId,
			Pageable page, Map<String, String> map) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(map, "map");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, page, map);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.query(
				this.getGetListSql(objList),
				objList, this.getMapper());
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param page ページ
	 * @param map 条件パラメータ
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId,
			Pageable page, Map<String, String> map) {
		Map<String, String> result = new HashMap<>();
		result.put(AppModel.KEY_USER_ID, userId.getValue());
		result.put(AppModel.KEY_APP_ID, appId.getValue());
		result.put("limit", Integer.toString(page.getPageSize()));
		result.put("offset",
				Integer.toString((page.getPageNumber()) * page.getPageSize()));
		result.putAll(this.toStringParameterList(map));
		return result;
	}

	/**
	 * 一覧取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return 一覧取得用Sql
	 */
	String getGetListSql(Map<String, Object> parameterList) {
		return this.getManagerSql().getGetListSql(parameterList);
	}

	public abstract Map<String, String> toStringParameterList(
			Map<String, String> map);

	public abstract ManagerSql getManagerSql();

	public abstract RowMapper<T> getMapper();
}
