package net.jp.yamabuki.service.manager.view.sql;

/**
 * ビューのSqlの定数クラス。
 *
 */
public final class ViewSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private ViewSqlConstants() {
	}

	public static final String TABLE = "VIEW_LIST";

	public static final String COL_ID = "ID";

	public static final String COL_USER_ID = "USER_ID";

	public static final String COL_APP_ID = "APP_ID";

	public static final String COL_NAME = "NAME";

	public static final String COL_PATH = "PATH";

	public static final String COL_CONTENT = "CONTENT";

	public static final String COL_LAST_MODIFIED = "LAST_MODIFIED";
}
