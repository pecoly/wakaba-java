package net.jp.yamabuki.service.manager;

import java.util.Map;

import net.jp.yamabuki.model.UserId;

/**
 * モデルの総件数を取得するインターフェース。
 *
 */
public interface GetUserModelCount {
	/**
	 * モデルの総件数を取得します。
	 * @param userId ユーザーId
	 * @param map 条件パラメータ
	 * @return モデルの総件数
	 */
	long execute(UserId userId, Map<String, String> map);
}
