package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.GetUserModelCount;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * アプリケーション件数を取得する実装クラス。
 *
 */
@Component("getAppCount")
public class GetAppCountImpl extends BaseManagerQuery
implements GetUserModelCount {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public long execute(UserId userId, Map<String, String> map) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(map, "map");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId, map));

		return this.queryForObject(
				this.appSql.getGetCountSql(parameterList),
				parameterList, Long.class);
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param map 条件パラメータ
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId,
			Map<String, String> map) {
		Map<String, String> result = new HashMap<>();
		result.put(AppModel.KEY_USER_ID, userId.getValue());

		return result;
	}
}
