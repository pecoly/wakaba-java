package net.jp.yamabuki.service.manager.testquery.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelAlreadyExistsException;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.AddAppModel;
import net.jp.yamabuki.service.manager.sql.BaseManagerUpdate;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * テストクエリを追加する実装クラス。
 *
 */
@Component("addTestQuery")
public class AddTestQueryImpl extends BaseManagerUpdate
implements AddAppModel<TestQuery> {

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	@Qualifier("testQuerySql")
	private ManagerSql managerSql;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(TestQuery testQuery) {
		Argument.isNotNull(testQuery, "testQuery");

		Map<String, Object> parameterList
				= this.toObjectParameterList(testQuery.toStringMap());

		this.update(this.managerSql.getRemoveSql(parameterList),
				parameterList);

		try {
			this.update(
					this.managerSql.getAddSql(parameterList),
					parameterList);
		}
		catch (DataIntegrityViolationException ex) {
			this.throwEx(testQuery);
		}
	}

	/**
	 * データ重複時の例外を投げます。
	 * @param testQuery テストクエリ
	 */
	void throwEx(TestQuery testQuery) {
		throw new ModelAlreadyExistsException(
				WakabaErrorCode.TEST_UPDATE_ALREADY_EXISTS, "");
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				new UpdateParameter("name", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("xml", stringConverter, new ArrayList<Validator>()),
		}));
	}
}
