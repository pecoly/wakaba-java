package net.jp.yamabuki.service.manager.view.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.View;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.LikeExpression;
import net.jp.yamabuki.model.sql.expression.StringValueExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.OrderByColumnStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.service.manager.sql.AppModelSql;
import net.jp.yamabuki.service.manager.sql.SqlFactory;

/**
 * ビューを取り扱うSqlを管理するクラス。
 *
 */
@Component("viewSql")
public class ViewSqlImpl extends AppModelSql {
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(this.sqlFactory);
	}

	@Override
	protected String getTableName() {
		return "VIEW_LIST";
	}

	@Override
	public String[] getSelectColumnNameList() {
		return new String[] {
				ViewSqlConstants.COL_ID,
				ViewSqlConstants.COL_USER_ID,
				ViewSqlConstants.COL_APP_ID,
				ViewSqlConstants.COL_NAME,
				ViewSqlConstants.COL_PATH,
				ViewSqlConstants.COL_CONTENT,
				ViewSqlConstants.COL_LAST_MODIFIED
		};
	}

	@Override
	protected WhereStatement getByKeyWhereStatement() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						this.getUserIdEqualExpression(),
						this.getAppIdEqualExpression(),
						new EqualExpression(ViewSqlConstants.COL_NAME,
								new KeyExpression(View.KEY_NAME)),
				}));
	}

	@Override
	protected List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map) {
		List<BaseExpression> result = new ArrayList<>();

		if (map.containsKey(View.KEY_NAME)) {
			result.add(new LikeExpression(ViewSqlConstants.COL_NAME,
					new KeyExpression(View.KEY_NAME)));
		}

		return result;
	}

	@Override
	protected OrderByStatement getOrderByStatement() {
		return new OrderByStatement(new OrderByColumnStatement[] {
				new OrderByColumnStatement(ViewSqlConstants.COL_NAME),
		});
	}

	@Override
	public String[] getInsertColumnNameList() {
		return new String[] {
				ViewSqlConstants.COL_USER_ID,
				ViewSqlConstants.COL_APP_ID,
				ViewSqlConstants.COL_NAME,
				ViewSqlConstants.COL_PATH,
				ViewSqlConstants.COL_CONTENT,
		};
	}

	@Override
	public ValueExpression[] getInsertValueExpressionList() {
		return new ValueExpression[] {
				new KeyExpression(AppModel.KEY_USER_ID),
				new KeyExpression(AppModel.KEY_APP_ID),
				new KeyExpression(View.KEY_NAME),
				new StringValueExpression(
						":userId || '/' || :appId || '/users/view/' || :name || '.vm'"),
				new KeyExpression(View.KEY_CONTENT),
		};
	}

	@Override
	protected AssignStatement[] getUpdateAssignStatementList() {
		return new AssignStatement[] {
				new AssignStatement(ViewSqlConstants.COL_NAME, new KeyExpression(View.KEY_NAME)),
				new AssignStatement(ViewSqlConstants.COL_PATH,
						new StringValueExpression(
								":userId || '/' || :appId || '/users/view/' || :name || '.vm'")),
				new AssignStatement(ViewSqlConstants.COL_CONTENT,
						new KeyExpression(View.KEY_CONTENT)),
		};
	}
}
