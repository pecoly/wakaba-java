package net.jp.yamabuki.service.manager.app.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppIdImpl;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserIdImpl;

import org.springframework.jdbc.core.RowMapper;

/**
 * アプリケーションを取得するマッパークラス。
 *
 */
public class AppMapper implements RowMapper<App> {
	@Override
	public App mapRow(ResultSet rs, int number) throws SQLException {
		String id = rs.getString("ID");
		String userId = rs.getString("USER_ID");
		String appId = rs.getString("APP_ID");
		String name = rs.getString("APP_NAME");

		return new App(new ModelIdImpl(id),
				new UserIdImpl(userId), new AppIdImpl(appId), name);
	}
}
