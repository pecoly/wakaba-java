package net.jp.yamabuki.service.manager.sql;

public final class AppModelSqlConstants {

	private AppModelSqlConstants() {
	}

	public static final String COL_ID = "ID";

	public static final String COL_USER_ID = "USER_ID";

	public static final String COL_APP_ID = "APP_ID";

	public static final String COL_LAST_MODIFIED = "LAST_MODIFIED";
}
