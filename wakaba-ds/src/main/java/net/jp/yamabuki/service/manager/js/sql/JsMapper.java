package net.jp.yamabuki.service.manager.js.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.resource.Js;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.DateTimeUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * Jsを取得するマッパークラス。
 *
 */
public class JsMapper implements RowMapper<Js> {
	@Override
	public Js mapRow(ResultSet rs, int number) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String name = rs.getString(JsSqlConstants.COL_NAME);
		String content = rs.getString(JsSqlConstants.COL_CONTENT);
		DateTime lastModified = DateTimeUtils.toDateTime(
				rs.getTimestamp(JsSqlConstants.COL_LAST_MODIFIED));

		return new Js(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				name, content, lastModified);
	}
}
