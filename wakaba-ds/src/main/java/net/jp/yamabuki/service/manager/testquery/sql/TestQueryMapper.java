package net.jp.yamabuki.service.manager.testquery.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.ModelIdImpl;
import net.jp.yamabuki.model.datasource.TestQuery;
import net.jp.yamabuki.service.manager.sql.AppModelSqlConstants;
import net.jp.yamabuki.util.AppIdUtils;
import net.jp.yamabuki.util.UserIdUtils;

import org.springframework.jdbc.core.RowMapper;

/**
 * テストクエリを取得するマッパークラス。
 *
 */
public class TestQueryMapper implements RowMapper<TestQuery> {

	@Override
	public TestQuery mapRow(ResultSet rs, int rowNumber) throws SQLException {
		String id = rs.getString(AppModelSqlConstants.COL_ID);
		String userId = rs.getString(AppModelSqlConstants.COL_USER_ID);
		String appId = rs.getString(AppModelSqlConstants.COL_APP_ID);
		String xml = rs.getString("XML");

		return new TestQuery(new ModelIdImpl(id),
				UserIdUtils.create(userId),
				AppIdUtils.create(appId),
				xml);
	}
}
