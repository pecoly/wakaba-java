package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.appuser.GetAppUserByKey;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * キーをもとにアプリケーションユーザーを取得する実装クラス。
 *
 */
@Component
public class GetAppUserByKeyImpl extends BaseManagerQuery
implements GetAppUserByKey {

	private RowMapper<AppUser> mapper = new AppUserMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	@Qualifier("appUserSql")
	private ManagerSql managerSql;

	@Override
	public AppUser execute(UserId userId, AppId appId, String loginId) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(loginId, "loginId");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, loginId);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetByKeySql(objList),
				objList, this.mapper);
	}

	@Override
	public AppUser execute(AppUser appUser) {
		Argument.isNotNull(appUser, "appUser");

		return this.execute(appUser.getUserId(),
				appUser.getAppId(), appUser.getLoginId());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				new QueryParameter("loginId", stringConverter),
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param loginId ログインId
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId, String loginId) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_APP_ID, appId.getValue());
		map.put(AppUser.KEY_LOGIN_ID, loginId);
		return map;
	}

	/**
	 * キーによるモデル取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return キーによるモデル取得用Sql
	 */
	String getGetByKeySql(Map<String, Object> parameterList) {
		return this.managerSql.getGetByKeySql(parameterList);
	}
}
