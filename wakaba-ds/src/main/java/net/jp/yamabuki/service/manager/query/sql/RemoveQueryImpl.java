package net.jp.yamabuki.service.manager.query.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.datasource.Query;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.sql.BaseRemoveAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * クエリを削除する実装クラス。
 *
 */
@Component("removeQuery")
public class RemoveQueryImpl extends BaseRemoveAppModel<Query> {

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("querySql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.integerIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.QUERY_NOT_FOUND,
				"id : " + id.getValue());
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
