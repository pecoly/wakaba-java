package net.jp.yamabuki.service.manager.sql.postgresql;

import net.jp.yamabuki.service.manager.sql.SqlUtils;
import net.jp.yamabuki.util.PostgreSqlUtils;

public class SqlUtilsImpl implements SqlUtils {

	@Override
	public String toLikeKeyword(String value) {
		return PostgreSqlUtils.toLikeKeyword(value);
	}
}
