package net.jp.yamabuki.service.manager.appuser.sql;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.AppUser;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.converter.IntegerConverter;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.validator.Validator;
import net.jp.yamabuki.service.manager.sql.BaseUpdateAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * アプリケーションユーザーを更新する実装クラス。
 *
 */
@Component("updateAppUser")
public class UpdateAppUserImpl extends BaseUpdateAppModel<AppUser> {

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	private IntegerConverter integerConverter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter loginPasswordUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("appUserSql")
	private ManagerSql managerSql;

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.APP_USER_NOT_FOUND,
				"id : " + id.getValue());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				new UpdateParameter("loginId", stringConverter, new ArrayList<Validator>()),
				this.loginPasswordUpdateParameter,
				new UpdateParameter("mainAuthority", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("subAuthority", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("userId", stringConverter, new ArrayList<Validator>()),
				new UpdateParameter("appId", stringConverter, new ArrayList<Validator>()),
				this.integerIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
