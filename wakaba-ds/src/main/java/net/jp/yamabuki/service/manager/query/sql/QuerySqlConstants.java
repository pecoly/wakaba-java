package net.jp.yamabuki.service.manager.query.sql;

/**
 * クエリのSqlの定数クラス。
 *
 */
public final class QuerySqlConstants {
	/**
	 * コンストラクタ。
	 */
	private QuerySqlConstants() {
	}

	public static final String COL_NAME = "NAME";

	public static final String COL_TYPE = "QUERY_TYPE";

	public static final String COL_XML = "XML";
}
