package net.jp.yamabuki.service.resource.sql;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.JsQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Js取得基底クラス。
 *
 */
public abstract class BaseJsQuery implements JsQuery {

	private UserId userId;

	private AppId appId;

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public void initialize(UserId userId, AppId appId) {
		this.userId = userId;
		this.appId = appId;
	}

	@Override
	public String getCommonJsName(String name) {
		try {
			return this.jdbc.queryForObject(
					this.getTimestampNameSql(),
					new Object[] { "*", "*", name },
					String.class);
		}
		catch (EmptyResultDataAccessException ex) {
			return name;
		}
	}

	@Override
	public String getUserJsName(String name) {
		try {
			return this.jdbc.queryForObject(
					this.getTimestampNameSql(),
					new Object[] { userId.getValue(), appId.getValue(), name },
					String.class);
		}
		catch (EmptyResultDataAccessException ex) {
			return name;
		}
	}

	/**
	 * メッセージを取得するためのSqlを取得します。
	 * @return メッセージを取得するためのSql
	 */
	protected abstract String getTimestampNameSql();
}
