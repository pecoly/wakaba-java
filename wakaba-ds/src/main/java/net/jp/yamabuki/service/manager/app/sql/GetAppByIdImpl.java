package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.GetUserModelById;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Idをもとにアプリケーションを取得する実装クラス。
 *
 */
@Component("getAppById")
public class GetAppByIdImpl extends BaseManagerQuery
implements GetUserModelById<App> {

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private QueryParameter integerIdQueryParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public App execute(UserId userId, ModelId id) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(id, "id");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(userId, id));

		return this.queryForObject(
				this.appSql.getGetByIdSql(parameterList), parameterList,
				new AppMapper());
	}

	@Override
	public App execute(App app) {
		Argument.isNotNull(app, "app");

		return this.execute(app.getUserId(),
				app.getId());
	}
	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.integerIdQueryParameter,
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param id アプリケーションId
	 * @return 取得用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId, ModelId id) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_ID, id.getValue());
		return map;
	}
}
