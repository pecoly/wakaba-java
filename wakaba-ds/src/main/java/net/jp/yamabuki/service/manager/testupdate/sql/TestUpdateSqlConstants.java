package net.jp.yamabuki.service.manager.testupdate.sql;

/**
 * テストアップデートのSqlの定数クラス。
 *
 */
public final class TestUpdateSqlConstants {
	/**
	 * コンストラクタ。
	 */
	private TestUpdateSqlConstants() {
	}

	public static final String TABLE = "TEST_UPDATE_LIST";

	public static final String COL_TYPE = "UPDATE_TYPE";

	public static final String COL_NAME = "NAME";

	public static final String COL_XML = "XML";
}
