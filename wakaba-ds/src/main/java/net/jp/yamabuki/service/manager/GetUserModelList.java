package net.jp.yamabuki.service.manager;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.UserModel;

/**
 * モデルの一覧を取得するインターフェース。
 *
 * @param <T> モデル
 */
public interface GetUserModelList<T extends UserModel> {
	/**
	 * モデルの一覧を取得します。
	 * @param userId ユーザーId
	 * @param page ページ範囲
	 * @param map 条件パラメータ
	 * @return モデルの一覧
	 */
	List<T> execute(UserId userId, Pageable page, Map<String, String> map);
}
