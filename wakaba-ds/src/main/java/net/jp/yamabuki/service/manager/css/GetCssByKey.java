package net.jp.yamabuki.service.manager.css;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.service.manager.GetModelByKey;

/**
 * キーをもとにCssを取得するインターフェース。
 *
 */
public interface GetCssByKey extends GetModelByKey<Css> {
	/**
	 * キーをもとにCssを取得します。
	 * キーはCssの拡張子なしの名前です。
	 * 見つからない場合はnullを返却します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name Cssの名前(拡張子なし)
	 * @return Css
	 */
	Css execute(UserId userId, AppId appId, String name);
}
