package net.jp.yamabuki.service.manager.css.sql;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.model.resource.Css;
import net.jp.yamabuki.service.manager.sql.BaseRemoveAppModel;
import net.jp.yamabuki.service.manager.sql.ManagerSql;

/**
 * Cssを削除する実装クラス。
 *
 */
@Component("removeCss")
public class RemoveCssImpl extends BaseRemoveAppModel<Css> {

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter appIdUpdateParameter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("cssSql")
	private ManagerSql managerSql;

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	public void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.appIdUpdateParameter,
				this.integerIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	@Override
	public void throwEx(ModelId id) {
		throw new ModelNotFoundException(
				WakabaErrorCode.CSS_NOT_FOUND,
				"id : " + id.getValue());
	}

	@Override
	public ManagerSql getManagerSql() {
		return this.managerSql;
	}
}
