package net.jp.yamabuki.service.manager.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.sql.expression.AndExpression;
import net.jp.yamabuki.model.sql.expression.BaseExpression;
import net.jp.yamabuki.model.sql.expression.EqualExpression;
import net.jp.yamabuki.model.sql.expression.KeyExpression;
import net.jp.yamabuki.model.sql.expression.ValueExpression;
import net.jp.yamabuki.model.sql.statement.FromStatement;
import net.jp.yamabuki.model.sql.statement.OrderByStatement;
import net.jp.yamabuki.model.sql.statement.WhereStatement;
import net.jp.yamabuki.model.sql.statement.delete.DeleteSql;
import net.jp.yamabuki.model.sql.statement.delete.DeleteStatement;
import net.jp.yamabuki.model.sql.statement.insert.InsertSql;
import net.jp.yamabuki.model.sql.statement.insert.InsertStatement;
import net.jp.yamabuki.model.sql.statement.insert.ValuesStatement;
import net.jp.yamabuki.model.sql.statement.select.SelectColumnStatement;
import net.jp.yamabuki.model.sql.statement.select.SelectSql;
import net.jp.yamabuki.model.sql.statement.select.SelectStatement;
import net.jp.yamabuki.model.sql.statement.update.AssignStatement;
import net.jp.yamabuki.model.sql.statement.update.SetStatement;
import net.jp.yamabuki.model.sql.statement.update.UpdateSql;
import net.jp.yamabuki.model.sql.statement.update.UpdateStatement;

/**
 * データ取得用のSqlを取得する基底クラス。
 *
 */
public abstract class AppModelSql implements ManagerSql {
	private SelectStatement selectStatement;
	private FromStatement fromStatement;
	private EqualExpression userIdEqualExpression;
	private EqualExpression appIdEqualExpression;

	private String getByIdSql;

	private String getByKeySql;

	private String removeSql;

	private String updateSql;

	private String insertSql;

	private SelectStatement selectCountStatement;

	private SqlFactory sqlFactory;

	public void initialize(SqlFactory sqlFactory) {
		this.sqlFactory = sqlFactory;

		this.selectStatement = new SelectStatement(this.getSelectColumnNameList());
		this.fromStatement = new FromStatement(this.getTableName());

		this.userIdEqualExpression = new EqualExpression(
				AppModelSqlConstants.COL_USER_ID, new KeyExpression(AppModel.KEY_USER_ID));
		this.appIdEqualExpression = new EqualExpression(
				AppModelSqlConstants.COL_APP_ID, new KeyExpression(AppModel.KEY_APP_ID));

		this.selectCountStatement = new SelectStatement(new SelectColumnStatement[] {
				new SelectColumnStatement("COUNT(*)", "TOTAL_COUNT"),
		});

		WhereStatement whereByIdForQuery = this.getByIdWhereStatementForQuery();
		WhereStatement whereByIdForUpdate = this.getByIdWhereStatementForUpdate();

		this.getByIdSql = new SelectSql(
				this.selectStatement,
				this.fromStatement,
				whereByIdForQuery
		).toString();

		this.getByKeySql = new SelectSql(
				this.selectStatement,
				this.fromStatement,
				this.getByKeyWhereStatement()
		).toString();

		this.removeSql = new DeleteSql(
				new DeleteStatement(this.getTableName()),
				whereByIdForUpdate
		).toString();

		this.updateSql = new UpdateSql(
				new UpdateStatement(this.getTableName()),
				new SetStatement(this.getUpdateAssignStatementList()),
				whereByIdForUpdate
		).toString();

		this.insertSql = new InsertSql(
				new InsertStatement(this.getTableName(), this.getInsertColumnNameList()),
				new ValuesStatement(this.getInsertValueExpressionList())
		).toString();
	}

	@Override
	public String getGetByIdSql(Map<String, Object> map) {
		return this.getByIdSql;
	}

	@Override
	public String getGetByKeySql(Map<String, Object> map) {
		return this.getByKeySql;
	}

	@Override
	public String getGetListSql(Map<String, Object> map) {
		return this.sqlFactory.createGetListSql(
				this.selectStatement,
				this.fromStatement,
				this.getListWhereStatement(map),
				this.getOrderByStatement());
	}

	@Override
	public String getGetCountSql(Map<String, Object> map) {
		return this.sqlFactory.createGetCountSql(
				this.selectCountStatement,
				this.fromStatement,
				this.getListWhereStatement(map));
	}

	@Override
	public String getAddSql(Map<String, Object> map) {
		return this.insertSql;
	}

	@Override
	public String getRemoveSql(Map<String, Object> map) {
		return this.removeSql;
	}

	@Override
	public String getUpdateSql(Map<String, Object> map) {
		return this.updateSql;
	}

	protected EqualExpression getUserIdEqualExpression() {
		return this.userIdEqualExpression;
	}

	protected EqualExpression getAppIdEqualExpression() {
		return this.appIdEqualExpression;
	}

	protected WhereStatement getByIdWhereStatementForQuery() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						new EqualExpression(AppModelSqlConstants.COL_USER_ID,
								new KeyExpression(AppModel.KEY_USER_ID)),
						new EqualExpression(AppModelSqlConstants.COL_APP_ID,
								new KeyExpression(AppModel.KEY_APP_ID)),
						new EqualExpression(AppModelSqlConstants.COL_ID,
								new KeyExpression(AppModel.KEY_ID)),
				}));
	}

	protected WhereStatement getByIdWhereStatementForUpdate() {
		return new WhereStatement(new AndExpression(
				new BaseExpression[] {
						new EqualExpression(AppModelSqlConstants.COL_USER_ID,
								new KeyExpression(AppModel.KEY_USER_ID)),
						new EqualExpression(AppModelSqlConstants.COL_APP_ID,
								new KeyExpression(AppModel.KEY_APP_ID)),
						new EqualExpression(AppModelSqlConstants.COL_ID,
								new KeyExpression(AppModel.KEY_ID)),
						new EqualExpression("TO_CHAR(LAST_MODIFIED, 'YYYYMMDDHHMISS')",
								new KeyExpression(AppModel.KEY_LAST_MODIFIED)),
				}));
	}

	protected List<BaseExpression> getRequireWhereExpressionList() {
		List<BaseExpression> result = new ArrayList<>(2);
		result.add(this.userIdEqualExpression);
		result.add(this.appIdEqualExpression);
		return result;
	}

	protected WhereStatement getListWhereStatement(Map<String, Object> map) {
		List<BaseExpression> whereNodeList = this.getRequireWhereExpressionList();
		whereNodeList.addAll(this.getOptionalWhereExpressionList(map));

		return new WhereStatement(new AndExpression(whereNodeList));
	}

	protected abstract String getTableName();

	protected abstract String[] getSelectColumnNameList();

	protected abstract WhereStatement getByKeyWhereStatement();

	protected abstract List<BaseExpression> getOptionalWhereExpressionList(Map<String, Object> map);

	protected abstract OrderByStatement getOrderByStatement();

	protected abstract AssignStatement[] getUpdateAssignStatementList();

	protected abstract String[] getInsertColumnNameList();

	protected abstract ValueExpression[] getInsertValueExpressionList();
}
