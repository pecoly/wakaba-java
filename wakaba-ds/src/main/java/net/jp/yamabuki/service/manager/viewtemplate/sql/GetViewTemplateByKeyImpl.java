package net.jp.yamabuki.service.manager.viewtemplate.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.ViewTemplate;
import net.jp.yamabuki.model.converter.StringConverter;
import net.jp.yamabuki.model.datasource.QueryParameter;
import net.jp.yamabuki.service.manager.sql.BaseManagerQuery;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.service.manager.viewtemplate.GetViewTemplateByKey;

/**
 * キーをもとにビューテンプレートを取得する実装クラス。
 *
 */
@Component
public class GetViewTemplateByKeyImpl extends BaseManagerQuery
implements GetViewTemplateByKey {

	private RowMapper<ViewTemplate> mapper = new ViewTemplateMapper();

	@Autowired
	private QueryParameter userIdQueryParameter;

	@Autowired
	private QueryParameter appIdQueryParameter;

	@Autowired
	private StringConverter stringConverter;

	@Autowired
	@Qualifier("viewTemplateSql")
	private ManagerSql managerSql;

	@Override
	public ViewTemplate execute(UserId userId, AppId appId, String name) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(appId, "appId");
		Argument.isNotNull(name, "name");

		Map<String, String> strList = this.toStringParameterList(
				userId, appId, name);

		Map<String, Object> objList
				= this.toObjectParameterList(strList);

		return this.queryForObject(
				this.getGetByKeySql(objList),
				objList, this.mapper);
	}

	@Override
	public ViewTemplate execute(ViewTemplate viewTemplate) {
		Argument.isNotNull(viewTemplate, "viewTemplate");

		return this.execute(viewTemplate.getUserId(),
				viewTemplate.getAppId(), viewTemplate.getName());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new QueryParameter[] {
				this.userIdQueryParameter,
				this.appIdQueryParameter,
				new QueryParameter("name", stringConverter),
		}));
	}

	/**
	 * 取得用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @param name 名前
	 * @return 取得用パラメータ
	 */
	Map<String, String> toStringParameterList(UserId userId, AppId appId, String name) {
		Map<String, String> result = new HashMap<>();
		result.put(AppModel.KEY_USER_ID, userId.getValue());
		result.put(AppModel.KEY_APP_ID, appId.getValue());
		result.put("name", name);
		return result;
	}

	/**
	 * キーによるモデル取得用Sqlを取得します。
	 * @param parameterList パラメータ一覧
	 * @return キーによるモデル取得用Sql
	 */
	String getGetByKeySql(Map<String, Object> parameterList) {
		return this.managerSql.getGetByKeySql(parameterList);
	}
}
