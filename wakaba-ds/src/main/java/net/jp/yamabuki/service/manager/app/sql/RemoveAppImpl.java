package net.jp.yamabuki.service.manager.app.sql;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.ModelNotFoundException;
import net.jp.yamabuki.model.App;
import net.jp.yamabuki.model.AppModel;
import net.jp.yamabuki.model.ModelId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.model.datasource.UpdateParameter;
import net.jp.yamabuki.service.manager.RemoveUserModel;
import net.jp.yamabuki.service.manager.sql.BaseManagerUpdate;
import net.jp.yamabuki.service.manager.sql.ManagerSql;
import net.jp.yamabuki.util.DateTimeUtils;

/**
 * アプリケーションを削除する実装クラス。
 *
 */
@Component("removeApp")
public class RemoveAppImpl extends BaseManagerUpdate
implements RemoveUserModel<App> {

	@Autowired
	private UpdateParameter userIdUpdateParameter;

	@Autowired
	private UpdateParameter integerIdUpdateParameter;

	@Autowired
	private UpdateParameter lastModifiedUpdateParameter;

	@Autowired
	@Qualifier("appSql")
	private ManagerSql appSql;

	@Override
	public void execute(UserId userId, ModelId id, DateTime lastModified) {
		Argument.isNotNull(userId, "userId");
		Argument.isNotNull(id, "id");
		Argument.isNotNull(lastModified, "lastModified");

		Map<String, Object> parameterList
				= this.toObjectParameterList(
						this.createParameterList(
								userId, id, lastModified));

		int count = this.update(
				this.appSql.getRemoveSql(parameterList),
				parameterList);

		if (count == 0) {
			throw new ModelNotFoundException(
					WakabaErrorCode.APP_NOT_FOUND,
					"id : " + id.getValue());
		}
	}

	@Override
	public void execute(App app) {
		Argument.isNotNull(app, "app");

		this.execute(app.getUserId(),
				app.getId(),
				app.getLastModified());
	}

	/**
	 * 初期化後処理。
	 */
	@PostConstruct
	void postConstruct() {
		this.initialize(Arrays.asList(new UpdateParameter[] {
				this.userIdUpdateParameter,
				this.integerIdUpdateParameter,
				this.lastModifiedUpdateParameter,
		}));
	}

	/**
	 * 削除用パラメータを作成します。
	 * @param userId ユーザーId
	 * @param id アプリケーションユーザーId
	 * @param lastModified 最終更新日時
	 * @return 削除用パラメータ
	 */
	Map<String, String> createParameterList(UserId userId,
			ModelId id, DateTime lastModified) {
		Map<String, String> map = new HashMap<>();
		map.put(AppModel.KEY_USER_ID, userId.getValue());
		map.put(AppModel.KEY_ID, id.getValue());
		map.put(App.KEY_LAST_MODIFIED,
				DateTimeUtils.toStringYYYYMMDDHHMISS(lastModified));
		return map;
	}
}
