package net.jp.yamabuki.service.resource;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * Jsを取得するためのインターフェース。
 *
 */
public interface JsQuery {
	/**
	 * 初期化します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 */
	void initialize(UserId userId, AppId appId);

	String getCommonJsName(String name);

	String getUserJsName(String name);
}
