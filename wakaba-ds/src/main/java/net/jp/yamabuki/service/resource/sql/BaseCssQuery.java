package net.jp.yamabuki.service.resource.sql;

import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.service.resource.CssQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Css取得基底クラス。
 *
 */
public abstract class BaseCssQuery implements CssQuery {

	private static Logger logger = LoggerFactory.getLogger("Yamabuki");

	private UserId userId;

	private AppId appId;

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public void initialize(UserId userId, AppId appId) {
		this.userId = userId;
		this.appId = appId;
	}

	@Override
	public String getCommonCssName(String name) {
		try {
			return this.jdbc.queryForObject(
					this.getTimestampNameSql(),
					new Object[] { "*", "*", name },
					String.class);
		}
		catch (EmptyResultDataAccessException ex) {
			return name;
		}
	}

	@Override
	public String getUserCssName(String name) {
		try {
			String sql = this.getTimestampNameSql();
			logger.info(sql);
			logger.info(String.format("{%s=%s, %s=%s, %s=%s}",
					"userId", userId.getValue(),
					"appId", appId.getValue(),
					"name", name));

			return this.jdbc.queryForObject(
					sql,
					new Object[] { userId.getValue(), appId.getValue(), name },
					String.class);
		}
		catch (EmptyResultDataAccessException ex) {
			return name;
		}
	}

	/**
	 * メッセージを取得するためのSqlを取得します。
	 * @return メッセージを取得するためのSql
	 */
	protected abstract String getTimestampNameSql();
}
