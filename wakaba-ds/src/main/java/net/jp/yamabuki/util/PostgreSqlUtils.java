package net.jp.yamabuki.util;

/**
 * Postgresql特有の処理を行うユーティリティクラス。
 *
 */
public final class PostgreSqlUtils {
	/**
	 * コンストラクタ。
	 */
	private PostgreSqlUtils() {
	}

	/**
	 * Like演算子用の単語に変換します。
	 * @param value 変換する単語
	 * @return 変換した単語
	 */
	public static String toLikeKeyword(String value) {
		if (StringUtils.isBlank(value)) {
			return "";
		}

		String s = value;
		s = s.replace("\\", "\\\\");
		s = s.replace("%", "\\%");
		s = s.replace("_", "\\_");
		s = "%" + s + "%";

		return s;
	}

}
