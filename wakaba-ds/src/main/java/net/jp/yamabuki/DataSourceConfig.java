package net.jp.yamabuki;

import org.springframework.stereotype.Component;

@Component
public class DataSourceConfig {
	private int maxRows = 1000;

	private int testMaxRows = 10;

	public int getMaxRows() {
		return this.maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getTestMaxRows() {
		return this.testMaxRows;
	}

	public void setTestMaxRows(int testMaxRows) {
		this.testMaxRows = testMaxRows;
	}
}
