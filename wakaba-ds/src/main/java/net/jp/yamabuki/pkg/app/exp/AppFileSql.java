package net.jp.yamabuki.pkg.app.exp;

public interface AppFileSql {
	String getGetSql();
	
	String getAddSql();
	
	String getRemoveSql();
}
