package net.jp.yamabuki.pkg.app.exp;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * AppFileをサーバーから取得するインターフェース。
 *
 */
public interface GetAppFile {
	/**
	 * AppFileをサーバーから取得します。
	 * @param userId ユーザーId
	 * @param appId アプリケーションId
	 * @return AppFile
	 */
	AppFile execute(UserId userId, AppId appId);
}
