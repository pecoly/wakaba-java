package net.jp.yamabuki.pkg.app.exp;

import net.jp.yamabuki.io.AppFile;

/**
 * AppFileをローカルに出力するインターフェース。
 *
 */
public interface OutputAppFile {
	/**
	 * AppFileをローカルに出力します。
	 * @param appFile 出力するAppFile
	 */
	void execute(AppFile appFile);
}
