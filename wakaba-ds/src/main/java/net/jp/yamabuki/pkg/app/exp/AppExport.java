package net.jp.yamabuki.pkg.app.exp;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * アプリケーションの設定ファイルをエクスポートするクラス。
 */
public class AppExport {
	private GetAppFile getAppFile;

	private OutputAppFile outputAppFile;

	/**
	 * コンストラクタ。
	 * @param getAppFile AppFileをサーバーから取得する処理
	 * @param outputAppFile AppFileをローカルに出力する処理
	 */
	public AppExport(GetAppFile getAppFile, OutputAppFile outputAppFile) {
		this.getAppFile = getAppFile;
		this.outputAppFile = outputAppFile;
	}

	/**
	 * エクスポートします。
	 */
	public void execute(UserId userId, AppId appId) {
		AppFile appFile = this.getAppFile.execute(userId, appId);
		this.outputAppFile.execute(appFile);
	}
}
