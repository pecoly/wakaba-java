package net.jp.yamabuki.pkg.app.exp;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import net.jp.yamabuki.exception.AppNotFoundException;
import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;

/**
 * AppFileをサーバーから取得する実装クラス。
 *
 */
@Component
public class GetAppFileImpl implements GetAppFile {
	@Autowired
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	private AppFileSql appFileSql;

	/**
	 * コンストラクタ。
	 */
	public GetAppFileImpl() {
	}

	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public void setAppFileSql(AppFileSql appFileSql) {
		this.appFileSql = appFileSql;
	}

	@Override
	public AppFile execute(UserId userId, AppId appId) {
		String sql = this.appFileSql.getGetSql();

		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId.getValue());
		map.put("appId", appId.getValue());

		String appName = this.jdbc.queryForObject(
				sql, map, String.class);

		if (appName == null) {
			throw new AppNotFoundException(userId, appId);
		}

		AppFile appFile = new AppFile();
		appFile.setUserId(userId);
		appFile.setAppId(appId);
		appFile.setAppName(appName);

		return appFile;
	}
}
