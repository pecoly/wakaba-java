package net.jp.yamabuki.pkg.app.imp;

import net.jp.yamabuki.io.AppFile;

public interface InputAppFile {
	String FILE_NAME = "app.properties";

	AppFile execute();
}
