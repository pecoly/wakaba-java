package net.jp.yamabuki.pkg.app.imp;

import net.jp.yamabuki.io.AppFile;
/**
 * アプリケーションの設定ファイルをインポートするクラス。
 */
public class AppImport {

	private InputAppFile inputAppFile;

	private AddAppFile addAppFile;

	/**
	 * コンストラクタ。
	 * @param inputAppFile ローカルから取得する処理
	 * @param addAppFile サーバーへ追加する処理
	 */
	public AppImport(InputAppFile inputAppFile, AddAppFile addAppFile) {
		this.inputAppFile = inputAppFile;
		this.addAppFile = addAppFile;
	}

	/**
	 * インポートします。
	 */
	public void execute() {
		AppFile appFile = this.inputAppFile.execute();
		this.addAppFile.execute(appFile);
	}
}
