package net.jp.yamabuki.pkg.app.exp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.AppFile;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

/**
 * アプリケーションの設定ファイルをZipへ出力するクラス。
 *
 */
public class OutputAppFileForZip implements OutputAppFile {
	private ZipArchiveOutputStream zip;

	/**
	 * コンストラクタ。
	 * @param zip 出力先
	 */
	public OutputAppFileForZip(ZipArchiveOutputStream zip) {
		this.zip = zip;
	}

	@Override
	public void execute(AppFile appFile) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		appFile.save(os);
		byte[] data = os.toByteArray();

		ZipArchiveEntry entry = new ZipArchiveEntry(AppFileConstants.FILE_NAME);
		try {
			this.zip.putArchiveEntry(entry);
			this.zip.write(data);
			this.zip.closeArchiveEntry();
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
