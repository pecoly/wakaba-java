package net.jp.yamabuki.pkg.app.exp;

/**
 * AppFileの定数クラス。
 *
 */
public final class AppFileConstants {
	/**
	 * コンストラクタ。
	 */
	private AppFileConstants() {
	}

	public static final String FILE_NAME = "app.properties";
}
