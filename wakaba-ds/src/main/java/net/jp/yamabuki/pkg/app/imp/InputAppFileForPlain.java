package net.jp.yamabuki.pkg.app.imp;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.util.PathUtils;

/**
 * アプリケーションの設定ファイルをファイルから読み込むクラス。
 *
 */
public class InputAppFileForPlain implements InputAppFile {
	private String directoryPath;

	/**
	 * コンストラクタ。
	 * @param directoryPath 入力元
	 */
	public InputAppFileForPlain(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	@Override
	public AppFile execute() {
		AppFile appFile = new AppFile();
		appFile.load(PathUtils.combine(this.directoryPath, FILE_NAME));
		return appFile;
	}
}
