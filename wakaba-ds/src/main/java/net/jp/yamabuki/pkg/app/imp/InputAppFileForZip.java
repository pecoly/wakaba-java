package net.jp.yamabuki.pkg.app.imp;

import java.io.IOException;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.pkg.app.exp.AppFileConstants;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;

/**
 * アプリケーションの設定ファイルをZipから読み込むクラス。
 *
 */
public class InputAppFileForZip implements InputAppFile {
	private ZipArchiveInputStream zip;

	/**
	 * コンストラクタ。
	 * @param zip 入力元
	 */
	public InputAppFileForZip(ZipArchiveInputStream zip) {
		this.zip = zip;
	}

	@Override
	public AppFile execute() {
		AppFile appFile = new AppFile();
		ZipArchiveEntry entry;
		while (true) {
			try {
				entry = this.zip.getNextZipEntry();
			}
			catch (IOException ex) {
				throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
			}

			if (entry == null) {
				break;
			}

			if (!entry.isDirectory()) {
				String name = entry.getName();
				if (name.equals(AppFileConstants.FILE_NAME)) {
					appFile.load(this.zip);
				}
			}
		}

		return appFile;
	}
}
