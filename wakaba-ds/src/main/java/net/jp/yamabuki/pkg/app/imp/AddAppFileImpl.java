package net.jp.yamabuki.pkg.app.imp;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import net.jp.yamabuki.check.Argument;
import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.pkg.app.exp.AppFileSql;

public class AddAppFileImpl implements AddAppFile {

	@Autowired
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	private AppFileSql appFileSql;

	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public void setAppFileSql(AppFileSql appFileSql) {
		this.appFileSql = appFileSql;
	}

	@Override
	public void execute(AppFile appFile) {
		Argument.isNotNull(appFile, "appFile");

		this.removeApp(appFile);
		this.importApp(appFile);
	}

	void importApp(AppFile appFile) {
		String sql = this.appFileSql.getAddSql();
		Map<String, Object> map = new HashMap<>();
		map.put("userId", appFile.getUserId().getValue());
		map.put("appId", appFile.getAppId().getValue());
		map.put("appName", appFile.getAppName());

		this.jdbc.update(sql, map);
	}

	void removeApp(AppFile appFile) {
		String sql = this.appFileSql.getRemoveSql();
		Map<String, Object> map = new HashMap<>();
		map.put("userId", appFile.getUserId().getValue());
		map.put("appId", appFile.getAppId().getValue());

		this.jdbc.update(sql, map);
	}
}
