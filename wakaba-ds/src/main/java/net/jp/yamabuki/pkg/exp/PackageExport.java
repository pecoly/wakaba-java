package net.jp.yamabuki.pkg.exp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import net.jp.yamabuki.exception.WakabaErrorCode;
import net.jp.yamabuki.exception.WakabaException;
import net.jp.yamabuki.model.AppId;
import net.jp.yamabuki.model.UserId;
import net.jp.yamabuki.pkg.app.exp.AppExport;
import net.jp.yamabuki.pkg.app.exp.GetAppFile;
import net.jp.yamabuki.pkg.app.exp.OutputAppFile;
import net.jp.yamabuki.pkg.app.exp.OutputAppFileForZip;

import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PackageExport {

	@Autowired
	private GetAppFile getAppFile;

	public void executeForZip(UserId userId, AppId appId, OutputStream out) {
		try (ZipArchiveOutputStream zip
				= new ZipArchiveOutputStream(out)) {
			zip.setEncoding(System.getProperty("file.encoding"));

			OutputAppFile outputAppFile = new OutputAppFileForZip(zip);

			// App Export
			AppExport ai = new AppExport(this.getAppFile, outputAppFile);
			ai.execute(userId, appId);
		}
		catch (FileNotFoundException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
		catch (IOException ex) {
			throw new WakabaException(WakabaErrorCode.UNDEFINIED, ex);
		}
	}
}
