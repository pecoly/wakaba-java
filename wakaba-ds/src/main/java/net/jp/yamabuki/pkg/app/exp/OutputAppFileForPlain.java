package net.jp.yamabuki.pkg.app.exp;

import net.jp.yamabuki.io.AppFile;
import net.jp.yamabuki.util.PathUtils;

/**
 * アプリケーションの設定ファイルをファイルへ出力するクラス。
 *
 */
public class OutputAppFileForPlain implements OutputAppFile {
	private String directoryPath;

	/**
	 * コンストラクタ。
	 * @param directoryPath 出力先
	 */
	public OutputAppFileForPlain(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	@Override
	public void execute(AppFile appFile) {
		appFile.save(PathUtils.combine(this.directoryPath, AppFileConstants.FILE_NAME));
	}
}
