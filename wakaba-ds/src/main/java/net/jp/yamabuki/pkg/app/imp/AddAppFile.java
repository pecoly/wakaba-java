package net.jp.yamabuki.pkg.app.imp;

import net.jp.yamabuki.io.AppFile;

public interface AddAppFile {
	void execute(AppFile appFile);
}
